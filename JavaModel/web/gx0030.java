/*
               File: Gx0030
        Description: Selection List producto
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:12.2
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/gx0030")
public final  class gx0030 extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new gx0030_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Selection List producto";
   }

}

