/*
               File: WebPanel1
        Description: Web Panel1
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 22, 2022 8:35:40.9
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/webpanel1")
public final  class webpanel1 extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new webpanel1_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Web Panel1";
   }

}

