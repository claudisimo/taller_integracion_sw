/*
               File: producto_impl
        Description: producto
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:0.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class producto_impl extends GXDataArea
{
   public void initenv( )
   {
      if ( GxWebError != 0 )
      {
         return  ;
      }
   }

   public void inittrn( )
   {
      initialize_properties( ) ;
      entryPointCalled = false ;
      gxfirstwebparm = httpContext.GetNextPar( ) ;
      gxfirstwebparm_bkp = gxfirstwebparm ;
      gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm, "High") ;
      if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
      {
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         dyncall( httpContext.GetNextPar( )) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
      {
         A7tippro_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         httpContext.ajax_rsp_assign_attri("", false, "A7tippro_codigo", GXutil.ltrim( GXutil.str( A7tippro_codigo, 4, 0)));
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_2( A7tippro_codigo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
      {
         httpContext.setAjaxEventMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = httpContext.GetNextPar( ) ;
      }
      else
      {
         if ( ! httpContext.IsValidAjaxCall( false) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = gxfirstwebparm_bkp ;
      }
      if ( ! entryPointCalled )
      {
         Gx_mode = gxfirstwebparm ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      Form.getMeta().addItem("Generator", "GeneXus Java", (short)(0)) ;
      Form.getMeta().addItem("Version", "10_1_8-58720", (short)(0)) ;
      Form.getMeta().addItem("Description", "producto", (short)(0)) ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      GX_FocusControl = edtpro_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      wbErr = false ;
      httpContext.setTheme("GeneXusX");
   }

   public producto_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public producto_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( producto_impl.class ));
   }

   public producto_impl( int remoteHandle ,
                         ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void webExecute( )
   {
      initenv( ) ;
      inittrn( ) ;
      if ( ( GxWebError == 0 ) && ! httpContext.isAjaxCallMode( ) )
      {
         MasterPageObj = new menu_usuario_impl (remoteHandle, context.copy());
         MasterPageObj.setDataArea(this,false);
         MasterPageObj.webExecute();
         if ( httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "max-age=0");
            }
            if ( (GXutil.strcmp("", httpContext.wjLoc)==0) )
            {
               httpContext.GX_webresponse.addString(httpContext.getJSONResponse( ));
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public void draw( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         disable_std_buttons( ) ;
         enableDisable( ) ;
         set_caption( ) ;
         /* Form start */
         wb_table1_2_033( true) ;
      }
      return  ;
   }

   public void wb_table1_2_033e( boolean wbgen )
   {
      if ( wbgen )
      {
      }
      /* Execute Exit event if defined. */
   }

   public void wb_table1_2_033( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table2_5_033( true) ;
      }
      return  ;
   }

   public void wb_table2_5_033e( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Control Group */
         ClassString = "Group" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroupdata_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"producto"+"</legend>") ;
         wb_table3_27_033( true) ;
      }
      return  ;
   }

   public void wb_table3_27_033e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_033e( true) ;
      }
      else
      {
         wb_table1_2_033e( false) ;
      }
   }

   public void wb_table3_27_033( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         ClassString = "ErrorViewer" ;
         StyleString = "" ;
         GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), StyleString, ClassString, "", "false");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table4_33_033( true) ;
      }
      return  ;
   }

   public void wb_table4_33_033e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"" ;
         ClassString = "BtnEnter" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "rounded", "EENTER.", TempTags, "", httpContext.getButtonType( ), "HLP_producto.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"" ;
         ClassString = "BtnCancel" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "rounded", "ECANCEL.", TempTags, "", httpContext.getButtonType( ), "HLP_producto.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"" ;
         ClassString = "BtnDelete" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 0, "rounded", "EDELETE.", TempTags, "", httpContext.getButtonType( ), "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_27_033e( true) ;
      }
      else
      {
         wb_table3_27_033e( false) ;
      }
   }

   public void wb_table4_33_033( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockpro_codigo_Internalname, "C�digo", "", "", lblTextblockpro_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtpro_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ",", "")), ((edtpro_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A10pro_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A10pro_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(38);\"", "", "", "", "", edtpro_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtpro_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblocktippro_codigo2_Internalname, "Tipo de Producto", "", "", lblTextblocktippro_codigo2_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A7tippro_codigo", GXutil.ltrim( GXutil.str( A7tippro_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edttippro_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A7tippro_codigo, (byte)(4), (byte)(0), ",", "")), ((edttippro_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A7tippro_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A7tippro_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(43);\"", "", "", "", "", edttippro_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edttippro_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_producto.htm");
         /* Static images/pictures */
         ClassString = "Image" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgprompt_7_Internalname, "prompt.gif", imgprompt_7_Link, "", "", "GeneXusX", imgprompt_7_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "''", "", "HLP_producto.htm");
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A8tippro_nombre", A8tippro_nombre);
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edttippro_nombre_Internalname, GXutil.rtrim( A8tippro_nombre), GXutil.rtrim( localUtil.format( A8tippro_nombre, "")), "", "", "", "", "", edttippro_nombre_Jsonclick, 0, ClassString, StyleString, "", 1, edttippro_nombre_Enabled, 0, 45, "chr", 1, "row", 45, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\"  style=\"height:23px\">") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockpro_nombre_Internalname, "Nombre", "", "", lblTextblockpro_nombre_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A11pro_nombre", A11pro_nombre);
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtpro_nombre_Internalname, GXutil.rtrim( A11pro_nombre), GXutil.rtrim( localUtil.format( A11pro_nombre, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(49);\"", "", "", "", "", edtpro_nombre_Jsonclick, 0, ClassString, StyleString, "", 1, edtpro_nombre_Enabled, 0, 45, "chr", 1, "row", 45, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockpro_descripcion_Internalname, "Descripci�n", "", "", lblTextblockpro_descripcion_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Multiple line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A12pro_descripcion", A12pro_descripcion);
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_html_textarea( httpContext, edtpro_descripcion_Internalname, GXutil.rtrim( A12pro_descripcion), "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(54);\"", (short)(0), 1, edtpro_descripcion_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "200", -1, "", true, "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockpro_precio_Internalname, "Precio", "", "", lblTextblockpro_precio_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A13pro_precio", GXutil.ltrim( GXutil.str( A13pro_precio, 10, 2)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtpro_precio_Internalname, GXutil.ltrim( localUtil.ntoc( A13pro_precio, (byte)(10), (byte)(2), ",", "")), ((edtpro_precio_Enabled!=0) ? GXutil.ltrim( localUtil.format( A13pro_precio, "ZZZZZZ9.99")) : localUtil.format( A13pro_precio, "ZZZZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(59);\"", "", "", "", "", edtpro_precio_Jsonclick, 0, ClassString, StyleString, "", 1, edtpro_precio_Enabled, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_producto.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table4_33_033e( true) ;
      }
      else
      {
         wb_table4_33_033e( false) ;
      }
   }

   public void wb_table2_5_033( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "<div style=\"WHITE-SPACE: nowrap\" class=\"ToolbarMain\">") ;
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_Internalname, context.getHttpContext().getImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_Visible, 1, "", "Primero", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_separator_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_Internalname, context.getHttpContext().getImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_Internalname, context.getHttpContext().getImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_Visible, 1, "", "Siguiente", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_separator_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_Internalname, context.getHttpContext().getImagePath( "29211874-e613-48e5-9011-8017d984217e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_Visible, 1, "", "Ultimo", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_separator_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_Internalname, context.getHttpContext().getImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_Visible, 1, "", "Seleccionar", 0, 0, 0, "", 0, "", 0, 0, 4, imgBtn_select_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "gx.popup.openPrompt('"+"gx0030"+"',["+"{Ctrl:gx.dom.el('"+"PRO_CODIGO"+"'), id:'"+"PRO_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 4, imgBtn_select_separator_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "gx.popup.openPrompt('"+"gx0030"+"',["+"{Ctrl:gx.dom.el('"+"PRO_CODIGO"+"'), id:'"+"PRO_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_Internalname, context.getHttpContext().getImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_Internalname, context.getHttpContext().getImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_Visible, 1, "", "Cancelar", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_Internalname, context.getHttpContext().getImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_producto.htm");
         httpContext.writeText( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_5_033e( true) ;
      }
      else
      {
         wb_table2_5_033e( false) ;
      }
   }

   public void userMain( )
   {
      standaloneStartup( ) ;
   }

   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
      process( ) ;
   }

   public void standaloneStartupServer( )
   {
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      httpContext.wbGlbDoneStart = (byte)(1) ;
      assign_properties_default( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "PRO_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtpro_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A10pro_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
            }
            else
            {
               A10pro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
            }
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edttippro_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edttippro_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "TIPPRO_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edttippro_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A7tippro_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A7tippro_codigo", GXutil.ltrim( GXutil.str( A7tippro_codigo, 4, 0)));
            }
            else
            {
               A7tippro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edttippro_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A7tippro_codigo", GXutil.ltrim( GXutil.str( A7tippro_codigo, 4, 0)));
            }
            A8tippro_nombre = httpContext.cgiGet( edttippro_nombre_Internalname) ;
            n8tippro_nombre = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A8tippro_nombre", A8tippro_nombre);
            A11pro_nombre = httpContext.cgiGet( edtpro_nombre_Internalname) ;
            n11pro_nombre = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A11pro_nombre", A11pro_nombre);
            n11pro_nombre = ((GXutil.strcmp("", A11pro_nombre)==0) ? true : false) ;
            A12pro_descripcion = httpContext.cgiGet( edtpro_descripcion_Internalname) ;
            n12pro_descripcion = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A12pro_descripcion", A12pro_descripcion);
            n12pro_descripcion = ((GXutil.strcmp("", A12pro_descripcion)==0) ? true : false) ;
            if ( ( ( localUtil.ctond( httpContext.cgiGet( edtpro_precio_Internalname)).doubleValue() < 0 ) ) || ( ( localUtil.ctond( httpContext.cgiGet( edtpro_precio_Internalname)).compareTo(DecimalUtil.doubleToDec(9999999.99,10,2)) > 0 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "PRO_PRECIO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtpro_precio_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A13pro_precio = DecimalUtil.ZERO ;
               n13pro_precio = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A13pro_precio", GXutil.ltrim( GXutil.str( A13pro_precio, 10, 2)));
            }
            else
            {
               A13pro_precio = localUtil.ctond( httpContext.cgiGet( edtpro_precio_Internalname)) ;
               n13pro_precio = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A13pro_precio", GXutil.ltrim( GXutil.str( A13pro_precio, 10, 2)));
            }
            n13pro_precio = ((DecimalUtil.ZERO.compareTo(A13pro_precio)==0) ? true : false) ;
            /* Read saved values. */
            Z10pro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z10pro_codigo"), ",", ".")) ;
            Z11pro_nombre = httpContext.cgiGet( "Z11pro_nombre") ;
            n11pro_nombre = ((GXutil.strcmp("", A11pro_nombre)==0) ? true : false) ;
            Z12pro_descripcion = httpContext.cgiGet( "Z12pro_descripcion") ;
            n12pro_descripcion = ((GXutil.strcmp("", A12pro_descripcion)==0) ? true : false) ;
            Z13pro_precio = localUtil.ctond( httpContext.cgiGet( "Z13pro_precio")) ;
            n13pro_precio = ((DecimalUtil.ZERO.compareTo(A13pro_precio)==0) ? true : false) ;
            Z7tippro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z7tippro_codigo"), ",", ".")) ;
            IsConfirmed = (short)(localUtil.ctol( httpContext.cgiGet( "IsConfirmed"), ",", ".")) ;
            IsModified = (short)(localUtil.ctol( httpContext.cgiGet( "IsModified"), ",", ".")) ;
            Gx_mode = httpContext.cgiGet( "Mode") ;
            Gx_mode = httpContext.cgiGet( "vMODE") ;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( GXutil.strcmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A10pro_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
               httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons( ) ;
               standaloneModal( ) ;
            }
            else
            {
               standaloneModal( ) ;
            }
         }
      }
   }

   public void process( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read Transaction buttons. */
         sEvt = httpContext.cgiGet( "_EventName") ;
         EvtGridId = httpContext.cgiGet( "_EventGridId") ;
         EvtRowId = httpContext.cgiGet( "_EventRowId") ;
         if ( GXutil.len( sEvt) > 0 )
         {
            sEvtType = GXutil.left( sEvt, 1) ;
            sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
            if ( GXutil.strcmp(sEvtType, "M") != 0 )
            {
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( GXutil.strcmp(Gx_mode, "DSP") != 0 )
                        {
                           btn_enter( ) ;
                        }
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                     else if ( GXutil.strcmp(sEvt, "FIRST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_first( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "PREVIOUS") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_previous( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "NEXT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_next( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "LAST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_last( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "SELECT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_select( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "DELETE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( GXutil.strcmp(Gx_mode, "DSP") != 0 )
                        {
                           btn_delete( ) ;
                        }
                     }
                  }
                  else
                  {
                  }
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void afterTrn( )
   {
      if ( trnEnded == 1 )
      {
         trnEnded = 0 ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            /* Clear variables for new insertion. */
            initAll033( ) ;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
         }
      }
   }

   public String toString( )
   {
      return "" ;
   }

   public GXContentInfo getContentInfo( )
   {
      return (GXContentInfo)(null) ;
   }

   public void disable_std_buttons( )
   {
      imgBtn_delete2_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
      imgBtn_delete2_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
      bttBtn_delete_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
      if ( ( GXutil.strcmp(sMode3, "DSP") == 0 ) || ( GXutil.strcmp(sMode3, "DLT") == 0 ) )
      {
         imgBtn_delete2_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
         imgBtn_delete2_separator_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
         bttBtn_delete_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
         if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_Visible, 5, 0)));
            imgBtn_enter2_separator_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_separator_Visible, 5, 0)));
            bttBtn_enter_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_enter_Visible, 5, 0)));
         }
         disableAttributes033( ) ;
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_confdelete"), 0, "");
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_mustconfirm"), 0, "");
         }
      }
   }

   public void confirm_030( )
   {
      beforeValidate033( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            onDeleteControls033( ) ;
         }
         else
         {
            checkExtendedTable033( ) ;
            closeExtendedTableCursors033( ) ;
         }
      }
      if ( AnyError == 0 )
      {
         IsConfirmed = (short)(1) ;
      }
   }

   public void resetCaption030( )
   {
   }

   public void zm033( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
         {
            Z11pro_nombre = T00033_A11pro_nombre[0] ;
            Z12pro_descripcion = T00033_A12pro_descripcion[0] ;
            Z13pro_precio = T00033_A13pro_precio[0] ;
            Z7tippro_codigo = T00033_A7tippro_codigo[0] ;
         }
         else
         {
            Z11pro_nombre = A11pro_nombre ;
            Z12pro_descripcion = A12pro_descripcion ;
            Z13pro_precio = A13pro_precio ;
            Z7tippro_codigo = A7tippro_codigo ;
         }
      }
      if ( GX_JID == -1 )
      {
         Z10pro_codigo = A10pro_codigo ;
         Z11pro_nombre = A11pro_nombre ;
         Z12pro_descripcion = A12pro_descripcion ;
         Z13pro_precio = A13pro_precio ;
         Z7tippro_codigo = A7tippro_codigo ;
         Z8tippro_nombre = A8tippro_nombre ;
      }
   }

   public void standaloneNotModal( )
   {
      imgprompt_7_Link = ((GXutil.strcmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0020"+"',["+"{Ctrl:gx.dom.el('"+"TIPPRO_CODIGO"+"'), id:'"+"TIPPRO_CODIGO"+"'"+",isOut: true}"+"],"+"null"+","+"'', false"+","+"false"+");") ;
      imgBtn_delete2_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
   }

   public void standaloneModal( )
   {
      if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
      {
         imgBtn_enter2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
      else
      {
         imgBtn_enter2_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
   }

   public void load033( )
   {
      /* Using cursor T00035 */
      pr_default.execute(3, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A11pro_nombre = T00035_A11pro_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A11pro_nombre", A11pro_nombre);
         n11pro_nombre = T00035_n11pro_nombre[0] ;
         A12pro_descripcion = T00035_A12pro_descripcion[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A12pro_descripcion", A12pro_descripcion);
         n12pro_descripcion = T00035_n12pro_descripcion[0] ;
         A13pro_precio = T00035_A13pro_precio[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A13pro_precio", GXutil.ltrim( GXutil.str( A13pro_precio, 10, 2)));
         n13pro_precio = T00035_n13pro_precio[0] ;
         A8tippro_nombre = T00035_A8tippro_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A8tippro_nombre", A8tippro_nombre);
         n8tippro_nombre = T00035_n8tippro_nombre[0] ;
         A7tippro_codigo = T00035_A7tippro_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A7tippro_codigo", GXutil.ltrim( GXutil.str( A7tippro_codigo, 4, 0)));
         zm033( -1) ;
      }
      pr_default.close(3);
      onLoadActions033( ) ;
   }

   public void onLoadActions033( )
   {
   }

   public void checkExtendedTable033( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      /* Using cursor T00034 */
      pr_default.execute(2, new Object[] {new Short(A7tippro_codigo)});
      if ( (pr_default.getStatus(2) == 101) )
      {
         AnyError7 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'tipo_producto'.", "ForeignKeyNotFound", 1, "TIPPRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edttippro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError7 == 0 )
      {
         A8tippro_nombre = T00034_A8tippro_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A8tippro_nombre", A8tippro_nombre);
         n8tippro_nombre = T00034_n8tippro_nombre[0] ;
      }
      pr_default.close(2);
   }

   public void closeExtendedTableCursors033( )
   {
      pr_default.close(2);
   }

   public void enableDisable( )
   {
   }

   public void gxload_2( short A7tippro_codigo )
   {
      /* Using cursor T00036 */
      pr_default.execute(4, new Object[] {new Short(A7tippro_codigo)});
      if ( (pr_default.getStatus(4) == 101) )
      {
         AnyError7 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'tipo_producto'.", "ForeignKeyNotFound", 1, "TIPPRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edttippro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError7 == 0 )
      {
         A8tippro_nombre = T00036_A8tippro_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A8tippro_nombre", A8tippro_nombre);
         n8tippro_nombre = T00036_n8tippro_nombre[0] ;
      }
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.GX_webresponse.addString("new Array( new Array(");
      httpContext.GX_webresponse.addString("\""+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A8tippro_nombre))+"\"");
      httpContext.GX_webresponse.addString(")");
      if ( (pr_default.getStatus(4) == 101) )
      {
         httpContext.GX_webresponse.addString(",");
         httpContext.GX_webresponse.addString("101");
      }
      httpContext.GX_webresponse.addString(")");
      pr_default.close(4);
   }

   public void getKey033( )
   {
      /* Using cursor T00037 */
      pr_default.execute(5, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(5) != 101) )
      {
         RcdFound3 = (short)(1) ;
      }
      else
      {
         RcdFound3 = (short)(0) ;
      }
      pr_default.close(5);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00033 */
      pr_default.execute(1, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm033( 1) ;
         RcdFound3 = (short)(1) ;
         A10pro_codigo = T00033_A10pro_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
         A11pro_nombre = T00033_A11pro_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A11pro_nombre", A11pro_nombre);
         n11pro_nombre = T00033_n11pro_nombre[0] ;
         A12pro_descripcion = T00033_A12pro_descripcion[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A12pro_descripcion", A12pro_descripcion);
         n12pro_descripcion = T00033_n12pro_descripcion[0] ;
         A13pro_precio = T00033_A13pro_precio[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A13pro_precio", GXutil.ltrim( GXutil.str( A13pro_precio, 10, 2)));
         n13pro_precio = T00033_n13pro_precio[0] ;
         A7tippro_codigo = T00033_A7tippro_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A7tippro_codigo", GXutil.ltrim( GXutil.str( A7tippro_codigo, 4, 0)));
         Z10pro_codigo = A10pro_codigo ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         load033( ) ;
         Gx_mode = sMode3 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         RcdFound3 = (short)(0) ;
         initializeNonKey033( ) ;
         sMode3 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal( ) ;
         Gx_mode = sMode3 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey033( ) ;
      if ( RcdFound3 == 0 )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound3 = (short)(0) ;
      /* Using cursor T00038 */
      pr_default.execute(6, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(6) != 101) )
      {
         while ( (pr_default.getStatus(6) != 101) && ( ( T00038_A10pro_codigo[0] < A10pro_codigo ) ) )
         {
            pr_default.readNext(6);
         }
         if ( (pr_default.getStatus(6) != 101) && ( ( T00038_A10pro_codigo[0] > A10pro_codigo ) ) )
         {
            A10pro_codigo = T00038_A10pro_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
            RcdFound3 = (short)(1) ;
         }
      }
      pr_default.close(6);
   }

   public void move_previous( )
   {
      RcdFound3 = (short)(0) ;
      /* Using cursor T00039 */
      pr_default.execute(7, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(7) != 101) )
      {
         while ( (pr_default.getStatus(7) != 101) && ( ( T00039_A10pro_codigo[0] > A10pro_codigo ) ) )
         {
            pr_default.readNext(7);
         }
         if ( (pr_default.getStatus(7) != 101) && ( ( T00039_A10pro_codigo[0] < A10pro_codigo ) ) )
         {
            A10pro_codigo = T00039_A10pro_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
            RcdFound3 = (short)(1) ;
         }
      }
      pr_default.close(7);
   }

   public void btn_enter( )
   {
      nKeyPressed = (byte)(1) ;
      getKey033( ) ;
      if ( RcdFound3 == 1 )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "PRO_CODIGO");
            AnyError = (short)(1) ;
            GX_FocusControl = edtpro_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else if ( A10pro_codigo != Z10pro_codigo )
         {
            A10pro_codigo = Z10pro_codigo ;
            httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "PRO_CODIGO");
            AnyError = (short)(1) ;
            GX_FocusControl = edtpro_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            delete( ) ;
            afterTrn( ) ;
            GX_FocusControl = edtpro_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            /* Update record */
            update033( ) ;
            GX_FocusControl = edtpro_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }
      else
      {
         if ( A10pro_codigo != Z10pro_codigo )
         {
            /* Insert record */
            GX_FocusControl = edtpro_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            insert033( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "" ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( GXutil.strcmp(Gx_mode, "UPD") == 0 )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "PRO_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtpro_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            else
            {
               /* Insert record */
               GX_FocusControl = edtpro_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               insert033( ) ;
               if ( AnyError == 1 )
               {
                  GX_FocusControl = "" ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
         }
      }
      afterTrn( ) ;
   }

   public void btn_delete( )
   {
      if ( A10pro_codigo != Z10pro_codigo )
      {
         A10pro_codigo = Z10pro_codigo ;
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforedlt"), 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      else
      {
         delete( ) ;
         afterTrn( ) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError != 0 )
      {
      }
      getByPrimaryKey( ) ;
      CloseOpenCursors();
   }

   public void btn_get( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      if ( RcdFound3 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
      }
      GX_FocusControl = edttippro_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_first( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart033( ) ;
      if ( RcdFound3 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edttippro_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd033( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_previous( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_previous( ) ;
      if ( RcdFound3 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edttippro_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_next( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_next( ) ;
      if ( RcdFound3 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edttippro_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_last( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart033( ) ;
      if ( RcdFound3 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         while ( RcdFound3 != 0 )
         {
            scanNext033( ) ;
         }
      }
      GX_FocusControl = edttippro_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd033( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_select( )
   {
      getEqualNoModal( ) ;
   }

   public void checkOptimisticConcurrency033( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         /* Using cursor T00032 */
         pr_default.execute(0, new Object[] {new Short(A10pro_codigo)});
         if ( (pr_default.getStatus(0) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"producto"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z11pro_nombre, T00032_A11pro_nombre[0]) != 0 ) || ( GXutil.strcmp(Z12pro_descripcion, T00032_A12pro_descripcion[0]) != 0 ) || ( Z13pro_precio.compareTo(T00032_A13pro_precio[0]) != 0 ) || ( Z7tippro_codigo != T00032_A7tippro_codigo[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"producto"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert033( )
   {
      beforeValidate033( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable033( ) ;
      }
      if ( AnyError == 0 )
      {
         zm033( 0) ;
         checkOptimisticConcurrency033( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm033( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert033( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000310 */
                  pr_default.execute(8, new Object[] {new Short(A10pro_codigo), new Boolean(n11pro_nombre), A11pro_nombre, new Boolean(n12pro_descripcion), A12pro_descripcion, new Boolean(n13pro_precio), A13pro_precio, new Short(A7tippro_codigo)});
                  if ( (pr_default.getStatus(8) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucadded"), 0, "");
                        resetCaption030( ) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load033( ) ;
         }
         endLevel033( ) ;
      }
      closeExtendedTableCursors033( ) ;
   }

   public void update033( )
   {
      beforeValidate033( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable033( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency033( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm033( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate033( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000311 */
                  pr_default.execute(9, new Object[] {new Boolean(n11pro_nombre), A11pro_nombre, new Boolean(n12pro_descripcion), A12pro_descripcion, new Boolean(n13pro_precio), A13pro_precio, new Short(A7tippro_codigo), new Short(A10pro_codigo)});
                  if ( (pr_default.getStatus(9) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"producto"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate033( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucupdated"), 0, "");
                        resetCaption030( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel033( ) ;
      }
      closeExtendedTableCursors033( ) ;
   }

   public void deferredUpdate033( )
   {
   }

   public void delete( )
   {
      beforeValidate033( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency033( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls033( ) ;
         afterConfirm033( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete033( ) ;
            if ( AnyError == 0 )
            {
               /* No cascading delete specified. */
               /* Using cursor T000312 */
               pr_default.execute(10, new Object[] {new Short(A10pro_codigo)});
               if ( AnyError == 0 )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( AnyError == 0 )
                  {
                     move_next( ) ;
                     if ( RcdFound3 == 0 )
                     {
                        initAll033( ) ;
                     }
                     else
                     {
                        getByPrimaryKey( ) ;
                     }
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucdeleted"), 0, "");
                     resetCaption030( ) ;
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode3 = Gx_mode ;
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      endLevel033( ) ;
      Gx_mode = sMode3 ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
   }

   public void onDeleteControls033( )
   {
      standaloneModal( ) ;
      if ( AnyError == 0 )
      {
         /* Delete mode formulas */
         /* Using cursor T000313 */
         pr_default.execute(11, new Object[] {new Short(A7tippro_codigo)});
         A8tippro_nombre = T000313_A8tippro_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A8tippro_nombre", A8tippro_nombre);
         n8tippro_nombre = T000313_n8tippro_nombre[0] ;
         pr_default.close(11);
      }
      if ( AnyError == 0 )
      {
         /* Using cursor T000314 */
         pr_default.execute(12, new Object[] {new Short(A10pro_codigo)});
         if ( (pr_default.getStatus(12) != 101) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_del", new Object[] {"Level1"}), "CannotDeleteReferencedRecord", 1, "");
            AnyError = (short)(1) ;
         }
         pr_default.close(12);
      }
   }

   public void endLevel033( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         pr_default.close(0);
      }
      if ( AnyError == 0 )
      {
         beforeComplete033( ) ;
      }
      if ( AnyError == 0 )
      {
         pr_default.close(11);
         Application.commit(context, remoteHandle, "DEFAULT", "producto");
         if ( AnyError == 0 )
         {
            confirmValues030( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         pr_default.close(11);
         Application.rollback(context, remoteHandle, "DEFAULT", "producto");
      }
      IsModified = (short)(0) ;
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanStart033( )
   {
      /* Using cursor T000315 */
      pr_default.execute(13);
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(13) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A10pro_codigo = T000315_A10pro_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
      }
      /* Load Subordinate Levels */
   }

   public void scanNext033( )
   {
      pr_default.readNext(13);
      RcdFound3 = (short)(0) ;
      if ( (pr_default.getStatus(13) != 101) )
      {
         RcdFound3 = (short)(1) ;
         A10pro_codigo = T000315_A10pro_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
      }
   }

   public void scanEnd033( )
   {
      pr_default.close(13);
   }

   public void afterConfirm033( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert033( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate033( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete033( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete033( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate033( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes033( )
   {
      edtpro_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtpro_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtpro_codigo_Enabled, 5, 0)));
      edttippro_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edttippro_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edttippro_codigo_Enabled, 5, 0)));
      edttippro_nombre_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edttippro_nombre_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edttippro_nombre_Enabled, 5, 0)));
      edtpro_nombre_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtpro_nombre_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtpro_nombre_Enabled, 5, 0)));
      edtpro_descripcion_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtpro_descripcion_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtpro_descripcion_Enabled, 5, 0)));
      edtpro_precio_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtpro_precio_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtpro_precio_Enabled, 5, 0)));
   }

   public void assign_properties_default( )
   {
   }

   public void confirmValues030( )
   {
   }

   public void renderHtmlHeaders( )
   {
      GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), "IE=EmulateIE7");
   }

   public void renderHtmlOpenForm( )
   {
      httpContext.writeText( "<title>") ;
      httpContext.writeText( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      MasterPageObj.master_styles();
      if ( ! httpContext.isSmartDevice( ) )
      {
         httpContext.AddJavascriptSource("gxgral.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      else
      {
         httpContext.AddJavascriptSource("gxapiSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfxSD.js", "?58720");
         httpContext.AddJavascriptSource("gxtypesSD.js", "?58720");
         httpContext.AddJavascriptSource("gxpopupSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfrmutlSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgridSD.js", "?58720");
         httpContext.AddJavascriptSource("JavaScripTableSD.js", "?58720");
         httpContext.AddJavascriptSource("rijndaelSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgralSD.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      FormProcess = " onkeyup=\"gx.evt.onkeyup(event)\" onkeypress=\"gx.evt.onkeypress(event,true,false)\" onkeydown=\"gx.evt.onkeypress(null,true,false)\"" ;
      httpContext.writeText( "<body") ;
      httpContext.writeText( " "+"class=\"Form\""+" "+" style=\"-moz-opacity:0;opacity:0;"+"background-color:"+WebUtils.getHTMLColor( Form.getIBackground())+";") ;
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         httpContext.writeText( " background-image:url("+httpContext.convertURL( Form.getBackground())+")") ;
      }
      httpContext.writeText( "\""+FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" onsubmit=\"try{return gx.csv.validForm()}catch(e){return true;}\" name=\"MAINFORM\" method=\"post\" action=\""+formatLink("producto") + "?" + GXutil.URLEncode(GXutil.rtrim(Gx_mode))+"\" class=\""+"Form"+"\">") ;
      GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
   }

   public void renderHtmlCloseForm( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      GxWebStd.gx_hidden_field( httpContext, "Z10pro_codigo", GXutil.ltrim( localUtil.ntoc( Z10pro_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z11pro_nombre", GXutil.rtrim( Z11pro_nombre));
      GxWebStd.gx_hidden_field( httpContext, "Z12pro_descripcion", GXutil.rtrim( Z12pro_descripcion));
      GxWebStd.gx_hidden_field( httpContext, "Z13pro_precio", GXutil.ltrim( localUtil.ntoc( Z13pro_precio, (byte)(10), (byte)(2), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z7tippro_codigo", GXutil.ltrim( localUtil.ntoc( Z7tippro_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsConfirmed", GXutil.ltrim( localUtil.ntoc( IsConfirmed, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsModified", GXutil.ltrim( localUtil.ntoc( IsModified, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Mode", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "vMODE", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      httpContext.writeTextNL( "</form>") ;
      include_jscripts( ) ;
   }

   public byte executeStartEvent( )
   {
      standaloneStartup( ) ;
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      return gxajaxcallmode ;
   }

   public void renderHtmlContent( )
   {
      draw( ) ;
   }

   public void dispatchEvents( )
   {
      process( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public String getPgmname( )
   {
      return "producto" ;
   }

   public String getPgmdesc( )
   {
      return "producto" ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("producto") + "?" + GXutil.URLEncode(GXutil.rtrim(Gx_mode)) ;
   }

   public void initializeNonKey033( )
   {
      A11pro_nombre = "" ;
      n11pro_nombre = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A11pro_nombre", A11pro_nombre);
      n11pro_nombre = ((GXutil.strcmp("", A11pro_nombre)==0) ? true : false) ;
      A12pro_descripcion = "" ;
      n12pro_descripcion = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A12pro_descripcion", A12pro_descripcion);
      n12pro_descripcion = ((GXutil.strcmp("", A12pro_descripcion)==0) ? true : false) ;
      A13pro_precio = DecimalUtil.ZERO ;
      n13pro_precio = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A13pro_precio", GXutil.ltrim( GXutil.str( A13pro_precio, 10, 2)));
      n13pro_precio = ((DecimalUtil.ZERO.compareTo(A13pro_precio)==0) ? true : false) ;
      A7tippro_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A7tippro_codigo", GXutil.ltrim( GXutil.str( A7tippro_codigo, 4, 0)));
      A8tippro_nombre = "" ;
      n8tippro_nombre = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A8tippro_nombre", A8tippro_nombre);
   }

   public void initAll033( )
   {
      A10pro_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
      initializeNonKey033( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void define_styles( )
   {
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?942080");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?58720");
      httpContext.AddJavascriptSource("producto.js", "?942081");
      /* End function include_jscripts */
   }

   public void init_default_properties( )
   {
      imgBtn_first_Internalname = "BTN_FIRST" ;
      imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR" ;
      imgBtn_previous_Internalname = "BTN_PREVIOUS" ;
      imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR" ;
      imgBtn_next_Internalname = "BTN_NEXT" ;
      imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR" ;
      imgBtn_last_Internalname = "BTN_LAST" ;
      imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR" ;
      imgBtn_select_Internalname = "BTN_SELECT" ;
      imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR" ;
      imgBtn_enter2_Internalname = "BTN_ENTER2" ;
      imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR" ;
      imgBtn_cancel2_Internalname = "BTN_CANCEL2" ;
      imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR" ;
      imgBtn_delete2_Internalname = "BTN_DELETE2" ;
      imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR" ;
      tblTabletoolbar_Internalname = "TABLETOOLBAR" ;
      lblTextblockpro_codigo_Internalname = "TEXTBLOCKPRO_CODIGO" ;
      edtpro_codigo_Internalname = "PRO_CODIGO" ;
      lblTextblocktippro_codigo2_Internalname = "TEXTBLOCKTIPPRO_CODIGO2" ;
      edttippro_codigo_Internalname = "TIPPRO_CODIGO" ;
      edttippro_nombre_Internalname = "TIPPRO_NOMBRE" ;
      lblTextblockpro_nombre_Internalname = "TEXTBLOCKPRO_NOMBRE" ;
      edtpro_nombre_Internalname = "PRO_NOMBRE" ;
      lblTextblockpro_descripcion_Internalname = "TEXTBLOCKPRO_DESCRIPCION" ;
      edtpro_descripcion_Internalname = "PRO_DESCRIPCION" ;
      lblTextblockpro_precio_Internalname = "TEXTBLOCKPRO_PRECIO" ;
      edtpro_precio_Internalname = "PRO_PRECIO" ;
      tblTable2_Internalname = "TABLE2" ;
      bttBtn_enter_Internalname = "BTN_ENTER" ;
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      bttBtn_delete_Internalname = "BTN_DELETE" ;
      tblTable1_Internalname = "TABLE1" ;
      grpGroupdata_Internalname = "GROUPDATA" ;
      tblTablemain_Internalname = "TABLEMAIN" ;
      Form.setInternalname( "FORM" );
      imgprompt_7_Internalname = "PROMPT_7" ;
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "producto" );
      imgBtn_delete2_separator_Visible = 1 ;
      imgBtn_delete2_Enabled = 1 ;
      imgBtn_delete2_Visible = 1 ;
      imgBtn_cancel2_separator_Visible = 1 ;
      imgBtn_cancel2_Visible = 1 ;
      imgBtn_enter2_separator_Visible = 1 ;
      imgBtn_enter2_Enabled = 1 ;
      imgBtn_enter2_Visible = 1 ;
      imgBtn_select_separator_Visible = 1 ;
      imgBtn_select_Visible = 1 ;
      imgBtn_last_separator_Visible = 1 ;
      imgBtn_last_Visible = 1 ;
      imgBtn_next_separator_Visible = 1 ;
      imgBtn_next_Visible = 1 ;
      imgBtn_previous_separator_Visible = 1 ;
      imgBtn_previous_Visible = 1 ;
      imgBtn_first_separator_Visible = 1 ;
      imgBtn_first_Visible = 1 ;
      edtpro_precio_Jsonclick = "" ;
      edtpro_precio_Enabled = 1 ;
      edtpro_descripcion_Enabled = 1 ;
      edtpro_nombre_Jsonclick = "" ;
      edtpro_nombre_Enabled = 1 ;
      edttippro_nombre_Jsonclick = "" ;
      edttippro_nombre_Enabled = 0 ;
      imgprompt_7_Visible = 1 ;
      imgprompt_7_Link = "" ;
      edttippro_codigo_Jsonclick = "" ;
      edttippro_codigo_Enabled = 1 ;
      edtpro_codigo_Jsonclick = "" ;
      edtpro_codigo_Enabled = 1 ;
      bttBtn_delete_Visible = 1 ;
      bttBtn_cancel_Visible = 1 ;
      bttBtn_enter_Visible = 1 ;
      httpContext.GX_msglist.setDisplaymode( (short)(1) );
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void valid_Tippro_codigo( short GX_Parm1 ,
                                    String GX_Parm2 )
   {
      A7tippro_codigo = GX_Parm1 ;
      A8tippro_nombre = GX_Parm2 ;
      n8tippro_nombre = false ;
      /* Using cursor T000313 */
      pr_default.execute(11, new Object[] {new Short(A7tippro_codigo)});
      if ( (pr_default.getStatus(11) == 101) )
      {
         AnyError7 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'tipo_producto'.", "ForeignKeyNotFound", 1, "TIPPRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edttippro_codigo_Internalname ;
      }
      if ( AnyError7 == 0 )
      {
         A8tippro_nombre = T000313_A8tippro_nombre[0] ;
         n8tippro_nombre = T000313_n8tippro_nombre[0] ;
      }
      pr_default.close(11);
      dynload_actions( ) ;
      if ( AnyError == 1 )
      {
         A8tippro_nombre = "" ;
         n8tippro_nombre = false ;
      }
      isValidOutput.add(GXutil.rtrim( A8tippro_nombre));
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(11);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      sPrefix = "" ;
      wcpOGx_mode = "" ;
      scmdbuf = "" ;
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      Gx_mode = "" ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      GX_FocusControl = "" ;
      sStyleString = "" ;
      ClassString = "" ;
      StyleString = "" ;
      TempTags = "" ;
      bttBtn_enter_Jsonclick = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      bttBtn_delete_Jsonclick = "" ;
      lblTextblockpro_codigo_Jsonclick = "" ;
      lblTextblocktippro_codigo2_Jsonclick = "" ;
      A8tippro_nombre = "" ;
      lblTextblockpro_nombre_Jsonclick = "" ;
      A11pro_nombre = "" ;
      lblTextblockpro_descripcion_Jsonclick = "" ;
      A12pro_descripcion = "" ;
      lblTextblockpro_precio_Jsonclick = "" ;
      A13pro_precio = DecimalUtil.ZERO ;
      imgBtn_first_Jsonclick = "" ;
      imgBtn_first_separator_Jsonclick = "" ;
      imgBtn_previous_Jsonclick = "" ;
      imgBtn_previous_separator_Jsonclick = "" ;
      imgBtn_next_Jsonclick = "" ;
      imgBtn_next_separator_Jsonclick = "" ;
      imgBtn_last_Jsonclick = "" ;
      imgBtn_last_separator_Jsonclick = "" ;
      imgBtn_select_Jsonclick = "" ;
      imgBtn_select_separator_Jsonclick = "" ;
      imgBtn_enter2_Jsonclick = "" ;
      imgBtn_enter2_separator_Jsonclick = "" ;
      imgBtn_cancel2_Jsonclick = "" ;
      imgBtn_cancel2_separator_Jsonclick = "" ;
      imgBtn_delete2_Jsonclick = "" ;
      imgBtn_delete2_separator_Jsonclick = "" ;
      Z11pro_nombre = "" ;
      Z12pro_descripcion = "" ;
      Z13pro_precio = DecimalUtil.ZERO ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      sMode3 = "" ;
      Z8tippro_nombre = "" ;
      T00035_A10pro_codigo = new short[1] ;
      T00035_A11pro_nombre = new String[] {""} ;
      T00035_n11pro_nombre = new boolean[] {false} ;
      T00035_A12pro_descripcion = new String[] {""} ;
      T00035_n12pro_descripcion = new boolean[] {false} ;
      T00035_A13pro_precio = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      T00035_n13pro_precio = new boolean[] {false} ;
      T00035_A8tippro_nombre = new String[] {""} ;
      T00035_n8tippro_nombre = new boolean[] {false} ;
      T00035_A7tippro_codigo = new short[1] ;
      T00034_A8tippro_nombre = new String[] {""} ;
      T00034_n8tippro_nombre = new boolean[] {false} ;
      T00036_A8tippro_nombre = new String[] {""} ;
      T00036_n8tippro_nombre = new boolean[] {false} ;
      T00037_A10pro_codigo = new short[1] ;
      T00033_A10pro_codigo = new short[1] ;
      T00033_A11pro_nombre = new String[] {""} ;
      T00033_n11pro_nombre = new boolean[] {false} ;
      T00033_A12pro_descripcion = new String[] {""} ;
      T00033_n12pro_descripcion = new boolean[] {false} ;
      T00033_A13pro_precio = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      T00033_n13pro_precio = new boolean[] {false} ;
      T00033_A7tippro_codigo = new short[1] ;
      T00038_A10pro_codigo = new short[1] ;
      T00039_A10pro_codigo = new short[1] ;
      T00032_A10pro_codigo = new short[1] ;
      T00032_A11pro_nombre = new String[] {""} ;
      T00032_n11pro_nombre = new boolean[] {false} ;
      T00032_A12pro_descripcion = new String[] {""} ;
      T00032_n12pro_descripcion = new boolean[] {false} ;
      T00032_A13pro_precio = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      T00032_n13pro_precio = new boolean[] {false} ;
      T00032_A7tippro_codigo = new short[1] ;
      T000313_A8tippro_nombre = new String[] {""} ;
      T000313_n8tippro_nombre = new boolean[] {false} ;
      T000314_A14fac_codigo = new short[1] ;
      T000314_A21detfac_codigo = new short[1] ;
      T000315_A10pro_codigo = new short[1] ;
      sDynURL = "" ;
      FormProcess = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      GXt_char3 = "" ;
      isValidOutput = new com.genexus.GxUnknownObjectCollection();
      pr_default = new DataStoreProvider(context, remoteHandle, new producto__default(),
         new Object[] {
             new Object[] {
            T00032_A10pro_codigo, T00032_A11pro_nombre, T00032_n11pro_nombre, T00032_A12pro_descripcion, T00032_n12pro_descripcion, T00032_A13pro_precio, T00032_n13pro_precio, T00032_A7tippro_codigo
            }
            , new Object[] {
            T00033_A10pro_codigo, T00033_A11pro_nombre, T00033_n11pro_nombre, T00033_A12pro_descripcion, T00033_n12pro_descripcion, T00033_A13pro_precio, T00033_n13pro_precio, T00033_A7tippro_codigo
            }
            , new Object[] {
            T00034_A8tippro_nombre, T00034_n8tippro_nombre
            }
            , new Object[] {
            T00035_A10pro_codigo, T00035_A11pro_nombre, T00035_n11pro_nombre, T00035_A12pro_descripcion, T00035_n12pro_descripcion, T00035_A13pro_precio, T00035_n13pro_precio, T00035_A8tippro_nombre, T00035_n8tippro_nombre, T00035_A7tippro_codigo
            }
            , new Object[] {
            T00036_A8tippro_nombre, T00036_n8tippro_nombre
            }
            , new Object[] {
            T00037_A10pro_codigo
            }
            , new Object[] {
            T00038_A10pro_codigo
            }
            , new Object[] {
            T00039_A10pro_codigo
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000313_A8tippro_nombre, T000313_n8tippro_nombre
            }
            , new Object[] {
            T000314_A14fac_codigo, T000314_A21detfac_codigo
            }
            , new Object[] {
            T000315_A10pro_codigo
            }
         }
      );
   }

   private byte GxWebError ;
   private byte nKeyPressed ;
   private byte Gx_BScreen ;
   private byte gxajaxcallmode ;
   private byte wbTemp ;
   private short A7tippro_codigo ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short A10pro_codigo ;
   private short Z10pro_codigo ;
   private short Z7tippro_codigo ;
   private short RcdFound3 ;
   private int trnEnded ;
   private int bttBtn_enter_Visible ;
   private int bttBtn_cancel_Visible ;
   private int bttBtn_delete_Visible ;
   private int edtpro_codigo_Enabled ;
   private int edttippro_codigo_Enabled ;
   private int imgprompt_7_Visible ;
   private int edttippro_nombre_Enabled ;
   private int edtpro_nombre_Enabled ;
   private int edtpro_descripcion_Enabled ;
   private int edtpro_precio_Enabled ;
   private int imgBtn_first_Visible ;
   private int imgBtn_first_separator_Visible ;
   private int imgBtn_previous_Visible ;
   private int imgBtn_previous_separator_Visible ;
   private int imgBtn_next_Visible ;
   private int imgBtn_next_separator_Visible ;
   private int imgBtn_last_Visible ;
   private int imgBtn_last_separator_Visible ;
   private int imgBtn_select_Visible ;
   private int imgBtn_select_separator_Visible ;
   private int imgBtn_enter2_Visible ;
   private int imgBtn_enter2_Enabled ;
   private int imgBtn_enter2_separator_Visible ;
   private int imgBtn_cancel2_Visible ;
   private int imgBtn_cancel2_separator_Visible ;
   private int imgBtn_delete2_Visible ;
   private int imgBtn_delete2_Enabled ;
   private int imgBtn_delete2_separator_Visible ;
   private int GX_JID ;
   private int AnyError7 ;
   private int idxLst ;
   private java.math.BigDecimal A13pro_precio ;
   private java.math.BigDecimal Z13pro_precio ;
   private String sPrefix ;
   private String wcpOGx_mode ;
   private String scmdbuf ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String Gx_mode ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String GX_FocusControl ;
   private String edtpro_codigo_Internalname ;
   private String sStyleString ;
   private String tblTablemain_Internalname ;
   private String ClassString ;
   private String StyleString ;
   private String grpGroupdata_Internalname ;
   private String tblTable1_Internalname ;
   private String TempTags ;
   private String bttBtn_enter_Internalname ;
   private String bttBtn_enter_Jsonclick ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String bttBtn_delete_Internalname ;
   private String bttBtn_delete_Jsonclick ;
   private String tblTable2_Internalname ;
   private String lblTextblockpro_codigo_Internalname ;
   private String lblTextblockpro_codigo_Jsonclick ;
   private String edtpro_codigo_Jsonclick ;
   private String lblTextblocktippro_codigo2_Internalname ;
   private String lblTextblocktippro_codigo2_Jsonclick ;
   private String edttippro_codigo_Internalname ;
   private String edttippro_codigo_Jsonclick ;
   private String imgprompt_7_Internalname ;
   private String imgprompt_7_Link ;
   private String edttippro_nombre_Internalname ;
   private String edttippro_nombre_Jsonclick ;
   private String lblTextblockpro_nombre_Internalname ;
   private String lblTextblockpro_nombre_Jsonclick ;
   private String edtpro_nombre_Internalname ;
   private String edtpro_nombre_Jsonclick ;
   private String lblTextblockpro_descripcion_Internalname ;
   private String lblTextblockpro_descripcion_Jsonclick ;
   private String edtpro_descripcion_Internalname ;
   private String lblTextblockpro_precio_Internalname ;
   private String lblTextblockpro_precio_Jsonclick ;
   private String edtpro_precio_Internalname ;
   private String edtpro_precio_Jsonclick ;
   private String tblTabletoolbar_Internalname ;
   private String imgBtn_first_Internalname ;
   private String imgBtn_first_Jsonclick ;
   private String imgBtn_first_separator_Internalname ;
   private String imgBtn_first_separator_Jsonclick ;
   private String imgBtn_previous_Internalname ;
   private String imgBtn_previous_Jsonclick ;
   private String imgBtn_previous_separator_Internalname ;
   private String imgBtn_previous_separator_Jsonclick ;
   private String imgBtn_next_Internalname ;
   private String imgBtn_next_Jsonclick ;
   private String imgBtn_next_separator_Internalname ;
   private String imgBtn_next_separator_Jsonclick ;
   private String imgBtn_last_Internalname ;
   private String imgBtn_last_Jsonclick ;
   private String imgBtn_last_separator_Internalname ;
   private String imgBtn_last_separator_Jsonclick ;
   private String imgBtn_select_Internalname ;
   private String imgBtn_select_Jsonclick ;
   private String imgBtn_select_separator_Internalname ;
   private String imgBtn_select_separator_Jsonclick ;
   private String imgBtn_enter2_Internalname ;
   private String imgBtn_enter2_Jsonclick ;
   private String imgBtn_enter2_separator_Internalname ;
   private String imgBtn_enter2_separator_Jsonclick ;
   private String imgBtn_cancel2_Internalname ;
   private String imgBtn_cancel2_Jsonclick ;
   private String imgBtn_cancel2_separator_Internalname ;
   private String imgBtn_cancel2_separator_Jsonclick ;
   private String imgBtn_delete2_Internalname ;
   private String imgBtn_delete2_Jsonclick ;
   private String imgBtn_delete2_separator_Internalname ;
   private String imgBtn_delete2_separator_Jsonclick ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String sMode3 ;
   private String sDynURL ;
   private String FormProcess ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private String GXt_char3 ;
   private boolean entryPointCalled ;
   private boolean wbErr ;
   private boolean n8tippro_nombre ;
   private boolean n11pro_nombre ;
   private boolean n12pro_descripcion ;
   private boolean n13pro_precio ;
   private String A8tippro_nombre ;
   private String A11pro_nombre ;
   private String A12pro_descripcion ;
   private String Z11pro_nombre ;
   private String Z12pro_descripcion ;
   private String Z8tippro_nombre ;
   private com.genexus.webpanels.GXMasterPage MasterPageObj ;
   private com.genexus.GxUnknownObjectCollection isValidOutput ;
   private IDataStoreProvider pr_default ;
   private short[] T00035_A10pro_codigo ;
   private String[] T00035_A11pro_nombre ;
   private boolean[] T00035_n11pro_nombre ;
   private String[] T00035_A12pro_descripcion ;
   private boolean[] T00035_n12pro_descripcion ;
   private java.math.BigDecimal[] T00035_A13pro_precio ;
   private boolean[] T00035_n13pro_precio ;
   private String[] T00035_A8tippro_nombre ;
   private boolean[] T00035_n8tippro_nombre ;
   private short[] T00035_A7tippro_codigo ;
   private String[] T00034_A8tippro_nombre ;
   private boolean[] T00034_n8tippro_nombre ;
   private String[] T00036_A8tippro_nombre ;
   private boolean[] T00036_n8tippro_nombre ;
   private short[] T00037_A10pro_codigo ;
   private short[] T00033_A10pro_codigo ;
   private String[] T00033_A11pro_nombre ;
   private boolean[] T00033_n11pro_nombre ;
   private String[] T00033_A12pro_descripcion ;
   private boolean[] T00033_n12pro_descripcion ;
   private java.math.BigDecimal[] T00033_A13pro_precio ;
   private boolean[] T00033_n13pro_precio ;
   private short[] T00033_A7tippro_codigo ;
   private short[] T00038_A10pro_codigo ;
   private short[] T00039_A10pro_codigo ;
   private short[] T00032_A10pro_codigo ;
   private String[] T00032_A11pro_nombre ;
   private boolean[] T00032_n11pro_nombre ;
   private String[] T00032_A12pro_descripcion ;
   private boolean[] T00032_n12pro_descripcion ;
   private java.math.BigDecimal[] T00032_A13pro_precio ;
   private boolean[] T00032_n13pro_precio ;
   private short[] T00032_A7tippro_codigo ;
   private String[] T000313_A8tippro_nombre ;
   private boolean[] T000313_n8tippro_nombre ;
   private short[] T000314_A14fac_codigo ;
   private short[] T000314_A21detfac_codigo ;
   private short[] T000315_A10pro_codigo ;
   private com.genexus.webpanels.GXWebForm Form ;
}

final  class producto__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00032", "SELECT [pro_codigo], [pro_nombre], [pro_descripcion], [pro_precio], [tippro_codigo] FROM [producto] WITH (UPDLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00033", "SELECT [pro_codigo], [pro_nombre], [pro_descripcion], [pro_precio], [tippro_codigo] FROM [producto] WITH (NOLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00034", "SELECT [tippro_nombre] FROM [tipo_producto] WITH (NOLOCK) WHERE [tippro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00035", "SELECT TM1.[pro_codigo], TM1.[pro_nombre], TM1.[pro_descripcion], TM1.[pro_precio], T2.[tippro_nombre], TM1.[tippro_codigo] FROM ([producto] TM1 WITH (NOLOCK) INNER JOIN [tipo_producto] T2 WITH (NOLOCK) ON T2.[tippro_codigo] = TM1.[tippro_codigo]) WHERE TM1.[pro_codigo] = ? ORDER BY TM1.[pro_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
         ,new ForEachCursor("T00036", "SELECT [tippro_nombre] FROM [tipo_producto] WITH (NOLOCK) WHERE [tippro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00037", "SELECT [pro_codigo] FROM [producto] WITH (NOLOCK) WHERE [pro_codigo] = ?  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00038", "SELECT TOP 1 [pro_codigo] FROM [producto] WITH (NOLOCK) WHERE ( [pro_codigo] > ?) ORDER BY [pro_codigo]  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new ForEachCursor("T00039", "SELECT TOP 1 [pro_codigo] FROM [producto] WITH (NOLOCK) WHERE ( [pro_codigo] < ?) ORDER BY [pro_codigo] DESC  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new UpdateCursor("T000310", "INSERT INTO [producto] ([pro_codigo], [pro_nombre], [pro_descripcion], [pro_precio], [tippro_codigo]) VALUES (?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T000311", "UPDATE [producto] SET [pro_nombre]=?, [pro_descripcion]=?, [pro_precio]=?, [tippro_codigo]=?  WHERE [pro_codigo] = ?", GX_NOMASK)
         ,new UpdateCursor("T000312", "DELETE FROM [producto]  WHERE [pro_codigo] = ?", GX_NOMASK)
         ,new ForEachCursor("T000313", "SELECT [tippro_nombre] FROM [tipo_producto] WITH (NOLOCK) WHERE [tippro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000314", "SELECT TOP 1 [fac_codigo], [detfac_codigo] FROM [facturaLevel1] WITH (NOLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,true )
         ,new ForEachCursor("T000315", "SELECT [pro_codigo] FROM [producto] WITH (NOLOCK) ORDER BY [pro_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((java.math.BigDecimal[]) buf[5])[0] = rslt.getBigDecimal(4,2) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(5) ;
               break;
            case 1 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((java.math.BigDecimal[]) buf[5])[0] = rslt.getBigDecimal(4,2) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(5) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 3 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((java.math.BigDecimal[]) buf[5])[0] = rslt.getBigDecimal(4,2) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((short[]) buf[9])[0] = rslt.getShort(6) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 5 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 6 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 7 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 12 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               break;
            case 13 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 1 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 2 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 3 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 4 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 5 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 6 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 7 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 8 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[2], 45);
               }
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[4], 200);
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.DECIMAL );
               }
               else
               {
                  stmt.setBigDecimal(4, (java.math.BigDecimal)parms[6], 2);
               }
               stmt.setShort(5, ((Number) parms[7]).shortValue());
               break;
            case 9 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(1, (String)parms[1], 45);
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(2, (String)parms[3], 200);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.DECIMAL );
               }
               else
               {
                  stmt.setBigDecimal(3, (java.math.BigDecimal)parms[5], 2);
               }
               stmt.setShort(4, ((Number) parms[6]).shortValue());
               stmt.setShort(5, ((Number) parms[7]).shortValue());
               break;
            case 10 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 11 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 12 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
      }
   }

}

