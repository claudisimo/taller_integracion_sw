/*
               File: Gx00C1
        Description: Selection List Level1
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:15.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/gx00c1")
public final  class gx00c1 extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new gx00c1_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Selection List Level1";
   }

}

