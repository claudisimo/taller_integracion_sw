/*
               File: Gx0080
        Description: Selection List permisos
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:13.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/gx0080")
public final  class gx0080 extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new gx0080_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Selection List permisos";
   }

}

