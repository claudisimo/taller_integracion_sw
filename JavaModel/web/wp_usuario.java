/*
               File: wp_usuario
        Description: wp_usuario
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:9.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/wp_usuario")
public final  class wp_usuario extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new wp_usuario_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "wp_usuario";
   }

}

