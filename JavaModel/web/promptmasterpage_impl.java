/*
               File: promptmasterpage_impl
        Description: Prompt Master Page
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:8.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class promptmasterpage_impl extends GXMasterPage
{
   public promptmasterpage_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public promptmasterpage_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( promptmasterpage_impl.class ));
   }

   public promptmasterpage_impl( int remoteHandle ,
                                 ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void initweb( )
   {
      initialize_properties( ) ;
      httpContext.setTheme("GeneXusX");
   }

   public void webExecute( )
   {
      initweb( ) ;
      if ( ! httpContext.isAjaxCallMode( ) )
      {
         pa032( ) ;
         if ( ! httpContext.isAjaxCallMode( ) )
         {
         }
         if ( ( GxWebError == 0 ) && ! httpContext.isAjaxCallMode( ) )
         {
            ws032( ) ;
            if ( ! httpContext.isAjaxCallMode( ) )
            {
               we032( ) ;
            }
         }
      }
      cleanup();
   }

   public void renderHtmlHeaders( )
   {
      getDataAreaObject().renderHtmlHeaders();
   }

   public void renderHtmlOpenForm( )
   {
      getDataAreaObject().renderHtmlOpenForm();
   }

   public void renderHtmlCloseForm032( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      getDataAreaObject().renderHtmlCloseForm();
      httpContext.AddJavascriptSource("promptmasterpage.js", "?942846");
      httpContext.writeTextNL( "</body>") ;
      httpContext.writeTextNL( "</html>") ;
   }

   public void wb030( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! wbLoad )
      {
         renderHtmlHeaders( ) ;
         renderHtmlOpenForm( ) ;
         if ( ! ShowMPWhenPopUp( ) && httpContext.isPopUpObject( ) )
         {
            getDataAreaObject().renderHtmlContent();
            return  ;
         }
         wb_table1_2_032( true) ;
      }
      else
      {
         wb_table1_2_032( false) ;
      }
      return  ;
   }

   public void wb_table1_2_032e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "<p></p>") ;
      }
      wbLoad = true ;
   }

   public void start032( )
   {
      wbLoad = false ;
      wbEnd = 0 ;
      wbStart = 0 ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      wbErr = false ;
      strup030( ) ;
      if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) )
      {
         if ( getDataAreaObject().executeStartEvent() != 0 )
         {
            httpContext.setAjaxCallMode();
         }
      }
   }

   public void ws032( )
   {
      start032( ) ;
      evt032( ) ;
   }

   public void evt032( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) && ! wbErr )
         {
            /* Read Web Panel buttons. */
            sEvt = httpContext.cgiGet( "_EventName") ;
            EvtGridId = httpContext.cgiGet( "_EventGridId") ;
            EvtRowId = httpContext.cgiGet( "_EventRowId") ;
            if ( GXutil.len( sEvt) > 0 )
            {
               sEvtType = GXutil.left( sEvt, 1) ;
               sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "RFR_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "START_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e11032 */
                        e11032 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "REFRESH_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e12032 */
                        e12032 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "LOAD_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e13032 */
                        e13032 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "ENTER_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( ! wbErr )
                        {
                           Rfr0gs = false ;
                           if ( ! Rfr0gs )
                           {
                           }
                           dynload_actions( ) ;
                        }
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                     else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                     }
                  }
                  else
                  {
                  }
               }
               if ( httpContext.wbHandled == 0 )
               {
                  getDataAreaObject().dispatchEvents();
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void we032( )
   {
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         Rfr0gs = true ;
         refresh( ) ;
         if ( ! GxWebStd.gx_redirect( httpContext) )
         {
            renderHtmlCloseForm032( ) ;
         }
      }
   }

   public void pa032( )
   {
      if ( nDonePA == 0 )
      {
         nDonePA = (byte)(1) ;
      }
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void refresh( )
   {
      rf032( ) ;
      /* End function Refresh */
   }

   public void rf032( )
   {
      /* Execute user event: e12032 */
      e12032 ();
      if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) )
      {
         /* Execute user event: e13032 */
         e13032 ();
         wb030( ) ;
      }
   }

   public void strup030( )
   {
      /* Before Start, stand alone formulas. */
      Gx_err = (short)(0) ;
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      /* Execute user event: e11032 */
      e11032 ();
      httpContext.wbGlbDoneStart = (byte)(1) ;
      /* After Start, stand alone formulas. */
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read saved SDTs. */
         /* Read variables values. */
         /* Read saved values. */
         /* Read subfile selected row values. */
         /* Read hidden variables. */
      }
      else
      {
         dynload_actions( ) ;
      }
   }

   protected void GXStart( )
   {
      /* Execute user event: e11032 */
      e11032 ();
      if (returnInSub) return;
   }

   public void e11032( )
   {
      /* Start Routine */
   }

   public void e12032( )
   {
      /* Refresh Routine */
   }

   protected void nextLoad( )
   {
   }

   protected void e13032( )
   {
      /* Load Routine */
   }

   public void wb_table1_2_032( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         sStyleString = sStyleString + " width: " + GXutil.ltrim( GXutil.str( 95, 10, 0)) + "%" + ";" ;
         GxWebStd.gx_table_start( httpContext, tblTable2_Internalname, tblTable2_Internalname, "", "Table95", 0, "", "", 0, 0, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td colspan=\"3\" >") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td style=\"width:0px\">") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td style=\"width:100%\">") ;
         wb_table2_8_032( true) ;
      }
      else
      {
         wb_table2_8_032( false) ;
      }
      return  ;
   }

   public void wb_table2_8_032e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td style=\"width:0px\">") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td colspan=\"3\" >") ;
         wb_table3_15_032( true) ;
      }
      else
      {
         wb_table3_15_032( false) ;
      }
      return  ;
   }

   public void wb_table3_15_032e( boolean wbgen )
   {
      if ( wbgen )
      {
         wb_table4_18_032( true) ;
      }
      else
      {
         wb_table4_18_032( false) ;
      }
      return  ;
   }

   public void wb_table4_18_032e( boolean wbgen )
   {
      if ( wbgen )
      {
         wb_table5_21_032( true) ;
      }
      else
      {
         wb_table5_21_032( false) ;
      }
      return  ;
   }

   public void wb_table5_21_032e( boolean wbgen )
   {
      if ( wbgen )
      {
         wb_table6_24_032( true) ;
      }
      else
      {
         wb_table6_24_032( false) ;
      }
      return  ;
   }

   public void wb_table6_24_032e( boolean wbgen )
   {
      if ( wbgen )
      {
         wb_table7_28_032( true) ;
      }
      else
      {
         wb_table7_28_032( false) ;
      }
      return  ;
   }

   public void wb_table7_28_032e( boolean wbgen )
   {
      if ( wbgen )
      {
         wb_table8_31_032( true) ;
      }
      else
      {
         wb_table8_31_032( false) ;
      }
      return  ;
   }

   public void wb_table8_31_032e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_032e( true) ;
      }
      else
      {
         wb_table1_2_032e( false) ;
      }
   }

   public void wb_table8_31_032( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable8_Internalname, tblTable8_Internalname, "", "HorizontalLine", 0, "", "", 0, 0, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table8_31_032e( true) ;
      }
      else
      {
         wb_table8_31_032e( false) ;
      }
   }

   public void wb_table7_28_032( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable7_Internalname, tblTable7_Internalname, "", "HorizontalSpace", 0, "", "", 0, 0, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table7_28_032e( true) ;
      }
      else
      {
         wb_table7_28_032e( false) ;
      }
   }

   public void wb_table6_24_032( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "TableBottom", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr align=\"left\" >") ;
         httpContext.writeText( "<td style=\"text-align:"+httpContext.getCssProperty( "Align", "center")+"\">") ;
         /* Text block */
         ClassString = "SmallText" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblock1_Internalname, "Footer Info", "", "", lblTextblock1_Jsonclick, "E_MPAGE.", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_PromptMasterPage.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table6_24_032e( true) ;
      }
      else
      {
         wb_table6_24_032e( false) ;
      }
   }

   public void wb_table5_21_032( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable6_Internalname, tblTable6_Internalname, "", "HorizontalSpace", 0, "", "", 0, 0, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table5_21_032e( true) ;
      }
      else
      {
         wb_table5_21_032e( false) ;
      }
   }

   public void wb_table4_18_032( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable5_Internalname, tblTable5_Internalname, "", "HorizontalLine", 0, "", "", 0, 0, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table4_18_032e( true) ;
      }
      else
      {
         wb_table4_18_032e( false) ;
      }
   }

   public void wb_table3_15_032( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable4_Internalname, tblTable4_Internalname, "", "HorizontalSpace", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_15_032e( true) ;
      }
      else
      {
         wb_table3_15_032e( false) ;
      }
   }

   public void wb_table2_8_032( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable3_Internalname, tblTable3_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         getDataAreaObject().renderHtmlContent();
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_8_032e( true) ;
      }
      else
      {
         wb_table2_8_032e( false) ;
      }
   }

   public void setparameters( Object[] obj )
   {
   }

   public String getresponse( String sGXDynURL )
   {
      initialize_properties( ) ;
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      sDynURL = sGXDynURL ;
      nGotPars = 1 ;
      nGXWrapped = 1 ;
      httpContext.setWrapped(true);
      pa032( ) ;
      ws032( ) ;
      we032( ) ;
      httpContext.setWrapped(false);
      httpContext.GX_msglist = BackMsgLst ;
      return ((java.io.ByteArrayOutputStream) httpContext.getOutputStream()).toString();
   }

   public void responsestatic( String sGXDynURL )
   {
   }

   public void master_styles( )
   {
      define_styles( ) ;
   }

   public void define_styles( )
   {
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= getDataAreaObject().getForm().getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( getDataAreaObject().getForm().getJscriptsrc().item(idxLst)), "?942854");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      if ( nGXWrapped != 1 )
      {
         httpContext.AddJavascriptSource("promptmasterpage.js", "?942854");
      }
      /* End function include_jscripts */
   }

   public void init_default_properties( )
   {
      tblTable3_Internalname = "TABLE3_MPAGE" ;
      tblTable4_Internalname = "TABLE4_MPAGE" ;
      tblTable5_Internalname = "TABLE5_MPAGE" ;
      tblTable6_Internalname = "TABLE6_MPAGE" ;
      lblTextblock1_Internalname = "TEXTBLOCK1_MPAGE" ;
      tblTable1_Internalname = "TABLE1_MPAGE" ;
      tblTable7_Internalname = "TABLE7_MPAGE" ;
      tblTable8_Internalname = "TABLE8_MPAGE" ;
      tblTable2_Internalname = "TABLE2_MPAGE" ;
      getDataAreaObject().getForm().setInternalname( "FORM_MPAGE" );
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      Contentholder.setDataArea(getDataAreaObject());
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      Contentholder = new com.genexus.webpanels.GXDataAreaControl();
      sPrefix = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      sStyleString = "" ;
      GXt_char1 = "" ;
      ClassString = "" ;
      StyleString = "" ;
      lblTextblock1_Jsonclick = "" ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      sDynURL = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte GxWebError ;
   private byte nDonePA ;
   private byte nGotPars ;
   private byte nGXWrapped ;
   private short wbEnd ;
   private short wbStart ;
   private short Gx_err ;
   private int idxLst ;
   private String sPrefix ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String sStyleString ;
   private String tblTable2_Internalname ;
   private String tblTable8_Internalname ;
   private String tblTable7_Internalname ;
   private String tblTable1_Internalname ;
   private String GXt_char1 ;
   private String ClassString ;
   private String StyleString ;
   private String lblTextblock1_Internalname ;
   private String lblTextblock1_Jsonclick ;
   private String tblTable6_Internalname ;
   private String tblTable5_Internalname ;
   private String tblTable4_Internalname ;
   private String tblTable3_Internalname ;
   private String sDynURL ;
   private boolean wbLoad ;
   private boolean Rfr0gs ;
   private boolean wbErr ;
   private boolean returnInSub ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private com.genexus.webpanels.GXDataAreaControl Contentholder ;
   private com.genexus.webpanels.GXWebForm Form ;
}

