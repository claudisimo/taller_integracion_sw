/*
               File: Menu_Administrador
        Description: Menu_ Administrador
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:8.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/menu_administrador")
public final  class menu_administrador extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new menu_administrador_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Menu_ Administrador";
   }

}

