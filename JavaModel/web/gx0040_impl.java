/*
               File: gx0040_impl
        Description: Selection List factura
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:12.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class gx0040_impl extends GXDataArea
{
   public gx0040_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public gx0040_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( gx0040_impl.class ));
   }

   public gx0040_impl( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void initweb( )
   {
      initialize_properties( ) ;
      if ( nGotPars == 0 )
      {
         entryPointCalled = false ;
         gxfirstwebparm = httpContext.GetNextPar( ) ;
         gxfirstwebparm_bkp = gxfirstwebparm ;
         gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm, "High") ;
         if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
         {
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            dyncall( httpContext.GetNextPar( )) ;
            return  ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            httpContext.setAjaxEventMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = httpContext.GetNextPar( ) ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
         {
            nRC_Grid1 = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            nGXsfl_51_idx = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            sGXsfl_51_idx = httpContext.GetNextPar( ) ;
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxnrgrid1_newrow( nRC_Grid1, nGXsfl_51_idx, sGXsfl_51_idx) ;
            return  ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
         {
            /* GeneXus formulas. */
            Gx_err = (short)(0) ;
            Grid1_PageSize51 = (int)(GXutil.lval( httpContext.GetNextPar( ))) ;
            AV6cfac_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV6cfac_codigo", GXutil.ltrim( GXutil.str( AV6cfac_codigo, 4, 0)));
            AV7cfac_numero = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV7cfac_numero", GXutil.ltrim( GXutil.str( AV7cfac_numero, 4, 0)));
            AV8cfac_fecha = localUtil.parseDateParm( httpContext.GetNextPar( )) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV8cfac_fecha", localUtil.format(AV8cfac_fecha, "99/99/99"));
            AV9cfac_subtotal = DecimalUtil.doubleToDec(GXutil.val( httpContext.GetNextPar( ), ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV9cfac_subtotal", GXutil.ltrim( GXutil.str( AV9cfac_subtotal, 10, 2)));
            AV10cfac_impuesto = DecimalUtil.doubleToDec(GXutil.val( httpContext.GetNextPar( ), ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV10cfac_impuesto", GXutil.ltrim( GXutil.str( AV10cfac_impuesto, 10, 2)));
            AV12cfac_estado = httpContext.GetNextPar( ) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV12cfac_estado", AV12cfac_estado);
            AV14ccli_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV14ccli_codigo", GXutil.ltrim( GXutil.str( AV14ccli_codigo, 4, 0)));
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxgrgrid1_refresh( Grid1_PageSize51, AV6cfac_codigo, AV7cfac_numero, AV8cfac_fecha, AV9cfac_subtotal, AV10cfac_impuesto, AV12cfac_estado, AV14ccli_codigo) ;
            return  ;
         }
         else
         {
            if ( ! httpContext.IsValidAjaxCall( false) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp ;
         }
         if ( ! entryPointCalled )
         {
            AV13pfac_codigo = (short)(GXutil.lval( gxfirstwebparm)) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV13pfac_codigo", GXutil.ltrim( GXutil.str( AV13pfac_codigo, 4, 0)));
         }
      }
      httpContext.setTheme("GeneXusX");
   }

   public void webExecute( )
   {
      initweb( ) ;
      if ( ! httpContext.isAjaxCallMode( ) )
      {
         MasterPageObj = new promptmasterpage_impl (remoteHandle, context.copy());
         MasterPageObj.setDataArea(this,true);
         MasterPageObj.webExecute();
         if ( httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "max-age=0");
            }
            if ( (GXutil.strcmp("", httpContext.wjLoc)==0) )
            {
               httpContext.GX_webresponse.addString(httpContext.getJSONResponse( ));
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public byte executeStartEvent( )
   {
      pa072( ) ;
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
      {
         start072( ) ;
      }
      return gxajaxcallmode ;
   }

   public void renderHtmlHeaders( )
   {
      GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), "IE=EmulateIE7");
   }

   public void renderHtmlOpenForm( )
   {
      httpContext.writeText( "<title>") ;
      httpContext.writeText( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      if ( nGXWrapped != 1 )
      {
         MasterPageObj.master_styles();
      }
      if ( ! httpContext.isSmartDevice( ) )
      {
         httpContext.AddJavascriptSource("gxgral.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      else
      {
         httpContext.AddJavascriptSource("gxapiSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfxSD.js", "?58720");
         httpContext.AddJavascriptSource("gxtypesSD.js", "?58720");
         httpContext.AddJavascriptSource("gxpopupSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfrmutlSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgridSD.js", "?58720");
         httpContext.AddJavascriptSource("JavaScripTableSD.js", "?58720");
         httpContext.AddJavascriptSource("rijndaelSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgralSD.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      httpContext.AddJavascriptSource("calendar.js", "?58720");
      httpContext.AddJavascriptSource("calendar-setup.js", "?58720");
      httpContext.AddJavascriptSource("calendar-es.js", "?58720");
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      FormProcess = " onkeyup=\"gx.evt.onkeyup(event)\" onkeypress=\"gx.evt.onkeypress(event,true,false)\" onkeydown=\"gx.evt.onkeypress(null,true,false)\"" ;
      httpContext.writeText( "<body") ;
      httpContext.writeText( " "+"class=\"Form\""+" "+" style=\"-moz-opacity:0;opacity:0;"+"background-color:"+WebUtils.getHTMLColor( Form.getIBackground())+";") ;
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         httpContext.writeText( " background-image:url("+httpContext.convertURL( Form.getBackground())+")") ;
      }
      httpContext.writeText( "\""+FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" onsubmit=\"try{return gx.csv.validForm()}catch(e){return true;}\" name=\"MAINFORM\" method=\"post\" action=\""+formatLink("gx0040") + "?" + GXutil.URLEncode(GXutil.ltrim(GXutil.str(AV13pfac_codigo,4,0)))+"\" class=\""+"Form"+"\">") ;
      GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
   }

   public void renderHtmlCloseForm( )
   {
      /* Send hidden variables. */
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCFAC_CODIGO", GXutil.ltrim( localUtil.ntoc( AV6cfac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCFAC_NUMERO", GXutil.ltrim( localUtil.ntoc( AV7cfac_numero, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCFAC_FECHA", localUtil.format(AV8cfac_fecha, "99/99/99"));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCFAC_SUBTOTAL", GXutil.ltrim( localUtil.ntoc( AV9cfac_subtotal, (byte)(10), (byte)(2), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCFAC_IMPUESTO", GXutil.ltrim( localUtil.ntoc( AV10cfac_impuesto, (byte)(10), (byte)(2), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCFAC_ESTADO", GXutil.rtrim( AV12cfac_estado));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCCLI_CODIGO", GXutil.ltrim( localUtil.ntoc( AV14ccli_codigo, (byte)(4), (byte)(0), ",", "")));
      /* Send saved values. */
      GxWebStd.gx_hidden_field( httpContext, "nRC_Grid1", GXutil.ltrim( localUtil.ntoc( nRC_Grid1, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "vPFAC_CODIGO", GXutil.ltrim( localUtil.ntoc( AV13pfac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GRID1_nFirstRecordOnPage", GXutil.ltrim( localUtil.ntoc( GRID1_nFirstRecordOnPage, (byte)(6), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GRID1_nEOF", GXutil.ltrim( localUtil.ntoc( GRID1_nEOF, (byte)(1), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      httpContext.writeTextNL( "</form>") ;
      include_jscripts( ) ;
   }

   public void renderHtmlContent( )
   {
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
      {
         we072( ) ;
      }
   }

   public void dispatchEvents( )
   {
      evt072( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public String getPgmname( )
   {
      return "Gx0040" ;
   }

   public String getPgmdesc( )
   {
      return "Selection List factura" ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("gx0040") + "?" + GXutil.URLEncode(GXutil.ltrim(GXutil.str(AV13pfac_codigo,4,0))) ;
   }

   public void wb070( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! wbLoad )
      {
         if ( nGXWrapped == 1 )
         {
            renderHtmlHeaders( ) ;
            renderHtmlOpenForm( ) ;
         }
         wb_table1_2_072( true) ;
      }
      else
      {
         wb_table1_2_072( false) ;
      }
      return  ;
   }

   public void wb_table1_2_072e( boolean wbgen )
   {
      if ( wbgen )
      {
      }
      wbLoad = true ;
   }

   public void start072( )
   {
      wbLoad = false ;
      wbEnd = 0 ;
      wbStart = 0 ;
      Form.getMeta().addItem("Generator", "GeneXus Java", (short)(0)) ;
      Form.getMeta().addItem("Version", "10_1_8-58720", (short)(0)) ;
      Form.getMeta().addItem("Description", "Selection List factura", (short)(0)) ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      wbErr = false ;
      strup070( ) ;
   }

   public void ws072( )
   {
      start072( ) ;
      evt072( ) ;
   }

   public void evt072( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) && ! wbErr )
         {
            /* Read Web Panel buttons. */
            sEvt = httpContext.cgiGet( "_EventName") ;
            EvtGridId = httpContext.cgiGet( "_EventGridId") ;
            EvtRowId = httpContext.cgiGet( "_EventRowId") ;
            if ( GXutil.len( sEvt) > 0 )
            {
               sEvtType = GXutil.left( sEvt, 1) ;
               sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
               /* Check if conditions changed and reset current page numbers */
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCFAC_CODIGO"), ",", ".") != AV6cfac_codigo )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCFAC_NUMERO"), ",", ".") != AV7cfac_numero )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( !( localUtil.ctod( httpContext.cgiGet( "GXH_vCFAC_FECHA"), 3).equals( AV8cfac_fecha ) ) )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctond( httpContext.cgiGet( "GXH_vCFAC_SUBTOTAL")).compareTo(AV9cfac_subtotal) != 0 )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctond( httpContext.cgiGet( "GXH_vCFAC_IMPUESTO")).compareTo(AV10cfac_impuesto) != 0 )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCFAC_ESTADO"), AV12cfac_estado) != 0 )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCCLI_CODIGO"), ",", ".") != AV14ccli_codigo )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( GXutil.strcmp(sEvtType, "M") != 0 )
               {
                  if ( GXutil.strcmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = GXutil.right( sEvt, 1) ;
                     if ( GXutil.strcmp(sEvtType, ".") == 0 )
                     {
                        sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                        if ( GXutil.strcmp(sEvt, "RFR") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           dynload_actions( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           dynload_actions( ) ;
                        }
                        else if ( GXutil.strcmp(sEvt, "GRID1PAGING") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           sEvt = httpContext.cgiGet( "GRID1PAGING") ;
                           if ( GXutil.strcmp(sEvt, "FIRST") == 0 )
                           {
                              subgrid1_firstpage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "PREV") == 0 )
                           {
                              subgrid1_previouspage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "NEXT") == 0 )
                           {
                              subgrid1_nextpage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "LAST") == 0 )
                           {
                              subgrid1_lastpage( ) ;
                           }
                        }
                     }
                     else
                     {
                        sEvtType = GXutil.right( sEvt, 4) ;
                        sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-4) ;
                        if ( ( GXutil.strcmp(GXutil.left( sEvt, 5), "START") == 0 ) || ( GXutil.strcmp(GXutil.left( sEvt, 4), "LOAD") == 0 ) || ( GXutil.strcmp(GXutil.left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_51_idx = (short)(GXutil.lval( sEvtType)) ;
                           sGXsfl_51_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_51_idx, 4, 0)), (short)(4), "0") ;
                           edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_51_idx ;
                           edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_51_idx ;
                           edtfac_numero_Internalname = "FAC_NUMERO_"+sGXsfl_51_idx ;
                           edtfac_fecha_Internalname = "FAC_FECHA_"+sGXsfl_51_idx ;
                           edtfac_subtotal_Internalname = "FAC_SUBTOTAL_"+sGXsfl_51_idx ;
                           edtfac_impuesto_Internalname = "FAC_IMPUESTO_"+sGXsfl_51_idx ;
                           edtfac_estado_Internalname = "FAC_ESTADO_"+sGXsfl_51_idx ;
                           AV5LinkSelection = httpContext.cgiGet( "GXimg"+edtavLinkselection_Internalname) ;
                           A14fac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtfac_codigo_Internalname), ",", ".")) ;
                           A15fac_numero = (short)(localUtil.ctol( httpContext.cgiGet( edtfac_numero_Internalname), ",", ".")) ;
                           n15fac_numero = false ;
                           A16fac_fecha = localUtil.ctod( httpContext.cgiGet( edtfac_fecha_Internalname), 3) ;
                           n16fac_fecha = false ;
                           A17fac_subtotal = localUtil.ctond( httpContext.cgiGet( edtfac_subtotal_Internalname)) ;
                           n17fac_subtotal = false ;
                           A18fac_impuesto = localUtil.ctond( httpContext.cgiGet( edtfac_impuesto_Internalname)) ;
                           A20fac_estado = httpContext.cgiGet( edtfac_estado_Internalname) ;
                           n20fac_estado = false ;
                           sEvtType = GXutil.right( sEvt, 1) ;
                           if ( GXutil.strcmp(sEvtType, ".") == 0 )
                           {
                              sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                              if ( GXutil.strcmp(sEvt, "START") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                                 /* Execute user event: e11072 */
                                 e11072 ();
                              }
                              else if ( GXutil.strcmp(sEvt, "LOAD") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                                 /* Execute user event: e12072 */
                                 e12072 ();
                              }
                              else if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false ;
                                    /* Set Refresh If Cfac_codigo Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCFAC_CODIGO"), ",", ".") != AV6cfac_codigo )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cfac_numero Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCFAC_NUMERO"), ",", ".") != AV7cfac_numero )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cfac_fecha Changed */
                                    if ( !( localUtil.ctod( httpContext.cgiGet( "GXH_vCFAC_FECHA"), 3).equals( AV8cfac_fecha ) ) )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cfac_subtotal Changed */
                                    if ( localUtil.ctond( httpContext.cgiGet( "GXH_vCFAC_SUBTOTAL")).compareTo(AV9cfac_subtotal) != 0 )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cfac_impuesto Changed */
                                    if ( localUtil.ctond( httpContext.cgiGet( "GXH_vCFAC_IMPUESTO")).compareTo(AV10cfac_impuesto) != 0 )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cfac_estado Changed */
                                    if ( GXutil.strcmp(httpContext.cgiGet( "GXH_vCFAC_ESTADO"), AV12cfac_estado) != 0 )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Ccli_codigo Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCCLI_CODIGO"), ",", ".") != AV14ccli_codigo )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: e13072 */
                                       e13072 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  httpContext.wbHandled = (byte)(1) ;
               }
            }
         }
      }
   }

   public void we072( )
   {
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         Rfr0gs = true ;
         refresh( ) ;
         if ( ! GxWebStd.gx_redirect( httpContext) )
         {
            if ( nGXWrapped == 1 )
            {
               renderHtmlCloseForm( ) ;
            }
         }
      }
   }

   public void pa072( )
   {
      if ( nDonePA == 0 )
      {
         GX_FocusControl = edtavCfac_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         nDonePA = (byte)(1) ;
      }
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void gxnrgrid1_newrow( short nRC_Grid1 ,
                                 short nGXsfl_51_idx ,
                                 String sGXsfl_51_idx )
   {
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_51_idx ;
      edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_51_idx ;
      edtfac_numero_Internalname = "FAC_NUMERO_"+sGXsfl_51_idx ;
      edtfac_fecha_Internalname = "FAC_FECHA_"+sGXsfl_51_idx ;
      edtfac_subtotal_Internalname = "FAC_SUBTOTAL_"+sGXsfl_51_idx ;
      edtfac_impuesto_Internalname = "FAC_IMPUESTO_"+sGXsfl_51_idx ;
      edtfac_estado_Internalname = "FAC_ESTADO_"+sGXsfl_51_idx ;
      while ( nGXsfl_51_idx <= nRC_Grid1 )
      {
         sendrow_512( ) ;
         nGXsfl_51_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_51_idx+1>subgrid1_recordsperpage( )) ? 1 : nGXsfl_51_idx+1)) ;
         sGXsfl_51_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_51_idx, 4, 0)), (short)(4), "0") ;
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_51_idx ;
         edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_51_idx ;
         edtfac_numero_Internalname = "FAC_NUMERO_"+sGXsfl_51_idx ;
         edtfac_fecha_Internalname = "FAC_FECHA_"+sGXsfl_51_idx ;
         edtfac_subtotal_Internalname = "FAC_SUBTOTAL_"+sGXsfl_51_idx ;
         edtfac_impuesto_Internalname = "FAC_IMPUESTO_"+sGXsfl_51_idx ;
         edtfac_estado_Internalname = "FAC_ESTADO_"+sGXsfl_51_idx ;
      }
      httpContext.GX_webresponse.addString(Grid1Container.ToJavascriptSource());
      /* End function gxnrGrid1_newrow */
   }

   public void gxgrgrid1_refresh( int Grid1_PageSize51 ,
                                  short AV6cfac_codigo ,
                                  short AV7cfac_numero ,
                                  java.util.Date AV8cfac_fecha ,
                                  java.math.BigDecimal AV9cfac_subtotal ,
                                  java.math.BigDecimal AV10cfac_impuesto ,
                                  String AV12cfac_estado ,
                                  short AV14ccli_codigo )
   {
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.disableOutput();
      subGrid1_Rows = (short)(Grid1_PageSize51) ;
      rf072( ) ;
      httpContext.enableOutput();
      httpContext.GX_webresponse.addString(Grid1Container.ToJavascriptSource());
      /* End function gxgrGrid1_refresh */
   }

   public void refresh( )
   {
      rf072( ) ;
      /* End function Refresh */
   }

   public void rf072( )
   {
      Grid1Container.setPageSize( subgrid1_recordsperpage( ) );
      wbStart = (short)(51) ;
      nGXsfl_51_idx = (short)(1) ;
      sGXsfl_51_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_51_idx, 4, 0)), (short)(4), "0") ;
      edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_51_idx ;
      edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_51_idx ;
      edtfac_numero_Internalname = "FAC_NUMERO_"+sGXsfl_51_idx ;
      edtfac_fecha_Internalname = "FAC_FECHA_"+sGXsfl_51_idx ;
      edtfac_subtotal_Internalname = "FAC_SUBTOTAL_"+sGXsfl_51_idx ;
      edtfac_impuesto_Internalname = "FAC_IMPUESTO_"+sGXsfl_51_idx ;
      edtfac_estado_Internalname = "FAC_ESTADO_"+sGXsfl_51_idx ;
      if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_51_idx ;
         edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_51_idx ;
         edtfac_numero_Internalname = "FAC_NUMERO_"+sGXsfl_51_idx ;
         edtfac_fecha_Internalname = "FAC_FECHA_"+sGXsfl_51_idx ;
         edtfac_subtotal_Internalname = "FAC_SUBTOTAL_"+sGXsfl_51_idx ;
         edtfac_impuesto_Internalname = "FAC_IMPUESTO_"+sGXsfl_51_idx ;
         edtfac_estado_Internalname = "FAC_ESTADO_"+sGXsfl_51_idx ;
         /* Using cursor H00073 */
         pr_default.execute(0, new Object[] {new Short(AV6cfac_codigo), new Short(AV7cfac_numero), AV8cfac_fecha, AV9cfac_subtotal, AV12cfac_estado, new Short(AV14ccli_codigo)});
         nGXsfl_51_idx = (short)(1) ;
         GRID1_nEOF = (byte)(0) ;
         while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( 10 == 0 ) || ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subgrid1_recordsperpage( ) ) ) ) )
         {
            A1cli_codigo = H00073_A1cli_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A1cli_codigo", GXutil.ltrim( GXutil.str( A1cli_codigo, 4, 0)));
            A20fac_estado = H00073_A20fac_estado[0] ;
            n20fac_estado = H00073_n20fac_estado[0] ;
            A16fac_fecha = H00073_A16fac_fecha[0] ;
            n16fac_fecha = H00073_n16fac_fecha[0] ;
            A15fac_numero = H00073_A15fac_numero[0] ;
            n15fac_numero = H00073_n15fac_numero[0] ;
            A14fac_codigo = H00073_A14fac_codigo[0] ;
            A17fac_subtotal = H00073_A17fac_subtotal[0] ;
            n17fac_subtotal = H00073_n17fac_subtotal[0] ;
            A17fac_subtotal = H00073_A17fac_subtotal[0] ;
            n17fac_subtotal = H00073_n17fac_subtotal[0] ;
            A18fac_impuesto = A17fac_subtotal.multiply(DecimalUtil.doubleToDec(0.19,4,2)) ;
            if ( A18fac_impuesto.compareTo(AV10cfac_impuesto) >= 0 )
            {
               /* Execute user event: e12072 */
               e12072 ();
            }
            pr_default.readNext(0);
         }
         GRID1_nEOF = (byte)(((pr_default.getStatus(0) == 101) ? 1 : 0)) ;
         pr_default.close(0);
         wbEnd = (short)(51) ;
         wb070( ) ;
      }
   }

   public int subgrid1_pagecount( )
   {
      return -1 ;
   }

   public int subgrid1_recordcount( )
   {
      return -1 ;
   }

   public int subgrid1_recordsperpage( )
   {
      if ( 10 > 0 )
      {
         if ( 1 > 0 )
         {
            return 10*1 ;
         }
         else
         {
            return 10 ;
         }
      }
      return -1 ;
   }

   public int subgrid1_currentpage( )
   {
      return -1 ;
   }

   public short subgrid1_firstpage( )
   {
      GRID1_nFirstRecordOnPage = 0 ;
      return (short)(0) ;
   }

   public short subgrid1_nextpage( )
   {
      if ( GRID1_nEOF == 0 )
      {
         GRID1_nFirstRecordOnPage = (int)(GRID1_nFirstRecordOnPage+subgrid1_recordsperpage( )) ;
      }
      return (short)(0) ;
   }

   public short subgrid1_previouspage( )
   {
      if ( GRID1_nFirstRecordOnPage >= subgrid1_recordsperpage( ) )
      {
         GRID1_nFirstRecordOnPage = (int)(GRID1_nFirstRecordOnPage-subgrid1_recordsperpage( )) ;
      }
      else
      {
         return (short)(2) ;
      }
      return (short)(0) ;
   }

   public short subgrid1_lastpage( )
   {
      subGrid1_Islastpage = 1 ;
      return (short)(3) ;
   }

   public int subgrid1_gotopage( int nPageNo )
   {
      if ( nPageNo > 0 )
      {
         GRID1_nFirstRecordOnPage = (int)(subgrid1_recordsperpage( )*(nPageNo-1)) ;
      }
      else
      {
         GRID1_nFirstRecordOnPage = 0 ;
      }
      return 0 ;
   }

   public void strup070( )
   {
      /* Before Start, stand alone formulas. */
      Gx_err = (short)(0) ;
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      /* Execute user event: e11072 */
      e11072 ();
      httpContext.wbGlbDoneStart = (byte)(1) ;
      /* After Start, stand alone formulas. */
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read saved SDTs. */
         /* Read variables values. */
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCfac_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCfac_codigo_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCFAC_CODIGO");
            GX_FocusControl = edtavCfac_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV6cfac_codigo = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV6cfac_codigo", GXutil.ltrim( GXutil.str( AV6cfac_codigo, 4, 0)));
         }
         else
         {
            AV6cfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtavCfac_codigo_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV6cfac_codigo", GXutil.ltrim( GXutil.str( AV6cfac_codigo, 4, 0)));
         }
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCfac_numero_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCfac_numero_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCFAC_NUMERO");
            GX_FocusControl = edtavCfac_numero_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV7cfac_numero = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV7cfac_numero", GXutil.ltrim( GXutil.str( AV7cfac_numero, 4, 0)));
         }
         else
         {
            AV7cfac_numero = (short)(localUtil.ctol( httpContext.cgiGet( edtavCfac_numero_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV7cfac_numero", GXutil.ltrim( GXutil.str( AV7cfac_numero, 4, 0)));
         }
         if ( localUtil.vcdate( httpContext.cgiGet( edtavCfac_fecha_Internalname), (byte)(3)) == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_faildate", new Object[] {"Fecha de la factura"}), 1, "vCFAC_FECHA");
            GX_FocusControl = edtavCfac_fecha_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV8cfac_fecha = GXutil.nullDate() ;
            httpContext.ajax_rsp_assign_attri("", false, "AV8cfac_fecha", localUtil.format(AV8cfac_fecha, "99/99/99"));
         }
         else
         {
            AV8cfac_fecha = localUtil.ctod( httpContext.cgiGet( edtavCfac_fecha_Internalname), 3) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV8cfac_fecha", localUtil.format(AV8cfac_fecha, "99/99/99"));
         }
         if ( ( ( localUtil.ctond( httpContext.cgiGet( edtavCfac_subtotal_Internalname)).doubleValue() < 0 ) ) || ( ( localUtil.ctond( httpContext.cgiGet( edtavCfac_subtotal_Internalname)).compareTo(DecimalUtil.doubleToDec(9999999.99,10,2)) > 0 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCFAC_SUBTOTAL");
            GX_FocusControl = edtavCfac_subtotal_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV9cfac_subtotal = DecimalUtil.ZERO ;
            httpContext.ajax_rsp_assign_attri("", false, "AV9cfac_subtotal", GXutil.ltrim( GXutil.str( AV9cfac_subtotal, 10, 2)));
         }
         else
         {
            AV9cfac_subtotal = localUtil.ctond( httpContext.cgiGet( edtavCfac_subtotal_Internalname)) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV9cfac_subtotal", GXutil.ltrim( GXutil.str( AV9cfac_subtotal, 10, 2)));
         }
         if ( ( ( localUtil.ctond( httpContext.cgiGet( edtavCfac_impuesto_Internalname)).doubleValue() < 0 ) ) || ( ( localUtil.ctond( httpContext.cgiGet( edtavCfac_impuesto_Internalname)).compareTo(DecimalUtil.doubleToDec(9999999.99,10,2)) > 0 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCFAC_IMPUESTO");
            GX_FocusControl = edtavCfac_impuesto_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV10cfac_impuesto = DecimalUtil.ZERO ;
            httpContext.ajax_rsp_assign_attri("", false, "AV10cfac_impuesto", GXutil.ltrim( GXutil.str( AV10cfac_impuesto, 10, 2)));
         }
         else
         {
            AV10cfac_impuesto = localUtil.ctond( httpContext.cgiGet( edtavCfac_impuesto_Internalname)) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV10cfac_impuesto", GXutil.ltrim( GXutil.str( AV10cfac_impuesto, 10, 2)));
         }
         AV12cfac_estado = httpContext.cgiGet( edtavCfac_estado_Internalname) ;
         httpContext.ajax_rsp_assign_attri("", false, "AV12cfac_estado", AV12cfac_estado);
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCcli_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCcli_codigo_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCCLI_CODIGO");
            GX_FocusControl = edtavCcli_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV14ccli_codigo = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV14ccli_codigo", GXutil.ltrim( GXutil.str( AV14ccli_codigo, 4, 0)));
         }
         else
         {
            AV14ccli_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtavCcli_codigo_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV14ccli_codigo", GXutil.ltrim( GXutil.str( AV14ccli_codigo, 4, 0)));
         }
         /* Read saved values. */
         nRC_Grid1 = (short)(localUtil.ctol( httpContext.cgiGet( "nRC_Grid1"), ",", ".")) ;
         AV13pfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "vPFAC_CODIGO"), ",", ".")) ;
         GRID1_nFirstRecordOnPage = (int)(localUtil.ctol( httpContext.cgiGet( "GRID1_nFirstRecordOnPage"), ",", ".")) ;
         GRID1_nEOF = (byte)(localUtil.ctol( httpContext.cgiGet( "GRID1_nEOF"), ",", ".")) ;
         /* Read subfile selected row values. */
         /* Read hidden variables. */
      }
      else
      {
         dynload_actions( ) ;
      }
   }

   protected void GXStart( )
   {
      /* Execute user event: e11072 */
      e11072 ();
      if (returnInSub) return;
   }

   public void e11072( )
   {
      /* Start Routine */
      Form.setCaption( GXutil.format( "Lista de Selecci�n %1", "factura", "", "", "", "", "", "", "", "") );
      httpContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.getCaption());
   }

   private void e12072( )
   {
      /* Load Routine */
      AV5LinkSelection = context.getHttpContext().getImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", "GeneXusX") ;
      if ( ( subGrid1_Islastpage == 1 ) || ( 10 == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subgrid1_recordsperpage( ) ) ) )
      {
         sendrow_512( ) ;
         GRID1_nEOF = (byte)(1) ;
         if ( ( subGrid1_Islastpage == 1 ) && ( ((int)(GRID1_nCurrentRecord) % (subgrid1_recordsperpage( ))) == 0 ) )
         {
            GRID1_nFirstRecordOnPage = GRID1_nCurrentRecord ;
         }
      }
      if ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage + subgrid1_recordsperpage( ) )
      {
         GRID1_nEOF = (byte)(0) ;
      }
      GRID1_nCurrentRecord = (int)(GRID1_nCurrentRecord+1) ;
   }

   public void GXEnter( )
   {
      /* Execute user event: e13072 */
      e13072 ();
      if (returnInSub) return;
   }

   public void e13072( )
   {
      /* Enter Routine */
      AV13pfac_codigo = A14fac_codigo ;
      httpContext.ajax_rsp_assign_attri("", false, "AV13pfac_codigo", GXutil.ltrim( GXutil.str( AV13pfac_codigo, 4, 0)));
      httpContext.setWebReturnParms(new Object[] {new Short(AV13pfac_codigo)});
      httpContext.nUserReturn = (byte)(1) ;
      returnInSub = true;
      if (true) return;
   }

   public void wb_table1_2_072( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td style=\"text-align:"+httpContext.getCssProperty( "Align", "center")+"\">") ;
         ClassString = "ErrorViewer" ;
         StyleString = "" ;
         GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), StyleString, ClassString, "", "false");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Control Group */
         ClassString = "FieldSet" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroup1_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"Filters"+"</legend>") ;
         wb_table2_9_072( true) ;
      }
      else
      {
         wb_table2_9_072( false) ;
      }
      return  ;
   }

   public void wb_table2_9_072e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Control Group */
         ClassString = "FieldSet" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroup2_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"Lista de Selecci�n"+"</legend>") ;
         wb_table3_48_072( true) ;
      }
      else
      {
         wb_table3_48_072( false) ;
      }
      return  ;
   }

   public void wb_table3_48_072e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_072e( true) ;
      }
      else
      {
         wb_table1_2_072e( false) ;
      }
   }

   public void wb_table3_48_072( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td colspan=\"3\" >") ;
         /*  Grid Control  */
         Grid1Container.SetWrapped(nGXWrapped);
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<div id=\""+"Grid1Container"+"DivS\" gxgridid=\"51\">") ;
            sStyleString = "" ;
            GxWebStd.gx_table_start( httpContext, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 0, sStyleString, "", 0);
            /* Subfile titles */
            httpContext.writeText( "<tr") ;
            httpContext.writeTextNL( ">") ;
            if ( subGrid1_Backcolorstyle == 0 )
            {
               subGrid1_Titlebackstyle = (byte)(0) ;
               if ( GXutil.len( subGrid1_Class) > 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Title" ;
               }
            }
            else
            {
               subGrid1_Titlebackstyle = (byte)(1) ;
               if ( subGrid1_Backcolorstyle == 1 )
               {
                  subGrid1_Titlebackcolor = subGrid1_Allbackcolor ;
                  if ( GXutil.len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"UniformTitle" ;
                  }
               }
               else
               {
                  if ( GXutil.len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title" ;
                  }
               }
            }
            httpContext.writeText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "la factura") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "la factura") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "la factura") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "la factura") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "la factura") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "la factura") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeTextNL( "</tr>") ;
            Grid1Container.AddObjectProperty("GridName", "Grid1");
         }
         else
         {
            if ( httpContext.isAjaxCallMode( ) )
            {
               Grid1Container = new com.genexus.webpanels.GXWebGrid(context);
            }
            else
            {
               Grid1Container.Clear();
            }
            Grid1Container.SetWrapped(nGXWrapped);
            Grid1Container.AddObjectProperty("GridName", "Grid1");
            Grid1Container.AddObjectProperty("Class", "Grid");
            Grid1Container.AddObjectProperty("Cellpadding", GXutil.ltrim( localUtil.ntoc( 1, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Cellspacing", GXutil.ltrim( localUtil.ntoc( 0, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Backcolorstyle", GXutil.ltrim( localUtil.ntoc( subGrid1_Backcolorstyle, (byte)(1), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("CmpContext", "");
            Grid1Container.AddObjectProperty("InMasterPage", "false");
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", httpContext.convertURL( AV5LinkSelection));
            Grid1Column.AddObjectProperty("Link", GXutil.rtrim( edtavLinkselection_Link));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A15fac_numero, (byte)(4), (byte)(0), ".", "")));
            Grid1Column.AddObjectProperty("Link", GXutil.rtrim( edtfac_numero_Link));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", localUtil.format(A16fac_fecha, "99/99/99"));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A17fac_subtotal, (byte)(10), (byte)(2), ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A18fac_impuesto, (byte)(10), (byte)(2), ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.rtrim( A20fac_estado));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Container.AddObjectProperty("Allowselection", "false");
            Grid1Container.AddObjectProperty("Allowcollapsing", ((subGrid1_Allowcollapsing==1) ? "true" : "false"));
            Grid1Container.AddObjectProperty("Collapsed", GXutil.ltrim( localUtil.ntoc( subGrid1_Collapsed, (byte)(9), (byte)(0), ".", "")));
         }
      }
      if ( wbEnd == 51 )
      {
         wbEnd = (short)(0) ;
         nRC_Grid1 = (short)(nGXsfl_51_idx-1) ;
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "</table>") ;
            httpContext.writeText( "</div>") ;
         }
         else
         {
            Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
            Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
            sStyleString = " style=\"display:none;\"" ;
            sStyleString = "" ;
            httpContext.writeText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
            httpContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
            GxWebStd.gx_hidden_field( httpContext, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
            if ( httpContext.isAjaxRequest( ) )
            {
               GxWebStd.gx_hidden_field( httpContext, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
            }
            else
            {
               httpContext.writeText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'>") ;
            }
         }
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td style=\"text-align:"+httpContext.getCssProperty( "Align", "right")+"\">") ;
         wb_table4_61_072( true) ;
      }
      else
      {
         wb_table4_61_072( false) ;
      }
      return  ;
   }

   public void wb_table4_61_072e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_48_072e( true) ;
      }
      else
      {
         wb_table3_48_072e( false) ;
      }
   }

   public void wb_table4_61_072( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"" ;
         ClassString = "BtnCancel" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+GXutil.str( 51, 3, 0)+","+"null"+");", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "rounded", "ECANCEL.", TempTags, "", httpContext.getButtonType( ), "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table4_61_072e( true) ;
      }
      else
      {
         wb_table4_61_072e( false) ;
      }
   }

   public void wb_table2_9_072( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_codigo_Internalname, "C�digo de la factura", "", "", lblTextblockfac_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV6cfac_codigo", GXutil.ltrim( GXutil.str( AV6cfac_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'" + sGXsfl_51_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCfac_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( AV6cfac_codigo, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV6cfac_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV6cfac_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(14);\"", "", "", "", "", edtavCfac_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_numero_Internalname, "N�mero de la factura", "", "", lblTextblockfac_numero_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV7cfac_numero", GXutil.ltrim( GXutil.str( AV7cfac_numero, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_51_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCfac_numero_Internalname, GXutil.ltrim( localUtil.ntoc( AV7cfac_numero, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV7cfac_numero), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV7cfac_numero), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(19);\"", "", "", "", "", edtavCfac_numero_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_fecha_Internalname, "Fecha de la factura", "", "", lblTextblockfac_fecha_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV8cfac_fecha", localUtil.format(AV8cfac_fecha, "99/99/99"));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_51_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         httpContext.writeText( "<div id=\""+edtavCfac_fecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCfac_fecha_Internalname, localUtil.format(AV8cfac_fecha, "99/99/99"), localUtil.format( AV8cfac_fecha, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(24);\"", "", "", "", "", edtavCfac_fecha_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 8, "chr", 1, "row", 8, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0040.htm");
         GxWebStd.gx_bitmap( httpContext, edtavCfac_fecha_Internalname+"_dp_trigger", "calendar-img.gif", "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;vertical-align:text-bottom", "", "", "", "", "", "HLP_Gx0040.htm");
         httpContext.writeTextNL( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_subtotal_Internalname, "Subtotal de la factura", "", "", lblTextblockfac_subtotal_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV9cfac_subtotal", GXutil.ltrim( GXutil.str( AV9cfac_subtotal, 10, 2)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_51_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCfac_subtotal_Internalname, GXutil.ltrim( localUtil.ntoc( AV9cfac_subtotal, (byte)(10), (byte)(2), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( AV9cfac_subtotal, "ZZZZZZ9.99")) : localUtil.format( AV9cfac_subtotal, "ZZZZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(29);\"", "", "", "", "", edtavCfac_subtotal_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_impuesto_Internalname, "Impuesto de la factura", "", "", lblTextblockfac_impuesto_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV10cfac_impuesto", GXutil.ltrim( GXutil.str( AV10cfac_impuesto, 10, 2)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_51_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCfac_impuesto_Internalname, GXutil.ltrim( localUtil.ntoc( AV10cfac_impuesto, (byte)(10), (byte)(2), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( AV10cfac_impuesto, "ZZZZZZ9.99")) : localUtil.format( AV10cfac_impuesto, "ZZZZZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(34);\"", "", "", "", "", edtavCfac_impuesto_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_estado_Internalname, "Estado de la factura", "", "", lblTextblockfac_estado_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV12cfac_estado", AV12cfac_estado);
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_51_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCfac_estado_Internalname, GXutil.rtrim( AV12cfac_estado), GXutil.rtrim( localUtil.format( AV12cfac_estado, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(39);\"", "", "", "", "", edtavCfac_estado_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockcli_codigo_Internalname, "C�digo del cliente", "", "", lblTextblockcli_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV14ccli_codigo", GXutil.ltrim( GXutil.str( AV14ccli_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_51_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCcli_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( AV14ccli_codigo, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV14ccli_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV14ccli_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(44);\"", "", "", "", "", edtavCcli_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0040.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_9_072e( true) ;
      }
      else
      {
         wb_table2_9_072e( false) ;
      }
   }

   public void setparameters( Object[] obj )
   {
      AV13pfac_codigo = ((Number) GXutil.testNumericType( getParm(obj,0), TypeConstants.SHORT)).shortValue() ;
      httpContext.ajax_rsp_assign_attri("", false, "AV13pfac_codigo", GXutil.ltrim( GXutil.str( AV13pfac_codigo, 4, 0)));
   }

   public String getresponse( String sGXDynURL )
   {
      initialize_properties( ) ;
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      sDynURL = sGXDynURL ;
      nGotPars = 1 ;
      nGXWrapped = 1 ;
      httpContext.setWrapped(true);
      pa072( ) ;
      ws072( ) ;
      we072( ) ;
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
      httpContext.setWrapped(false);
      httpContext.GX_msglist = BackMsgLst ;
      return ((java.io.ByteArrayOutputStream) httpContext.getOutputStream()).toString();
   }

   public void responsestatic( String sGXDynURL )
   {
   }

   public void define_styles( )
   {
      httpContext.AddStyleSheetFile("calendar-system.css", "?95080");
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?9421295");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?58720");
      httpContext.AddJavascriptSource("gx0040.js", "?9421295");
      /* End function include_jscripts */
   }

   public void sendrow_512( )
   {
      wb070( ) ;
      if ( ( 10 * 1 == 0 ) || ( nGXsfl_51_idx <= subgrid1_recordsperpage( ) * 1 ) )
      {
         Grid1Row = GXWebRow.GetNew(context,Grid1Container) ;
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = (byte)(0) ;
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd" ;
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = (byte)(0) ;
            subGrid1_Backcolor = subGrid1_Allbackcolor ;
            httpContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGrid1_Backcolor, 9, 0)));
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform" ;
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = (byte)(1) ;
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd" ;
            }
            subGrid1_Backcolor = (int)(0xF0F0F0) ;
            httpContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGrid1_Backcolor, 9, 0)));
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = (byte)(1) ;
            if ( ((int)(nGXsfl_51_idx) % (2)) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0) ;
               httpContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGrid1_Backcolor, 9, 0)));
               if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even" ;
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0xF0F0F0) ;
               httpContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGrid1_Backcolor, 9, 0)));
               if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd" ;
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<tr ") ;
            httpContext.writeText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            httpContext.writeText( " gxrow=\""+sGXsfl_51_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Static Bitmap Variable */
         edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+PrivateUtilities.encodeJSConstant( GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ",", "")))+"'"+"]);" ;
         ClassString = "Image" ;
         StyleString = "" ;
         Grid1Row.AddColumnProperties("bitmap", 1, httpContext.isAjaxCallMode( ), new Object[] {edtavLinkselection_Internalname,AV5LinkSelection,edtavLinkselection_Link,"","","GeneXusX",new Integer(-1),new Integer(1),"","Seleccionar",new Integer(0),new Integer(-1),new Integer(0),"px",new Integer(0),"px",new Integer(0),new Integer(0),new Integer(0),"","",StyleString,ClassString,"","","''",""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtfac_codigo_Internalname,GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ",", "")),localUtil.format( DecimalUtil.doubleToDec(A14fac_codigo), "ZZZ9"),"","","","","",edtfac_codigo_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(51),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         edtfac_numero_Link = "javascript:gx.popup.gxReturn(["+"'"+PrivateUtilities.encodeJSConstant( GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ",", "")))+"'"+"]);" ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtfac_numero_Internalname,GXutil.ltrim( localUtil.ntoc( A15fac_numero, (byte)(4), (byte)(0), ",", "")),localUtil.format( DecimalUtil.doubleToDec(A15fac_numero), "ZZZ9"),"","",edtfac_numero_Link,"","Seleccionar",edtfac_numero_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(51),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtfac_fecha_Internalname,localUtil.format(A16fac_fecha, "99/99/99"),localUtil.format( A16fac_fecha, "99/99/99"),"","","","","",edtfac_fecha_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(8),new Integer(0),new Integer(0),new Integer(51),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtfac_subtotal_Internalname,GXutil.ltrim( localUtil.ntoc( A17fac_subtotal, (byte)(10), (byte)(2), ",", "")),localUtil.format( A17fac_subtotal, "ZZZZZZ9.99"),"","","","","",edtfac_subtotal_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(10),new Integer(0),new Integer(0),new Integer(51),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtfac_impuesto_Internalname,GXutil.ltrim( localUtil.ntoc( A18fac_impuesto, (byte)(10), (byte)(2), ",", "")),localUtil.format( A18fac_impuesto, "ZZZZZZ9.99"),"","","","","",edtfac_impuesto_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(10),new Integer(0),new Integer(0),new Integer(51),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtfac_estado_Internalname,GXutil.rtrim( A20fac_estado),"","","","","","",edtfac_estado_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(10),new Integer(0),new Integer(0),new Integer(51),new Integer(1),new Integer(1),new Boolean(true),"left"});
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_51_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_51_idx+1>subgrid1_recordsperpage( )) ? 1 : nGXsfl_51_idx+1)) ;
         sGXsfl_51_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_51_idx, 4, 0)), (short)(4), "0") ;
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_51_idx ;
         edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_51_idx ;
         edtfac_numero_Internalname = "FAC_NUMERO_"+sGXsfl_51_idx ;
         edtfac_fecha_Internalname = "FAC_FECHA_"+sGXsfl_51_idx ;
         edtfac_subtotal_Internalname = "FAC_SUBTOTAL_"+sGXsfl_51_idx ;
         edtfac_impuesto_Internalname = "FAC_IMPUESTO_"+sGXsfl_51_idx ;
         edtfac_estado_Internalname = "FAC_ESTADO_"+sGXsfl_51_idx ;
      }
      /* End function sendrow_512 */
   }

   public void init_default_properties( )
   {
      lblTextblockfac_codigo_Internalname = "TEXTBLOCKFAC_CODIGO" ;
      edtavCfac_codigo_Internalname = "vCFAC_CODIGO" ;
      lblTextblockfac_numero_Internalname = "TEXTBLOCKFAC_NUMERO" ;
      edtavCfac_numero_Internalname = "vCFAC_NUMERO" ;
      lblTextblockfac_fecha_Internalname = "TEXTBLOCKFAC_FECHA" ;
      edtavCfac_fecha_Internalname = "vCFAC_FECHA" ;
      lblTextblockfac_subtotal_Internalname = "TEXTBLOCKFAC_SUBTOTAL" ;
      edtavCfac_subtotal_Internalname = "vCFAC_SUBTOTAL" ;
      lblTextblockfac_impuesto_Internalname = "TEXTBLOCKFAC_IMPUESTO" ;
      edtavCfac_impuesto_Internalname = "vCFAC_IMPUESTO" ;
      lblTextblockfac_estado_Internalname = "TEXTBLOCKFAC_ESTADO" ;
      edtavCfac_estado_Internalname = "vCFAC_ESTADO" ;
      lblTextblockcli_codigo_Internalname = "TEXTBLOCKCLI_CODIGO" ;
      edtavCcli_codigo_Internalname = "vCCLI_CODIGO" ;
      tblTable2_Internalname = "TABLE2" ;
      grpGroup1_Internalname = "GROUP1" ;
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      tblTable4_Internalname = "TABLE4" ;
      tblTable3_Internalname = "TABLE3" ;
      grpGroup2_Internalname = "GROUP2" ;
      tblTable1_Internalname = "TABLE1" ;
      Form.setInternalname( "FORM" );
      subGrid1_Internalname = "GRID1" ;
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      edtfac_estado_Jsonclick = "" ;
      edtfac_impuesto_Jsonclick = "" ;
      edtfac_subtotal_Jsonclick = "" ;
      edtfac_fecha_Jsonclick = "" ;
      edtfac_numero_Jsonclick = "" ;
      edtfac_codigo_Jsonclick = "" ;
      edtavCcli_codigo_Jsonclick = "" ;
      edtavCfac_estado_Jsonclick = "" ;
      edtavCfac_impuesto_Jsonclick = "" ;
      edtavCfac_subtotal_Jsonclick = "" ;
      edtavCfac_fecha_Jsonclick = "" ;
      edtavCfac_numero_Jsonclick = "" ;
      edtavCfac_codigo_Jsonclick = "" ;
      subGrid1_Allowcollapsing = (byte)(0) ;
      edtfac_numero_Link = "" ;
      edtavLinkselection_Link = "" ;
      subGrid1_Class = "Grid" ;
      subGrid1_Backcolorstyle = (byte)(2) ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "Selection List factura" );
      httpContext.GX_msglist.setDisplaymode( (short)(1) );
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      AV8cfac_fecha = GXutil.nullDate() ;
      AV9cfac_subtotal = DecimalUtil.ZERO ;
      AV10cfac_impuesto = DecimalUtil.ZERO ;
      AV12cfac_estado = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      sDynURL = "" ;
      FormProcess = "" ;
      GX_FocusControl = "" ;
      sPrefix = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      edtavLinkselection_Internalname = "" ;
      edtfac_codigo_Internalname = "" ;
      edtfac_numero_Internalname = "" ;
      edtfac_fecha_Internalname = "" ;
      edtfac_subtotal_Internalname = "" ;
      edtfac_impuesto_Internalname = "" ;
      edtfac_estado_Internalname = "" ;
      AV5LinkSelection = "" ;
      A16fac_fecha = GXutil.nullDate() ;
      A17fac_subtotal = DecimalUtil.ZERO ;
      A18fac_impuesto = DecimalUtil.ZERO ;
      A20fac_estado = "" ;
      Grid1Container = new com.genexus.webpanels.GXWebGrid(context);
      scmdbuf = "" ;
      H00073_A1cli_codigo = new short[1] ;
      H00073_A20fac_estado = new String[] {""} ;
      H00073_n20fac_estado = new boolean[] {false} ;
      H00073_A16fac_fecha = new java.util.Date[] {GXutil.nullDate()} ;
      H00073_n16fac_fecha = new boolean[] {false} ;
      H00073_A15fac_numero = new short[1] ;
      H00073_n15fac_numero = new boolean[] {false} ;
      H00073_A14fac_codigo = new short[1] ;
      H00073_A17fac_subtotal = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      H00073_n17fac_subtotal = new boolean[] {false} ;
      sStyleString = "" ;
      ClassString = "" ;
      StyleString = "" ;
      subGrid1_Linesclass = "" ;
      GXt_char3 = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      GXt_char4 = "" ;
      GXt_char5 = "" ;
      Grid1Column = new com.genexus.webpanels.GXWebColumn();
      TempTags = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      lblTextblockfac_codigo_Jsonclick = "" ;
      lblTextblockfac_numero_Jsonclick = "" ;
      lblTextblockfac_fecha_Jsonclick = "" ;
      GXt_char6 = "" ;
      lblTextblockfac_subtotal_Jsonclick = "" ;
      lblTextblockfac_impuesto_Jsonclick = "" ;
      lblTextblockfac_estado_Jsonclick = "" ;
      lblTextblockcli_codigo_Jsonclick = "" ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      Grid1Row = new com.genexus.webpanels.GXWebRow();
      GXt_char7 = "" ;
      ROClassString = "" ;
      GXt_char8 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new gx0040__default(),
         new Object[] {
             new Object[] {
            H00073_A1cli_codigo, H00073_A20fac_estado, H00073_n20fac_estado, H00073_A16fac_fecha, H00073_n16fac_fecha, H00073_A15fac_numero, H00073_n15fac_numero, H00073_A14fac_codigo, H00073_A17fac_subtotal, H00073_n17fac_subtotal
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte nGotPars ;
   private byte GxWebError ;
   private byte gxajaxcallmode ;
   private byte GRID1_nEOF ;
   private byte nDonePA ;
   private byte subGrid1_Backcolorstyle ;
   private byte subGrid1_Titlebackstyle ;
   private byte subGrid1_Allowcollapsing ;
   private byte subGrid1_Collapsed ;
   private byte nGXWrapped ;
   private byte subGrid1_Backstyle ;
   private short nRC_Grid1 ;
   private short nGXsfl_51_idx=1 ;
   private short Gx_err ;
   private short AV6cfac_codigo ;
   private short AV7cfac_numero ;
   private short AV14ccli_codigo ;
   private short AV13pfac_codigo ;
   private short wbEnd ;
   private short wbStart ;
   private short A14fac_codigo ;
   private short A15fac_numero ;
   private short subGrid1_Rows ;
   private short A1cli_codigo ;
   private int Grid1_PageSize51 ;
   private int GRID1_nFirstRecordOnPage ;
   private int subGrid1_Islastpage ;
   private int GRID1_nCurrentRecord ;
   private int subGrid1_Titlebackcolor ;
   private int subGrid1_Allbackcolor ;
   private int idxLst ;
   private int subGrid1_Backcolor ;
   private java.math.BigDecimal AV9cfac_subtotal ;
   private java.math.BigDecimal AV10cfac_impuesto ;
   private java.math.BigDecimal A17fac_subtotal ;
   private java.math.BigDecimal A18fac_impuesto ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String sGXsfl_51_idx="0001" ;
   private String sDynURL ;
   private String FormProcess ;
   private String GX_FocusControl ;
   private String sPrefix ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String edtavLinkselection_Internalname ;
   private String edtfac_codigo_Internalname ;
   private String edtfac_numero_Internalname ;
   private String edtfac_fecha_Internalname ;
   private String edtfac_subtotal_Internalname ;
   private String edtfac_impuesto_Internalname ;
   private String edtfac_estado_Internalname ;
   private String edtavCfac_codigo_Internalname ;
   private String scmdbuf ;
   private String edtavCfac_numero_Internalname ;
   private String edtavCfac_fecha_Internalname ;
   private String edtavCfac_subtotal_Internalname ;
   private String edtavCfac_impuesto_Internalname ;
   private String edtavCfac_estado_Internalname ;
   private String edtavCcli_codigo_Internalname ;
   private String sStyleString ;
   private String tblTable1_Internalname ;
   private String ClassString ;
   private String StyleString ;
   private String grpGroup1_Internalname ;
   private String grpGroup2_Internalname ;
   private String tblTable3_Internalname ;
   private String subGrid1_Internalname ;
   private String subGrid1_Class ;
   private String subGrid1_Linesclass ;
   private String GXt_char3 ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private String GXt_char4 ;
   private String GXt_char5 ;
   private String edtavLinkselection_Link ;
   private String edtfac_numero_Link ;
   private String tblTable4_Internalname ;
   private String TempTags ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String tblTable2_Internalname ;
   private String lblTextblockfac_codigo_Internalname ;
   private String lblTextblockfac_codigo_Jsonclick ;
   private String edtavCfac_codigo_Jsonclick ;
   private String lblTextblockfac_numero_Internalname ;
   private String lblTextblockfac_numero_Jsonclick ;
   private String edtavCfac_numero_Jsonclick ;
   private String lblTextblockfac_fecha_Internalname ;
   private String lblTextblockfac_fecha_Jsonclick ;
   private String edtavCfac_fecha_Jsonclick ;
   private String GXt_char6 ;
   private String lblTextblockfac_subtotal_Internalname ;
   private String lblTextblockfac_subtotal_Jsonclick ;
   private String edtavCfac_subtotal_Jsonclick ;
   private String lblTextblockfac_impuesto_Internalname ;
   private String lblTextblockfac_impuesto_Jsonclick ;
   private String edtavCfac_impuesto_Jsonclick ;
   private String lblTextblockfac_estado_Internalname ;
   private String lblTextblockfac_estado_Jsonclick ;
   private String edtavCfac_estado_Jsonclick ;
   private String lblTextblockcli_codigo_Internalname ;
   private String lblTextblockcli_codigo_Jsonclick ;
   private String edtavCcli_codigo_Jsonclick ;
   private String GXt_char7 ;
   private String ROClassString ;
   private String edtfac_codigo_Jsonclick ;
   private String edtfac_numero_Jsonclick ;
   private String edtfac_fecha_Jsonclick ;
   private String edtfac_subtotal_Jsonclick ;
   private String edtfac_impuesto_Jsonclick ;
   private String edtfac_estado_Jsonclick ;
   private String GXt_char8 ;
   private java.util.Date AV8cfac_fecha ;
   private java.util.Date A16fac_fecha ;
   private boolean entryPointCalled ;
   private boolean wbLoad ;
   private boolean Rfr0gs ;
   private boolean wbErr ;
   private boolean n15fac_numero ;
   private boolean n16fac_fecha ;
   private boolean n17fac_subtotal ;
   private boolean n20fac_estado ;
   private boolean returnInSub ;
   private String AV12cfac_estado ;
   private String A20fac_estado ;
   private String AV5LinkSelection ;
   private com.genexus.webpanels.GXWebGrid Grid1Container ;
   private com.genexus.webpanels.GXWebRow Grid1Row ;
   private com.genexus.webpanels.GXWebColumn Grid1Column ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private com.genexus.webpanels.GXMasterPage MasterPageObj ;
   private IDataStoreProvider pr_default ;
   private short[] H00073_A1cli_codigo ;
   private String[] H00073_A20fac_estado ;
   private boolean[] H00073_n20fac_estado ;
   private java.util.Date[] H00073_A16fac_fecha ;
   private boolean[] H00073_n16fac_fecha ;
   private short[] H00073_A15fac_numero ;
   private boolean[] H00073_n15fac_numero ;
   private short[] H00073_A14fac_codigo ;
   private java.math.BigDecimal[] H00073_A17fac_subtotal ;
   private boolean[] H00073_n17fac_subtotal ;
   private com.genexus.webpanels.GXWebForm Form ;
}

final  class gx0040__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("H00073", "SELECT T1.[cli_codigo], T1.[fac_estado], T1.[fac_fecha], T1.[fac_numero], T1.[fac_codigo], COALESCE( T2.[fac_subtotal], 0) AS fac_subtotal FROM ([factura] T1 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T3.[detfac_cantidad] * CAST(COALESCE( T4.[pro_precio], 0) AS decimal( 15, 5))) AS fac_subtotal, T3.[fac_codigo] FROM ([facturaLevel1] T3 WITH (NOLOCK) INNER JOIN [producto] T4 WITH (NOLOCK) ON T4.[pro_codigo] = T3.[pro_codigo]) GROUP BY T3.[fac_codigo] ) T2 ON T2.[fac_codigo] = T1.[fac_codigo]) WHERE (T1.[fac_codigo] >= ?) AND (T1.[fac_numero] >= ?) AND (T1.[fac_fecha] >= ?) AND (COALESCE( T2.[fac_subtotal], 0) >= ?) AND (T1.[fac_estado] >= ?) AND (T1.[cli_codigo] >= ?) ORDER BY T1.[fac_codigo]  OPTION (FAST 11)",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,11,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((short[]) buf[5])[0] = rslt.getShort(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(5) ;
               ((java.math.BigDecimal[]) buf[8])[0] = rslt.getBigDecimal(6,2) ;
               ((boolean[]) buf[9])[0] = rslt.wasNull();
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setBigDecimal(4, (java.math.BigDecimal)parms[3], 2);
               stmt.setVarchar(5, (String)parms[4], 10);
               stmt.setShort(6, ((Number) parms[5]).shortValue());
               break;
      }
   }

}

