/*
               File: login_impl
        Description: login
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 10:57:19.87
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class login_impl extends GXWebPanel
{
   public login_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public login_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( login_impl.class ));
   }

   public login_impl( int remoteHandle ,
                      ModelContext context )
   {
      super( remoteHandle , context);
   }

   public void executeCmdLine( String args[] )
   {
      nGotPars = 1 ;
      webExecute();
   }

   protected void createObjects( )
   {
   }

   public void initweb( )
   {
      initialize_properties( ) ;
      if ( nGotPars == 0 )
      {
         entryPointCalled = false ;
         gxfirstwebparm = httpContext.GetNextPar( ) ;
         gxfirstwebparm_bkp = gxfirstwebparm ;
         gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm, "High") ;
         if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
         {
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            dyncall( httpContext.GetNextPar( )) ;
            return  ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            httpContext.setAjaxEventMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = httpContext.GetNextPar( ) ;
         }
         else
         {
            if ( ! httpContext.IsValidAjaxCall( false) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp ;
         }
      }
      httpContext.setTheme("GeneXusX");
   }

   public void webExecute( )
   {
      initweb( ) ;
      if ( ! httpContext.isAjaxCallMode( ) )
      {
         pa0F2( ) ;
         if ( ! httpContext.isAjaxCallMode( ) )
         {
         }
         if ( ( GxWebError == 0 ) && ! httpContext.isAjaxCallMode( ) )
         {
            ws0F2( ) ;
            if ( ! httpContext.isAjaxCallMode( ) )
            {
               we0F2( ) ;
            }
         }
         if ( httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "max-age=0");
            }
            if ( (GXutil.strcmp("", httpContext.wjLoc)==0) )
            {
               httpContext.GX_webresponse.addString(httpContext.getJSONResponse( ));
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      cleanup();
   }

   public void renderHtmlHeaders( )
   {
      GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), "IE=EmulateIE7");
   }

   public void renderHtmlOpenForm( )
   {
      httpContext.writeText( "<title>") ;
      httpContext.writeText( "login") ;
      httpContext.writeTextNL( "</title>") ;
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      if ( ! httpContext.isSmartDevice( ) )
      {
         httpContext.AddJavascriptSource("gxgral.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      else
      {
         httpContext.AddJavascriptSource("gxapiSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfxSD.js", "?58720");
         httpContext.AddJavascriptSource("gxtypesSD.js", "?58720");
         httpContext.AddJavascriptSource("gxpopupSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfrmutlSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgridSD.js", "?58720");
         httpContext.AddJavascriptSource("JavaScripTableSD.js", "?58720");
         httpContext.AddJavascriptSource("rijndaelSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgralSD.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      httpContext.closeHtmlHeader();
      FormProcess = " onkeyup=\"gx.evt.onkeyup(event)\" onkeypress=\"gx.evt.onkeypress(event,true,false)\" onkeydown=\"gx.evt.onkeypress(null,true,false)\"" ;
      httpContext.writeText( "<body") ;
      httpContext.writeText( " "+"class=\"Form\""+" "+" style=\"-moz-opacity:0;opacity:0;") ;
      httpContext.writeText( "\""+FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" onsubmit=\"try{return gx.csv.validForm()}catch(e){return true;}\" name=\"MAINFORM\" method=\"post\" action=\""+formatLink("login") +"\" class=\""+"Form"+"\">") ;
      GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
   }

   public void renderHtmlCloseForm0F2( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      httpContext.writeTextNL( "</form>") ;
      include_jscripts( ) ;
      httpContext.writeTextNL( "</body>") ;
      httpContext.writeTextNL( "</html>") ;
   }

   public void wb0F0( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! wbLoad )
      {
         renderHtmlHeaders( ) ;
         renderHtmlOpenForm( ) ;
         GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), "", "", "", "false");
         wb_table1_2_0F2( true) ;
      }
      else
      {
         wb_table1_2_0F2( false) ;
      }
      return  ;
   }

   public void wb_table1_2_0F2e( boolean wbgen )
   {
      if ( wbgen )
      {
      }
      wbLoad = true ;
   }

   public void start0F2( )
   {
      wbLoad = false ;
      wbEnd = 0 ;
      wbStart = 0 ;
      Form.getMeta().addItem("Generator", "GeneXus Java", (short)(0)) ;
      Form.getMeta().addItem("Version", "10_1_8-58720", (short)(0)) ;
      Form.getMeta().addItem("Description", "login", (short)(0)) ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      wbErr = false ;
      strup0F0( ) ;
   }

   public void ws0F2( )
   {
      start0F2( ) ;
      evt0F2( ) ;
   }

   public void evt0F2( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) && ! wbErr )
         {
            /* Read Web Panel buttons. */
            sEvt = httpContext.cgiGet( "_EventName") ;
            EvtGridId = httpContext.cgiGet( "_EventGridId") ;
            EvtRowId = httpContext.cgiGet( "_EventRowId") ;
            if ( GXutil.len( sEvt) > 0 )
            {
               sEvtType = GXutil.left( sEvt, 1) ;
               sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "RFR") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "START") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e110F2 */
                        e110F2 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( ! wbErr )
                        {
                           Rfr0gs = false ;
                           if ( ! Rfr0gs )
                           {
                              /* Execute user event: e120F2 */
                              e120F2 ();
                           }
                           dynload_actions( ) ;
                        }
                     }
                     else if ( GXutil.strcmp(sEvt, "LOAD") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e130F2 */
                        e130F2 ();
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                     else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                     }
                  }
                  else
                  {
                  }
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void we0F2( )
   {
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         Rfr0gs = true ;
         refresh( ) ;
         if ( ! GxWebStd.gx_redirect( httpContext) )
         {
            renderHtmlCloseForm0F2( ) ;
         }
      }
   }

   public void pa0F2( )
   {
      if ( nDonePA == 0 )
      {
         GX_FocusControl = edtavUsuario_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         nDonePA = (byte)(1) ;
      }
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void refresh( )
   {
      rf0F2( ) ;
      /* End function Refresh */
   }

   public void rf0F2( )
   {
      if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) )
      {
         /* Execute user event: e130F2 */
         e130F2 ();
         wb0F0( ) ;
      }
   }

   public void strup0F0( )
   {
      /* Before Start, stand alone formulas. */
      Gx_err = (short)(0) ;
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      /* Execute user event: e110F2 */
      e110F2 ();
      httpContext.wbGlbDoneStart = (byte)(1) ;
      /* After Start, stand alone formulas. */
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read saved SDTs. */
         /* Read variables values. */
         AV5usuario = httpContext.cgiGet( edtavUsuario_Internalname) ;
         httpContext.ajax_rsp_assign_attri("", false, "AV5usuario", AV5usuario);
         AV6clave = httpContext.cgiGet( edtavClave_Internalname) ;
         httpContext.ajax_rsp_assign_attri("", false, "AV6clave", AV6clave);
         /* Read saved values. */
         /* Read subfile selected row values. */
         /* Read hidden variables. */
      }
      else
      {
         dynload_actions( ) ;
      }
   }

   protected void GXStart( )
   {
      /* Execute user event: e110F2 */
      e110F2 ();
      if (returnInSub) return;
   }

   public void e110F2( )
   {
      /* Start Routine */
      AV7session.destroy();
   }

   public void GXEnter( )
   {
      /* Execute user event: e120F2 */
      e120F2 ();
      if (returnInSub) return;
   }

   public void e120F2( )
   {
      /* Enter Routine */
      AV10GXLvl6 = (byte)(0) ;
      /* Using cursor H000F2 */
      pr_default.execute(0, new Object[] {AV5usuario, AV6clave});
      while ( (pr_default.getStatus(0) != 101) )
      {
         A38usu_clave = H000F2_A38usu_clave[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A38usu_clave", A38usu_clave);
         A37usu_usuario = H000F2_A37usu_usuario[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A37usu_usuario", A37usu_usuario);
         A25rol_codigo = H000F2_A25rol_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         AV10GXLvl6 = (byte)(1) ;
         AV7session.setValue("usuario", AV5usuario);
         httpContext.ajax_rsp_assign_attri("", false, "AV5usuario", AV5usuario);
         if ( A25rol_codigo == 1 )
         {
            AV7session.setValue("rol", "admin");
            httpContext.wjLoc = formatLink("wp_administrador")  ;
         }
         else
         {
            AV7session.setValue("rol", "usuario");
            httpContext.wjLoc = formatLink("wp_usuario")  ;
         }
         GXv_char4[0] = AV5usuario ;
         GXv_dtime5[0] = GXutil.now( ) ;
         GXv_char6[0] = "Ingreso con credenciales v�lidas" ;
         new guarda_bitacora(remoteHandle, context).execute( GXv_char4, GXv_dtime5, GXv_char6) ;
         login_impl.this.AV5usuario = GXv_char4[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "AV5usuario", AV5usuario);
         pr_default.readNext(0);
      }
      pr_default.close(0);
      if ( AV10GXLvl6 == 0 )
      {
         AV7session.setValue("usuario", AV5usuario);
         httpContext.ajax_rsp_assign_attri("", false, "AV5usuario", AV5usuario);
         lblMensaje_Caption = "Por favor ingrese credenciales correctas" ;
         httpContext.ajax_rsp_assign_prop("", false, lblMensaje_Internalname, "Caption", lblMensaje_Caption);
         GXv_char6[0] = AV5usuario ;
         GXv_dtime5[0] = GXutil.now( ) ;
         GXv_char4[0] = "Intento de ingreso con credenciales no v�lidas" ;
         new guarda_bitacora(remoteHandle, context).execute( GXv_char6, GXv_dtime5, GXv_char4) ;
         login_impl.this.AV5usuario = GXv_char6[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "AV5usuario", AV5usuario);
      }
   }

   protected void nextLoad( )
   {
   }

   protected void e130F2( )
   {
      /* Load Routine */
   }

   public void wb_table1_2_0F2( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         sStyleString = sStyleString + " height: " + GXutil.ltrim( GXutil.str( 100, 10, 0)) + "%" + ";" ;
         sStyleString = sStyleString + " width: " + GXutil.ltrim( GXutil.str( 100, 10, 0)) + "%" + ";" ;
         GxWebStd.gx_table_start( httpContext, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr align=\"center\" >") ;
         httpContext.writeText( "<td valign=\"center\"  style=\"text-align:"+httpContext.getCssProperty( "Align", "center")+";width:100%\">") ;
         wb_table2_5_0F2( true) ;
      }
      else
      {
         wb_table2_5_0F2( false) ;
      }
      return  ;
   }

   public void wb_table2_5_0F2e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_0F2e( true) ;
      }
      else
      {
         wb_table1_2_0F2e( false) ;
      }
   }

   public void wb_table2_5_0F2( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         sStyleString = sStyleString + " height: " + GXutil.ltrim( GXutil.str( 250, 10, 0)) + "px" + ";" ;
         sStyleString = sStyleString + " width: " + GXutil.ltrim( GXutil.str( 454, 10, 0)) + "px" + ";" ;
         GxWebStd.gx_table_start( httpContext, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "center", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr align=\"center\" >") ;
         httpContext.writeText( "<td valign=\"top\"  style=\"text-align:"+httpContext.getCssProperty( "Align", "center")+"\">") ;
         httpContext.writeText( "<p align=\"center\">") ;
         httpContext.writeText( "ACCESO AL SISTEMA DE FACTURACION ") ;
         httpContext.writeText( "</p>") ;
         wb_table3_8_0F2( true) ;
      }
      else
      {
         wb_table3_8_0F2( false) ;
      }
      return  ;
   }

   public void wb_table3_8_0F2e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "<p>") ;
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"" ;
         ClassString = "SpecialButtons" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttButton1_Internalname, "", "Confirmar", bttButton1_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "rounded", "EENTER.", TempTags, "", httpContext.getButtonType( ), "HLP_login.htm");
         httpContext.writeText( "</p>") ;
         httpContext.writeText( "<p></p>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "font-family:'Arial'; font-size:10.0pt; font-weight:normal; font-style:normal; color:#FF0000;" ;
         GxWebStd.gx_label_ctrl( httpContext, lblMensaje_Internalname, lblMensaje_Caption, "", "", lblMensaje_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_login.htm");
         httpContext.writeText( "<p></p>") ;
         httpContext.writeText( "<p>") ;
         httpContext.writeText( "CLAUDIO NAVARRO D�AZ") ;
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "TALSW1301-8-TALLER DE INTEGRACI�N DE SOFTWARE") ;
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "IACC - 2022 ") ;
         httpContext.writeText( "</p>") ;
         httpContext.writeText( "<p></p>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_5_0F2e( true) ;
      }
      else
      {
         wb_table2_5_0F2e( false) ;
      }
   }

   public void wb_table3_8_0F2( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td style=\"width:59px\">") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblock1_Internalname, "Usuario", "", "", lblTextblock1_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_login.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td style=\"width:84px\">") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV5usuario", AV5usuario);
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavUsuario_Internalname, GXutil.rtrim( AV5usuario), GXutil.rtrim( localUtil.format( AV5usuario, "XXXXXXXXXX")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(13);\"", "", "", "", "", edtavUsuario_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_login.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblock2_Internalname, "Password", "", "", lblTextblock2_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_login.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV6clave", AV6clave);
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavClave_Internalname, GXutil.rtrim( AV6clave), GXutil.rtrim( localUtil.format( AV6clave, "XXXXXXXXXX")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(18);\"", "", "", "", "", edtavClave_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 10, "chr", 1, "row", 10, (byte)(-1), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_login.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_8_0F2e( true) ;
      }
      else
      {
         wb_table3_8_0F2e( false) ;
      }
   }

   public void setparameters( Object[] obj )
   {
   }

   public String getresponse( String sGXDynURL )
   {
      initialize_properties( ) ;
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      sDynURL = sGXDynURL ;
      nGotPars = 1 ;
      nGXWrapped = 1 ;
      httpContext.setWrapped(true);
      pa0F2( ) ;
      ws0F2( ) ;
      we0F2( ) ;
      httpContext.setWrapped(false);
      httpContext.GX_msglist = BackMsgLst ;
      return ((java.io.ByteArrayOutputStream) httpContext.getOutputStream()).toString();
   }

   public void responsestatic( String sGXDynURL )
   {
   }

   public void define_styles( )
   {
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?1057201");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?58720");
      httpContext.AddJavascriptSource("login.js", "?1057201");
      /* End function include_jscripts */
   }

   public void init_default_properties( )
   {
      lblTextblock1_Internalname = "TEXTBLOCK1" ;
      edtavUsuario_Internalname = "vUSUARIO" ;
      lblTextblock2_Internalname = "TEXTBLOCK2" ;
      edtavClave_Internalname = "vCLAVE" ;
      tblTable1_Internalname = "TABLE1" ;
      bttButton1_Internalname = "BUTTON1" ;
      lblMensaje_Internalname = "MENSAJE" ;
      tblTable3_Internalname = "TABLE3" ;
      tblTable4_Internalname = "TABLE4" ;
      Form.setInternalname( "FORM" );
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      edtavClave_Jsonclick = "" ;
      edtavUsuario_Jsonclick = "" ;
      lblMensaje_Caption = "" ;
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      sDynURL = "" ;
      FormProcess = "" ;
      GXt_char1 = "" ;
      GXt_char2 = "" ;
      GX_FocusControl = "" ;
      sPrefix = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      AV5usuario = "" ;
      AV6clave = "" ;
      AV7session = httpContext.getWebSession();
      scmdbuf = "" ;
      H000F2_A36usu_codigo = new short[1] ;
      H000F2_A38usu_clave = new String[] {""} ;
      H000F2_A37usu_usuario = new String[] {""} ;
      H000F2_A25rol_codigo = new short[1] ;
      A38usu_clave = "" ;
      A37usu_usuario = "" ;
      GXv_char6 = new String [1] ;
      GXv_dtime5 = new java.util.Date [1] ;
      GXv_char4 = new String [1] ;
      sStyleString = "" ;
      TempTags = "" ;
      ClassString = "" ;
      StyleString = "" ;
      bttButton1_Jsonclick = "" ;
      lblMensaje_Jsonclick = "" ;
      lblTextblock1_Jsonclick = "" ;
      lblTextblock2_Jsonclick = "" ;
      GXt_char3 = "" ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      pr_default = new DataStoreProvider(context, remoteHandle, new login__default(),
         new Object[] {
             new Object[] {
            H000F2_A36usu_codigo, H000F2_A38usu_clave, H000F2_A37usu_usuario, H000F2_A25rol_codigo
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte nGotPars ;
   private byte GxWebError ;
   private byte nDonePA ;
   private byte AV10GXLvl6 ;
   private byte nGXWrapped ;
   private short wbEnd ;
   private short wbStart ;
   private short Gx_err ;
   private short A25rol_codigo ;
   private int idxLst ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String sDynURL ;
   private String FormProcess ;
   private String GXt_char1 ;
   private String GXt_char2 ;
   private String GX_FocusControl ;
   private String sPrefix ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String edtavUsuario_Internalname ;
   private String AV5usuario ;
   private String AV6clave ;
   private String edtavClave_Internalname ;
   private String scmdbuf ;
   private String A38usu_clave ;
   private String A37usu_usuario ;
   private String lblMensaje_Caption ;
   private String lblMensaje_Internalname ;
   private String GXv_char6[] ;
   private String GXv_char4[] ;
   private String sStyleString ;
   private String tblTable4_Internalname ;
   private String tblTable3_Internalname ;
   private String TempTags ;
   private String ClassString ;
   private String StyleString ;
   private String bttButton1_Internalname ;
   private String bttButton1_Jsonclick ;
   private String lblMensaje_Jsonclick ;
   private String tblTable1_Internalname ;
   private String lblTextblock1_Internalname ;
   private String lblTextblock1_Jsonclick ;
   private String edtavUsuario_Jsonclick ;
   private String lblTextblock2_Internalname ;
   private String lblTextblock2_Jsonclick ;
   private String GXt_char3 ;
   private String edtavClave_Jsonclick ;
   private java.util.Date GXv_dtime5[] ;
   private boolean entryPointCalled ;
   private boolean wbLoad ;
   private boolean Rfr0gs ;
   private boolean wbErr ;
   private boolean returnInSub ;
   private com.genexus.webpanels.GXWebForm Form ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private IDataStoreProvider pr_default ;
   private short[] H000F2_A36usu_codigo ;
   private String[] H000F2_A38usu_clave ;
   private String[] H000F2_A37usu_usuario ;
   private short[] H000F2_A25rol_codigo ;
   private com.genexus.webpanels.WebSession AV7session ;
}

final  class login__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("H000F2", "SELECT [usu_codigo], [usu_clave], [usu_usuario], [rol_codigo] FROM [usuario] WITH (NOLOCK) WHERE ([usu_usuario] = ?) AND ([usu_clave] = ?) ORDER BY [usu_codigo] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,100,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setString(1, (String)parms[0], 10);
               stmt.setString(2, (String)parms[1], 10);
               break;
      }
   }

}

