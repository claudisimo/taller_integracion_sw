/*
               File: reorg
        Description: Table Manager
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 8:21:18.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.ui.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.util.*;

public final  class reorg extends GXProcedure
{
   public reorg( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( reorg.class ), "" );
   }

   public reorg( int remoteHandle ,
                 ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      if ( previousCheck() )
      {
         executeReorganization( ) ;
      }
   }

   private void FirstActions( )
   {
      /* Load data into tables. */
   }

   public void Reorganizebitacora( ) throws SQLException
   {
      String cmdBuffer ;
      /* Indices for table bitacora */
      try
      {
         cmdBuffer = " CREATE NONCLUSTERED INDEX [UBITACORA] ON [bitacora] ([bit_codigo] DESC) ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
      }
      catch(SQLException ex)
      {
         cmdBuffer = " DROP INDEX [UBITACORA] ON [bitacora] ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
         cmdBuffer = " CREATE NONCLUSTERED INDEX [UBITACORA] ON [bitacora] ([bit_codigo] DESC) ";
         ExecuteDirectSQL.executeWithThrow(context, remoteHandle, "DEFAULT", cmdBuffer) ;
      }
   }

   private void tablesCount( )
   {
      if ( ! GXReorganization.isResumeMode( ) )
      {
         /* Using cursor P00012 */
         pr_default.execute(0);
         bitacoraCount = P00012_AbitacoraCount[0] ;
         pr_default.close(0);
         GXReorganization.printRecordCount ( "bitacora" ,  bitacoraCount );
      }
   }

   private boolean previousCheck( )
   {
      if ( ! GXReorganization.isResumeMode( ) )
      {
         if ( GXutil.dbmsVersion( context, remoteHandle, "DEFAULT") < 9 )
         {
            GXReorganization.setCheckError ( localUtil.getMessages().getMessage("GXM_bad_DBMS_version", new Object[] {"2005"}) ) ;
            return false ;
         }
      }
      if ( ! GXReorganization.mustRunCheck( ) )
      {
         return true ;
      }
      if ( GXutil.isSQLSERVER2005( context, remoteHandle, "DEFAULT") )
      {
         /* Using cursor P00023 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            sSchemaVar = P00023_AsSchemaVar[0] ;
            nsSchemaVar = P00023_nsSchemaVar[0] ;
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }
      else
      {
         /* Using cursor P00034 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            sSchemaVar = P00034_AsSchemaVar[0] ;
            nsSchemaVar = P00034_nsSchemaVar[0] ;
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }
      return true ;
   }

   private void executeOnlyTablesReorganization( )
   {
      callSubmit( "Reorganizebitacora" ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"bitacora",""}) ,  1 , new Object[]{ });
   }

   private void executeOnlyRisReorganization( )
   {
   }

   private void executeTablesReorganization( )
   {
      executeOnlyTablesReorganization( ) ;
      executeOnlyRisReorganization( ) ;
      ReorgSubmitThreadPool.startProcess();
   }

   private void setPrecedence( )
   {
      setPrecedencetables( ) ;
      setPrecedenceris( ) ;
   }

   private void setPrecedencetables( )
   {
      GXReorganization.addMsg( 1 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"bitacora",""}) );
      ReorgSubmitThreadPool.addBlock( "Reorganizebitacora" );
   }

   private void setPrecedenceris( )
   {
   }

   private void executeReorganization( )
   {
      if ( ErrCode == 0 )
      {
         tablesCount( ) ;
         if ( ! GXReorganization.getRecordCount( ) )
         {
            FirstActions( ) ;
            setPrecedence( ) ;
            executeTablesReorganization( ) ;
         }
      }
   }

   public void UtilsCleanup( )
   {
      cleanup();
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void submitReorg( int submitId ,
                            Object [] submitParms ) throws SQLException
   {
      UserInformation submitUI = Application.getConnectionManager().createUserInformation(Namespace.getNamespace(context.getNAME_SPACE()));
      switch ( submitId )
      {
            case 1 :
               GXReorganization.replaceMsg( 1 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"bitacora",""})+" STARTED" );
               Reorganizebitacora( ) ;
               GXReorganization.replaceMsg( 1 ,  localUtil.getMessages().getMessage("GXM_fileupdate", new Object[] {"bitacora",""})+" ENDED" );
               try { submitUI.disconnect(); } catch(Exception submitExc) { ; }
               break;
      }
   }

   public void initialize( )
   {
      scmdbuf = "" ;
      P00012_AbitacoraCount = new int[1] ;
      sSchemaVar = "" ;
      nsSchemaVar = false ;
      P00023_AsSchemaVar = new String[] {""} ;
      P00023_nsSchemaVar = new boolean[] {false} ;
      P00034_AsSchemaVar = new String[] {""} ;
      P00034_nsSchemaVar = new boolean[] {false} ;
      pr_default = new DataStoreProvider(context, remoteHandle, new reorg__default(),
         new Object[] {
             new Object[] {
            P00012_AbitacoraCount
            }
            , new Object[] {
            P00023_AsSchemaVar
            }
            , new Object[] {
            P00034_AsSchemaVar
            }
         }
      );
      /* GeneXus formulas. */
   }

   protected short ErrCode ;
   protected int bitacoraCount ;
   protected String scmdbuf ;
   protected String sSchemaVar ;
   protected boolean nsSchemaVar ;
   protected IDataStoreProvider pr_default ;
   protected int[] P00012_AbitacoraCount ;
   protected String[] P00023_AsSchemaVar ;
   protected boolean[] P00023_nsSchemaVar ;
   protected String[] P00034_AsSchemaVar ;
   protected boolean[] P00034_nsSchemaVar ;
}

final  class reorg__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P00012", "SELECT COUNT(*) FROM [bitacora] ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,100,0,false )
         ,new ForEachCursor("P00023", "SELECT SCHEMA_NAME() ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,100,0,false )
         ,new ForEachCursor("P00034", "SELECT USER_NAME() ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,100,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((int[]) buf[0])[0] = rslt.getInt(1) ;
               break;
            case 1 :
               ((String[]) buf[0])[0] = rslt.getString(1, 255) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getString(1, 255) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
      }
   }

}

