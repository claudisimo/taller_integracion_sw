/*
               File: permiso_impl
        Description: permiso
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:5.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class permiso_impl extends GXDataArea
{
   public void initenv( )
   {
      if ( GxWebError != 0 )
      {
         return  ;
      }
   }

   public void inittrn( )
   {
      initialize_properties( ) ;
      entryPointCalled = false ;
      gxfirstwebparm = httpContext.GetNextPar( ) ;
      gxfirstwebparm_bkp = gxfirstwebparm ;
      gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm, "High") ;
      if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
      {
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         dyncall( httpContext.GetNextPar( )) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
      {
         A25rol_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_3( A25rol_codigo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
      {
         A28men_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_4( A28men_codigo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
      {
         httpContext.setAjaxEventMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = httpContext.GetNextPar( ) ;
      }
      else
      {
         if ( ! httpContext.IsValidAjaxCall( false) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = gxfirstwebparm_bkp ;
      }
      Form.getMeta().addItem("Generator", "GeneXus Java", (short)(0)) ;
      Form.getMeta().addItem("Version", "10_1_8-58720", (short)(0)) ;
      Form.getMeta().addItem("Description", "permiso", (short)(0)) ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      GX_FocusControl = edtrol_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      wbErr = false ;
      httpContext.setTheme("GeneXusX");
   }

   public permiso_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public permiso_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( permiso_impl.class ));
   }

   public permiso_impl( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void webExecute( )
   {
      initenv( ) ;
      inittrn( ) ;
      if ( ( GxWebError == 0 ) && ! httpContext.isAjaxCallMode( ) )
      {
         MasterPageObj = new menu_administrador_impl (remoteHandle, context.copy());
         MasterPageObj.setDataArea(this,false);
         MasterPageObj.webExecute();
         if ( httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "max-age=0");
            }
            if ( (GXutil.strcmp("", httpContext.wjLoc)==0) )
            {
               httpContext.GX_webresponse.addString(httpContext.getJSONResponse( ));
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public void draw( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         disable_std_buttons( ) ;
         enableDisable( ) ;
         set_caption( ) ;
         /* Form start */
         wb_table1_2_088( true) ;
      }
      return  ;
   }

   public void wb_table1_2_088e( boolean wbgen )
   {
      if ( wbgen )
      {
      }
      /* Execute Exit event if defined. */
   }

   public void wb_table1_2_088( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table2_5_088( true) ;
      }
      return  ;
   }

   public void wb_table2_5_088e( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Control Group */
         ClassString = "Group" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroupdata_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"permiso"+"</legend>") ;
         wb_table3_27_088( true) ;
      }
      return  ;
   }

   public void wb_table3_27_088e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_088e( true) ;
      }
      else
      {
         wb_table1_2_088e( false) ;
      }
   }

   public void wb_table3_27_088( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         ClassString = "ErrorViewer" ;
         StyleString = "" ;
         GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), StyleString, ClassString, "", "false");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table4_33_088( true) ;
      }
      return  ;
   }

   public void wb_table4_33_088e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"" ;
         ClassString = "BtnEnter" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "rounded", "EENTER.", TempTags, "", httpContext.getButtonType( ), "HLP_permiso.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"" ;
         ClassString = "BtnCancel" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "rounded", "ECANCEL.", TempTags, "", httpContext.getButtonType( ), "HLP_permiso.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"" ;
         ClassString = "BtnDelete" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "rounded", "EDELETE.", TempTags, "", httpContext.getButtonType( ), "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_27_088e( true) ;
      }
      else
      {
         wb_table3_27_088e( false) ;
      }
   }

   public void wb_table4_33_088( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockrol_codigo_Internalname, "C�digo del rol", "", "", lblTextblockrol_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtrol_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A25rol_codigo, (byte)(4), (byte)(0), ",", "")), ((edtrol_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A25rol_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A25rol_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(38);\"", "", "", "", "", edtrol_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtrol_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_permiso.htm");
         /* Static images/pictures */
         ClassString = "Image" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgprompt_25_Internalname, "prompt.gif", imgprompt_25_Link, "", "", "GeneXusX", imgprompt_25_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "''", "", "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockmen_codigo_Internalname, "C�digo del men�", "", "", lblTextblockmen_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtmen_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A28men_codigo, (byte)(4), (byte)(0), ",", "")), ((edtmen_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A28men_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A28men_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(43);\"", "", "", "", "", edtmen_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtmen_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_permiso.htm");
         /* Static images/pictures */
         ClassString = "Image" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgprompt_28_Internalname, "prompt.gif", imgprompt_28_Link, "", "", "GeneXusX", imgprompt_28_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "''", "", "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockper_fecha_Internalname, "per_fecha", "", "", lblTextblockper_fecha_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A32per_fecha", localUtil.format(A32per_fecha, "99/99/99"));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         httpContext.writeText( "<div id=\""+edtper_fecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( httpContext, edtper_fecha_Internalname, localUtil.format(A32per_fecha, "99/99/99"), localUtil.format( A32per_fecha, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(48);\"", "", "", "", "", edtper_fecha_Jsonclick, 0, ClassString, StyleString, "", 1, edtper_fecha_Enabled, 0, 8, "chr", 1, "row", 8, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_permiso.htm");
         GxWebStd.gx_bitmap( httpContext, edtper_fecha_Internalname+"_dp_trigger", "calendar-img.gif", "", "", "", "", ((1==0)||(edtper_fecha_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;vertical-align:text-bottom", "", "", "", "", "", "HLP_permiso.htm");
         httpContext.writeTextNL( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockper_guardar_Internalname, "per_guardar", "", "", lblTextblockper_guardar_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A33per_guardar", GXutil.ltrim( GXutil.str( A33per_guardar, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtper_guardar_Internalname, GXutil.ltrim( localUtil.ntoc( A33per_guardar, (byte)(4), (byte)(0), ",", "")), ((edtper_guardar_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A33per_guardar), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A33per_guardar), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(53);\"", "", "", "", "", edtper_guardar_Jsonclick, 0, ClassString, StyleString, "", 1, edtper_guardar_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockper_editar_Internalname, "per_editar", "", "", lblTextblockper_editar_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A34per_editar", GXutil.ltrim( GXutil.str( A34per_editar, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtper_editar_Internalname, GXutil.ltrim( localUtil.ntoc( A34per_editar, (byte)(4), (byte)(0), ",", "")), ((edtper_editar_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A34per_editar), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A34per_editar), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(58);\"", "", "", "", "", edtper_editar_Jsonclick, 0, ClassString, StyleString, "", 1, edtper_editar_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockper_eliminar_Internalname, "per_eliminar", "", "", lblTextblockper_eliminar_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A35per_eliminar", GXutil.ltrim( GXutil.str( A35per_eliminar, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtper_eliminar_Internalname, GXutil.ltrim( localUtil.ntoc( A35per_eliminar, (byte)(4), (byte)(0), ",", "")), ((edtper_eliminar_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A35per_eliminar), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A35per_eliminar), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(63);\"", "", "", "", "", edtper_eliminar_Jsonclick, 0, ClassString, StyleString, "", 1, edtper_eliminar_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_permiso.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table4_33_088e( true) ;
      }
      else
      {
         wb_table4_33_088e( false) ;
      }
   }

   public void wb_table2_5_088( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "<div style=\"WHITE-SPACE: nowrap\" class=\"ToolbarMain\">") ;
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_Internalname, context.getHttpContext().getImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_Visible, 1, "", "Primero", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_separator_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_Internalname, context.getHttpContext().getImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_Internalname, context.getHttpContext().getImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_Visible, 1, "", "Siguiente", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_separator_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_Internalname, context.getHttpContext().getImagePath( "29211874-e613-48e5-9011-8017d984217e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_Visible, 1, "", "Ultimo", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_separator_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_Internalname, context.getHttpContext().getImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_Visible, 1, "", "Seleccionar", 0, 0, 0, "", 0, "", 0, 0, 4, imgBtn_select_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "gx.popup.openPrompt('"+"gx0080"+"',["+"{Ctrl:gx.dom.el('"+"ROL_CODIGO"+"'), id:'"+"ROL_CODIGO"+"'"+",isOut: true}"+","+"{Ctrl:gx.dom.el('"+"MEN_CODIGO"+"'), id:'"+"MEN_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 4, imgBtn_select_separator_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "gx.popup.openPrompt('"+"gx0080"+"',["+"{Ctrl:gx.dom.el('"+"ROL_CODIGO"+"'), id:'"+"ROL_CODIGO"+"'"+",isOut: true}"+","+"{Ctrl:gx.dom.el('"+"MEN_CODIGO"+"'), id:'"+"MEN_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_Internalname, context.getHttpContext().getImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_Internalname, context.getHttpContext().getImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_Visible, 1, "", "Cancelar", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_Internalname, context.getHttpContext().getImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_permiso.htm");
         httpContext.writeText( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_5_088e( true) ;
      }
      else
      {
         wb_table2_5_088e( false) ;
      }
   }

   public void userMain( )
   {
      standaloneStartup( ) ;
   }

   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
      process( ) ;
   }

   public void standaloneStartupServer( )
   {
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      httpContext.wbGlbDoneStart = (byte)(1) ;
      assign_properties_default( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtrol_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtrol_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "ROL_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtrol_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A25rol_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
            }
            else
            {
               A25rol_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtrol_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
            }
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtmen_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtmen_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "MEN_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtmen_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A28men_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
            }
            else
            {
               A28men_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtmen_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
            }
            if ( localUtil.vcdate( httpContext.cgiGet( edtper_fecha_Internalname), (byte)(3)) == 0 )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_faildate", new Object[] {"per_fecha"}), 1, "PER_FECHA");
               AnyError = (short)(1) ;
               GX_FocusControl = edtper_fecha_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A32per_fecha = GXutil.nullDate() ;
               httpContext.ajax_rsp_assign_attri("", false, "A32per_fecha", localUtil.format(A32per_fecha, "99/99/99"));
            }
            else
            {
               A32per_fecha = localUtil.ctod( httpContext.cgiGet( edtper_fecha_Internalname), 3) ;
               httpContext.ajax_rsp_assign_attri("", false, "A32per_fecha", localUtil.format(A32per_fecha, "99/99/99"));
            }
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtper_guardar_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtper_guardar_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "PER_GUARDAR");
               AnyError = (short)(1) ;
               GX_FocusControl = edtper_guardar_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A33per_guardar = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A33per_guardar", GXutil.ltrim( GXutil.str( A33per_guardar, 4, 0)));
            }
            else
            {
               A33per_guardar = (short)(localUtil.ctol( httpContext.cgiGet( edtper_guardar_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A33per_guardar", GXutil.ltrim( GXutil.str( A33per_guardar, 4, 0)));
            }
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtper_editar_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtper_editar_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "PER_EDITAR");
               AnyError = (short)(1) ;
               GX_FocusControl = edtper_editar_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A34per_editar = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A34per_editar", GXutil.ltrim( GXutil.str( A34per_editar, 4, 0)));
            }
            else
            {
               A34per_editar = (short)(localUtil.ctol( httpContext.cgiGet( edtper_editar_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A34per_editar", GXutil.ltrim( GXutil.str( A34per_editar, 4, 0)));
            }
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtper_eliminar_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtper_eliminar_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "PER_ELIMINAR");
               AnyError = (short)(1) ;
               GX_FocusControl = edtper_eliminar_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A35per_eliminar = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A35per_eliminar", GXutil.ltrim( GXutil.str( A35per_eliminar, 4, 0)));
            }
            else
            {
               A35per_eliminar = (short)(localUtil.ctol( httpContext.cgiGet( edtper_eliminar_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A35per_eliminar", GXutil.ltrim( GXutil.str( A35per_eliminar, 4, 0)));
            }
            /* Read saved values. */
            Z25rol_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z25rol_codigo"), ",", ".")) ;
            Z28men_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z28men_codigo"), ",", ".")) ;
            Z32per_fecha = localUtil.ctod( httpContext.cgiGet( "Z32per_fecha"), 0) ;
            Z33per_guardar = (short)(localUtil.ctol( httpContext.cgiGet( "Z33per_guardar"), ",", ".")) ;
            Z34per_editar = (short)(localUtil.ctol( httpContext.cgiGet( "Z34per_editar"), ",", ".")) ;
            Z35per_eliminar = (short)(localUtil.ctol( httpContext.cgiGet( "Z35per_eliminar"), ",", ".")) ;
            IsConfirmed = (short)(localUtil.ctol( httpContext.cgiGet( "IsConfirmed"), ",", ".")) ;
            IsModified = (short)(localUtil.ctol( httpContext.cgiGet( "IsModified"), ",", ".")) ;
            Gx_mode = httpContext.cgiGet( "Mode") ;
            Gx_mode = httpContext.cgiGet( "vMODE") ;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( GXutil.strcmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A25rol_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
               httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
               A28men_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
               httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }
   }

   public void process( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read Transaction buttons. */
         sEvt = httpContext.cgiGet( "_EventName") ;
         EvtGridId = httpContext.cgiGet( "_EventGridId") ;
         EvtRowId = httpContext.cgiGet( "_EventRowId") ;
         if ( GXutil.len( sEvt) > 0 )
         {
            sEvtType = GXutil.left( sEvt, 1) ;
            sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
            if ( GXutil.strcmp(sEvtType, "M") != 0 )
            {
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_enter( ) ;
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                     else if ( GXutil.strcmp(sEvt, "FIRST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_first( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "PREVIOUS") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_previous( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "NEXT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_next( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "LAST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_last( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "SELECT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_select( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "DELETE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_delete( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        afterkeyloadscreen( ) ;
                     }
                  }
                  else
                  {
                  }
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void afterTrn( )
   {
      if ( trnEnded == 1 )
      {
         trnEnded = 0 ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            /* Clear variables for new insertion. */
            initAll088( ) ;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
         }
      }
   }

   public String toString( )
   {
      return "" ;
   }

   public GXContentInfo getContentInfo( )
   {
      return (GXContentInfo)(null) ;
   }

   public void disable_std_buttons( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
      {
         imgBtn_delete2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
      }
   }

   public void disable_std_buttons_dsp( )
   {
      imgBtn_delete2_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
      imgBtn_delete2_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
      bttBtn_delete_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
      imgBtn_first_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_first_Visible, 5, 0)));
      imgBtn_first_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_first_separator_Visible, 5, 0)));
      imgBtn_previous_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_previous_Visible, 5, 0)));
      imgBtn_previous_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_previous_separator_Visible, 5, 0)));
      imgBtn_next_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_next_Visible, 5, 0)));
      imgBtn_next_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_next_separator_Visible, 5, 0)));
      imgBtn_last_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_last_Visible, 5, 0)));
      imgBtn_last_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_last_separator_Visible, 5, 0)));
      imgBtn_select_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_select_Visible, 5, 0)));
      imgBtn_select_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_select_separator_Visible, 5, 0)));
      imgBtn_delete2_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
      imgBtn_delete2_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
      bttBtn_delete_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
      if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
      {
         imgBtn_enter2_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_Visible, 5, 0)));
         imgBtn_enter2_separator_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_separator_Visible, 5, 0)));
         bttBtn_enter_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_enter_Visible, 5, 0)));
      }
      disableAttributes088( ) ;
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_confdelete"), 0, "");
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_mustconfirm"), 0, "");
         }
      }
   }

   public void resetCaption080( )
   {
   }

   public void zm088( int GX_JID )
   {
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
         {
            Z32per_fecha = T00083_A32per_fecha[0] ;
            Z33per_guardar = T00083_A33per_guardar[0] ;
            Z34per_editar = T00083_A34per_editar[0] ;
            Z35per_eliminar = T00083_A35per_eliminar[0] ;
         }
         else
         {
            Z32per_fecha = A32per_fecha ;
            Z33per_guardar = A33per_guardar ;
            Z34per_editar = A34per_editar ;
            Z35per_eliminar = A35per_eliminar ;
         }
      }
      if ( GX_JID == -2 )
      {
         Z32per_fecha = A32per_fecha ;
         Z33per_guardar = A33per_guardar ;
         Z34per_editar = A34per_editar ;
         Z35per_eliminar = A35per_eliminar ;
         Z25rol_codigo = A25rol_codigo ;
         Z28men_codigo = A28men_codigo ;
      }
   }

   public void standaloneNotModal( )
   {
      imgprompt_25_Link = ((GXutil.strcmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0060"+"',["+"{Ctrl:gx.dom.el('"+"ROL_CODIGO"+"'), id:'"+"ROL_CODIGO"+"'"+",isOut: true}"+"],"+"null"+","+"'', false"+","+"true"+");") ;
      imgprompt_28_Link = ((GXutil.strcmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0070"+"',["+"{Ctrl:gx.dom.el('"+"MEN_CODIGO"+"'), id:'"+"MEN_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");") ;
   }

   public void standaloneModal( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
      {
         imgBtn_delete2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
      }
      else
      {
         imgBtn_delete2_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
      }
      if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
      {
         imgBtn_enter2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
      else
      {
         imgBtn_enter2_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
   }

   public void load088( )
   {
      /* Using cursor T00086 */
      pr_default.execute(4, new Object[] {new Short(A25rol_codigo), new Short(A28men_codigo)});
      if ( (pr_default.getStatus(4) != 101) )
      {
         RcdFound8 = (short)(1) ;
         A32per_fecha = T00086_A32per_fecha[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A32per_fecha", localUtil.format(A32per_fecha, "99/99/99"));
         A33per_guardar = T00086_A33per_guardar[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A33per_guardar", GXutil.ltrim( GXutil.str( A33per_guardar, 4, 0)));
         A34per_editar = T00086_A34per_editar[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A34per_editar", GXutil.ltrim( GXutil.str( A34per_editar, 4, 0)));
         A35per_eliminar = T00086_A35per_eliminar[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A35per_eliminar", GXutil.ltrim( GXutil.str( A35per_eliminar, 4, 0)));
         zm088( -2) ;
      }
      pr_default.close(4);
      onLoadActions088( ) ;
   }

   public void onLoadActions088( )
   {
   }

   public void checkExtendedTable088( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      /* Using cursor T00084 */
      pr_default.execute(2, new Object[] {new Short(A25rol_codigo)});
      if ( (pr_default.getStatus(2) == 101) )
      {
         AnyError25 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'rol'.", "ForeignKeyNotFound", 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError25 == 0 )
      {
      }
      pr_default.close(2);
      /* Using cursor T00085 */
      pr_default.execute(3, new Object[] {new Short(A28men_codigo)});
      if ( (pr_default.getStatus(3) == 101) )
      {
         AnyError28 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'menu'.", "ForeignKeyNotFound", 1, "MEN_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtmen_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError28 == 0 )
      {
      }
      pr_default.close(3);
      if ( ! ( GXutil.nullDate().equals(A32per_fecha) || (( A32per_fecha.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A32per_fecha.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Campo per_fecha fuera de rango", "OutOfRange", 1, "");
         AnyError = (short)(1) ;
      }
   }

   public void closeExtendedTableCursors088( )
   {
      pr_default.close(2);
      pr_default.close(3);
   }

   public void enableDisable( )
   {
   }

   public void gxload_3( short A25rol_codigo )
   {
      /* Using cursor T00087 */
      pr_default.execute(5, new Object[] {new Short(A25rol_codigo)});
      if ( (pr_default.getStatus(5) == 101) )
      {
         AnyError25 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'rol'.", "ForeignKeyNotFound", 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError25 == 0 )
      {
      }
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.GX_webresponse.addString("new Array( new Array(");
      httpContext.GX_webresponse.addString("");
      httpContext.GX_webresponse.addString(")");
      if ( (pr_default.getStatus(5) == 101) )
      {
         httpContext.GX_webresponse.addString(",");
         httpContext.GX_webresponse.addString("101");
      }
      httpContext.GX_webresponse.addString(")");
      pr_default.close(5);
   }

   public void gxload_4( short A28men_codigo )
   {
      /* Using cursor T00088 */
      pr_default.execute(6, new Object[] {new Short(A28men_codigo)});
      if ( (pr_default.getStatus(6) == 101) )
      {
         AnyError28 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'menu'.", "ForeignKeyNotFound", 1, "MEN_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtmen_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError28 == 0 )
      {
      }
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.GX_webresponse.addString("new Array( new Array(");
      httpContext.GX_webresponse.addString("");
      httpContext.GX_webresponse.addString(")");
      if ( (pr_default.getStatus(6) == 101) )
      {
         httpContext.GX_webresponse.addString(",");
         httpContext.GX_webresponse.addString("101");
      }
      httpContext.GX_webresponse.addString(")");
      pr_default.close(6);
   }

   public void getKey088( )
   {
      /* Using cursor T00089 */
      pr_default.execute(7, new Object[] {new Short(A25rol_codigo), new Short(A28men_codigo)});
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound8 = (short)(1) ;
      }
      else
      {
         RcdFound8 = (short)(0) ;
      }
      pr_default.close(7);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00083 */
      pr_default.execute(1, new Object[] {new Short(A25rol_codigo), new Short(A28men_codigo)});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm088( 2) ;
         RcdFound8 = (short)(1) ;
         A32per_fecha = T00083_A32per_fecha[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A32per_fecha", localUtil.format(A32per_fecha, "99/99/99"));
         A33per_guardar = T00083_A33per_guardar[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A33per_guardar", GXutil.ltrim( GXutil.str( A33per_guardar, 4, 0)));
         A34per_editar = T00083_A34per_editar[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A34per_editar", GXutil.ltrim( GXutil.str( A34per_editar, 4, 0)));
         A35per_eliminar = T00083_A35per_eliminar[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A35per_eliminar", GXutil.ltrim( GXutil.str( A35per_eliminar, 4, 0)));
         A25rol_codigo = T00083_A25rol_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         A28men_codigo = T00083_A28men_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
         Z25rol_codigo = A25rol_codigo ;
         Z28men_codigo = A28men_codigo ;
         sMode8 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal( ) ;
         load088( ) ;
         Gx_mode = sMode8 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         RcdFound8 = (short)(0) ;
         initializeNonKey088( ) ;
         sMode8 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal( ) ;
         Gx_mode = sMode8 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey088( ) ;
      if ( RcdFound8 == 0 )
      {
         Gx_mode = "INS" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound8 = (short)(0) ;
      /* Using cursor T000810 */
      pr_default.execute(8, new Object[] {new Short(A25rol_codigo), new Short(A25rol_codigo), new Short(A28men_codigo)});
      if ( (pr_default.getStatus(8) != 101) )
      {
         while ( (pr_default.getStatus(8) != 101) && ( ( T000810_A25rol_codigo[0] < A25rol_codigo ) || ( T000810_A25rol_codigo[0] == A25rol_codigo ) && ( T000810_A28men_codigo[0] < A28men_codigo ) ) )
         {
            pr_default.readNext(8);
         }
         if ( (pr_default.getStatus(8) != 101) && ( ( T000810_A25rol_codigo[0] > A25rol_codigo ) || ( T000810_A25rol_codigo[0] == A25rol_codigo ) && ( T000810_A28men_codigo[0] > A28men_codigo ) ) )
         {
            A25rol_codigo = T000810_A25rol_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
            A28men_codigo = T000810_A28men_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
            RcdFound8 = (short)(1) ;
         }
      }
      pr_default.close(8);
   }

   public void move_previous( )
   {
      RcdFound8 = (short)(0) ;
      /* Using cursor T000811 */
      pr_default.execute(9, new Object[] {new Short(A25rol_codigo), new Short(A25rol_codigo), new Short(A28men_codigo)});
      if ( (pr_default.getStatus(9) != 101) )
      {
         while ( (pr_default.getStatus(9) != 101) && ( ( T000811_A25rol_codigo[0] > A25rol_codigo ) || ( T000811_A25rol_codigo[0] == A25rol_codigo ) && ( T000811_A28men_codigo[0] > A28men_codigo ) ) )
         {
            pr_default.readNext(9);
         }
         if ( (pr_default.getStatus(9) != 101) && ( ( T000811_A25rol_codigo[0] < A25rol_codigo ) || ( T000811_A25rol_codigo[0] == A25rol_codigo ) && ( T000811_A28men_codigo[0] < A28men_codigo ) ) )
         {
            A25rol_codigo = T000811_A25rol_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
            A28men_codigo = T000811_A28men_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
            RcdFound8 = (short)(1) ;
         }
      }
      pr_default.close(9);
   }

   public void btn_enter( )
   {
      nKeyPressed = (byte)(1) ;
      getKey088( ) ;
      if ( RcdFound8 == 1 )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "ROL_CODIGO");
            AnyError = (short)(1) ;
            GX_FocusControl = edtrol_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else if ( ( A25rol_codigo != Z25rol_codigo ) || ( A28men_codigo != Z28men_codigo ) )
         {
            A25rol_codigo = Z25rol_codigo ;
            httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
            A28men_codigo = Z28men_codigo ;
            httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "ROL_CODIGO");
            AnyError = (short)(1) ;
            GX_FocusControl = edtrol_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            delete( ) ;
            afterTrn( ) ;
            GX_FocusControl = edtrol_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            Gx_mode = "UPD" ;
            httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            /* Update record */
            update088( ) ;
            GX_FocusControl = edtrol_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }
      else
      {
         if ( ( A25rol_codigo != Z25rol_codigo ) || ( A28men_codigo != Z28men_codigo ) )
         {
            Gx_mode = "INS" ;
            httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            /* Insert record */
            GX_FocusControl = edtrol_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            insert088( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "" ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( GXutil.strcmp(Gx_mode, "UPD") == 0 )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "ROL_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtrol_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            else
            {
               Gx_mode = "INS" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               /* Insert record */
               GX_FocusControl = edtrol_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               insert088( ) ;
               if ( AnyError == 1 )
               {
                  GX_FocusControl = "" ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
         }
      }
      afterTrn( ) ;
   }

   public void btn_delete( )
   {
      if ( ( A25rol_codigo != Z25rol_codigo ) || ( A28men_codigo != Z28men_codigo ) )
      {
         A25rol_codigo = Z25rol_codigo ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         A28men_codigo = Z28men_codigo ;
         httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforedlt"), 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      else
      {
         delete( ) ;
         afterTrn( ) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError != 0 )
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      getByPrimaryKey( ) ;
      CloseOpenCursors();
   }

   public void btn_get( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      if ( RcdFound8 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
      }
      GX_FocusControl = edtper_fecha_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_first( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart088( ) ;
      if ( RcdFound8 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      GX_FocusControl = edtper_fecha_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd088( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_previous( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_previous( ) ;
      if ( RcdFound8 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      GX_FocusControl = edtper_fecha_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_next( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_next( ) ;
      if ( RcdFound8 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      GX_FocusControl = edtper_fecha_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_last( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart088( ) ;
      if ( RcdFound8 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         while ( RcdFound8 != 0 )
         {
            scanNext088( ) ;
         }
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      GX_FocusControl = edtper_fecha_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd088( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_select( )
   {
      getEqualNoModal( ) ;
   }

   public void checkOptimisticConcurrency088( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         /* Using cursor T00082 */
         pr_default.execute(0, new Object[] {new Short(A25rol_codigo), new Short(A28men_codigo)});
         if ( (pr_default.getStatus(0) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"permisos"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || !( Z32per_fecha.equals( T00082_A32per_fecha[0] ) ) || ( Z33per_guardar != T00082_A33per_guardar[0] ) || ( Z34per_editar != T00082_A34per_editar[0] ) || ( Z35per_eliminar != T00082_A35per_eliminar[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"permisos"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert088( )
   {
      beforeValidate088( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable088( ) ;
      }
      if ( AnyError == 0 )
      {
         zm088( 0) ;
         checkOptimisticConcurrency088( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm088( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert088( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000812 */
                  pr_default.execute(10, new Object[] {A32per_fecha, new Short(A33per_guardar), new Short(A34per_editar), new Short(A35per_eliminar), new Short(A25rol_codigo), new Short(A28men_codigo)});
                  if ( (pr_default.getStatus(10) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucadded"), 0, "");
                        resetCaption080( ) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load088( ) ;
         }
         endLevel088( ) ;
      }
      closeExtendedTableCursors088( ) ;
   }

   public void update088( )
   {
      beforeValidate088( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable088( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency088( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm088( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate088( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000813 */
                  pr_default.execute(11, new Object[] {A32per_fecha, new Short(A33per_guardar), new Short(A34per_editar), new Short(A35per_eliminar), new Short(A25rol_codigo), new Short(A28men_codigo)});
                  if ( (pr_default.getStatus(11) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"permisos"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate088( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucupdated"), 0, "");
                        resetCaption080( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel088( ) ;
      }
      closeExtendedTableCursors088( ) ;
   }

   public void deferredUpdate088( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      beforeValidate088( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency088( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls088( ) ;
         afterConfirm088( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete088( ) ;
            if ( AnyError == 0 )
            {
               /* No cascading delete specified. */
               /* Using cursor T000814 */
               pr_default.execute(12, new Object[] {new Short(A25rol_codigo), new Short(A28men_codigo)});
               if ( AnyError == 0 )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( AnyError == 0 )
                  {
                     move_next( ) ;
                     if ( RcdFound8 == 0 )
                     {
                        initAll088( ) ;
                        Gx_mode = "INS" ;
                        httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     }
                     else
                     {
                        getByPrimaryKey( ) ;
                        Gx_mode = "UPD" ;
                        httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     }
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucdeleted"), 0, "");
                     resetCaption080( ) ;
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode8 = Gx_mode ;
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      endLevel088( ) ;
      Gx_mode = sMode8 ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
   }

   public void onDeleteControls088( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel088( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         pr_default.close(0);
      }
      if ( AnyError == 0 )
      {
         beforeComplete088( ) ;
      }
      if ( AnyError == 0 )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "permiso");
         if ( AnyError == 0 )
         {
            confirmValues080( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "permiso");
      }
      IsModified = (short)(0) ;
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanStart088( )
   {
      /* Using cursor T000815 */
      pr_default.execute(13);
      RcdFound8 = (short)(0) ;
      if ( (pr_default.getStatus(13) != 101) )
      {
         RcdFound8 = (short)(1) ;
         A25rol_codigo = T000815_A25rol_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         A28men_codigo = T000815_A28men_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
      }
      /* Load Subordinate Levels */
   }

   public void scanNext088( )
   {
      pr_default.readNext(13);
      RcdFound8 = (short)(0) ;
      if ( (pr_default.getStatus(13) != 101) )
      {
         RcdFound8 = (short)(1) ;
         A25rol_codigo = T000815_A25rol_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         A28men_codigo = T000815_A28men_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
      }
   }

   public void scanEnd088( )
   {
      pr_default.close(13);
   }

   public void afterConfirm088( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert088( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate088( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete088( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete088( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate088( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes088( )
   {
      edtrol_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtrol_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtrol_codigo_Enabled, 5, 0)));
      edtmen_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtmen_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtmen_codigo_Enabled, 5, 0)));
      edtper_fecha_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtper_fecha_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtper_fecha_Enabled, 5, 0)));
      edtper_guardar_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtper_guardar_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtper_guardar_Enabled, 5, 0)));
      edtper_editar_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtper_editar_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtper_editar_Enabled, 5, 0)));
      edtper_eliminar_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtper_eliminar_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtper_eliminar_Enabled, 5, 0)));
   }

   public void assign_properties_default( )
   {
   }

   public void confirmValues080( )
   {
   }

   public void renderHtmlHeaders( )
   {
      GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), "IE=EmulateIE7");
   }

   public void renderHtmlOpenForm( )
   {
      httpContext.writeText( "<title>") ;
      httpContext.writeText( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      MasterPageObj.master_styles();
      if ( ! httpContext.isSmartDevice( ) )
      {
         httpContext.AddJavascriptSource("gxgral.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      else
      {
         httpContext.AddJavascriptSource("gxapiSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfxSD.js", "?58720");
         httpContext.AddJavascriptSource("gxtypesSD.js", "?58720");
         httpContext.AddJavascriptSource("gxpopupSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfrmutlSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgridSD.js", "?58720");
         httpContext.AddJavascriptSource("JavaScripTableSD.js", "?58720");
         httpContext.AddJavascriptSource("rijndaelSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgralSD.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      httpContext.AddJavascriptSource("calendar.js", "?58720");
      httpContext.AddJavascriptSource("calendar-setup.js", "?58720");
      httpContext.AddJavascriptSource("calendar-es.js", "?58720");
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      FormProcess = " onkeyup=\"gx.evt.onkeyup(event)\" onkeypress=\"gx.evt.onkeypress(event,true,false)\" onkeydown=\"gx.evt.onkeypress(null,true,false)\"" ;
      httpContext.writeText( "<body") ;
      httpContext.writeText( " "+"class=\"Form\""+" "+" style=\"-moz-opacity:0;opacity:0;"+"background-color:"+WebUtils.getHTMLColor( Form.getIBackground())+";") ;
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         httpContext.writeText( " background-image:url("+httpContext.convertURL( Form.getBackground())+")") ;
      }
      httpContext.writeText( "\""+FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" onsubmit=\"try{return gx.csv.validForm()}catch(e){return true;}\" name=\"MAINFORM\" method=\"post\" action=\""+formatLink("permiso") +"\" class=\""+"Form"+"\">") ;
      GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
   }

   public void renderHtmlCloseForm( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      GxWebStd.gx_hidden_field( httpContext, "Z25rol_codigo", GXutil.ltrim( localUtil.ntoc( Z25rol_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z28men_codigo", GXutil.ltrim( localUtil.ntoc( Z28men_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z32per_fecha", localUtil.dtoc( Z32per_fecha, 0, "/"));
      GxWebStd.gx_hidden_field( httpContext, "Z33per_guardar", GXutil.ltrim( localUtil.ntoc( Z33per_guardar, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z34per_editar", GXutil.ltrim( localUtil.ntoc( Z34per_editar, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z35per_eliminar", GXutil.ltrim( localUtil.ntoc( Z35per_eliminar, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsConfirmed", GXutil.ltrim( localUtil.ntoc( IsConfirmed, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsModified", GXutil.ltrim( localUtil.ntoc( IsModified, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Mode", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "vMODE", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      httpContext.writeTextNL( "</form>") ;
      include_jscripts( ) ;
   }

   public byte executeStartEvent( )
   {
      standaloneStartup( ) ;
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      return gxajaxcallmode ;
   }

   public void renderHtmlContent( )
   {
      draw( ) ;
   }

   public void dispatchEvents( )
   {
      process( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public String getPgmname( )
   {
      return "permiso" ;
   }

   public String getPgmdesc( )
   {
      return "permiso" ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("permiso")  ;
   }

   public void initializeNonKey088( )
   {
      A32per_fecha = GXutil.nullDate() ;
      httpContext.ajax_rsp_assign_attri("", false, "A32per_fecha", localUtil.format(A32per_fecha, "99/99/99"));
      A33per_guardar = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A33per_guardar", GXutil.ltrim( GXutil.str( A33per_guardar, 4, 0)));
      A34per_editar = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A34per_editar", GXutil.ltrim( GXutil.str( A34per_editar, 4, 0)));
      A35per_eliminar = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A35per_eliminar", GXutil.ltrim( GXutil.str( A35per_eliminar, 4, 0)));
   }

   public void initAll088( )
   {
      A25rol_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
      A28men_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A28men_codigo", GXutil.ltrim( GXutil.str( A28men_codigo, 4, 0)));
      initializeNonKey088( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void define_styles( )
   {
      httpContext.AddStyleSheetFile("calendar-system.css", "?95080");
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?942616");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?58720");
      httpContext.AddJavascriptSource("permiso.js", "?942616");
      /* End function include_jscripts */
   }

   public void init_default_properties( )
   {
      imgBtn_first_Internalname = "BTN_FIRST" ;
      imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR" ;
      imgBtn_previous_Internalname = "BTN_PREVIOUS" ;
      imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR" ;
      imgBtn_next_Internalname = "BTN_NEXT" ;
      imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR" ;
      imgBtn_last_Internalname = "BTN_LAST" ;
      imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR" ;
      imgBtn_select_Internalname = "BTN_SELECT" ;
      imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR" ;
      imgBtn_enter2_Internalname = "BTN_ENTER2" ;
      imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR" ;
      imgBtn_cancel2_Internalname = "BTN_CANCEL2" ;
      imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR" ;
      imgBtn_delete2_Internalname = "BTN_DELETE2" ;
      imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR" ;
      tblTabletoolbar_Internalname = "TABLETOOLBAR" ;
      lblTextblockrol_codigo_Internalname = "TEXTBLOCKROL_CODIGO" ;
      edtrol_codigo_Internalname = "ROL_CODIGO" ;
      lblTextblockmen_codigo_Internalname = "TEXTBLOCKMEN_CODIGO" ;
      edtmen_codigo_Internalname = "MEN_CODIGO" ;
      lblTextblockper_fecha_Internalname = "TEXTBLOCKPER_FECHA" ;
      edtper_fecha_Internalname = "PER_FECHA" ;
      lblTextblockper_guardar_Internalname = "TEXTBLOCKPER_GUARDAR" ;
      edtper_guardar_Internalname = "PER_GUARDAR" ;
      lblTextblockper_editar_Internalname = "TEXTBLOCKPER_EDITAR" ;
      edtper_editar_Internalname = "PER_EDITAR" ;
      lblTextblockper_eliminar_Internalname = "TEXTBLOCKPER_ELIMINAR" ;
      edtper_eliminar_Internalname = "PER_ELIMINAR" ;
      tblTable2_Internalname = "TABLE2" ;
      bttBtn_enter_Internalname = "BTN_ENTER" ;
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      bttBtn_delete_Internalname = "BTN_DELETE" ;
      tblTable1_Internalname = "TABLE1" ;
      grpGroupdata_Internalname = "GROUPDATA" ;
      tblTablemain_Internalname = "TABLEMAIN" ;
      Form.setInternalname( "FORM" );
      imgprompt_25_Internalname = "PROMPT_25" ;
      imgprompt_28_Internalname = "PROMPT_28" ;
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "permiso" );
      imgBtn_delete2_separator_Visible = 1 ;
      imgBtn_delete2_Enabled = 1 ;
      imgBtn_delete2_Visible = 1 ;
      imgBtn_cancel2_separator_Visible = 1 ;
      imgBtn_cancel2_Visible = 1 ;
      imgBtn_enter2_separator_Visible = 1 ;
      imgBtn_enter2_Enabled = 1 ;
      imgBtn_enter2_Visible = 1 ;
      imgBtn_select_separator_Visible = 1 ;
      imgBtn_select_Visible = 1 ;
      imgBtn_last_separator_Visible = 1 ;
      imgBtn_last_Visible = 1 ;
      imgBtn_next_separator_Visible = 1 ;
      imgBtn_next_Visible = 1 ;
      imgBtn_previous_separator_Visible = 1 ;
      imgBtn_previous_Visible = 1 ;
      imgBtn_first_separator_Visible = 1 ;
      imgBtn_first_Visible = 1 ;
      edtper_eliminar_Jsonclick = "" ;
      edtper_eliminar_Enabled = 1 ;
      edtper_editar_Jsonclick = "" ;
      edtper_editar_Enabled = 1 ;
      edtper_guardar_Jsonclick = "" ;
      edtper_guardar_Enabled = 1 ;
      edtper_fecha_Jsonclick = "" ;
      edtper_fecha_Enabled = 1 ;
      imgprompt_28_Visible = 1 ;
      imgprompt_28_Link = "" ;
      edtmen_codigo_Jsonclick = "" ;
      edtmen_codigo_Enabled = 1 ;
      imgprompt_25_Visible = 1 ;
      imgprompt_25_Link = "" ;
      edtrol_codigo_Jsonclick = "" ;
      edtrol_codigo_Enabled = 1 ;
      bttBtn_delete_Visible = 1 ;
      bttBtn_cancel_Visible = 1 ;
      bttBtn_enter_Visible = 1 ;
      httpContext.GX_msglist.setDisplaymode( (short)(1) );
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void afterkeyloadscreen( )
   {
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      /* Using cursor T000816 */
      pr_default.execute(14, new Object[] {new Short(A25rol_codigo)});
      if ( (pr_default.getStatus(14) == 101) )
      {
         AnyError25 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'rol'.", "ForeignKeyNotFound", 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError25 == 0 )
      {
      }
      pr_default.close(14);
      /* Using cursor T000817 */
      pr_default.execute(15, new Object[] {new Short(A28men_codigo)});
      if ( (pr_default.getStatus(15) == 101) )
      {
         AnyError28 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'menu'.", "ForeignKeyNotFound", 1, "MEN_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtmen_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError28 == 0 )
      {
      }
      pr_default.close(15);
      GX_FocusControl = edtper_fecha_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      /* End function AfterKeyLoadScreen */
   }

   public void valid_Rol_codigo( short GX_Parm1 )
   {
      A25rol_codigo = GX_Parm1 ;
      /* Using cursor T000816 */
      pr_default.execute(14, new Object[] {new Short(A25rol_codigo)});
      if ( (pr_default.getStatus(14) == 101) )
      {
         AnyError25 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'rol'.", "ForeignKeyNotFound", 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
      }
      if ( AnyError25 == 0 )
      {
      }
      pr_default.close(14);
      dynload_actions( ) ;
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   public void valid_Men_codigo( short GX_Parm1 ,
                                 short GX_Parm2 ,
                                 java.util.Date GX_Parm3 ,
                                 short GX_Parm4 ,
                                 short GX_Parm5 ,
                                 short GX_Parm6 )
   {
      A25rol_codigo = GX_Parm1 ;
      A28men_codigo = GX_Parm2 ;
      A32per_fecha = GX_Parm3 ;
      A33per_guardar = GX_Parm4 ;
      A34per_editar = GX_Parm5 ;
      A35per_eliminar = GX_Parm6 ;
      httpContext.wbHandled = (byte)(1) ;
      afterkeyloadscreen( ) ;
      draw( ) ;
      /* Using cursor T000817 */
      pr_default.execute(15, new Object[] {new Short(A28men_codigo)});
      if ( (pr_default.getStatus(15) == 101) )
      {
         AnyError28 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'menu'.", "ForeignKeyNotFound", 1, "MEN_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtmen_codigo_Internalname ;
      }
      if ( AnyError28 == 0 )
      {
      }
      pr_default.close(15);
      dynload_actions( ) ;
      if ( AnyError == 1 )
      {
      }
      isValidOutput.add(localUtil.format(A32per_fecha, "99/99/99"));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A33per_guardar, (byte)(4), (byte)(0), ".", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A34per_editar, (byte)(4), (byte)(0), ".", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A35per_eliminar, (byte)(4), (byte)(0), ".", "")));
      isValidOutput.add(GXutil.rtrim( Gx_mode));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z25rol_codigo, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z28men_codigo, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(localUtil.dtoc( Z32per_fecha, 0, "/"));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z33per_guardar, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z34per_editar, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z35per_eliminar, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(imgBtn_delete2_Enabled);
      isValidOutput.add(imgBtn_enter2_Enabled);
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(14);
      pr_default.close(15);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      sPrefix = "" ;
      scmdbuf = "" ;
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      GX_FocusControl = "" ;
      sStyleString = "" ;
      ClassString = "" ;
      StyleString = "" ;
      TempTags = "" ;
      bttBtn_enter_Jsonclick = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      bttBtn_delete_Jsonclick = "" ;
      lblTextblockrol_codigo_Jsonclick = "" ;
      lblTextblockmen_codigo_Jsonclick = "" ;
      lblTextblockper_fecha_Jsonclick = "" ;
      A32per_fecha = GXutil.nullDate() ;
      lblTextblockper_guardar_Jsonclick = "" ;
      lblTextblockper_editar_Jsonclick = "" ;
      lblTextblockper_eliminar_Jsonclick = "" ;
      imgBtn_first_Jsonclick = "" ;
      imgBtn_first_separator_Jsonclick = "" ;
      imgBtn_previous_Jsonclick = "" ;
      imgBtn_previous_separator_Jsonclick = "" ;
      imgBtn_next_Jsonclick = "" ;
      imgBtn_next_separator_Jsonclick = "" ;
      imgBtn_last_Jsonclick = "" ;
      imgBtn_last_separator_Jsonclick = "" ;
      imgBtn_select_Jsonclick = "" ;
      imgBtn_select_separator_Jsonclick = "" ;
      imgBtn_enter2_Jsonclick = "" ;
      imgBtn_enter2_separator_Jsonclick = "" ;
      imgBtn_cancel2_Jsonclick = "" ;
      imgBtn_cancel2_separator_Jsonclick = "" ;
      imgBtn_delete2_Jsonclick = "" ;
      imgBtn_delete2_separator_Jsonclick = "" ;
      Z32per_fecha = GXutil.nullDate() ;
      Gx_mode = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      T00086_A32per_fecha = new java.util.Date[] {GXutil.nullDate()} ;
      T00086_A33per_guardar = new short[1] ;
      T00086_A34per_editar = new short[1] ;
      T00086_A35per_eliminar = new short[1] ;
      T00086_A25rol_codigo = new short[1] ;
      T00086_A28men_codigo = new short[1] ;
      T00084_A25rol_codigo = new short[1] ;
      T00085_A28men_codigo = new short[1] ;
      T00087_A25rol_codigo = new short[1] ;
      T00088_A28men_codigo = new short[1] ;
      T00089_A25rol_codigo = new short[1] ;
      T00089_A28men_codigo = new short[1] ;
      T00083_A32per_fecha = new java.util.Date[] {GXutil.nullDate()} ;
      T00083_A33per_guardar = new short[1] ;
      T00083_A34per_editar = new short[1] ;
      T00083_A35per_eliminar = new short[1] ;
      T00083_A25rol_codigo = new short[1] ;
      T00083_A28men_codigo = new short[1] ;
      sMode8 = "" ;
      T000810_A25rol_codigo = new short[1] ;
      T000810_A28men_codigo = new short[1] ;
      T000811_A25rol_codigo = new short[1] ;
      T000811_A28men_codigo = new short[1] ;
      T00082_A32per_fecha = new java.util.Date[] {GXutil.nullDate()} ;
      T00082_A33per_guardar = new short[1] ;
      T00082_A34per_editar = new short[1] ;
      T00082_A35per_eliminar = new short[1] ;
      T00082_A25rol_codigo = new short[1] ;
      T00082_A28men_codigo = new short[1] ;
      T000815_A25rol_codigo = new short[1] ;
      T000815_A28men_codigo = new short[1] ;
      sDynURL = "" ;
      FormProcess = "" ;
      GXt_char3 = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      T000816_A25rol_codigo = new short[1] ;
      T000817_A28men_codigo = new short[1] ;
      isValidOutput = new com.genexus.GxUnknownObjectCollection();
      pr_default = new DataStoreProvider(context, remoteHandle, new permiso__default(),
         new Object[] {
             new Object[] {
            T00082_A32per_fecha, T00082_A33per_guardar, T00082_A34per_editar, T00082_A35per_eliminar, T00082_A25rol_codigo, T00082_A28men_codigo
            }
            , new Object[] {
            T00083_A32per_fecha, T00083_A33per_guardar, T00083_A34per_editar, T00083_A35per_eliminar, T00083_A25rol_codigo, T00083_A28men_codigo
            }
            , new Object[] {
            T00084_A25rol_codigo
            }
            , new Object[] {
            T00085_A28men_codigo
            }
            , new Object[] {
            T00086_A32per_fecha, T00086_A33per_guardar, T00086_A34per_editar, T00086_A35per_eliminar, T00086_A25rol_codigo, T00086_A28men_codigo
            }
            , new Object[] {
            T00087_A25rol_codigo
            }
            , new Object[] {
            T00088_A28men_codigo
            }
            , new Object[] {
            T00089_A25rol_codigo, T00089_A28men_codigo
            }
            , new Object[] {
            T000810_A25rol_codigo, T000810_A28men_codigo
            }
            , new Object[] {
            T000811_A25rol_codigo, T000811_A28men_codigo
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000815_A25rol_codigo, T000815_A28men_codigo
            }
            , new Object[] {
            T000816_A25rol_codigo
            }
            , new Object[] {
            T000817_A28men_codigo
            }
         }
      );
   }

   private byte GxWebError ;
   private byte nKeyPressed ;
   private byte Gx_BScreen ;
   private byte gxajaxcallmode ;
   private byte wbTemp ;
   private short A25rol_codigo ;
   private short A28men_codigo ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short A33per_guardar ;
   private short A34per_editar ;
   private short A35per_eliminar ;
   private short Z25rol_codigo ;
   private short Z28men_codigo ;
   private short Z33per_guardar ;
   private short Z34per_editar ;
   private short Z35per_eliminar ;
   private short RcdFound8 ;
   private int trnEnded ;
   private int bttBtn_enter_Visible ;
   private int bttBtn_cancel_Visible ;
   private int bttBtn_delete_Visible ;
   private int edtrol_codigo_Enabled ;
   private int imgprompt_25_Visible ;
   private int edtmen_codigo_Enabled ;
   private int imgprompt_28_Visible ;
   private int edtper_fecha_Enabled ;
   private int edtper_guardar_Enabled ;
   private int edtper_editar_Enabled ;
   private int edtper_eliminar_Enabled ;
   private int imgBtn_first_Visible ;
   private int imgBtn_first_separator_Visible ;
   private int imgBtn_previous_Visible ;
   private int imgBtn_previous_separator_Visible ;
   private int imgBtn_next_Visible ;
   private int imgBtn_next_separator_Visible ;
   private int imgBtn_last_Visible ;
   private int imgBtn_last_separator_Visible ;
   private int imgBtn_select_Visible ;
   private int imgBtn_select_separator_Visible ;
   private int imgBtn_enter2_Visible ;
   private int imgBtn_enter2_Enabled ;
   private int imgBtn_enter2_separator_Visible ;
   private int imgBtn_cancel2_Visible ;
   private int imgBtn_cancel2_separator_Visible ;
   private int imgBtn_delete2_Visible ;
   private int imgBtn_delete2_Enabled ;
   private int imgBtn_delete2_separator_Visible ;
   private int GX_JID ;
   private int AnyError25 ;
   private int AnyError28 ;
   private int idxLst ;
   private String sPrefix ;
   private String scmdbuf ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String GX_FocusControl ;
   private String edtrol_codigo_Internalname ;
   private String sStyleString ;
   private String tblTablemain_Internalname ;
   private String ClassString ;
   private String StyleString ;
   private String grpGroupdata_Internalname ;
   private String tblTable1_Internalname ;
   private String TempTags ;
   private String bttBtn_enter_Internalname ;
   private String bttBtn_enter_Jsonclick ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String bttBtn_delete_Internalname ;
   private String bttBtn_delete_Jsonclick ;
   private String tblTable2_Internalname ;
   private String lblTextblockrol_codigo_Internalname ;
   private String lblTextblockrol_codigo_Jsonclick ;
   private String edtrol_codigo_Jsonclick ;
   private String imgprompt_25_Internalname ;
   private String imgprompt_25_Link ;
   private String lblTextblockmen_codigo_Internalname ;
   private String lblTextblockmen_codigo_Jsonclick ;
   private String edtmen_codigo_Internalname ;
   private String edtmen_codigo_Jsonclick ;
   private String imgprompt_28_Internalname ;
   private String imgprompt_28_Link ;
   private String lblTextblockper_fecha_Internalname ;
   private String lblTextblockper_fecha_Jsonclick ;
   private String edtper_fecha_Internalname ;
   private String edtper_fecha_Jsonclick ;
   private String lblTextblockper_guardar_Internalname ;
   private String lblTextblockper_guardar_Jsonclick ;
   private String edtper_guardar_Internalname ;
   private String edtper_guardar_Jsonclick ;
   private String lblTextblockper_editar_Internalname ;
   private String lblTextblockper_editar_Jsonclick ;
   private String edtper_editar_Internalname ;
   private String edtper_editar_Jsonclick ;
   private String lblTextblockper_eliminar_Internalname ;
   private String lblTextblockper_eliminar_Jsonclick ;
   private String edtper_eliminar_Internalname ;
   private String edtper_eliminar_Jsonclick ;
   private String tblTabletoolbar_Internalname ;
   private String imgBtn_first_Internalname ;
   private String imgBtn_first_Jsonclick ;
   private String imgBtn_first_separator_Internalname ;
   private String imgBtn_first_separator_Jsonclick ;
   private String imgBtn_previous_Internalname ;
   private String imgBtn_previous_Jsonclick ;
   private String imgBtn_previous_separator_Internalname ;
   private String imgBtn_previous_separator_Jsonclick ;
   private String imgBtn_next_Internalname ;
   private String imgBtn_next_Jsonclick ;
   private String imgBtn_next_separator_Internalname ;
   private String imgBtn_next_separator_Jsonclick ;
   private String imgBtn_last_Internalname ;
   private String imgBtn_last_Jsonclick ;
   private String imgBtn_last_separator_Internalname ;
   private String imgBtn_last_separator_Jsonclick ;
   private String imgBtn_select_Internalname ;
   private String imgBtn_select_Jsonclick ;
   private String imgBtn_select_separator_Internalname ;
   private String imgBtn_select_separator_Jsonclick ;
   private String imgBtn_enter2_Internalname ;
   private String imgBtn_enter2_Jsonclick ;
   private String imgBtn_enter2_separator_Internalname ;
   private String imgBtn_enter2_separator_Jsonclick ;
   private String imgBtn_cancel2_Internalname ;
   private String imgBtn_cancel2_Jsonclick ;
   private String imgBtn_cancel2_separator_Internalname ;
   private String imgBtn_cancel2_separator_Jsonclick ;
   private String imgBtn_delete2_Internalname ;
   private String imgBtn_delete2_Jsonclick ;
   private String imgBtn_delete2_separator_Internalname ;
   private String imgBtn_delete2_separator_Jsonclick ;
   private String Gx_mode ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String sMode8 ;
   private String sDynURL ;
   private String FormProcess ;
   private String GXt_char3 ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private java.util.Date A32per_fecha ;
   private java.util.Date Z32per_fecha ;
   private boolean entryPointCalled ;
   private boolean wbErr ;
   private com.genexus.webpanels.GXMasterPage MasterPageObj ;
   private com.genexus.GxUnknownObjectCollection isValidOutput ;
   private IDataStoreProvider pr_default ;
   private java.util.Date[] T00086_A32per_fecha ;
   private short[] T00086_A33per_guardar ;
   private short[] T00086_A34per_editar ;
   private short[] T00086_A35per_eliminar ;
   private short[] T00086_A25rol_codigo ;
   private short[] T00086_A28men_codigo ;
   private short[] T00084_A25rol_codigo ;
   private short[] T00085_A28men_codigo ;
   private short[] T00087_A25rol_codigo ;
   private short[] T00088_A28men_codigo ;
   private short[] T00089_A25rol_codigo ;
   private short[] T00089_A28men_codigo ;
   private java.util.Date[] T00083_A32per_fecha ;
   private short[] T00083_A33per_guardar ;
   private short[] T00083_A34per_editar ;
   private short[] T00083_A35per_eliminar ;
   private short[] T00083_A25rol_codigo ;
   private short[] T00083_A28men_codigo ;
   private short[] T000810_A25rol_codigo ;
   private short[] T000810_A28men_codigo ;
   private short[] T000811_A25rol_codigo ;
   private short[] T000811_A28men_codigo ;
   private java.util.Date[] T00082_A32per_fecha ;
   private short[] T00082_A33per_guardar ;
   private short[] T00082_A34per_editar ;
   private short[] T00082_A35per_eliminar ;
   private short[] T00082_A25rol_codigo ;
   private short[] T00082_A28men_codigo ;
   private short[] T000815_A25rol_codigo ;
   private short[] T000815_A28men_codigo ;
   private short[] T000816_A25rol_codigo ;
   private short[] T000817_A28men_codigo ;
   private com.genexus.webpanels.GXWebForm Form ;
}

final  class permiso__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00082", "SELECT [per_fecha], [per_guardar], [per_editar], [per_eliminar], [rol_codigo], [men_codigo] FROM [permisos] WITH (UPDLOCK) WHERE [rol_codigo] = ? AND [men_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00083", "SELECT [per_fecha], [per_guardar], [per_editar], [per_eliminar], [rol_codigo], [men_codigo] FROM [permisos] WITH (NOLOCK) WHERE [rol_codigo] = ? AND [men_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00084", "SELECT [rol_codigo] FROM [rol] WITH (NOLOCK) WHERE [rol_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00085", "SELECT [men_codigo] FROM [menu] WITH (NOLOCK) WHERE [men_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00086", "SELECT TM1.[per_fecha], TM1.[per_guardar], TM1.[per_editar], TM1.[per_eliminar], TM1.[rol_codigo], TM1.[men_codigo] FROM [permisos] TM1 WITH (NOLOCK) WHERE TM1.[rol_codigo] = ? and TM1.[men_codigo] = ? ORDER BY TM1.[rol_codigo], TM1.[men_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
         ,new ForEachCursor("T00087", "SELECT [rol_codigo] FROM [rol] WITH (NOLOCK) WHERE [rol_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00088", "SELECT [men_codigo] FROM [menu] WITH (NOLOCK) WHERE [men_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00089", "SELECT [rol_codigo], [men_codigo] FROM [permisos] WITH (NOLOCK) WHERE [rol_codigo] = ? AND [men_codigo] = ?  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000810", "SELECT TOP 1 [rol_codigo], [men_codigo] FROM [permisos] WITH (NOLOCK) WHERE ( [rol_codigo] > ? or [rol_codigo] = ? and [men_codigo] > ?) ORDER BY [rol_codigo], [men_codigo]  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new ForEachCursor("T000811", "SELECT TOP 1 [rol_codigo], [men_codigo] FROM [permisos] WITH (NOLOCK) WHERE ( [rol_codigo] < ? or [rol_codigo] = ? and [men_codigo] < ?) ORDER BY [rol_codigo] DESC, [men_codigo] DESC  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new UpdateCursor("T000812", "INSERT INTO [permisos] ([per_fecha], [per_guardar], [per_editar], [per_eliminar], [rol_codigo], [men_codigo]) VALUES (?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T000813", "UPDATE [permisos] SET [per_fecha]=?, [per_guardar]=?, [per_editar]=?, [per_eliminar]=?  WHERE [rol_codigo] = ? AND [men_codigo] = ?", GX_NOMASK)
         ,new UpdateCursor("T000814", "DELETE FROM [permisos]  WHERE [rol_codigo] = ? AND [men_codigo] = ?", GX_NOMASK)
         ,new ForEachCursor("T000815", "SELECT [rol_codigo], [men_codigo] FROM [permisos] WITH (NOLOCK) ORDER BY [rol_codigo], [men_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
         ,new ForEachCursor("T000816", "SELECT [rol_codigo] FROM [rol] WITH (NOLOCK) WHERE [rol_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000817", "SELECT [men_codigo] FROM [menu] WITH (NOLOCK) WHERE [men_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               ((short[]) buf[4])[0] = rslt.getShort(5) ;
               ((short[]) buf[5])[0] = rslt.getShort(6) ;
               break;
            case 1 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               ((short[]) buf[4])[0] = rslt.getShort(5) ;
               ((short[]) buf[5])[0] = rslt.getShort(6) ;
               break;
            case 2 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 3 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 4 :
               ((java.util.Date[]) buf[0])[0] = rslt.getGXDate(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               ((short[]) buf[4])[0] = rslt.getShort(5) ;
               ((short[]) buf[5])[0] = rslt.getShort(6) ;
               break;
            case 5 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 6 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 7 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               break;
            case 8 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               break;
            case 9 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               break;
            case 13 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               break;
            case 14 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 15 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 1 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 2 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 3 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 4 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 5 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 6 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 7 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 8 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               break;
            case 9 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               break;
            case 10 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               stmt.setShort(5, ((Number) parms[4]).shortValue());
               stmt.setShort(6, ((Number) parms[5]).shortValue());
               break;
            case 11 :
               stmt.setDate(1, (java.util.Date)parms[0]);
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               stmt.setShort(5, ((Number) parms[4]).shortValue());
               stmt.setShort(6, ((Number) parms[5]).shortValue());
               break;
            case 12 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 14 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 15 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
      }
   }

}

