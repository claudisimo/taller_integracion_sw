/*
               File: login
        Description: login
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 10:57:19.87
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/login")
public final  class login extends GXWebObjectStub
{
   public static void main( String args[] )
   {
      ApplicationContext.getInstance().setCurrentLocation( "" );
      Application.init(GXcfg.class);
      login_impl pgm = new login_impl (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
      GXStaticWebPanel.copyFiles();
   }

   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new login_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "login";
   }

}

