/*
               File: detalle_factura
        Description: detalle_factura
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 22, 2022 12:30:36.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/detalle_factura")
public final  class detalle_factura extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new detalle_factura_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "detalle_factura";
   }

}

