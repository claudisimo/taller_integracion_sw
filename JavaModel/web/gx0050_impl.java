/*
               File: gx0050_impl
        Description: Selection List detalle_factura
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 22, 2022 12:30:43.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class gx0050_impl extends GXDataArea
{
   public gx0050_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public gx0050_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( gx0050_impl.class ));
   }

   public gx0050_impl( int remoteHandle ,
                       ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void initweb( )
   {
      initialize_properties( ) ;
      if ( nGotPars == 0 )
      {
         entryPointCalled = false ;
         gxfirstwebparm = httpContext.GetNextPar( ) ;
         gxfirstwebparm_bkp = gxfirstwebparm ;
         gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm, "High") ;
         if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
         {
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            dyncall( httpContext.GetNextPar( )) ;
            return  ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            httpContext.setAjaxEventMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = httpContext.GetNextPar( ) ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
         {
            nRC_Grid1 = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            nGXsfl_46_idx = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            sGXsfl_46_idx = httpContext.GetNextPar( ) ;
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxnrgrid1_newrow( nRC_Grid1, nGXsfl_46_idx, sGXsfl_46_idx) ;
            return  ;
         }
         else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
         {
            /* GeneXus formulas. */
            Gx_err = (short)(0) ;
            Grid1_PageSize46 = (int)(GXutil.lval( httpContext.GetNextPar( ))) ;
            AV6cdetfac_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV6cdetfac_codigo", GXutil.ltrim( GXutil.str( AV6cdetfac_codigo, 4, 0)));
            AV7cdetfac_cantidad = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV7cdetfac_cantidad", GXutil.ltrim( GXutil.str( AV7cdetfac_cantidad, 4, 0)));
            AV8cdetfac_precio = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV8cdetfac_precio", GXutil.ltrim( GXutil.str( AV8cdetfac_precio, 4, 0)));
            AV9cdetfac_total = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV9cdetfac_total", GXutil.ltrim( GXutil.str( AV9cdetfac_total, 4, 0)));
            AV10cfac_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV10cfac_codigo", GXutil.ltrim( GXutil.str( AV10cfac_codigo, 4, 0)));
            AV11cpro_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV11cpro_codigo", GXutil.ltrim( GXutil.str( AV11cpro_codigo, 4, 0)));
            httpContext.setAjaxCallMode();
            if ( ! httpContext.IsValidAjaxCall( true) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxgrgrid1_refresh( Grid1_PageSize46, AV6cdetfac_codigo, AV7cdetfac_cantidad, AV8cdetfac_precio, AV9cdetfac_total, AV10cfac_codigo, AV11cpro_codigo) ;
            return  ;
         }
         else
         {
            if ( ! httpContext.IsValidAjaxCall( false) )
            {
               GxWebError = (byte)(1) ;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp ;
         }
         if ( ! entryPointCalled )
         {
            AV12pdetfac_codigo = (short)(GXutil.lval( gxfirstwebparm)) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV12pdetfac_codigo", GXutil.ltrim( GXutil.str( AV12pdetfac_codigo, 4, 0)));
         }
      }
      httpContext.setTheme("GeneXusX");
   }

   public void webExecute( )
   {
      initweb( ) ;
      if ( ! httpContext.isAjaxCallMode( ) )
      {
         MasterPageObj = new promptmasterpage_impl (remoteHandle, context.copy());
         MasterPageObj.setDataArea(this,true);
         MasterPageObj.webExecute();
         if ( httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "max-age=0");
            }
            if ( (GXutil.strcmp("", httpContext.wjLoc)==0) )
            {
               httpContext.GX_webresponse.addString(httpContext.getJSONResponse( ));
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public byte executeStartEvent( )
   {
      pa082( ) ;
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
      {
         start082( ) ;
      }
      return gxajaxcallmode ;
   }

   public void renderHtmlHeaders( )
   {
      GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), "IE=EmulateIE7");
   }

   public void renderHtmlOpenForm( )
   {
      httpContext.writeText( "<title>") ;
      httpContext.writeText( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      if ( nGXWrapped != 1 )
      {
         MasterPageObj.master_styles();
      }
      if ( ! httpContext.isSmartDevice( ) )
      {
         httpContext.AddJavascriptSource("gxgral.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      else
      {
         httpContext.AddJavascriptSource("gxapiSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfxSD.js", "?58720");
         httpContext.AddJavascriptSource("gxtypesSD.js", "?58720");
         httpContext.AddJavascriptSource("gxpopupSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfrmutlSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgridSD.js", "?58720");
         httpContext.AddJavascriptSource("JavaScripTableSD.js", "?58720");
         httpContext.AddJavascriptSource("rijndaelSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgralSD.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      FormProcess = " onkeyup=\"gx.evt.onkeyup(event)\" onkeypress=\"gx.evt.onkeypress(event,true,false)\" onkeydown=\"gx.evt.onkeypress(null,true,false)\"" ;
      httpContext.writeText( "<body") ;
      httpContext.writeText( " "+"class=\"Form\""+" "+" style=\"-moz-opacity:0;opacity:0;"+"background-color:"+WebUtils.getHTMLColor( Form.getIBackground())+";") ;
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         httpContext.writeText( " background-image:url("+httpContext.convertURL( Form.getBackground())+")") ;
      }
      httpContext.writeText( "\""+FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" onsubmit=\"try{return gx.csv.validForm()}catch(e){return true;}\" name=\"MAINFORM\" method=\"post\" action=\""+formatLink("gx0050") + "?" + GXutil.URLEncode(GXutil.ltrim(GXutil.str(AV12pdetfac_codigo,4,0)))+"\" class=\""+"Form"+"\">") ;
      GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
   }

   public void renderHtmlCloseForm( )
   {
      /* Send hidden variables. */
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCDETFAC_CODIGO", GXutil.ltrim( localUtil.ntoc( AV6cdetfac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCDETFAC_CANTIDAD", GXutil.ltrim( localUtil.ntoc( AV7cdetfac_cantidad, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCDETFAC_PRECIO", GXutil.ltrim( localUtil.ntoc( AV8cdetfac_precio, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCDETFAC_TOTAL", GXutil.ltrim( localUtil.ntoc( AV9cdetfac_total, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCFAC_CODIGO", GXutil.ltrim( localUtil.ntoc( AV10cfac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GXH_vCPRO_CODIGO", GXutil.ltrim( localUtil.ntoc( AV11cpro_codigo, (byte)(4), (byte)(0), ",", "")));
      /* Send saved values. */
      GxWebStd.gx_hidden_field( httpContext, "nRC_Grid1", GXutil.ltrim( localUtil.ntoc( nRC_Grid1, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "vPDETFAC_CODIGO", GXutil.ltrim( localUtil.ntoc( AV12pdetfac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GRID1_nFirstRecordOnPage", GXutil.ltrim( localUtil.ntoc( GRID1_nFirstRecordOnPage, (byte)(6), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GRID1_nEOF", GXutil.ltrim( localUtil.ntoc( GRID1_nEOF, (byte)(1), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      httpContext.writeTextNL( "</form>") ;
      include_jscripts( ) ;
   }

   public void renderHtmlContent( )
   {
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
      {
         we082( ) ;
      }
   }

   public void dispatchEvents( )
   {
      evt082( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public String getPgmname( )
   {
      return "Gx0050" ;
   }

   public String getPgmdesc( )
   {
      return "Selection List detalle_factura" ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("gx0050") + "?" + GXutil.URLEncode(GXutil.ltrim(GXutil.str(AV12pdetfac_codigo,4,0))) ;
   }

   public void wb080( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! wbLoad )
      {
         if ( nGXWrapped == 1 )
         {
            renderHtmlHeaders( ) ;
            renderHtmlOpenForm( ) ;
         }
         wb_table1_2_082( true) ;
      }
      else
      {
         wb_table1_2_082( false) ;
      }
      return  ;
   }

   public void wb_table1_2_082e( boolean wbgen )
   {
      if ( wbgen )
      {
      }
      wbLoad = true ;
   }

   public void start082( )
   {
      wbLoad = false ;
      wbEnd = 0 ;
      wbStart = 0 ;
      Form.getMeta().addItem("Generator", "GeneXus Java", (short)(0)) ;
      Form.getMeta().addItem("Version", "10_1_8-58720", (short)(0)) ;
      Form.getMeta().addItem("Description", "Selection List detalle_factura", (short)(0)) ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      wbErr = false ;
      strup080( ) ;
   }

   public void ws082( )
   {
      start082( ) ;
      evt082( ) ;
   }

   public void evt082( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) && ! wbErr )
         {
            /* Read Web Panel buttons. */
            sEvt = httpContext.cgiGet( "_EventName") ;
            EvtGridId = httpContext.cgiGet( "_EventGridId") ;
            EvtRowId = httpContext.cgiGet( "_EventRowId") ;
            if ( GXutil.len( sEvt) > 0 )
            {
               sEvtType = GXutil.left( sEvt, 1) ;
               sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
               /* Check if conditions changed and reset current page numbers */
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCDETFAC_CODIGO"), ",", ".") != AV6cdetfac_codigo )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCDETFAC_CANTIDAD"), ",", ".") != AV7cdetfac_cantidad )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCDETFAC_PRECIO"), ",", ".") != AV8cdetfac_precio )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCDETFAC_TOTAL"), ",", ".") != AV9cdetfac_total )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCFAC_CODIGO"), ",", ".") != AV10cfac_codigo )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCPRO_CODIGO"), ",", ".") != AV11cpro_codigo )
               {
                  GRID1_nFirstRecordOnPage = 0 ;
               }
               if ( GXutil.strcmp(sEvtType, "M") != 0 )
               {
                  if ( GXutil.strcmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = GXutil.right( sEvt, 1) ;
                     if ( GXutil.strcmp(sEvtType, ".") == 0 )
                     {
                        sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                        if ( GXutil.strcmp(sEvt, "RFR") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           dynload_actions( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           dynload_actions( ) ;
                        }
                        else if ( GXutil.strcmp(sEvt, "GRID1PAGING") == 0 )
                        {
                           httpContext.wbHandled = (byte)(1) ;
                           sEvt = httpContext.cgiGet( "GRID1PAGING") ;
                           if ( GXutil.strcmp(sEvt, "FIRST") == 0 )
                           {
                              subgrid1_firstpage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "PREV") == 0 )
                           {
                              subgrid1_previouspage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "NEXT") == 0 )
                           {
                              subgrid1_nextpage( ) ;
                           }
                           else if ( GXutil.strcmp(sEvt, "LAST") == 0 )
                           {
                              subgrid1_lastpage( ) ;
                           }
                        }
                     }
                     else
                     {
                        sEvtType = GXutil.right( sEvt, 4) ;
                        sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-4) ;
                        if ( ( GXutil.strcmp(GXutil.left( sEvt, 5), "START") == 0 ) || ( GXutil.strcmp(GXutil.left( sEvt, 4), "LOAD") == 0 ) || ( GXutil.strcmp(GXutil.left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_46_idx = (short)(GXutil.lval( sEvtType)) ;
                           sGXsfl_46_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_46_idx, 4, 0)), (short)(4), "0") ;
                           edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_46_idx ;
                           edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_46_idx ;
                           edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_46_idx ;
                           edtdetfac_precio_Internalname = "DETFAC_PRECIO_"+sGXsfl_46_idx ;
                           edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_46_idx ;
                           edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_46_idx ;
                           edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_46_idx ;
                           AV5LinkSelection = httpContext.cgiGet( "GXimg"+edtavLinkselection_Internalname) ;
                           A21detfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_codigo_Internalname), ",", ".")) ;
                           A22detfac_cantidad = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_cantidad_Internalname), ",", ".")) ;
                           n22detfac_cantidad = false ;
                           A23detfac_precio = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_precio_Internalname), ",", ".")) ;
                           n23detfac_precio = false ;
                           A24detfac_total = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_total_Internalname), ",", ".")) ;
                           n24detfac_total = false ;
                           A14fac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtfac_codigo_Internalname), ",", ".")) ;
                           A10pro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".")) ;
                           sEvtType = GXutil.right( sEvt, 1) ;
                           if ( GXutil.strcmp(sEvtType, ".") == 0 )
                           {
                              sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                              if ( GXutil.strcmp(sEvt, "START") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                                 /* Execute user event: e11082 */
                                 e11082 ();
                              }
                              else if ( GXutil.strcmp(sEvt, "LOAD") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                                 /* Execute user event: e12082 */
                                 e12082 ();
                              }
                              else if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false ;
                                    /* Set Refresh If Cdetfac_codigo Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCDETFAC_CODIGO"), ",", ".") != AV6cdetfac_codigo )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cdetfac_cantidad Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCDETFAC_CANTIDAD"), ",", ".") != AV7cdetfac_cantidad )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cdetfac_precio Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCDETFAC_PRECIO"), ",", ".") != AV8cdetfac_precio )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cdetfac_total Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCDETFAC_TOTAL"), ",", ".") != AV9cdetfac_total )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cfac_codigo Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCFAC_CODIGO"), ",", ".") != AV10cfac_codigo )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    /* Set Refresh If Cpro_codigo Changed */
                                    if ( localUtil.ctol( httpContext.cgiGet( "GXH_vCPRO_CODIGO"), ",", ".") != AV11cpro_codigo )
                                    {
                                       Rfr0gs = true ;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: e13082 */
                                       e13082 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                              {
                                 httpContext.wbHandled = (byte)(1) ;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  httpContext.wbHandled = (byte)(1) ;
               }
            }
         }
      }
   }

   public void we082( )
   {
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         Rfr0gs = true ;
         refresh( ) ;
         if ( ! GxWebStd.gx_redirect( httpContext) )
         {
            if ( nGXWrapped == 1 )
            {
               renderHtmlCloseForm( ) ;
            }
         }
      }
   }

   public void pa082( )
   {
      if ( nDonePA == 0 )
      {
         GX_FocusControl = edtavCdetfac_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         nDonePA = (byte)(1) ;
      }
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void gxnrgrid1_newrow( short nRC_Grid1 ,
                                 short nGXsfl_46_idx ,
                                 String sGXsfl_46_idx )
   {
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_46_idx ;
      edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_46_idx ;
      edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_46_idx ;
      edtdetfac_precio_Internalname = "DETFAC_PRECIO_"+sGXsfl_46_idx ;
      edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_46_idx ;
      edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_46_idx ;
      edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_46_idx ;
      while ( nGXsfl_46_idx <= nRC_Grid1 )
      {
         sendrow_462( ) ;
         nGXsfl_46_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_46_idx+1>subgrid1_recordsperpage( )) ? 1 : nGXsfl_46_idx+1)) ;
         sGXsfl_46_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_46_idx, 4, 0)), (short)(4), "0") ;
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_46_idx ;
         edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_46_idx ;
         edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_46_idx ;
         edtdetfac_precio_Internalname = "DETFAC_PRECIO_"+sGXsfl_46_idx ;
         edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_46_idx ;
         edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_46_idx ;
         edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_46_idx ;
      }
      httpContext.GX_webresponse.addString(Grid1Container.ToJavascriptSource());
      /* End function gxnrGrid1_newrow */
   }

   public void gxgrgrid1_refresh( int Grid1_PageSize46 ,
                                  short AV6cdetfac_codigo ,
                                  short AV7cdetfac_cantidad ,
                                  short AV8cdetfac_precio ,
                                  short AV9cdetfac_total ,
                                  short AV10cfac_codigo ,
                                  short AV11cpro_codigo )
   {
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.disableOutput();
      subGrid1_Rows = (short)(Grid1_PageSize46) ;
      rf082( ) ;
      httpContext.enableOutput();
      httpContext.GX_webresponse.addString(Grid1Container.ToJavascriptSource());
      /* End function gxgrGrid1_refresh */
   }

   public void refresh( )
   {
      rf082( ) ;
      /* End function Refresh */
   }

   public void rf082( )
   {
      Grid1Container.setPageSize( subgrid1_recordsperpage( ) );
      wbStart = (short)(46) ;
      nGXsfl_46_idx = (short)(1) ;
      sGXsfl_46_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_46_idx, 4, 0)), (short)(4), "0") ;
      edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_46_idx ;
      edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_46_idx ;
      edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_46_idx ;
      edtdetfac_precio_Internalname = "DETFAC_PRECIO_"+sGXsfl_46_idx ;
      edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_46_idx ;
      edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_46_idx ;
      edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_46_idx ;
      if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_46_idx ;
         edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_46_idx ;
         edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_46_idx ;
         edtdetfac_precio_Internalname = "DETFAC_PRECIO_"+sGXsfl_46_idx ;
         edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_46_idx ;
         edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_46_idx ;
         edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_46_idx ;
         /* Using cursor H00082 */
         pr_default.execute(0, new Object[] {new Short(AV6cdetfac_codigo), new Short(AV7cdetfac_cantidad), new Short(AV8cdetfac_precio), new Short(AV9cdetfac_total), new Short(AV10cfac_codigo), new Short(AV11cpro_codigo)});
         nGXsfl_46_idx = (short)(1) ;
         GRID1_nEOF = (byte)(0) ;
         while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( 10 == 0 ) || ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subgrid1_recordsperpage( ) ) ) ) )
         {
            A10pro_codigo = H00082_A10pro_codigo[0] ;
            A14fac_codigo = H00082_A14fac_codigo[0] ;
            A24detfac_total = H00082_A24detfac_total[0] ;
            n24detfac_total = H00082_n24detfac_total[0] ;
            A23detfac_precio = H00082_A23detfac_precio[0] ;
            n23detfac_precio = H00082_n23detfac_precio[0] ;
            A22detfac_cantidad = H00082_A22detfac_cantidad[0] ;
            n22detfac_cantidad = H00082_n22detfac_cantidad[0] ;
            A21detfac_codigo = H00082_A21detfac_codigo[0] ;
            /* Execute user event: e12082 */
            e12082 ();
            pr_default.readNext(0);
         }
         GRID1_nEOF = (byte)(((pr_default.getStatus(0) == 101) ? 1 : 0)) ;
         pr_default.close(0);
         wbEnd = (short)(46) ;
         wb080( ) ;
      }
   }

   public int subgrid1_pagecount( )
   {
      GRID1_nRecordCount = subgrid1_recordcount( ) ;
      if ( ((int)(GRID1_nRecordCount) % (subgrid1_recordsperpage( ))) == 0 )
      {
         return (int)(GXutil.Int( GRID1_nRecordCount/ (double) (subgrid1_recordsperpage( )))) ;
      }
      return (int)(GXutil.Int( GRID1_nRecordCount/ (double) (subgrid1_recordsperpage( )))+1) ;
   }

   public int subgrid1_recordcount( )
   {
      /* Using cursor H00083 */
      pr_default.execute(1, new Object[] {new Short(AV6cdetfac_codigo), new Short(AV7cdetfac_cantidad), new Short(AV8cdetfac_precio), new Short(AV9cdetfac_total), new Short(AV10cfac_codigo), new Short(AV11cpro_codigo)});
      GRID1_nRecordCount = H00083_AGRID1_nRecordCount[0] ;
      pr_default.close(1);
      return GRID1_nRecordCount ;
   }

   public int subgrid1_recordsperpage( )
   {
      if ( 10 > 0 )
      {
         if ( 1 > 0 )
         {
            return 10*1 ;
         }
         else
         {
            return 10 ;
         }
      }
      return -1 ;
   }

   public int subgrid1_currentpage( )
   {
      return (int)(GXutil.Int( GRID1_nFirstRecordOnPage/ (double) (subgrid1_recordsperpage( )))+1) ;
   }

   public short subgrid1_firstpage( )
   {
      GRID1_nFirstRecordOnPage = 0 ;
      return (short)(0) ;
   }

   public short subgrid1_nextpage( )
   {
      GRID1_nRecordCount = subgrid1_recordcount( ) ;
      if ( ( GRID1_nRecordCount >= subgrid1_recordsperpage( ) ) && ( GRID1_nEOF == 0 ) )
      {
         GRID1_nFirstRecordOnPage = (int)(GRID1_nFirstRecordOnPage+subgrid1_recordsperpage( )) ;
      }
      else
      {
         return (short)(2) ;
      }
      return (short)(0) ;
   }

   public short subgrid1_previouspage( )
   {
      if ( GRID1_nFirstRecordOnPage >= subgrid1_recordsperpage( ) )
      {
         GRID1_nFirstRecordOnPage = (int)(GRID1_nFirstRecordOnPage-subgrid1_recordsperpage( )) ;
      }
      else
      {
         return (short)(2) ;
      }
      return (short)(0) ;
   }

   public short subgrid1_lastpage( )
   {
      GRID1_nRecordCount = subgrid1_recordcount( ) ;
      if ( GRID1_nRecordCount > subgrid1_recordsperpage( ) )
      {
         if ( ((int)(GRID1_nRecordCount) % (subgrid1_recordsperpage( ))) == 0 )
         {
            GRID1_nFirstRecordOnPage = (int)(GRID1_nRecordCount-subgrid1_recordsperpage( )) ;
         }
         else
         {
            GRID1_nFirstRecordOnPage = (int)(GRID1_nRecordCount-((int)(GRID1_nRecordCount) % (subgrid1_recordsperpage( )))) ;
         }
      }
      else
      {
         GRID1_nFirstRecordOnPage = 0 ;
      }
      return (short)(0) ;
   }

   public int subgrid1_gotopage( int nPageNo )
   {
      if ( nPageNo > 0 )
      {
         GRID1_nFirstRecordOnPage = (int)(subgrid1_recordsperpage( )*(nPageNo-1)) ;
      }
      else
      {
         GRID1_nFirstRecordOnPage = 0 ;
      }
      return 0 ;
   }

   public void strup080( )
   {
      /* Before Start, stand alone formulas. */
      Gx_err = (short)(0) ;
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      /* Execute user event: e11082 */
      e11082 ();
      httpContext.wbGlbDoneStart = (byte)(1) ;
      /* After Start, stand alone formulas. */
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read saved SDTs. */
         /* Read variables values. */
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCdetfac_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCdetfac_codigo_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCDETFAC_CODIGO");
            GX_FocusControl = edtavCdetfac_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV6cdetfac_codigo = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV6cdetfac_codigo", GXutil.ltrim( GXutil.str( AV6cdetfac_codigo, 4, 0)));
         }
         else
         {
            AV6cdetfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtavCdetfac_codigo_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV6cdetfac_codigo", GXutil.ltrim( GXutil.str( AV6cdetfac_codigo, 4, 0)));
         }
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCdetfac_cantidad_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCdetfac_cantidad_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCDETFAC_CANTIDAD");
            GX_FocusControl = edtavCdetfac_cantidad_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV7cdetfac_cantidad = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV7cdetfac_cantidad", GXutil.ltrim( GXutil.str( AV7cdetfac_cantidad, 4, 0)));
         }
         else
         {
            AV7cdetfac_cantidad = (short)(localUtil.ctol( httpContext.cgiGet( edtavCdetfac_cantidad_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV7cdetfac_cantidad", GXutil.ltrim( GXutil.str( AV7cdetfac_cantidad, 4, 0)));
         }
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCdetfac_precio_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCdetfac_precio_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCDETFAC_PRECIO");
            GX_FocusControl = edtavCdetfac_precio_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV8cdetfac_precio = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV8cdetfac_precio", GXutil.ltrim( GXutil.str( AV8cdetfac_precio, 4, 0)));
         }
         else
         {
            AV8cdetfac_precio = (short)(localUtil.ctol( httpContext.cgiGet( edtavCdetfac_precio_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV8cdetfac_precio", GXutil.ltrim( GXutil.str( AV8cdetfac_precio, 4, 0)));
         }
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCdetfac_total_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCdetfac_total_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCDETFAC_TOTAL");
            GX_FocusControl = edtavCdetfac_total_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV9cdetfac_total = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV9cdetfac_total", GXutil.ltrim( GXutil.str( AV9cdetfac_total, 4, 0)));
         }
         else
         {
            AV9cdetfac_total = (short)(localUtil.ctol( httpContext.cgiGet( edtavCdetfac_total_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV9cdetfac_total", GXutil.ltrim( GXutil.str( AV9cdetfac_total, 4, 0)));
         }
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCfac_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCfac_codigo_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCFAC_CODIGO");
            GX_FocusControl = edtavCfac_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV10cfac_codigo = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV10cfac_codigo", GXutil.ltrim( GXutil.str( AV10cfac_codigo, 4, 0)));
         }
         else
         {
            AV10cfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtavCfac_codigo_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV10cfac_codigo", GXutil.ltrim( GXutil.str( AV10cfac_codigo, 4, 0)));
         }
         if ( ( ( localUtil.ctol( httpContext.cgiGet( edtavCpro_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtavCpro_codigo_Internalname), ",", ".") > 9999 ) ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "vCPRO_CODIGO");
            GX_FocusControl = edtavCpro_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            wbErr = true ;
            AV11cpro_codigo = (short)(0) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV11cpro_codigo", GXutil.ltrim( GXutil.str( AV11cpro_codigo, 4, 0)));
         }
         else
         {
            AV11cpro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtavCpro_codigo_Internalname), ",", ".")) ;
            httpContext.ajax_rsp_assign_attri("", false, "AV11cpro_codigo", GXutil.ltrim( GXutil.str( AV11cpro_codigo, 4, 0)));
         }
         /* Read saved values. */
         nRC_Grid1 = (short)(localUtil.ctol( httpContext.cgiGet( "nRC_Grid1"), ",", ".")) ;
         AV12pdetfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "vPDETFAC_CODIGO"), ",", ".")) ;
         GRID1_nFirstRecordOnPage = (int)(localUtil.ctol( httpContext.cgiGet( "GRID1_nFirstRecordOnPage"), ",", ".")) ;
         GRID1_nEOF = (byte)(localUtil.ctol( httpContext.cgiGet( "GRID1_nEOF"), ",", ".")) ;
         /* Read subfile selected row values. */
         /* Read hidden variables. */
      }
      else
      {
         dynload_actions( ) ;
      }
   }

   protected void GXStart( )
   {
      /* Execute user event: e11082 */
      e11082 ();
      if (returnInSub) return;
   }

   public void e11082( )
   {
      /* Start Routine */
      Form.setCaption( GXutil.format( "Lista de Selecci�n %1", "detalle_factura", "", "", "", "", "", "", "", "") );
      httpContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.getCaption());
   }

   private void e12082( )
   {
      /* Load Routine */
      AV5LinkSelection = context.getHttpContext().getImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", "GeneXusX") ;
      if ( ( subGrid1_Islastpage == 1 ) || ( 10 == 0 ) || ( ( GRID1_nCurrentRecord >= GRID1_nFirstRecordOnPage ) && ( GRID1_nCurrentRecord < GRID1_nFirstRecordOnPage + subgrid1_recordsperpage( ) ) ) )
      {
         sendrow_462( ) ;
      }
      GRID1_nCurrentRecord = (int)(GRID1_nCurrentRecord+1) ;
   }

   public void GXEnter( )
   {
      /* Execute user event: e13082 */
      e13082 ();
      if (returnInSub) return;
   }

   public void e13082( )
   {
      /* Enter Routine */
      AV12pdetfac_codigo = A21detfac_codigo ;
      httpContext.ajax_rsp_assign_attri("", false, "AV12pdetfac_codigo", GXutil.ltrim( GXutil.str( AV12pdetfac_codigo, 4, 0)));
      httpContext.setWebReturnParms(new Object[] {new Short(AV12pdetfac_codigo)});
      httpContext.nUserReturn = (byte)(1) ;
      returnInSub = true;
      if (true) return;
   }

   public void wb_table1_2_082( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td style=\"text-align:"+httpContext.getCssProperty( "Align", "center")+"\">") ;
         ClassString = "ErrorViewer" ;
         StyleString = "" ;
         GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), StyleString, ClassString, "", "false");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Control Group */
         ClassString = "FieldSet" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroup1_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"Filters"+"</legend>") ;
         wb_table2_9_082( true) ;
      }
      else
      {
         wb_table2_9_082( false) ;
      }
      return  ;
   }

   public void wb_table2_9_082e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Control Group */
         ClassString = "FieldSet" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroup2_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"Lista de Selecci�n"+"</legend>") ;
         wb_table3_43_082( true) ;
      }
      else
      {
         wb_table3_43_082( false) ;
      }
      return  ;
   }

   public void wb_table3_43_082e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_082e( true) ;
      }
      else
      {
         wb_table1_2_082e( false) ;
      }
   }

   public void wb_table3_43_082( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td colspan=\"3\" >") ;
         /*  Grid Control  */
         Grid1Container.SetWrapped(nGXWrapped);
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<div id=\""+"Grid1Container"+"DivS\" gxgridid=\"46\">") ;
            sStyleString = "" ;
            GxWebStd.gx_table_start( httpContext, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 0, sStyleString, "", 0);
            /* Subfile titles */
            httpContext.writeText( "<tr") ;
            httpContext.writeTextNL( ">") ;
            if ( subGrid1_Backcolorstyle == 0 )
            {
               subGrid1_Titlebackstyle = (byte)(0) ;
               if ( GXutil.len( subGrid1_Class) > 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Title" ;
               }
            }
            else
            {
               subGrid1_Titlebackstyle = (byte)(1) ;
               if ( subGrid1_Backcolorstyle == 1 )
               {
                  subGrid1_Titlebackcolor = subGrid1_Allbackcolor ;
                  if ( GXutil.len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"UniformTitle" ;
                  }
               }
               else
               {
                  if ( GXutil.len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title" ;
                  }
               }
            }
            httpContext.writeText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "detfac_codigo") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "detfac_cantidad") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "detfac_precio") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "detfac_total") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "C�digo de la factura") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid1_Linesclass+"\" "+" style=\""+((-1==0) ? "display:none;" : "")+""+"\" "+">") ;
            httpContext.writeValue( "C�digo del producto") ;
            httpContext.writeTextNL( "</th>") ;
            httpContext.writeTextNL( "</tr>") ;
            Grid1Container.AddObjectProperty("GridName", "Grid1");
         }
         else
         {
            if ( httpContext.isAjaxCallMode( ) )
            {
               Grid1Container = new com.genexus.webpanels.GXWebGrid(context);
            }
            else
            {
               Grid1Container.Clear();
            }
            Grid1Container.SetWrapped(nGXWrapped);
            Grid1Container.AddObjectProperty("GridName", "Grid1");
            Grid1Container.AddObjectProperty("Class", "Grid");
            Grid1Container.AddObjectProperty("Cellpadding", GXutil.ltrim( localUtil.ntoc( 1, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Cellspacing", GXutil.ltrim( localUtil.ntoc( 0, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("Backcolorstyle", GXutil.ltrim( localUtil.ntoc( subGrid1_Backcolorstyle, (byte)(1), (byte)(0), ".", "")));
            Grid1Container.AddObjectProperty("CmpContext", "");
            Grid1Container.AddObjectProperty("InMasterPage", "false");
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", httpContext.convertURL( AV5LinkSelection));
            Grid1Column.AddObjectProperty("Link", GXutil.rtrim( edtavLinkselection_Link));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A22detfac_cantidad, (byte)(4), (byte)(0), ".", "")));
            Grid1Column.AddObjectProperty("Link", GXutil.rtrim( edtdetfac_cantidad_Link));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A23detfac_precio, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A24detfac_total, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Column = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
            Grid1Column.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ".", "")));
            Grid1Container.AddColumnProperties(Grid1Column);
            Grid1Container.AddObjectProperty("Allowselection", "false");
            Grid1Container.AddObjectProperty("Allowcollapsing", ((subGrid1_Allowcollapsing==1) ? "true" : "false"));
            Grid1Container.AddObjectProperty("Collapsed", GXutil.ltrim( localUtil.ntoc( subGrid1_Collapsed, (byte)(9), (byte)(0), ".", "")));
         }
      }
      if ( wbEnd == 46 )
      {
         wbEnd = (short)(0) ;
         nRC_Grid1 = (short)(nGXsfl_46_idx-1) ;
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "</table>") ;
            httpContext.writeText( "</div>") ;
         }
         else
         {
            Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
            Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
            sStyleString = " style=\"display:none;\"" ;
            sStyleString = "" ;
            httpContext.writeText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
            httpContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
            GxWebStd.gx_hidden_field( httpContext, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
            if ( httpContext.isAjaxRequest( ) )
            {
               GxWebStd.gx_hidden_field( httpContext, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
            }
            else
            {
               httpContext.writeText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'>") ;
            }
         }
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td style=\"text-align:"+httpContext.getCssProperty( "Align", "right")+"\">") ;
         wb_table4_56_082( true) ;
      }
      else
      {
         wb_table4_56_082( false) ;
      }
      return  ;
   }

   public void wb_table4_56_082e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_43_082e( true) ;
      }
      else
      {
         wb_table3_43_082e( false) ;
      }
   }

   public void wb_table4_56_082( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable4_Internalname, tblTable4_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"" ;
         ClassString = "BtnCancel" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+GXutil.str( 46, 3, 0)+","+"null"+");", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "rounded", "ECANCEL.", TempTags, "", httpContext.getButtonType( ), "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table4_56_082e( true) ;
      }
      else
      {
         wb_table4_56_082e( false) ;
      }
   }

   public void wb_table2_9_082( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockdetfac_codigo_Internalname, "detfac_codigo", "", "", lblTextblockdetfac_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV6cdetfac_codigo", GXutil.ltrim( GXutil.str( AV6cdetfac_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'" + sGXsfl_46_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCdetfac_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( AV6cdetfac_codigo, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV6cdetfac_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV6cdetfac_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(14);\"", "", "", "", "", edtavCdetfac_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockdetfac_cantidad_Internalname, "detfac_cantidad", "", "", lblTextblockdetfac_cantidad_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV7cdetfac_cantidad", GXutil.ltrim( GXutil.str( AV7cdetfac_cantidad, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'" + sGXsfl_46_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCdetfac_cantidad_Internalname, GXutil.ltrim( localUtil.ntoc( AV7cdetfac_cantidad, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV7cdetfac_cantidad), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV7cdetfac_cantidad), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(19);\"", "", "", "", "", edtavCdetfac_cantidad_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockdetfac_precio_Internalname, "detfac_precio", "", "", lblTextblockdetfac_precio_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV8cdetfac_precio", GXutil.ltrim( GXutil.str( AV8cdetfac_precio, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_46_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCdetfac_precio_Internalname, GXutil.ltrim( localUtil.ntoc( AV8cdetfac_precio, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV8cdetfac_precio), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV8cdetfac_precio), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(24);\"", "", "", "", "", edtavCdetfac_precio_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockdetfac_total_Internalname, "detfac_total", "", "", lblTextblockdetfac_total_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV9cdetfac_total", GXutil.ltrim( GXutil.str( AV9cdetfac_total, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'" + sGXsfl_46_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCdetfac_total_Internalname, GXutil.ltrim( localUtil.ntoc( AV9cdetfac_total, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV9cdetfac_total), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV9cdetfac_total), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(29);\"", "", "", "", "", edtavCdetfac_total_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_codigo_Internalname, "C�digo de la factura", "", "", lblTextblockfac_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV10cfac_codigo", GXutil.ltrim( GXutil.str( AV10cfac_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_46_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCfac_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( AV10cfac_codigo, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV10cfac_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV10cfac_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(34);\"", "", "", "", "", edtavCfac_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockpro_codigo_Internalname, "C�digo del producto", "", "", lblTextblockpro_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "AV11cpro_codigo", GXutil.ltrim( GXutil.str( AV11cpro_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'" + sGXsfl_46_idx + "',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtavCpro_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( AV11cpro_codigo, (byte)(4), (byte)(0), ",", "")), ((1!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(AV11cpro_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(AV11cpro_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(39);\"", "", "", "", "", edtavCpro_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, 1, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_Gx0050.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_9_082e( true) ;
      }
      else
      {
         wb_table2_9_082e( false) ;
      }
   }

   public void setparameters( Object[] obj )
   {
      AV12pdetfac_codigo = ((Number) GXutil.testNumericType( getParm(obj,0), TypeConstants.SHORT)).shortValue() ;
      httpContext.ajax_rsp_assign_attri("", false, "AV12pdetfac_codigo", GXutil.ltrim( GXutil.str( AV12pdetfac_codigo, 4, 0)));
   }

   public String getresponse( String sGXDynURL )
   {
      initialize_properties( ) ;
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      sDynURL = sGXDynURL ;
      nGotPars = 1 ;
      nGXWrapped = 1 ;
      httpContext.setWrapped(true);
      pa082( ) ;
      ws082( ) ;
      we082( ) ;
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
      httpContext.setWrapped(false);
      httpContext.GX_msglist = BackMsgLst ;
      return ((java.io.ByteArrayOutputStream) httpContext.getOutputStream()).toString();
   }

   public void responsestatic( String sGXDynURL )
   {
   }

   public void define_styles( )
   {
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?12304431");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?58720");
      httpContext.AddJavascriptSource("gx0050.js", "?12304431");
      /* End function include_jscripts */
   }

   public void sendrow_462( )
   {
      wb080( ) ;
      if ( ( 10 * 1 == 0 ) || ( nGXsfl_46_idx <= subgrid1_recordsperpage( ) * 1 ) )
      {
         Grid1Row = GXWebRow.GetNew(context,Grid1Container) ;
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = (byte)(0) ;
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd" ;
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = (byte)(0) ;
            subGrid1_Backcolor = subGrid1_Allbackcolor ;
            httpContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGrid1_Backcolor, 9, 0)));
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform" ;
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = (byte)(1) ;
            if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd" ;
            }
            subGrid1_Backcolor = (int)(0xF0F0F0) ;
            httpContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGrid1_Backcolor, 9, 0)));
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = (byte)(1) ;
            if ( ((int)(nGXsfl_46_idx) % (2)) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0) ;
               httpContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGrid1_Backcolor, 9, 0)));
               if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even" ;
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0xF0F0F0) ;
               httpContext.ajax_rsp_assign_prop("", false, "Grid1ContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGrid1_Backcolor, 9, 0)));
               if ( GXutil.strcmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd" ;
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<tr ") ;
            httpContext.writeText( " class=\""+subGrid1_Linesclass+"\" style=\""+""+"\"") ;
            httpContext.writeText( " gxrow=\""+sGXsfl_46_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Static Bitmap Variable */
         edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+PrivateUtilities.encodeJSConstant( GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ",", "")))+"'"+"]);" ;
         ClassString = "Image" ;
         StyleString = "" ;
         Grid1Row.AddColumnProperties("bitmap", 1, httpContext.isAjaxCallMode( ), new Object[] {edtavLinkselection_Internalname,AV5LinkSelection,edtavLinkselection_Link,"","","GeneXusX",new Integer(-1),new Integer(1),"","Seleccionar",new Integer(0),new Integer(-1),new Integer(0),"px",new Integer(0),"px",new Integer(0),new Integer(0),new Integer(0),"","",StyleString,ClassString,"","","''",""});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtdetfac_codigo_Internalname,GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ",", "")),localUtil.format( DecimalUtil.doubleToDec(A21detfac_codigo), "ZZZ9"),"","","","","",edtdetfac_codigo_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(46),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         edtdetfac_cantidad_Link = "javascript:gx.popup.gxReturn(["+"'"+PrivateUtilities.encodeJSConstant( GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ",", "")))+"'"+"]);" ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtdetfac_cantidad_Internalname,GXutil.ltrim( localUtil.ntoc( A22detfac_cantidad, (byte)(4), (byte)(0), ",", "")),localUtil.format( DecimalUtil.doubleToDec(A22detfac_cantidad), "ZZZ9"),"","",edtdetfac_cantidad_Link,"","Seleccionar",edtdetfac_cantidad_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(46),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtdetfac_precio_Internalname,GXutil.ltrim( localUtil.ntoc( A23detfac_precio, (byte)(4), (byte)(0), ",", "")),localUtil.format( DecimalUtil.doubleToDec(A23detfac_precio), "ZZZ9"),"","","","","",edtdetfac_precio_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(46),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtdetfac_total_Internalname,GXutil.ltrim( localUtil.ntoc( A24detfac_total, (byte)(4), (byte)(0), ",", "")),localUtil.format( DecimalUtil.doubleToDec(A24detfac_total), "ZZZ9"),"","","","","",edtdetfac_total_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(46),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtfac_codigo_Internalname,GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ",", "")),localUtil.format( DecimalUtil.doubleToDec(A14fac_codigo), "ZZZ9"),"","","","","",edtfac_codigo_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(46),new Integer(1),new Integer(1),new Boolean(true),"right"});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            httpContext.writeText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((-1==0) ? "display:none;" : "")+"\">") ;
         }
         /* Single line edit */
         ClassString = "Attribute" ;
         StyleString = "" ;
         ROClassString = ClassString ;
         Grid1Row.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtpro_codigo_Internalname,GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ",", "")),localUtil.format( DecimalUtil.doubleToDec(A10pro_codigo), "ZZZ9"),"","","","","",edtpro_codigo_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(0),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(46),new Integer(1),new Integer(1),new Boolean(true),"right"});
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_46_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_46_idx+1>subgrid1_recordsperpage( )) ? 1 : nGXsfl_46_idx+1)) ;
         sGXsfl_46_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_46_idx, 4, 0)), (short)(4), "0") ;
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_46_idx ;
         edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_46_idx ;
         edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_46_idx ;
         edtdetfac_precio_Internalname = "DETFAC_PRECIO_"+sGXsfl_46_idx ;
         edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_46_idx ;
         edtfac_codigo_Internalname = "FAC_CODIGO_"+sGXsfl_46_idx ;
         edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_46_idx ;
      }
      /* End function sendrow_462 */
   }

   public void init_default_properties( )
   {
      lblTextblockdetfac_codigo_Internalname = "TEXTBLOCKDETFAC_CODIGO" ;
      edtavCdetfac_codigo_Internalname = "vCDETFAC_CODIGO" ;
      lblTextblockdetfac_cantidad_Internalname = "TEXTBLOCKDETFAC_CANTIDAD" ;
      edtavCdetfac_cantidad_Internalname = "vCDETFAC_CANTIDAD" ;
      lblTextblockdetfac_precio_Internalname = "TEXTBLOCKDETFAC_PRECIO" ;
      edtavCdetfac_precio_Internalname = "vCDETFAC_PRECIO" ;
      lblTextblockdetfac_total_Internalname = "TEXTBLOCKDETFAC_TOTAL" ;
      edtavCdetfac_total_Internalname = "vCDETFAC_TOTAL" ;
      lblTextblockfac_codigo_Internalname = "TEXTBLOCKFAC_CODIGO" ;
      edtavCfac_codigo_Internalname = "vCFAC_CODIGO" ;
      lblTextblockpro_codigo_Internalname = "TEXTBLOCKPRO_CODIGO" ;
      edtavCpro_codigo_Internalname = "vCPRO_CODIGO" ;
      tblTable2_Internalname = "TABLE2" ;
      grpGroup1_Internalname = "GROUP1" ;
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      tblTable4_Internalname = "TABLE4" ;
      tblTable3_Internalname = "TABLE3" ;
      grpGroup2_Internalname = "GROUP2" ;
      tblTable1_Internalname = "TABLE1" ;
      Form.setInternalname( "FORM" );
      subGrid1_Internalname = "GRID1" ;
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      edtpro_codigo_Jsonclick = "" ;
      edtfac_codigo_Jsonclick = "" ;
      edtdetfac_total_Jsonclick = "" ;
      edtdetfac_precio_Jsonclick = "" ;
      edtdetfac_cantidad_Jsonclick = "" ;
      edtdetfac_codigo_Jsonclick = "" ;
      edtavCpro_codigo_Jsonclick = "" ;
      edtavCfac_codigo_Jsonclick = "" ;
      edtavCdetfac_total_Jsonclick = "" ;
      edtavCdetfac_precio_Jsonclick = "" ;
      edtavCdetfac_cantidad_Jsonclick = "" ;
      edtavCdetfac_codigo_Jsonclick = "" ;
      subGrid1_Allowcollapsing = (byte)(0) ;
      edtdetfac_cantidad_Link = "" ;
      edtavLinkselection_Link = "" ;
      subGrid1_Class = "Grid" ;
      subGrid1_Backcolorstyle = (byte)(2) ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "Selection List detalle_factura" );
      httpContext.GX_msglist.setDisplaymode( (short)(1) );
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      sDynURL = "" ;
      FormProcess = "" ;
      GX_FocusControl = "" ;
      sPrefix = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      edtavLinkselection_Internalname = "" ;
      edtdetfac_codigo_Internalname = "" ;
      edtdetfac_cantidad_Internalname = "" ;
      edtdetfac_precio_Internalname = "" ;
      edtdetfac_total_Internalname = "" ;
      edtfac_codigo_Internalname = "" ;
      edtpro_codigo_Internalname = "" ;
      AV5LinkSelection = "" ;
      Grid1Container = new com.genexus.webpanels.GXWebGrid(context);
      scmdbuf = "" ;
      H00082_A10pro_codigo = new short[1] ;
      H00082_A14fac_codigo = new short[1] ;
      H00082_A24detfac_total = new short[1] ;
      H00082_n24detfac_total = new boolean[] {false} ;
      H00082_A23detfac_precio = new short[1] ;
      H00082_n23detfac_precio = new boolean[] {false} ;
      H00082_A22detfac_cantidad = new short[1] ;
      H00082_n22detfac_cantidad = new boolean[] {false} ;
      H00082_A21detfac_codigo = new short[1] ;
      H00083_AGRID1_nRecordCount = new int[1] ;
      sStyleString = "" ;
      ClassString = "" ;
      StyleString = "" ;
      subGrid1_Linesclass = "" ;
      GXt_char3 = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      GXt_char4 = "" ;
      GXt_char5 = "" ;
      Grid1Column = new com.genexus.webpanels.GXWebColumn();
      GXt_char6 = "" ;
      TempTags = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      lblTextblockdetfac_codigo_Jsonclick = "" ;
      lblTextblockdetfac_cantidad_Jsonclick = "" ;
      lblTextblockdetfac_precio_Jsonclick = "" ;
      lblTextblockdetfac_total_Jsonclick = "" ;
      lblTextblockfac_codigo_Jsonclick = "" ;
      lblTextblockpro_codigo_Jsonclick = "" ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      Grid1Row = new com.genexus.webpanels.GXWebRow();
      GXt_char7 = "" ;
      ROClassString = "" ;
      GXt_char8 = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new gx0050__default(),
         new Object[] {
             new Object[] {
            H00082_A10pro_codigo, H00082_A14fac_codigo, H00082_A24detfac_total, H00082_n24detfac_total, H00082_A23detfac_precio, H00082_n23detfac_precio, H00082_A22detfac_cantidad, H00082_n22detfac_cantidad, H00082_A21detfac_codigo
            }
            , new Object[] {
            H00083_AGRID1_nRecordCount
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte nGotPars ;
   private byte GxWebError ;
   private byte gxajaxcallmode ;
   private byte GRID1_nEOF ;
   private byte nDonePA ;
   private byte subGrid1_Backcolorstyle ;
   private byte subGrid1_Titlebackstyle ;
   private byte subGrid1_Allowcollapsing ;
   private byte subGrid1_Collapsed ;
   private byte nGXWrapped ;
   private byte subGrid1_Backstyle ;
   private short nRC_Grid1 ;
   private short nGXsfl_46_idx=1 ;
   private short Gx_err ;
   private short AV6cdetfac_codigo ;
   private short AV7cdetfac_cantidad ;
   private short AV8cdetfac_precio ;
   private short AV9cdetfac_total ;
   private short AV10cfac_codigo ;
   private short AV11cpro_codigo ;
   private short AV12pdetfac_codigo ;
   private short wbEnd ;
   private short wbStart ;
   private short A21detfac_codigo ;
   private short A22detfac_cantidad ;
   private short A23detfac_precio ;
   private short A24detfac_total ;
   private short A14fac_codigo ;
   private short A10pro_codigo ;
   private short subGrid1_Rows ;
   private int Grid1_PageSize46 ;
   private int GRID1_nFirstRecordOnPage ;
   private int subGrid1_Islastpage ;
   private int GRID1_nCurrentRecord ;
   private int GRID1_nRecordCount ;
   private int subGrid1_Titlebackcolor ;
   private int subGrid1_Allbackcolor ;
   private int idxLst ;
   private int subGrid1_Backcolor ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String sGXsfl_46_idx="0001" ;
   private String sDynURL ;
   private String FormProcess ;
   private String GX_FocusControl ;
   private String sPrefix ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String edtavLinkselection_Internalname ;
   private String edtdetfac_codigo_Internalname ;
   private String edtdetfac_cantidad_Internalname ;
   private String edtdetfac_precio_Internalname ;
   private String edtdetfac_total_Internalname ;
   private String edtfac_codigo_Internalname ;
   private String edtpro_codigo_Internalname ;
   private String edtavCdetfac_codigo_Internalname ;
   private String scmdbuf ;
   private String edtavCdetfac_cantidad_Internalname ;
   private String edtavCdetfac_precio_Internalname ;
   private String edtavCdetfac_total_Internalname ;
   private String edtavCfac_codigo_Internalname ;
   private String edtavCpro_codigo_Internalname ;
   private String sStyleString ;
   private String tblTable1_Internalname ;
   private String ClassString ;
   private String StyleString ;
   private String grpGroup1_Internalname ;
   private String grpGroup2_Internalname ;
   private String tblTable3_Internalname ;
   private String subGrid1_Internalname ;
   private String subGrid1_Class ;
   private String subGrid1_Linesclass ;
   private String GXt_char3 ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private String GXt_char4 ;
   private String GXt_char5 ;
   private String edtavLinkselection_Link ;
   private String edtdetfac_cantidad_Link ;
   private String GXt_char6 ;
   private String tblTable4_Internalname ;
   private String TempTags ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String tblTable2_Internalname ;
   private String lblTextblockdetfac_codigo_Internalname ;
   private String lblTextblockdetfac_codigo_Jsonclick ;
   private String edtavCdetfac_codigo_Jsonclick ;
   private String lblTextblockdetfac_cantidad_Internalname ;
   private String lblTextblockdetfac_cantidad_Jsonclick ;
   private String edtavCdetfac_cantidad_Jsonclick ;
   private String lblTextblockdetfac_precio_Internalname ;
   private String lblTextblockdetfac_precio_Jsonclick ;
   private String edtavCdetfac_precio_Jsonclick ;
   private String lblTextblockdetfac_total_Internalname ;
   private String lblTextblockdetfac_total_Jsonclick ;
   private String edtavCdetfac_total_Jsonclick ;
   private String lblTextblockfac_codigo_Internalname ;
   private String lblTextblockfac_codigo_Jsonclick ;
   private String edtavCfac_codigo_Jsonclick ;
   private String lblTextblockpro_codigo_Internalname ;
   private String lblTextblockpro_codigo_Jsonclick ;
   private String edtavCpro_codigo_Jsonclick ;
   private String GXt_char7 ;
   private String ROClassString ;
   private String edtdetfac_codigo_Jsonclick ;
   private String edtdetfac_cantidad_Jsonclick ;
   private String edtdetfac_precio_Jsonclick ;
   private String edtdetfac_total_Jsonclick ;
   private String edtfac_codigo_Jsonclick ;
   private String edtpro_codigo_Jsonclick ;
   private String GXt_char8 ;
   private boolean entryPointCalled ;
   private boolean wbLoad ;
   private boolean Rfr0gs ;
   private boolean wbErr ;
   private boolean n22detfac_cantidad ;
   private boolean n23detfac_precio ;
   private boolean n24detfac_total ;
   private boolean returnInSub ;
   private String AV5LinkSelection ;
   private com.genexus.webpanels.GXWebGrid Grid1Container ;
   private com.genexus.webpanels.GXWebRow Grid1Row ;
   private com.genexus.webpanels.GXWebColumn Grid1Column ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private com.genexus.webpanels.GXMasterPage MasterPageObj ;
   private IDataStoreProvider pr_default ;
   private short[] H00082_A10pro_codigo ;
   private short[] H00082_A14fac_codigo ;
   private short[] H00082_A24detfac_total ;
   private boolean[] H00082_n24detfac_total ;
   private short[] H00082_A23detfac_precio ;
   private boolean[] H00082_n23detfac_precio ;
   private short[] H00082_A22detfac_cantidad ;
   private boolean[] H00082_n22detfac_cantidad ;
   private short[] H00082_A21detfac_codigo ;
   private int[] H00083_AGRID1_nRecordCount ;
   private com.genexus.webpanels.GXWebForm Form ;
}

final  class gx0050__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("H00082", "SELECT [pro_codigo], [fac_codigo], [detfac_total], [detfac_precio], [detfac_cantidad], [detfac_codigo] FROM [detalle_factura] WITH (NOLOCK) WHERE ([detfac_codigo] >= ?) AND ([detfac_cantidad] >= ?) AND ([detfac_precio] >= ?) AND ([detfac_total] >= ?) AND ([fac_codigo] >= ?) AND ([pro_codigo] >= ?) ORDER BY [detfac_codigo]  OPTION (FAST 11)",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,11,0,false )
         ,new ForEachCursor("H00083", "SELECT COUNT(*) FROM [detalle_factura] WITH (NOLOCK) WHERE ([detfac_codigo] >= ?) AND ([detfac_cantidad] >= ?) AND ([detfac_precio] >= ?) AND ([detfac_total] >= ?) AND ([fac_codigo] >= ?) AND ([pro_codigo] >= ?) ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,1,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((short[]) buf[4])[0] = rslt.getShort(4) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((short[]) buf[6])[0] = rslt.getShort(5) ;
               ((boolean[]) buf[7])[0] = rslt.wasNull();
               ((short[]) buf[8])[0] = rslt.getShort(6) ;
               break;
            case 1 :
               ((int[]) buf[0])[0] = rslt.getInt(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               stmt.setShort(5, ((Number) parms[4]).shortValue());
               stmt.setShort(6, ((Number) parms[5]).shortValue());
               break;
            case 1 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               stmt.setShort(5, ((Number) parms[4]).shortValue());
               stmt.setShort(6, ((Number) parms[5]).shortValue());
               break;
      }
   }

}

