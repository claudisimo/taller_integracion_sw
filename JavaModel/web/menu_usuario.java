/*
               File: Menu_Usuario
        Description: Menu_ Usuario
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 10:57:20.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/menu_usuario")
public final  class menu_usuario extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new menu_usuario_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Menu_ Usuario";
   }

}

