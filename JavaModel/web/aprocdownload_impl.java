/*
               File: aprocdownload_impl
        Description: Proc Download
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:41:58.40
       Program type: Main program
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class aprocdownload_impl extends GXWebProcedure
{
   public aprocdownload_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public void webExecute( )
   {
      entryPointCalled = false ;
      gxfirstwebparm = httpContext.GetNextPar( ) ;
      /* GeneXus formulas */
      /* Output device settings */
      privateExecute();
   }

   private void privateExecute( )
   {
      if ( GXutil.strcmp(AV9session.getValue("rol"), "admin") == 0 )
      {
         AV10archivo = "manual_sistema.pdf" ;
         AV12ruta = "c:\\manuales\\manual_sistema.pdf" ;
      }
      else
      {
         AV10archivo = "manual_usuario.pdf" ;
         AV12ruta = "c:\\manuales\\manual_usuario.pdf" ;
      }
      if ( ! httpContext.isAjaxRequest( ) )
      {
         AV8HttpResponse.addHeader("Content-Type", "application/pdf");
      }
      if ( ! httpContext.isAjaxRequest( ) )
      {
         AV8HttpResponse.addHeader("Content-Disposition", "attachment;filename="+AV10archivo);
      }
      AV8HttpResponse.addFile(AV12ruta);
      if ( ! httpContext.isAjaxRequest( ) )
      {
         AV8HttpResponse.addHeader("Pragma", "public");
      }
      if ( ! httpContext.isAjaxRequest( ) )
      {
         AV8HttpResponse.addHeader("Cache-Control", "max-age=0");
      }
      if ( ! (GXutil.strcmp("", httpContext.wjLoc)==0) )
      {
         httpContext.redirect( httpContext.wjLoc );
         httpContext.wjLoc = "" ;
      }
      cleanup();
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      super.cleanup();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      gxfirstwebparm = "" ;
      AV9session = httpContext.getWebSession();
      AV10archivo = "" ;
      AV12ruta = "" ;
      AV8HttpResponse = httpContext.getHttpResponse();
      GXt_char1 = "" ;
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
   private String gxfirstwebparm ;
   private String AV10archivo ;
   private String AV12ruta ;
   private String GXt_char1 ;
   private boolean entryPointCalled ;
   private com.genexus.webpanels.WebSession AV9session ;
   private com.genexus.internet.HttpResponse AV8HttpResponse ;
}

