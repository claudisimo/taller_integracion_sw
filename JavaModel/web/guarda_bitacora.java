/*
               File: guarda_bitacora
        Description: guarda_bitacora
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:41:58.31
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;
import com.genexus.search.*;

public final  class guarda_bitacora extends GXProcedure
{
   public guarda_bitacora( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( guarda_bitacora.class ), "" );
   }

   public guarda_bitacora( int remoteHandle ,
                           ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public String executeUdp( String[] aP0 ,
                             java.util.Date[] aP1 )
   {
      guarda_bitacora.this.AV8bit_usuario = aP0[0];
      guarda_bitacora.this.AV9bit_fecha = aP1[0];
      guarda_bitacora.this.AV10bit_descripcion = aP2[0];
      guarda_bitacora.this.aP2 = new String[] {""};
      initialize();
      privateExecute();
      return aP2[0];
   }

   public void execute( String[] aP0 ,
                        java.util.Date[] aP1 ,
                        String[] aP2 )
   {
      execute_int(aP0, aP1, aP2);
   }

   private void execute_int( String[] aP0 ,
                             java.util.Date[] aP1 ,
                             String[] aP2 )
   {
      guarda_bitacora.this.AV8bit_usuario = aP0[0];
      this.aP0 = aP0;
      guarda_bitacora.this.AV9bit_fecha = aP1[0];
      this.aP1 = aP1;
      guarda_bitacora.this.AV10bit_descripcion = aP2[0];
      this.aP2 = aP2;
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
      privateExecute();
   }

   private void privateExecute( )
   {
      AV14GXLvl1 = (byte)(0) ;
      /* Using cursor P00012 */
      pr_default.execute(0);
      while ( (pr_default.getStatus(0) != 101) )
      {
         A43bit_codigo = P00012_A43bit_codigo[0] ;
         AV14GXLvl1 = (byte)(1) ;
         AV11bit_codigo = (short)(A43bit_codigo+1) ;
         /* Exit For each command. Update data (if necessary), close cursors & exit. */
         if (true) break;
         pr_default.readNext(0);
      }
      pr_default.close(0);
      if ( AV14GXLvl1 == 0 )
      {
         AV11bit_codigo = (short)(1) ;
      }
      /*
         INSERT RECORD ON TABLE bitacora

      */
      A43bit_codigo = AV11bit_codigo ;
      A40bit_usuario = AV8bit_usuario ;
      A41bit_fecha = AV9bit_fecha ;
      A42bit_descripcion = AV10bit_descripcion ;
      /* Using cursor P00013 */
      pr_default.execute(1, new Object[] {new Short(A43bit_codigo), A40bit_usuario, A41bit_fecha, A42bit_descripcion});
      if ( (pr_default.getStatus(1) == 1) )
      {
         Gx_err = (short)(1) ;
         Gx_emsg = localUtil.getMessages().getMessage("GXM_noupdate") ;
      }
      else
      {
         Gx_err = (short)(0) ;
         Gx_emsg = "" ;
      }
      /* End Insert */
      Application.commit(context, remoteHandle, "DEFAULT", "guarda_bitacora");
      cleanup();
   }

   protected void cleanup( )
   {
      this.aP0[0] = guarda_bitacora.this.AV8bit_usuario;
      this.aP1[0] = guarda_bitacora.this.AV9bit_fecha;
      this.aP2[0] = guarda_bitacora.this.AV10bit_descripcion;
      CloseOpenCursors();
      exitApplication();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      scmdbuf = "" ;
      P00012_A43bit_codigo = new short[1] ;
      A40bit_usuario = "" ;
      A41bit_fecha = GXutil.resetTime( GXutil.nullDate() );
      A42bit_descripcion = "" ;
      Gx_emsg = "" ;
      pr_default = new DataStoreProvider(context, remoteHandle, new guarda_bitacora__default(),
         new Object[] {
             new Object[] {
            P00012_A43bit_codigo
            }
            , new Object[] {
            }
         }
      );
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte AV14GXLvl1 ;
   private short A43bit_codigo ;
   private short AV11bit_codigo ;
   private short Gx_err ;
   private int GX_INS11 ;
   private String AV8bit_usuario ;
   private String AV10bit_descripcion ;
   private String scmdbuf ;
   private String A40bit_usuario ;
   private String A42bit_descripcion ;
   private String Gx_emsg ;
   private java.util.Date AV9bit_fecha ;
   private java.util.Date A41bit_fecha ;
   private String[] aP0 ;
   private java.util.Date[] aP1 ;
   private String[] aP2 ;
   private IDataStoreProvider pr_default ;
   private short[] P00012_A43bit_codigo ;
}

final  class guarda_bitacora__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("P00012", "SELECT TOP 1 [bit_codigo] FROM [bitacora] WITH (NOLOCK) ORDER BY [bit_codigo] DESC ",false, GX_NOMASK + GX_MASKLOOPLOCK, false, this,1,0,true )
         ,new UpdateCursor("P00013", "INSERT INTO [bitacora] ([bit_codigo], [bit_usuario], [bit_fecha], [bit_descripcion]) VALUES (?, ?, ?, ?)", GX_NOMASK + GX_MASKLOOPLOCK)
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 1 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setString(2, (String)parms[1], 10);
               stmt.setDateTime(3, (java.util.Date)parms[2], false);
               stmt.setString(4, (String)parms[3], 200);
               break;
      }
   }

}

