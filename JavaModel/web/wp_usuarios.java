/*
               File: wp_usuarios
        Description: wp_usuarios
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:9.58
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/wp_usuarios")
public final  class wp_usuarios extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new wp_usuarios_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "wp_usuarios";
   }

}

