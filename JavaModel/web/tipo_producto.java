/*
               File: tipo_producto
        Description: tipo_producto
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:41:59.37
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/tipo_producto")
public final  class tipo_producto extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new tipo_producto_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "tipo_producto";
   }

}

