/*
               File: usuario_impl
        Description: usuario
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:6.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class usuario_impl extends GXDataArea
{
   public void initenv( )
   {
      if ( GxWebError != 0 )
      {
         return  ;
      }
   }

   public void inittrn( )
   {
      initialize_properties( ) ;
      entryPointCalled = false ;
      gxfirstwebparm = httpContext.GetNextPar( ) ;
      gxfirstwebparm_bkp = gxfirstwebparm ;
      gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm, "High") ;
      if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
      {
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         dyncall( httpContext.GetNextPar( )) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
      {
         A25rol_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_3( A25rol_codigo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
      {
         httpContext.setAjaxEventMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = httpContext.GetNextPar( ) ;
      }
      else
      {
         if ( ! httpContext.IsValidAjaxCall( false) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = gxfirstwebparm_bkp ;
      }
      if ( ! entryPointCalled )
      {
         Gx_mode = gxfirstwebparm ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      Form.getMeta().addItem("Generator", "GeneXus Java", (short)(0)) ;
      Form.getMeta().addItem("Version", "10_1_8-58720", (short)(0)) ;
      Form.getMeta().addItem("Description", "usuario", (short)(0)) ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      GX_FocusControl = edtusu_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      wbErr = false ;
      httpContext.setTheme("GeneXusX");
   }

   public usuario_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public usuario_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( usuario_impl.class ));
   }

   public usuario_impl( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void webExecute( )
   {
      initenv( ) ;
      inittrn( ) ;
      if ( ( GxWebError == 0 ) && ! httpContext.isAjaxCallMode( ) )
      {
         MasterPageObj = new menu_administrador_impl (remoteHandle, context.copy());
         MasterPageObj.setDataArea(this,false);
         MasterPageObj.webExecute();
         if ( httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "max-age=0");
            }
            if ( (GXutil.strcmp("", httpContext.wjLoc)==0) )
            {
               httpContext.GX_webresponse.addString(httpContext.getJSONResponse( ));
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public void draw( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         disable_std_buttons( ) ;
         enableDisable( ) ;
         set_caption( ) ;
         /* Form start */
         wb_table1_2_099( true) ;
      }
      return  ;
   }

   public void wb_table1_2_099e( boolean wbgen )
   {
      if ( wbgen )
      {
      }
      /* Execute Exit event if defined. */
   }

   public void wb_table1_2_099( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table2_5_099( true) ;
      }
      return  ;
   }

   public void wb_table2_5_099e( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Control Group */
         ClassString = "Group" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroupdata_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"usuario"+"</legend>") ;
         wb_table3_27_099( true) ;
      }
      return  ;
   }

   public void wb_table3_27_099e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_099e( true) ;
      }
      else
      {
         wb_table1_2_099e( false) ;
      }
   }

   public void wb_table3_27_099( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         ClassString = "ErrorViewer" ;
         StyleString = "" ;
         GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), StyleString, ClassString, "", "false");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table4_33_099( true) ;
      }
      return  ;
   }

   public void wb_table4_33_099e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"" ;
         ClassString = "BtnEnter" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "rounded", "EENTER.", TempTags, "", httpContext.getButtonType( ), "HLP_usuario.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"" ;
         ClassString = "BtnCancel" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "rounded", "ECANCEL.", TempTags, "", httpContext.getButtonType( ), "HLP_usuario.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"" ;
         ClassString = "BtnDelete" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 0, "rounded", "EDELETE.", TempTags, "", httpContext.getButtonType( ), "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_27_099e( true) ;
      }
      else
      {
         wb_table3_27_099e( false) ;
      }
   }

   public void wb_table4_33_099( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockusu_codigo_Internalname, "C�digo", "", "", lblTextblockusu_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtusu_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A36usu_codigo, (byte)(4), (byte)(0), ",", "")), ((edtusu_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A36usu_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A36usu_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(38);\"", "", "", "", "", edtusu_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtusu_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockusu_usuario_Internalname, "Usuario", "", "", lblTextblockusu_usuario_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A37usu_usuario", A37usu_usuario);
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtusu_usuario_Internalname, GXutil.rtrim( A37usu_usuario), GXutil.rtrim( localUtil.format( A37usu_usuario, "XXXXXXXXXX")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(43);\"", "", "", "", "", edtusu_usuario_Jsonclick, 0, ClassString, StyleString, "", 1, edtusu_usuario_Enabled, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockusu_clave_Internalname, "Clave", "", "", lblTextblockusu_clave_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A38usu_clave", A38usu_clave);
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtusu_clave_Internalname, GXutil.rtrim( A38usu_clave), GXutil.rtrim( localUtil.format( A38usu_clave, "XXXXXXXXXX")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(48);\"", "", "", "", "", edtusu_clave_Jsonclick, 0, ClassString, StyleString, "", 1, edtusu_clave_Enabled, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockusu_fecha_creacion_Internalname, "Fecha creaci�n", "", "", lblTextblockusu_fecha_creacion_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A39usu_fecha_creacion", localUtil.format(A39usu_fecha_creacion, "99/99/99"));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         httpContext.writeText( "<div id=\""+edtusu_fecha_creacion_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( httpContext, edtusu_fecha_creacion_Internalname, localUtil.format(A39usu_fecha_creacion, "99/99/99"), localUtil.format( A39usu_fecha_creacion, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(53);\"", "", "", "", "", edtusu_fecha_creacion_Jsonclick, 0, ClassString, StyleString, "", 1, edtusu_fecha_creacion_Enabled, 0, 8, "chr", 1, "row", 8, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_usuario.htm");
         GxWebStd.gx_bitmap( httpContext, edtusu_fecha_creacion_Internalname+"_dp_trigger", "calendar-img.gif", "", "", "", "", ((1==0)||(edtusu_fecha_creacion_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;vertical-align:text-bottom", "", "", "", "", "", "HLP_usuario.htm");
         httpContext.writeTextNL( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockrol_codigo_Internalname, "Rol", "", "", lblTextblockrol_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtrol_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A25rol_codigo, (byte)(4), (byte)(0), ",", "")), ((edtrol_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A25rol_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A25rol_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(58);\"", "", "", "", "", edtrol_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtrol_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_usuario.htm");
         /* Static images/pictures */
         ClassString = "Image" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgprompt_25_Internalname, "prompt.gif", imgprompt_25_Link, "", "", "GeneXusX", imgprompt_25_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "''", "", "HLP_usuario.htm");
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A26rol_nombre", A26rol_nombre);
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtrol_nombre_Internalname, GXutil.rtrim( A26rol_nombre), GXutil.rtrim( localUtil.format( A26rol_nombre, "")), "", "", "", "", "", edtrol_nombre_Jsonclick, 0, ClassString, StyleString, "", 1, edtrol_nombre_Enabled, 0, 45, "chr", 1, "row", 45, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_usuario.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table4_33_099e( true) ;
      }
      else
      {
         wb_table4_33_099e( false) ;
      }
   }

   public void wb_table2_5_099( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "<div style=\"WHITE-SPACE: nowrap\" class=\"ToolbarMain\">") ;
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_Internalname, context.getHttpContext().getImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_Visible, 1, "", "Primero", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_separator_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_Internalname, context.getHttpContext().getImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_Internalname, context.getHttpContext().getImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_Visible, 1, "", "Siguiente", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_separator_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_Internalname, context.getHttpContext().getImagePath( "29211874-e613-48e5-9011-8017d984217e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_Visible, 1, "", "Ultimo", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_separator_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_Internalname, context.getHttpContext().getImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_Visible, 1, "", "Seleccionar", 0, 0, 0, "", 0, "", 0, 0, 4, imgBtn_select_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "gx.popup.openPrompt('"+"gx0090"+"',["+"{Ctrl:gx.dom.el('"+"USU_CODIGO"+"'), id:'"+"USU_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 4, imgBtn_select_separator_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "gx.popup.openPrompt('"+"gx0090"+"',["+"{Ctrl:gx.dom.el('"+"USU_CODIGO"+"'), id:'"+"USU_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_Internalname, context.getHttpContext().getImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_Internalname, context.getHttpContext().getImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_Visible, 1, "", "Cancelar", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_Internalname, context.getHttpContext().getImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_usuario.htm");
         httpContext.writeText( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_5_099e( true) ;
      }
      else
      {
         wb_table2_5_099e( false) ;
      }
   }

   public void userMain( )
   {
      standaloneStartup( ) ;
   }

   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
      process( ) ;
   }

   public void standaloneStartupServer( )
   {
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      httpContext.wbGlbDoneStart = (byte)(1) ;
      assign_properties_default( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtusu_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtusu_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "USU_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtusu_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A36usu_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
            }
            else
            {
               A36usu_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtusu_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
            }
            A37usu_usuario = httpContext.cgiGet( edtusu_usuario_Internalname) ;
            httpContext.ajax_rsp_assign_attri("", false, "A37usu_usuario", A37usu_usuario);
            A38usu_clave = httpContext.cgiGet( edtusu_clave_Internalname) ;
            httpContext.ajax_rsp_assign_attri("", false, "A38usu_clave", A38usu_clave);
            if ( localUtil.vcdate( httpContext.cgiGet( edtusu_fecha_creacion_Internalname), (byte)(3)) == 0 )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_faildate", new Object[] {"Fecha de creac�n del usuario"}), 1, "USU_FECHA_CREACION");
               AnyError = (short)(1) ;
               GX_FocusControl = edtusu_fecha_creacion_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A39usu_fecha_creacion = GXutil.nullDate() ;
               httpContext.ajax_rsp_assign_attri("", false, "A39usu_fecha_creacion", localUtil.format(A39usu_fecha_creacion, "99/99/99"));
            }
            else
            {
               A39usu_fecha_creacion = localUtil.ctod( httpContext.cgiGet( edtusu_fecha_creacion_Internalname), 3) ;
               httpContext.ajax_rsp_assign_attri("", false, "A39usu_fecha_creacion", localUtil.format(A39usu_fecha_creacion, "99/99/99"));
            }
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtrol_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtrol_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "ROL_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtrol_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A25rol_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
            }
            else
            {
               A25rol_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtrol_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
            }
            A26rol_nombre = httpContext.cgiGet( edtrol_nombre_Internalname) ;
            n26rol_nombre = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A26rol_nombre", A26rol_nombre);
            /* Read saved values. */
            Z36usu_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z36usu_codigo"), ",", ".")) ;
            Z37usu_usuario = httpContext.cgiGet( "Z37usu_usuario") ;
            Z38usu_clave = httpContext.cgiGet( "Z38usu_clave") ;
            Z39usu_fecha_creacion = localUtil.ctod( httpContext.cgiGet( "Z39usu_fecha_creacion"), 0) ;
            Z25rol_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z25rol_codigo"), ",", ".")) ;
            IsConfirmed = (short)(localUtil.ctol( httpContext.cgiGet( "IsConfirmed"), ",", ".")) ;
            IsModified = (short)(localUtil.ctol( httpContext.cgiGet( "IsModified"), ",", ".")) ;
            Gx_mode = httpContext.cgiGet( "Mode") ;
            Gx_mode = httpContext.cgiGet( "vMODE") ;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( GXutil.strcmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A36usu_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
               httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons( ) ;
               standaloneModal( ) ;
            }
            else
            {
               standaloneModal( ) ;
            }
         }
      }
   }

   public void process( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read Transaction buttons. */
         sEvt = httpContext.cgiGet( "_EventName") ;
         EvtGridId = httpContext.cgiGet( "_EventGridId") ;
         EvtRowId = httpContext.cgiGet( "_EventRowId") ;
         if ( GXutil.len( sEvt) > 0 )
         {
            sEvtType = GXutil.left( sEvt, 1) ;
            sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
            if ( GXutil.strcmp(sEvtType, "M") != 0 )
            {
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( GXutil.strcmp(Gx_mode, "DSP") != 0 )
                        {
                           btn_enter( ) ;
                        }
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                     else if ( GXutil.strcmp(sEvt, "FIRST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_first( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "PREVIOUS") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_previous( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "NEXT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_next( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "LAST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_last( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "SELECT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_select( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "DELETE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( GXutil.strcmp(Gx_mode, "DSP") != 0 )
                        {
                           btn_delete( ) ;
                        }
                     }
                  }
                  else
                  {
                  }
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void afterTrn( )
   {
      if ( trnEnded == 1 )
      {
         trnEnded = 0 ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            /* Clear variables for new insertion. */
            initAll099( ) ;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
         }
      }
   }

   public String toString( )
   {
      return "" ;
   }

   public GXContentInfo getContentInfo( )
   {
      return (GXContentInfo)(null) ;
   }

   public void disable_std_buttons( )
   {
      imgBtn_delete2_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
      imgBtn_delete2_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
      bttBtn_delete_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
      if ( ( GXutil.strcmp(sMode9, "DSP") == 0 ) || ( GXutil.strcmp(sMode9, "DLT") == 0 ) )
      {
         imgBtn_delete2_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
         imgBtn_delete2_separator_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
         bttBtn_delete_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
         if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_Visible, 5, 0)));
            imgBtn_enter2_separator_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_separator_Visible, 5, 0)));
            bttBtn_enter_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_enter_Visible, 5, 0)));
         }
         disableAttributes099( ) ;
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_confdelete"), 0, "");
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_mustconfirm"), 0, "");
         }
      }
   }

   public void confirm_090( )
   {
      beforeValidate099( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            onDeleteControls099( ) ;
         }
         else
         {
            checkExtendedTable099( ) ;
            closeExtendedTableCursors099( ) ;
         }
      }
      if ( AnyError == 0 )
      {
         IsConfirmed = (short)(1) ;
      }
   }

   public void resetCaption090( )
   {
   }

   public void zm099( int GX_JID )
   {
      if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
         {
            Z37usu_usuario = T00093_A37usu_usuario[0] ;
            Z38usu_clave = T00093_A38usu_clave[0] ;
            Z39usu_fecha_creacion = T00093_A39usu_fecha_creacion[0] ;
            Z25rol_codigo = T00093_A25rol_codigo[0] ;
         }
         else
         {
            Z37usu_usuario = A37usu_usuario ;
            Z38usu_clave = A38usu_clave ;
            Z39usu_fecha_creacion = A39usu_fecha_creacion ;
            Z25rol_codigo = A25rol_codigo ;
         }
      }
      if ( GX_JID == -2 )
      {
         Z36usu_codigo = A36usu_codigo ;
         Z37usu_usuario = A37usu_usuario ;
         Z38usu_clave = A38usu_clave ;
         Z39usu_fecha_creacion = A39usu_fecha_creacion ;
         Z25rol_codigo = A25rol_codigo ;
         Z26rol_nombre = A26rol_nombre ;
      }
   }

   public void standaloneNotModal( )
   {
      imgprompt_25_Link = ((GXutil.strcmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0060"+"',["+"{Ctrl:gx.dom.el('"+"ROL_CODIGO"+"'), id:'"+"ROL_CODIGO"+"'"+",isOut: true}"+"],"+"null"+","+"'', false"+","+"false"+");") ;
      imgBtn_delete2_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
   }

   public void standaloneModal( )
   {
      if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
      {
         imgBtn_enter2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
      else
      {
         imgBtn_enter2_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
   }

   public void load099( )
   {
      /* Using cursor T00095 */
      pr_default.execute(3, new Object[] {new Short(A36usu_codigo)});
      if ( (pr_default.getStatus(3) != 101) )
      {
         RcdFound9 = (short)(1) ;
         A37usu_usuario = T00095_A37usu_usuario[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A37usu_usuario", A37usu_usuario);
         A38usu_clave = T00095_A38usu_clave[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A38usu_clave", A38usu_clave);
         A39usu_fecha_creacion = T00095_A39usu_fecha_creacion[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A39usu_fecha_creacion", localUtil.format(A39usu_fecha_creacion, "99/99/99"));
         A26rol_nombre = T00095_A26rol_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A26rol_nombre", A26rol_nombre);
         n26rol_nombre = T00095_n26rol_nombre[0] ;
         A25rol_codigo = T00095_A25rol_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         zm099( -2) ;
      }
      pr_default.close(3);
      onLoadActions099( ) ;
   }

   public void onLoadActions099( )
   {
   }

   public void checkExtendedTable099( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      if ( ! ( GXutil.nullDate().equals(A39usu_fecha_creacion) || (( A39usu_fecha_creacion.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A39usu_fecha_creacion.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Campo Fecha de creac�n del usuario fuera de rango", "OutOfRange", 1, "");
         AnyError = (short)(1) ;
      }
      /* Using cursor T00094 */
      pr_default.execute(2, new Object[] {new Short(A25rol_codigo)});
      if ( (pr_default.getStatus(2) == 101) )
      {
         AnyError25 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'rol'.", "ForeignKeyNotFound", 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError25 == 0 )
      {
         A26rol_nombre = T00094_A26rol_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A26rol_nombre", A26rol_nombre);
         n26rol_nombre = T00094_n26rol_nombre[0] ;
      }
      pr_default.close(2);
   }

   public void closeExtendedTableCursors099( )
   {
      pr_default.close(2);
   }

   public void enableDisable( )
   {
   }

   public void gxload_3( short A25rol_codigo )
   {
      /* Using cursor T00096 */
      pr_default.execute(4, new Object[] {new Short(A25rol_codigo)});
      if ( (pr_default.getStatus(4) == 101) )
      {
         AnyError25 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'rol'.", "ForeignKeyNotFound", 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError25 == 0 )
      {
         A26rol_nombre = T00096_A26rol_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A26rol_nombre", A26rol_nombre);
         n26rol_nombre = T00096_n26rol_nombre[0] ;
      }
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.GX_webresponse.addString("new Array( new Array(");
      httpContext.GX_webresponse.addString("\""+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A26rol_nombre))+"\"");
      httpContext.GX_webresponse.addString(")");
      if ( (pr_default.getStatus(4) == 101) )
      {
         httpContext.GX_webresponse.addString(",");
         httpContext.GX_webresponse.addString("101");
      }
      httpContext.GX_webresponse.addString(")");
      pr_default.close(4);
   }

   public void getKey099( )
   {
      /* Using cursor T00097 */
      pr_default.execute(5, new Object[] {new Short(A36usu_codigo)});
      if ( (pr_default.getStatus(5) != 101) )
      {
         RcdFound9 = (short)(1) ;
      }
      else
      {
         RcdFound9 = (short)(0) ;
      }
      pr_default.close(5);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00093 */
      pr_default.execute(1, new Object[] {new Short(A36usu_codigo)});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm099( 2) ;
         RcdFound9 = (short)(1) ;
         A36usu_codigo = T00093_A36usu_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
         A37usu_usuario = T00093_A37usu_usuario[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A37usu_usuario", A37usu_usuario);
         A38usu_clave = T00093_A38usu_clave[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A38usu_clave", A38usu_clave);
         A39usu_fecha_creacion = T00093_A39usu_fecha_creacion[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A39usu_fecha_creacion", localUtil.format(A39usu_fecha_creacion, "99/99/99"));
         A25rol_codigo = T00093_A25rol_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
         Z36usu_codigo = A36usu_codigo ;
         sMode9 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         load099( ) ;
         Gx_mode = sMode9 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         RcdFound9 = (short)(0) ;
         initializeNonKey099( ) ;
         sMode9 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal( ) ;
         Gx_mode = sMode9 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey099( ) ;
      if ( RcdFound9 == 0 )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound9 = (short)(0) ;
      /* Using cursor T00098 */
      pr_default.execute(6, new Object[] {new Short(A36usu_codigo)});
      if ( (pr_default.getStatus(6) != 101) )
      {
         while ( (pr_default.getStatus(6) != 101) && ( ( T00098_A36usu_codigo[0] < A36usu_codigo ) ) )
         {
            pr_default.readNext(6);
         }
         if ( (pr_default.getStatus(6) != 101) && ( ( T00098_A36usu_codigo[0] > A36usu_codigo ) ) )
         {
            A36usu_codigo = T00098_A36usu_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
            RcdFound9 = (short)(1) ;
         }
      }
      pr_default.close(6);
   }

   public void move_previous( )
   {
      RcdFound9 = (short)(0) ;
      /* Using cursor T00099 */
      pr_default.execute(7, new Object[] {new Short(A36usu_codigo)});
      if ( (pr_default.getStatus(7) != 101) )
      {
         while ( (pr_default.getStatus(7) != 101) && ( ( T00099_A36usu_codigo[0] > A36usu_codigo ) ) )
         {
            pr_default.readNext(7);
         }
         if ( (pr_default.getStatus(7) != 101) && ( ( T00099_A36usu_codigo[0] < A36usu_codigo ) ) )
         {
            A36usu_codigo = T00099_A36usu_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
            RcdFound9 = (short)(1) ;
         }
      }
      pr_default.close(7);
   }

   public void btn_enter( )
   {
      nKeyPressed = (byte)(1) ;
      getKey099( ) ;
      if ( RcdFound9 == 1 )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "USU_CODIGO");
            AnyError = (short)(1) ;
            GX_FocusControl = edtusu_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else if ( A36usu_codigo != Z36usu_codigo )
         {
            A36usu_codigo = Z36usu_codigo ;
            httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "USU_CODIGO");
            AnyError = (short)(1) ;
            GX_FocusControl = edtusu_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            delete( ) ;
            afterTrn( ) ;
            GX_FocusControl = edtusu_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            /* Update record */
            update099( ) ;
            GX_FocusControl = edtusu_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }
      else
      {
         if ( A36usu_codigo != Z36usu_codigo )
         {
            /* Insert record */
            GX_FocusControl = edtusu_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            insert099( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "" ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( GXutil.strcmp(Gx_mode, "UPD") == 0 )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "USU_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtusu_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            else
            {
               /* Insert record */
               GX_FocusControl = edtusu_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               insert099( ) ;
               if ( AnyError == 1 )
               {
                  GX_FocusControl = "" ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
         }
      }
      afterTrn( ) ;
   }

   public void btn_delete( )
   {
      if ( A36usu_codigo != Z36usu_codigo )
      {
         A36usu_codigo = Z36usu_codigo ;
         httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforedlt"), 1, "USU_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtusu_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      else
      {
         delete( ) ;
         afterTrn( ) ;
         GX_FocusControl = edtusu_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError != 0 )
      {
      }
      getByPrimaryKey( ) ;
      CloseOpenCursors();
   }

   public void btn_get( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      if ( RcdFound9 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "USU_CODIGO");
         AnyError = (short)(1) ;
      }
      GX_FocusControl = edtusu_usuario_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_first( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart099( ) ;
      if ( RcdFound9 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edtusu_usuario_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd099( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_previous( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_previous( ) ;
      if ( RcdFound9 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edtusu_usuario_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_next( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_next( ) ;
      if ( RcdFound9 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edtusu_usuario_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_last( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart099( ) ;
      if ( RcdFound9 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         while ( RcdFound9 != 0 )
         {
            scanNext099( ) ;
         }
      }
      GX_FocusControl = edtusu_usuario_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd099( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_select( )
   {
      getEqualNoModal( ) ;
   }

   public void checkOptimisticConcurrency099( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         /* Using cursor T00092 */
         pr_default.execute(0, new Object[] {new Short(A36usu_codigo)});
         if ( (pr_default.getStatus(0) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"usuario"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( GXutil.strcmp(Z37usu_usuario, T00092_A37usu_usuario[0]) != 0 ) || ( GXutil.strcmp(Z38usu_clave, T00092_A38usu_clave[0]) != 0 ) || !( Z39usu_fecha_creacion.equals( T00092_A39usu_fecha_creacion[0] ) ) || ( Z25rol_codigo != T00092_A25rol_codigo[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"usuario"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert099( )
   {
      beforeValidate099( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable099( ) ;
      }
      if ( AnyError == 0 )
      {
         zm099( 0) ;
         checkOptimisticConcurrency099( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm099( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert099( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000910 */
                  pr_default.execute(8, new Object[] {new Short(A36usu_codigo), A37usu_usuario, A38usu_clave, A39usu_fecha_creacion, new Short(A25rol_codigo)});
                  if ( (pr_default.getStatus(8) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucadded"), 0, "");
                        resetCaption090( ) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load099( ) ;
         }
         endLevel099( ) ;
      }
      closeExtendedTableCursors099( ) ;
   }

   public void update099( )
   {
      beforeValidate099( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable099( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency099( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm099( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate099( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000911 */
                  pr_default.execute(9, new Object[] {A37usu_usuario, A38usu_clave, A39usu_fecha_creacion, new Short(A25rol_codigo), new Short(A36usu_codigo)});
                  if ( (pr_default.getStatus(9) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"usuario"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate099( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucupdated"), 0, "");
                        resetCaption090( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel099( ) ;
      }
      closeExtendedTableCursors099( ) ;
   }

   public void deferredUpdate099( )
   {
   }

   public void delete( )
   {
      beforeValidate099( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency099( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls099( ) ;
         afterConfirm099( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete099( ) ;
            if ( AnyError == 0 )
            {
               /* No cascading delete specified. */
               /* Using cursor T000912 */
               pr_default.execute(10, new Object[] {new Short(A36usu_codigo)});
               if ( AnyError == 0 )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( AnyError == 0 )
                  {
                     move_next( ) ;
                     if ( RcdFound9 == 0 )
                     {
                        initAll099( ) ;
                     }
                     else
                     {
                        getByPrimaryKey( ) ;
                     }
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucdeleted"), 0, "");
                     resetCaption090( ) ;
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode9 = Gx_mode ;
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      endLevel099( ) ;
      Gx_mode = sMode9 ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
   }

   public void onDeleteControls099( )
   {
      standaloneModal( ) ;
      if ( AnyError == 0 )
      {
         /* Delete mode formulas */
         /* Using cursor T000913 */
         pr_default.execute(11, new Object[] {new Short(A25rol_codigo)});
         A26rol_nombre = T000913_A26rol_nombre[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A26rol_nombre", A26rol_nombre);
         n26rol_nombre = T000913_n26rol_nombre[0] ;
         pr_default.close(11);
      }
   }

   public void endLevel099( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         pr_default.close(0);
      }
      if ( AnyError == 0 )
      {
         beforeComplete099( ) ;
      }
      if ( AnyError == 0 )
      {
         pr_default.close(11);
         Application.commit(context, remoteHandle, "DEFAULT", "usuario");
         if ( AnyError == 0 )
         {
            confirmValues090( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         pr_default.close(11);
         Application.rollback(context, remoteHandle, "DEFAULT", "usuario");
      }
      IsModified = (short)(0) ;
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanStart099( )
   {
      /* Using cursor T000914 */
      pr_default.execute(12);
      RcdFound9 = (short)(0) ;
      if ( (pr_default.getStatus(12) != 101) )
      {
         RcdFound9 = (short)(1) ;
         A36usu_codigo = T000914_A36usu_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
      }
      /* Load Subordinate Levels */
   }

   public void scanNext099( )
   {
      pr_default.readNext(12);
      RcdFound9 = (short)(0) ;
      if ( (pr_default.getStatus(12) != 101) )
      {
         RcdFound9 = (short)(1) ;
         A36usu_codigo = T000914_A36usu_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
      }
   }

   public void scanEnd099( )
   {
      pr_default.close(12);
   }

   public void afterConfirm099( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert099( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate099( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete099( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete099( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate099( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes099( )
   {
      edtusu_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtusu_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtusu_codigo_Enabled, 5, 0)));
      edtusu_usuario_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtusu_usuario_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtusu_usuario_Enabled, 5, 0)));
      edtusu_clave_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtusu_clave_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtusu_clave_Enabled, 5, 0)));
      edtusu_fecha_creacion_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtusu_fecha_creacion_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtusu_fecha_creacion_Enabled, 5, 0)));
      edtrol_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtrol_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtrol_codigo_Enabled, 5, 0)));
      edtrol_nombre_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtrol_nombre_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtrol_nombre_Enabled, 5, 0)));
   }

   public void assign_properties_default( )
   {
   }

   public void confirmValues090( )
   {
   }

   public void renderHtmlHeaders( )
   {
      GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), "IE=EmulateIE7");
   }

   public void renderHtmlOpenForm( )
   {
      httpContext.writeText( "<title>") ;
      httpContext.writeText( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      MasterPageObj.master_styles();
      if ( ! httpContext.isSmartDevice( ) )
      {
         httpContext.AddJavascriptSource("gxgral.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      else
      {
         httpContext.AddJavascriptSource("gxapiSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfxSD.js", "?58720");
         httpContext.AddJavascriptSource("gxtypesSD.js", "?58720");
         httpContext.AddJavascriptSource("gxpopupSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfrmutlSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgridSD.js", "?58720");
         httpContext.AddJavascriptSource("JavaScripTableSD.js", "?58720");
         httpContext.AddJavascriptSource("rijndaelSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgralSD.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      httpContext.AddJavascriptSource("calendar.js", "?58720");
      httpContext.AddJavascriptSource("calendar-setup.js", "?58720");
      httpContext.AddJavascriptSource("calendar-es.js", "?58720");
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      FormProcess = " onkeyup=\"gx.evt.onkeyup(event)\" onkeypress=\"gx.evt.onkeypress(event,true,false)\" onkeydown=\"gx.evt.onkeypress(null,true,false)\"" ;
      httpContext.writeText( "<body") ;
      httpContext.writeText( " "+"class=\"Form\""+" "+" style=\"-moz-opacity:0;opacity:0;"+"background-color:"+WebUtils.getHTMLColor( Form.getIBackground())+";") ;
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         httpContext.writeText( " background-image:url("+httpContext.convertURL( Form.getBackground())+")") ;
      }
      httpContext.writeText( "\""+FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" onsubmit=\"try{return gx.csv.validForm()}catch(e){return true;}\" name=\"MAINFORM\" method=\"post\" action=\""+formatLink("usuario") + "?" + GXutil.URLEncode(GXutil.rtrim(Gx_mode))+"\" class=\""+"Form"+"\">") ;
      GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
   }

   public void renderHtmlCloseForm( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      GxWebStd.gx_hidden_field( httpContext, "Z36usu_codigo", GXutil.ltrim( localUtil.ntoc( Z36usu_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z37usu_usuario", GXutil.rtrim( Z37usu_usuario));
      GxWebStd.gx_hidden_field( httpContext, "Z38usu_clave", GXutil.rtrim( Z38usu_clave));
      GxWebStd.gx_hidden_field( httpContext, "Z39usu_fecha_creacion", localUtil.dtoc( Z39usu_fecha_creacion, 0, "/"));
      GxWebStd.gx_hidden_field( httpContext, "Z25rol_codigo", GXutil.ltrim( localUtil.ntoc( Z25rol_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsConfirmed", GXutil.ltrim( localUtil.ntoc( IsConfirmed, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsModified", GXutil.ltrim( localUtil.ntoc( IsModified, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Mode", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "vMODE", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      httpContext.writeTextNL( "</form>") ;
      include_jscripts( ) ;
   }

   public byte executeStartEvent( )
   {
      standaloneStartup( ) ;
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      return gxajaxcallmode ;
   }

   public void renderHtmlContent( )
   {
      draw( ) ;
   }

   public void dispatchEvents( )
   {
      process( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public String getPgmname( )
   {
      return "usuario" ;
   }

   public String getPgmdesc( )
   {
      return "usuario" ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("usuario") + "?" + GXutil.URLEncode(GXutil.rtrim(Gx_mode)) ;
   }

   public void initializeNonKey099( )
   {
      A37usu_usuario = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "A37usu_usuario", A37usu_usuario);
      A38usu_clave = "" ;
      httpContext.ajax_rsp_assign_attri("", false, "A38usu_clave", A38usu_clave);
      A39usu_fecha_creacion = GXutil.nullDate() ;
      httpContext.ajax_rsp_assign_attri("", false, "A39usu_fecha_creacion", localUtil.format(A39usu_fecha_creacion, "99/99/99"));
      A25rol_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A25rol_codigo", GXutil.ltrim( GXutil.str( A25rol_codigo, 4, 0)));
      A26rol_nombre = "" ;
      n26rol_nombre = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A26rol_nombre", A26rol_nombre);
   }

   public void initAll099( )
   {
      A36usu_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A36usu_codigo", GXutil.ltrim( GXutil.str( A36usu_codigo, 4, 0)));
      initializeNonKey099( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void define_styles( )
   {
      httpContext.AddStyleSheetFile("calendar-system.css", "?95080");
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?942719");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?58720");
      httpContext.AddJavascriptSource("usuario.js", "?942719");
      /* End function include_jscripts */
   }

   public void init_default_properties( )
   {
      imgBtn_first_Internalname = "BTN_FIRST" ;
      imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR" ;
      imgBtn_previous_Internalname = "BTN_PREVIOUS" ;
      imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR" ;
      imgBtn_next_Internalname = "BTN_NEXT" ;
      imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR" ;
      imgBtn_last_Internalname = "BTN_LAST" ;
      imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR" ;
      imgBtn_select_Internalname = "BTN_SELECT" ;
      imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR" ;
      imgBtn_enter2_Internalname = "BTN_ENTER2" ;
      imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR" ;
      imgBtn_cancel2_Internalname = "BTN_CANCEL2" ;
      imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR" ;
      imgBtn_delete2_Internalname = "BTN_DELETE2" ;
      imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR" ;
      tblTabletoolbar_Internalname = "TABLETOOLBAR" ;
      lblTextblockusu_codigo_Internalname = "TEXTBLOCKUSU_CODIGO" ;
      edtusu_codigo_Internalname = "USU_CODIGO" ;
      lblTextblockusu_usuario_Internalname = "TEXTBLOCKUSU_USUARIO" ;
      edtusu_usuario_Internalname = "USU_USUARIO" ;
      lblTextblockusu_clave_Internalname = "TEXTBLOCKUSU_CLAVE" ;
      edtusu_clave_Internalname = "USU_CLAVE" ;
      lblTextblockusu_fecha_creacion_Internalname = "TEXTBLOCKUSU_FECHA_CREACION" ;
      edtusu_fecha_creacion_Internalname = "USU_FECHA_CREACION" ;
      lblTextblockrol_codigo_Internalname = "TEXTBLOCKROL_CODIGO" ;
      edtrol_codigo_Internalname = "ROL_CODIGO" ;
      edtrol_nombre_Internalname = "ROL_NOMBRE" ;
      tblTable2_Internalname = "TABLE2" ;
      bttBtn_enter_Internalname = "BTN_ENTER" ;
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      bttBtn_delete_Internalname = "BTN_DELETE" ;
      tblTable1_Internalname = "TABLE1" ;
      grpGroupdata_Internalname = "GROUPDATA" ;
      tblTablemain_Internalname = "TABLEMAIN" ;
      Form.setInternalname( "FORM" );
      imgprompt_25_Internalname = "PROMPT_25" ;
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "usuario" );
      imgBtn_delete2_separator_Visible = 1 ;
      imgBtn_delete2_Enabled = 1 ;
      imgBtn_delete2_Visible = 1 ;
      imgBtn_cancel2_separator_Visible = 1 ;
      imgBtn_cancel2_Visible = 1 ;
      imgBtn_enter2_separator_Visible = 1 ;
      imgBtn_enter2_Enabled = 1 ;
      imgBtn_enter2_Visible = 1 ;
      imgBtn_select_separator_Visible = 1 ;
      imgBtn_select_Visible = 1 ;
      imgBtn_last_separator_Visible = 1 ;
      imgBtn_last_Visible = 1 ;
      imgBtn_next_separator_Visible = 1 ;
      imgBtn_next_Visible = 1 ;
      imgBtn_previous_separator_Visible = 1 ;
      imgBtn_previous_Visible = 1 ;
      imgBtn_first_separator_Visible = 1 ;
      imgBtn_first_Visible = 1 ;
      edtrol_nombre_Jsonclick = "" ;
      edtrol_nombre_Enabled = 0 ;
      imgprompt_25_Visible = 1 ;
      imgprompt_25_Link = "" ;
      edtrol_codigo_Jsonclick = "" ;
      edtrol_codigo_Enabled = 1 ;
      edtusu_fecha_creacion_Jsonclick = "" ;
      edtusu_fecha_creacion_Enabled = 1 ;
      edtusu_clave_Jsonclick = "" ;
      edtusu_clave_Enabled = 1 ;
      edtusu_usuario_Jsonclick = "" ;
      edtusu_usuario_Enabled = 1 ;
      edtusu_codigo_Jsonclick = "" ;
      edtusu_codigo_Enabled = 1 ;
      bttBtn_delete_Visible = 1 ;
      bttBtn_cancel_Visible = 1 ;
      bttBtn_enter_Visible = 1 ;
      httpContext.GX_msglist.setDisplaymode( (short)(1) );
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void valid_Rol_codigo( short GX_Parm1 ,
                                 String GX_Parm2 )
   {
      A25rol_codigo = GX_Parm1 ;
      A26rol_nombre = GX_Parm2 ;
      n26rol_nombre = false ;
      /* Using cursor T000913 */
      pr_default.execute(11, new Object[] {new Short(A25rol_codigo)});
      if ( (pr_default.getStatus(11) == 101) )
      {
         AnyError25 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'rol'.", "ForeignKeyNotFound", 1, "ROL_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtrol_codigo_Internalname ;
      }
      if ( AnyError25 == 0 )
      {
         A26rol_nombre = T000913_A26rol_nombre[0] ;
         n26rol_nombre = T000913_n26rol_nombre[0] ;
      }
      pr_default.close(11);
      dynload_actions( ) ;
      if ( AnyError == 1 )
      {
         A26rol_nombre = "" ;
         n26rol_nombre = false ;
      }
      isValidOutput.add(GXutil.rtrim( A26rol_nombre));
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(11);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      sPrefix = "" ;
      wcpOGx_mode = "" ;
      scmdbuf = "" ;
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      Gx_mode = "" ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      GX_FocusControl = "" ;
      sStyleString = "" ;
      ClassString = "" ;
      StyleString = "" ;
      TempTags = "" ;
      bttBtn_enter_Jsonclick = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      bttBtn_delete_Jsonclick = "" ;
      lblTextblockusu_codigo_Jsonclick = "" ;
      lblTextblockusu_usuario_Jsonclick = "" ;
      A37usu_usuario = "" ;
      lblTextblockusu_clave_Jsonclick = "" ;
      A38usu_clave = "" ;
      lblTextblockusu_fecha_creacion_Jsonclick = "" ;
      A39usu_fecha_creacion = GXutil.nullDate() ;
      lblTextblockrol_codigo_Jsonclick = "" ;
      A26rol_nombre = "" ;
      imgBtn_first_Jsonclick = "" ;
      imgBtn_first_separator_Jsonclick = "" ;
      imgBtn_previous_Jsonclick = "" ;
      imgBtn_previous_separator_Jsonclick = "" ;
      imgBtn_next_Jsonclick = "" ;
      imgBtn_next_separator_Jsonclick = "" ;
      imgBtn_last_Jsonclick = "" ;
      imgBtn_last_separator_Jsonclick = "" ;
      imgBtn_select_Jsonclick = "" ;
      imgBtn_select_separator_Jsonclick = "" ;
      imgBtn_enter2_Jsonclick = "" ;
      imgBtn_enter2_separator_Jsonclick = "" ;
      imgBtn_cancel2_Jsonclick = "" ;
      imgBtn_cancel2_separator_Jsonclick = "" ;
      imgBtn_delete2_Jsonclick = "" ;
      imgBtn_delete2_separator_Jsonclick = "" ;
      Z37usu_usuario = "" ;
      Z38usu_clave = "" ;
      Z39usu_fecha_creacion = GXutil.nullDate() ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      sMode9 = "" ;
      Z26rol_nombre = "" ;
      T00095_A36usu_codigo = new short[1] ;
      T00095_A37usu_usuario = new String[] {""} ;
      T00095_A38usu_clave = new String[] {""} ;
      T00095_A39usu_fecha_creacion = new java.util.Date[] {GXutil.nullDate()} ;
      T00095_A26rol_nombre = new String[] {""} ;
      T00095_n26rol_nombre = new boolean[] {false} ;
      T00095_A25rol_codigo = new short[1] ;
      T00094_A26rol_nombre = new String[] {""} ;
      T00094_n26rol_nombre = new boolean[] {false} ;
      T00096_A26rol_nombre = new String[] {""} ;
      T00096_n26rol_nombre = new boolean[] {false} ;
      T00097_A36usu_codigo = new short[1] ;
      T00093_A36usu_codigo = new short[1] ;
      T00093_A37usu_usuario = new String[] {""} ;
      T00093_A38usu_clave = new String[] {""} ;
      T00093_A39usu_fecha_creacion = new java.util.Date[] {GXutil.nullDate()} ;
      T00093_A25rol_codigo = new short[1] ;
      T00098_A36usu_codigo = new short[1] ;
      T00099_A36usu_codigo = new short[1] ;
      T00092_A36usu_codigo = new short[1] ;
      T00092_A37usu_usuario = new String[] {""} ;
      T00092_A38usu_clave = new String[] {""} ;
      T00092_A39usu_fecha_creacion = new java.util.Date[] {GXutil.nullDate()} ;
      T00092_A25rol_codigo = new short[1] ;
      T000913_A26rol_nombre = new String[] {""} ;
      T000913_n26rol_nombre = new boolean[] {false} ;
      T000914_A36usu_codigo = new short[1] ;
      sDynURL = "" ;
      FormProcess = "" ;
      GXt_char3 = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      isValidOutput = new com.genexus.GxUnknownObjectCollection();
      pr_default = new DataStoreProvider(context, remoteHandle, new usuario__default(),
         new Object[] {
             new Object[] {
            T00092_A36usu_codigo, T00092_A37usu_usuario, T00092_A38usu_clave, T00092_A39usu_fecha_creacion, T00092_A25rol_codigo
            }
            , new Object[] {
            T00093_A36usu_codigo, T00093_A37usu_usuario, T00093_A38usu_clave, T00093_A39usu_fecha_creacion, T00093_A25rol_codigo
            }
            , new Object[] {
            T00094_A26rol_nombre, T00094_n26rol_nombre
            }
            , new Object[] {
            T00095_A36usu_codigo, T00095_A37usu_usuario, T00095_A38usu_clave, T00095_A39usu_fecha_creacion, T00095_A26rol_nombre, T00095_n26rol_nombre, T00095_A25rol_codigo
            }
            , new Object[] {
            T00096_A26rol_nombre, T00096_n26rol_nombre
            }
            , new Object[] {
            T00097_A36usu_codigo
            }
            , new Object[] {
            T00098_A36usu_codigo
            }
            , new Object[] {
            T00099_A36usu_codigo
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000913_A26rol_nombre, T000913_n26rol_nombre
            }
            , new Object[] {
            T000914_A36usu_codigo
            }
         }
      );
   }

   private byte GxWebError ;
   private byte nKeyPressed ;
   private byte Gx_BScreen ;
   private byte gxajaxcallmode ;
   private byte wbTemp ;
   private short A25rol_codigo ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short A36usu_codigo ;
   private short Z36usu_codigo ;
   private short Z25rol_codigo ;
   private short RcdFound9 ;
   private int trnEnded ;
   private int bttBtn_enter_Visible ;
   private int bttBtn_cancel_Visible ;
   private int bttBtn_delete_Visible ;
   private int edtusu_codigo_Enabled ;
   private int edtusu_usuario_Enabled ;
   private int edtusu_clave_Enabled ;
   private int edtusu_fecha_creacion_Enabled ;
   private int edtrol_codigo_Enabled ;
   private int imgprompt_25_Visible ;
   private int edtrol_nombre_Enabled ;
   private int imgBtn_first_Visible ;
   private int imgBtn_first_separator_Visible ;
   private int imgBtn_previous_Visible ;
   private int imgBtn_previous_separator_Visible ;
   private int imgBtn_next_Visible ;
   private int imgBtn_next_separator_Visible ;
   private int imgBtn_last_Visible ;
   private int imgBtn_last_separator_Visible ;
   private int imgBtn_select_Visible ;
   private int imgBtn_select_separator_Visible ;
   private int imgBtn_enter2_Visible ;
   private int imgBtn_enter2_Enabled ;
   private int imgBtn_enter2_separator_Visible ;
   private int imgBtn_cancel2_Visible ;
   private int imgBtn_cancel2_separator_Visible ;
   private int imgBtn_delete2_Visible ;
   private int imgBtn_delete2_Enabled ;
   private int imgBtn_delete2_separator_Visible ;
   private int GX_JID ;
   private int AnyError25 ;
   private int idxLst ;
   private String sPrefix ;
   private String wcpOGx_mode ;
   private String scmdbuf ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String Gx_mode ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String GX_FocusControl ;
   private String edtusu_codigo_Internalname ;
   private String sStyleString ;
   private String tblTablemain_Internalname ;
   private String ClassString ;
   private String StyleString ;
   private String grpGroupdata_Internalname ;
   private String tblTable1_Internalname ;
   private String TempTags ;
   private String bttBtn_enter_Internalname ;
   private String bttBtn_enter_Jsonclick ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String bttBtn_delete_Internalname ;
   private String bttBtn_delete_Jsonclick ;
   private String tblTable2_Internalname ;
   private String lblTextblockusu_codigo_Internalname ;
   private String lblTextblockusu_codigo_Jsonclick ;
   private String edtusu_codigo_Jsonclick ;
   private String lblTextblockusu_usuario_Internalname ;
   private String lblTextblockusu_usuario_Jsonclick ;
   private String A37usu_usuario ;
   private String edtusu_usuario_Internalname ;
   private String edtusu_usuario_Jsonclick ;
   private String lblTextblockusu_clave_Internalname ;
   private String lblTextblockusu_clave_Jsonclick ;
   private String A38usu_clave ;
   private String edtusu_clave_Internalname ;
   private String edtusu_clave_Jsonclick ;
   private String lblTextblockusu_fecha_creacion_Internalname ;
   private String lblTextblockusu_fecha_creacion_Jsonclick ;
   private String edtusu_fecha_creacion_Internalname ;
   private String edtusu_fecha_creacion_Jsonclick ;
   private String lblTextblockrol_codigo_Internalname ;
   private String lblTextblockrol_codigo_Jsonclick ;
   private String edtrol_codigo_Internalname ;
   private String edtrol_codigo_Jsonclick ;
   private String imgprompt_25_Internalname ;
   private String imgprompt_25_Link ;
   private String edtrol_nombre_Internalname ;
   private String edtrol_nombre_Jsonclick ;
   private String tblTabletoolbar_Internalname ;
   private String imgBtn_first_Internalname ;
   private String imgBtn_first_Jsonclick ;
   private String imgBtn_first_separator_Internalname ;
   private String imgBtn_first_separator_Jsonclick ;
   private String imgBtn_previous_Internalname ;
   private String imgBtn_previous_Jsonclick ;
   private String imgBtn_previous_separator_Internalname ;
   private String imgBtn_previous_separator_Jsonclick ;
   private String imgBtn_next_Internalname ;
   private String imgBtn_next_Jsonclick ;
   private String imgBtn_next_separator_Internalname ;
   private String imgBtn_next_separator_Jsonclick ;
   private String imgBtn_last_Internalname ;
   private String imgBtn_last_Jsonclick ;
   private String imgBtn_last_separator_Internalname ;
   private String imgBtn_last_separator_Jsonclick ;
   private String imgBtn_select_Internalname ;
   private String imgBtn_select_Jsonclick ;
   private String imgBtn_select_separator_Internalname ;
   private String imgBtn_select_separator_Jsonclick ;
   private String imgBtn_enter2_Internalname ;
   private String imgBtn_enter2_Jsonclick ;
   private String imgBtn_enter2_separator_Internalname ;
   private String imgBtn_enter2_separator_Jsonclick ;
   private String imgBtn_cancel2_Internalname ;
   private String imgBtn_cancel2_Jsonclick ;
   private String imgBtn_cancel2_separator_Internalname ;
   private String imgBtn_cancel2_separator_Jsonclick ;
   private String imgBtn_delete2_Internalname ;
   private String imgBtn_delete2_Jsonclick ;
   private String imgBtn_delete2_separator_Internalname ;
   private String imgBtn_delete2_separator_Jsonclick ;
   private String Z37usu_usuario ;
   private String Z38usu_clave ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String sMode9 ;
   private String sDynURL ;
   private String FormProcess ;
   private String GXt_char3 ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private java.util.Date A39usu_fecha_creacion ;
   private java.util.Date Z39usu_fecha_creacion ;
   private boolean entryPointCalled ;
   private boolean wbErr ;
   private boolean n26rol_nombre ;
   private String A26rol_nombre ;
   private String Z26rol_nombre ;
   private com.genexus.webpanels.GXMasterPage MasterPageObj ;
   private com.genexus.GxUnknownObjectCollection isValidOutput ;
   private IDataStoreProvider pr_default ;
   private short[] T00095_A36usu_codigo ;
   private String[] T00095_A37usu_usuario ;
   private String[] T00095_A38usu_clave ;
   private java.util.Date[] T00095_A39usu_fecha_creacion ;
   private String[] T00095_A26rol_nombre ;
   private boolean[] T00095_n26rol_nombre ;
   private short[] T00095_A25rol_codigo ;
   private String[] T00094_A26rol_nombre ;
   private boolean[] T00094_n26rol_nombre ;
   private String[] T00096_A26rol_nombre ;
   private boolean[] T00096_n26rol_nombre ;
   private short[] T00097_A36usu_codigo ;
   private short[] T00093_A36usu_codigo ;
   private String[] T00093_A37usu_usuario ;
   private String[] T00093_A38usu_clave ;
   private java.util.Date[] T00093_A39usu_fecha_creacion ;
   private short[] T00093_A25rol_codigo ;
   private short[] T00098_A36usu_codigo ;
   private short[] T00099_A36usu_codigo ;
   private short[] T00092_A36usu_codigo ;
   private String[] T00092_A37usu_usuario ;
   private String[] T00092_A38usu_clave ;
   private java.util.Date[] T00092_A39usu_fecha_creacion ;
   private short[] T00092_A25rol_codigo ;
   private String[] T000913_A26rol_nombre ;
   private boolean[] T000913_n26rol_nombre ;
   private short[] T000914_A36usu_codigo ;
   private com.genexus.webpanels.GXWebForm Form ;
}

final  class usuario__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00092", "SELECT [usu_codigo], [usu_usuario], [usu_clave], [usu_fecha_creacion], [rol_codigo] FROM [usuario] WITH (UPDLOCK) WHERE [usu_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00093", "SELECT [usu_codigo], [usu_usuario], [usu_clave], [usu_fecha_creacion], [rol_codigo] FROM [usuario] WITH (NOLOCK) WHERE [usu_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00094", "SELECT [rol_nombre] FROM [rol] WITH (NOLOCK) WHERE [rol_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00095", "SELECT TM1.[usu_codigo], TM1.[usu_usuario], TM1.[usu_clave], TM1.[usu_fecha_creacion], T2.[rol_nombre], TM1.[rol_codigo] FROM ([usuario] TM1 WITH (NOLOCK) INNER JOIN [rol] T2 WITH (NOLOCK) ON T2.[rol_codigo] = TM1.[rol_codigo]) WHERE TM1.[usu_codigo] = ? ORDER BY TM1.[usu_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
         ,new ForEachCursor("T00096", "SELECT [rol_nombre] FROM [rol] WITH (NOLOCK) WHERE [rol_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00097", "SELECT [usu_codigo] FROM [usuario] WITH (NOLOCK) WHERE [usu_codigo] = ?  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00098", "SELECT TOP 1 [usu_codigo] FROM [usuario] WITH (NOLOCK) WHERE ( [usu_codigo] > ?) ORDER BY [usu_codigo]  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new ForEachCursor("T00099", "SELECT TOP 1 [usu_codigo] FROM [usuario] WITH (NOLOCK) WHERE ( [usu_codigo] < ?) ORDER BY [usu_codigo] DESC  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new UpdateCursor("T000910", "INSERT INTO [usuario] ([usu_codigo], [usu_usuario], [usu_clave], [usu_fecha_creacion], [rol_codigo]) VALUES (?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T000911", "UPDATE [usuario] SET [usu_usuario]=?, [usu_clave]=?, [usu_fecha_creacion]=?, [rol_codigo]=?  WHERE [usu_codigo] = ?", GX_NOMASK)
         ,new UpdateCursor("T000912", "DELETE FROM [usuario]  WHERE [usu_codigo] = ?", GX_NOMASK)
         ,new ForEachCursor("T000913", "SELECT [rol_nombre] FROM [rol] WITH (NOLOCK) WHERE [rol_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000914", "SELECT [usu_codigo] FROM [usuario] WITH (NOLOCK) ORDER BY [usu_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(4) ;
               ((short[]) buf[4])[0] = rslt.getShort(5) ;
               break;
            case 1 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(4) ;
               ((short[]) buf[4])[0] = rslt.getShort(5) ;
               break;
            case 2 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 3 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
               ((String[]) buf[2])[0] = rslt.getString(3, 10) ;
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(4) ;
               ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               ((short[]) buf[6])[0] = rslt.getShort(6) ;
               break;
            case 4 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 5 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 6 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 7 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 11 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 12 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 1 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 2 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 3 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 4 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 5 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 6 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 7 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 8 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setString(2, (String)parms[1], 10);
               stmt.setString(3, (String)parms[2], 10);
               stmt.setDate(4, (java.util.Date)parms[3]);
               stmt.setShort(5, ((Number) parms[4]).shortValue());
               break;
            case 9 :
               stmt.setString(1, (String)parms[0], 10);
               stmt.setString(2, (String)parms[1], 10);
               stmt.setDate(3, (java.util.Date)parms[2]);
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               stmt.setShort(5, ((Number) parms[4]).shortValue());
               break;
            case 10 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 11 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
      }
   }

}

