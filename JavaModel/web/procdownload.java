/*
               File: ProcDownload
        Description: Stub for ProcDownload
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:41:58.39
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import java.sql.*;
import com.genexus.db.*;
import com.genexus.*;
import com.genexus.distributed.*;
import com.genexus.search.*;

public final  class procdownload extends GXProcedure
{
   public static void main( String args[] )
   {
      Application.init(GXcfg.class);
      procdownload pgm = new procdownload (-1);
      Application.realMainProgram = pgm;
      pgm.executeCmdLine(args);
   }

   public void executeCmdLine( String args[] )
   {

      execute();
   }

   public procdownload( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( procdownload.class ), "" );
   }

   public procdownload( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context, "" );
   }

   public void execute( )
   {
      execute_int();
   }

   private void execute_int( )
   {
      initialize();
      /* GeneXus formulas */
      /* Output device settings */
   }

   protected void cleanup( )
   {
      CloseOpenCursors();
      Application.cleanup(context, this, remoteHandle);
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private short Gx_err ;
}

