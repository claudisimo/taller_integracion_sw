/*
               File: factura_impl
        Description: factura
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:1.6
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class factura_impl extends GXDataArea
{
   public void initenv( )
   {
      if ( GxWebError != 0 )
      {
         return  ;
      }
   }

   public void inittrn( )
   {
      initialize_properties( ) ;
      entryPointCalled = false ;
      gxfirstwebparm = httpContext.GetNextPar( ) ;
      gxfirstwebparm_bkp = gxfirstwebparm ;
      gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm, "High") ;
      if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
      {
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         dyncall( httpContext.GetNextPar( )) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
      {
         A1cli_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         httpContext.ajax_rsp_assign_attri("", false, "A1cli_codigo", GXutil.ltrim( GXutil.str( A1cli_codigo, 4, 0)));
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_10( A1cli_codigo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_13") == 0 )
      {
         A10pro_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_13( A10pro_codigo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
      {
         httpContext.setAjaxEventMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = httpContext.GetNextPar( ) ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridfactura_detalle") == 0 )
      {
         nRC_Gridfactura_detalle = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         nGXsfl_77_idx = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         sGXsfl_77_idx = httpContext.GetNextPar( ) ;
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxnrgridfactura_detalle_newrow( nRC_Gridfactura_detalle, nGXsfl_77_idx, sGXsfl_77_idx) ;
         return  ;
      }
      else
      {
         if ( ! httpContext.IsValidAjaxCall( false) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = gxfirstwebparm_bkp ;
      }
      if ( ! entryPointCalled )
      {
         Gx_mode = gxfirstwebparm ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      Form.getMeta().addItem("Generator", "GeneXus Java", (short)(0)) ;
      Form.getMeta().addItem("Version", "10_1_8-58720", (short)(0)) ;
      Form.getMeta().addItem("Description", "factura", (short)(0)) ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      GX_FocusControl = edtfac_numero_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      wbErr = false ;
      httpContext.setTheme("GeneXusX");
   }

   public factura_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public factura_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( factura_impl.class ));
   }

   public factura_impl( int remoteHandle ,
                        ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void webExecute( )
   {
      initenv( ) ;
      inittrn( ) ;
      if ( ( GxWebError == 0 ) && ! httpContext.isAjaxCallMode( ) )
      {
         MasterPageObj = new menu_usuario_impl (remoteHandle, context.copy());
         MasterPageObj.setDataArea(this,false);
         MasterPageObj.webExecute();
         if ( httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "max-age=0");
            }
            if ( (GXutil.strcmp("", httpContext.wjLoc)==0) )
            {
               httpContext.GX_webresponse.addString(httpContext.getJSONResponse( ));
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public void draw( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         disable_std_buttons( ) ;
         enableDisable( ) ;
         set_caption( ) ;
         /* Form start */
         wb_table1_2_044( true) ;
      }
      return  ;
   }

   public void wb_table1_2_044e( boolean wbgen )
   {
      if ( wbgen )
      {
      }
      /* Execute Exit event if defined. */
   }

   public void wb_table1_2_044( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table2_5_044( true) ;
      }
      return  ;
   }

   public void wb_table2_5_044e( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Control Group */
         ClassString = "Group" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroupdata_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"factura"+"</legend>") ;
         wb_table3_27_044( true) ;
      }
      return  ;
   }

   public void wb_table3_27_044e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_044e( true) ;
      }
      else
      {
         wb_table1_2_044e( false) ;
      }
   }

   public void wb_table3_27_044( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         ClassString = "ErrorViewer" ;
         StyleString = "" ;
         GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), StyleString, ClassString, "", "false");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table4_33_044( true) ;
      }
      return  ;
   }

   public void wb_table4_33_044e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"" ;
         ClassString = "BtnEnter" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "rounded", "EENTER.", TempTags, "", httpContext.getButtonType( ), "HLP_factura.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"" ;
         ClassString = "BtnCancel" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "rounded", "ECANCEL.", TempTags, "", httpContext.getButtonType( ), "HLP_factura.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"" ;
         ClassString = "BtnDelete" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 0, "rounded", "EDELETE.", TempTags, "", httpContext.getButtonType( ), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_27_044e( true) ;
      }
      else
      {
         wb_table3_27_044e( false) ;
      }
   }

   public void wb_table4_33_044( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_numero_Internalname, "N�mero", "", "", lblTextblockfac_numero_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A15fac_numero", GXutil.ltrim( GXutil.str( A15fac_numero, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtfac_numero_Internalname, GXutil.ltrim( localUtil.ntoc( A15fac_numero, (byte)(4), (byte)(0), ",", "")), ((edtfac_numero_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A15fac_numero), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A15fac_numero), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(38);\"", "", "", "", "", edtfac_numero_Jsonclick, 0, ClassString, StyleString, "", 1, edtfac_numero_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_fecha_Internalname, "Fecha", "", "", lblTextblockfac_fecha_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A16fac_fecha", localUtil.format(A16fac_fecha, "99/99/99"));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         httpContext.writeText( "<div id=\""+edtfac_fecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( httpContext, edtfac_fecha_Internalname, localUtil.format(A16fac_fecha, "99/99/99"), localUtil.format( A16fac_fecha, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(43);\"", "", "", "", "", edtfac_fecha_Jsonclick, 0, ClassString, StyleString, "", 1, edtfac_fecha_Enabled, 0, 8, "chr", 1, "row", 8, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_factura.htm");
         GxWebStd.gx_bitmap( httpContext, edtfac_fecha_Internalname+"_dp_trigger", "calendar-img.gif", "", "", "", "", ((1==0)||(edtfac_fecha_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;vertical-align:text-bottom", "", "", "", "", "", "HLP_factura.htm");
         httpContext.writeTextNL( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_subtotal_Internalname, "Sub Total", "", "", lblTextblockfac_subtotal_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtfac_subtotal_Internalname, GXutil.ltrim( localUtil.ntoc( A17fac_subtotal, (byte)(10), (byte)(2), ",", "")), ((edtfac_subtotal_Enabled!=0) ? GXutil.ltrim( localUtil.format( A17fac_subtotal, "ZZZZZZ9.99")) : localUtil.format( A17fac_subtotal, "ZZZZZZ9.99")), "", "", "", "", "", edtfac_subtotal_Jsonclick, 0, ClassString, StyleString, "", 1, edtfac_subtotal_Enabled, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_impuesto_Internalname, "Impuesto", "", "", lblTextblockfac_impuesto_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtfac_impuesto_Internalname, GXutil.ltrim( localUtil.ntoc( A18fac_impuesto, (byte)(10), (byte)(2), ",", "")), ((edtfac_impuesto_Enabled!=0) ? GXutil.ltrim( localUtil.format( A18fac_impuesto, "ZZZZZZ9.99")) : localUtil.format( A18fac_impuesto, "ZZZZZZ9.99")), "", "", "", "", "", edtfac_impuesto_Jsonclick, 0, ClassString, StyleString, "", 1, edtfac_impuesto_Enabled, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_total_Internalname, "Total", "", "", lblTextblockfac_total_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtfac_total_Internalname, GXutil.ltrim( localUtil.ntoc( A19fac_total, (byte)(10), (byte)(2), ",", "")), ((edtfac_total_Enabled!=0) ? GXutil.ltrim( localUtil.format( A19fac_total, "ZZZZZZ9.99")) : localUtil.format( A19fac_total, "ZZZZZZ9.99")), "", "", "", "", "", edtfac_total_Jsonclick, 0, ClassString, StyleString, "", 1, edtfac_total_Enabled, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_estado_Internalname, "Estado", "", "", lblTextblockfac_estado_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A20fac_estado", A20fac_estado);
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtfac_estado_Internalname, GXutil.rtrim( A20fac_estado), GXutil.rtrim( localUtil.format( A20fac_estado, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(63);\"", "", "", "", "", edtfac_estado_Jsonclick, 0, ClassString, StyleString, "", 1, edtfac_estado_Enabled, 0, 10, "chr", 1, "row", 10, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockcli_nombres_Internalname, "Cliente", "", "", lblTextblockcli_nombres_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A1cli_codigo", GXutil.ltrim( GXutil.str( A1cli_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtcli_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A1cli_codigo, (byte)(4), (byte)(0), ",", "")), ((edtcli_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A1cli_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A1cli_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(68);\"", "", "", "", "", edtcli_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtcli_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_factura.htm");
         /* Static images/pictures */
         ClassString = "Image" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgprompt_1_Internalname, "prompt.gif", imgprompt_1_Link, "", "", "GeneXusX", imgprompt_1_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "''", "", "HLP_factura.htm");
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A2cli_nombres", A2cli_nombres);
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtcli_nombres_Internalname, GXutil.rtrim( A2cli_nombres), GXutil.rtrim( localUtil.format( A2cli_nombres, "")), "", "", "", "", "", edtcli_nombres_Jsonclick, 0, ClassString, StyleString, "", 1, edtcli_nombres_Enabled, 0, 45, "chr", 1, "row", 45, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_factura.htm");
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A3cli_apellidos", A3cli_apellidos);
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtcli_apellidos_Internalname, GXutil.rtrim( A3cli_apellidos), GXutil.rtrim( localUtil.format( A3cli_apellidos, "")), "", "", "", "", "", edtcli_apellidos_Jsonclick, 0, ClassString, StyleString, "", 1, edtcli_apellidos_Enabled, 0, 45, "chr", 1, "row", 45, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "left", "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td colspan=\"2\" >") ;
         httpContext.writeText( "<br>") ;
         wb_table5_73_044( true) ;
      }
      return  ;
   }

   public void wb_table5_73_044e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "<hr class=\"HRDefault\">") ;
         /*  Grid Control  */
         Gridfactura_detalleContainer.AddObjectProperty("GridName", "Gridfactura_detalle");
         Gridfactura_detalleContainer.AddObjectProperty("Class", "Grid");
         Gridfactura_detalleContainer.AddObjectProperty("Cellpadding", GXutil.ltrim( localUtil.ntoc( 1, (byte)(4), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddObjectProperty("Cellspacing", GXutil.ltrim( localUtil.ntoc( 2, (byte)(4), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddObjectProperty("Backcolorstyle", GXutil.ltrim( localUtil.ntoc( subGridfactura_detalle_Backcolorstyle, (byte)(1), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddObjectProperty("CmpContext", "");
         Gridfactura_detalleContainer.AddObjectProperty("InMasterPage", "false");
         Gridfactura_detalleColumn = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
         Gridfactura_detalleColumn.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ".", "")));
         Gridfactura_detalleColumn.AddObjectProperty("Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_codigo_Enabled, (byte)(5), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddColumnProperties(Gridfactura_detalleColumn);
         Gridfactura_detalleColumn = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
         Gridfactura_detalleColumn.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ".", "")));
         Gridfactura_detalleColumn.AddObjectProperty("Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_codigo_Enabled, (byte)(5), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddColumnProperties(Gridfactura_detalleColumn);
         Gridfactura_detalleColumn = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
         Gridfactura_detalleContainer.AddColumnProperties(Gridfactura_detalleColumn);
         Gridfactura_detalleColumn = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
         Gridfactura_detalleColumn.AddObjectProperty("Value", GXutil.rtrim( A11pro_nombre));
         Gridfactura_detalleColumn.AddObjectProperty("Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_nombre_Enabled, (byte)(5), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddColumnProperties(Gridfactura_detalleColumn);
         Gridfactura_detalleColumn = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
         Gridfactura_detalleColumn.AddObjectProperty("Value", GXutil.rtrim( A12pro_descripcion));
         Gridfactura_detalleColumn.AddObjectProperty("Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_descripcion_Enabled, (byte)(5), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddColumnProperties(Gridfactura_detalleColumn);
         Gridfactura_detalleColumn = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
         Gridfactura_detalleColumn.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A22detfac_cantidad, (byte)(4), (byte)(0), ".", "")));
         Gridfactura_detalleColumn.AddObjectProperty("Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_cantidad_Enabled, (byte)(5), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddColumnProperties(Gridfactura_detalleColumn);
         Gridfactura_detalleColumn = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
         Gridfactura_detalleColumn.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A13pro_precio, (byte)(10), (byte)(2), ".", "")));
         Gridfactura_detalleColumn.AddObjectProperty("Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_precio_Enabled, (byte)(5), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddColumnProperties(Gridfactura_detalleColumn);
         Gridfactura_detalleColumn = GXWebColumn.GetNew(httpContext.isAjaxCallMode( )) ;
         Gridfactura_detalleColumn.AddObjectProperty("Value", GXutil.ltrim( localUtil.ntoc( A24detfac_total, (byte)(10), (byte)(2), ".", "")));
         Gridfactura_detalleColumn.AddObjectProperty("Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_total_Enabled, (byte)(5), (byte)(0), ".", "")));
         Gridfactura_detalleContainer.AddColumnProperties(Gridfactura_detalleColumn);
         Gridfactura_detalleContainer.AddObjectProperty("Allowselection", "false");
         Gridfactura_detalleContainer.AddObjectProperty("Allowcollapsing", ((subGridfactura_detalle_Allowcollapsing==1) ? "true" : "false"));
         Gridfactura_detalleContainer.AddObjectProperty("Collapsed", GXutil.ltrim( localUtil.ntoc( subGridfactura_detalle_Collapsed, (byte)(9), (byte)(0), ".", "")));
         nGXsfl_77_idx = (short)(0) ;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount12 = (short)(5) ;
            if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
            {
               /* Display confirmed (stored) records */
               nRcdExists_12 = (short)(1) ;
               scanStart0412( ) ;
               while ( RcdFound12 != 0 )
               {
                  init_level_properties12( ) ;
                  getByPrimaryKey0412( ) ;
                  addRow0412( ) ;
                  scanNext0412( ) ;
               }
               scanEnd0412( ) ;
               nBlankRcdCount12 = (short)(5) ;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            B17fac_subtotal = A17fac_subtotal ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            standaloneNotModal0412( ) ;
            standaloneModal0412( ) ;
            sMode12 = Gx_mode ;
            while ( nGXsfl_77_idx < nRC_Gridfactura_detalle )
            {
               readRow0412( ) ;
               edtdetfac_codigo_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "DETFAC_CODIGO_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
               edtpro_codigo_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "PRO_CODIGO_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
               edtpro_nombre_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "PRO_NOMBRE_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
               edtpro_descripcion_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "PRO_DESCRIPCION_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
               edtdetfac_cantidad_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "DETFAC_CANTIDAD_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
               edtpro_precio_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "PRO_PRECIO_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
               edtdetfac_total_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "DETFAC_TOTAL_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
               imgprompt_1_Link = httpContext.cgiGet( "PROMPT_10_"+sGXsfl_77_idx+"Link") ;
               if ( ( nRcdExists_12 == 0 ) && ( GXutil.strcmp(Gx_mode, "INS") != 0 ) )
               {
                  Gx_mode = "INS" ;
                  httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal0412( ) ;
               }
               sendRow0412( ) ;
            }
            Gx_mode = sMode12 ;
            httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            A17fac_subtotal = B17fac_subtotal ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount12 = (short)(5) ;
            nRcdExists_12 = (short)(1) ;
            scanStart0412( ) ;
            while ( RcdFound12 != 0 )
            {
               init_level_properties12( ) ;
               getByPrimaryKey0412( ) ;
               standaloneNotModal0412( ) ;
               standaloneModal0412( ) ;
               addRow0412( ) ;
               scanNext0412( ) ;
            }
            scanEnd0412( ) ;
         }
         /* Initialize fields for 'new' records and send them. */
         if ( GXutil.strcmp(Gx_mode, "DSP") != 0 )
         {
            sMode12 = Gx_mode ;
            Gx_mode = "INS" ;
            httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            initAll0412( ) ;
            init_level_properties12( ) ;
            B17fac_subtotal = A17fac_subtotal ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            standaloneNotModal0412( ) ;
            standaloneModal0412( ) ;
            nRcdExists_12 = (short)(0) ;
            nIsMod_12 = (short)(0) ;
            nBlankRcdCount12 = (short)(nBlankRcdUsr12+nBlankRcdCount12) ;
            fRowAdded = 0 ;
            while ( nBlankRcdCount12 > 0 )
            {
               addRow0412( ) ;
               if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
               {
                  fRowAdded = 1 ;
                  GX_FocusControl = edtpro_codigo_Internalname ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               nBlankRcdCount12 = (short)(nBlankRcdCount12-1) ;
            }
            Gx_mode = sMode12 ;
            httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            A17fac_subtotal = B17fac_subtotal ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         }
         sStyleString = " style=\"display:none;\"" ;
         sStyleString = "" ;
         httpContext.writeText( "<div id=\""+"Gridfactura_detalleContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         httpContext.ajax_rsp_assign_grid("_"+"Gridfactura_detalle", Gridfactura_detalleContainer);
         GxWebStd.gx_hidden_field( httpContext, "Gridfactura_detalleContainerData", Gridfactura_detalleContainer.ToJavascriptSource());
         if ( httpContext.isAjaxRequest( ) )
         {
            GxWebStd.gx_hidden_field( httpContext, "Gridfactura_detalleContainerData"+"V", Gridfactura_detalleContainer.GridValuesHidden());
         }
         else
         {
            httpContext.writeText( "<input type=\"hidden\" "+"name=\""+"Gridfactura_detalleContainerData"+"V"+"\" value='"+Gridfactura_detalleContainer.GridValuesHidden()+"'>") ;
         }
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table4_33_044e( true) ;
      }
      else
      {
         wb_table4_33_044e( false) ;
      }
   }

   public void wb_table5_73_044( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable3_Internalname, tblTable3_Internalname, "", "Table95", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td class=\"SubTitle\" >") ;
         /* Text block */
         ClassString = "" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTitledetalle_Internalname, "Detalle", "", "", lblTitledetalle_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table5_73_044e( true) ;
      }
      else
      {
         wb_table5_73_044e( false) ;
      }
   }

   public void wb_table2_5_044( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "<div style=\"WHITE-SPACE: nowrap\" class=\"ToolbarMain\">") ;
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_Internalname, context.getHttpContext().getImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_Visible, 1, "", "Primero", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_separator_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_Internalname, context.getHttpContext().getImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_Internalname, context.getHttpContext().getImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_Visible, 1, "", "Siguiente", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_separator_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_Internalname, context.getHttpContext().getImagePath( "29211874-e613-48e5-9011-8017d984217e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_Visible, 1, "", "Ultimo", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_separator_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_Internalname, context.getHttpContext().getImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_Visible, 1, "", "Seleccionar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_select_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_select_separator_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_Internalname, context.getHttpContext().getImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_Internalname, context.getHttpContext().getImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_Visible, 1, "", "Cancelar", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_Internalname, context.getHttpContext().getImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_factura.htm");
         httpContext.writeText( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_5_044e( true) ;
      }
      else
      {
         wb_table2_5_044e( false) ;
      }
   }

   public void userMain( )
   {
      standaloneStartup( ) ;
   }

   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
      process( ) ;
   }

   public void standaloneStartupServer( )
   {
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      httpContext.wbGlbDoneStart = (byte)(1) ;
      assign_properties_default( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtfac_numero_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtfac_numero_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "FAC_NUMERO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtfac_numero_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A15fac_numero = (short)(0) ;
               n15fac_numero = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A15fac_numero", GXutil.ltrim( GXutil.str( A15fac_numero, 4, 0)));
            }
            else
            {
               A15fac_numero = (short)(localUtil.ctol( httpContext.cgiGet( edtfac_numero_Internalname), ",", ".")) ;
               n15fac_numero = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A15fac_numero", GXutil.ltrim( GXutil.str( A15fac_numero, 4, 0)));
            }
            n15fac_numero = ((0==A15fac_numero) ? true : false) ;
            if ( localUtil.vcdate( httpContext.cgiGet( edtfac_fecha_Internalname), (byte)(3)) == 0 )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_faildate", new Object[] {"Fecha de la factura"}), 1, "FAC_FECHA");
               AnyError = (short)(1) ;
               GX_FocusControl = edtfac_fecha_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A16fac_fecha = GXutil.nullDate() ;
               n16fac_fecha = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A16fac_fecha", localUtil.format(A16fac_fecha, "99/99/99"));
            }
            else
            {
               A16fac_fecha = localUtil.ctod( httpContext.cgiGet( edtfac_fecha_Internalname), 3) ;
               n16fac_fecha = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A16fac_fecha", localUtil.format(A16fac_fecha, "99/99/99"));
            }
            n16fac_fecha = (GXutil.nullDate().equals(A16fac_fecha) ? true : false) ;
            A17fac_subtotal = localUtil.ctond( httpContext.cgiGet( edtfac_subtotal_Internalname)) ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            A18fac_impuesto = localUtil.ctond( httpContext.cgiGet( edtfac_impuesto_Internalname)) ;
            httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
            A19fac_total = localUtil.ctond( httpContext.cgiGet( edtfac_total_Internalname)) ;
            httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
            A20fac_estado = httpContext.cgiGet( edtfac_estado_Internalname) ;
            n20fac_estado = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A20fac_estado", A20fac_estado);
            n20fac_estado = ((GXutil.strcmp("", A20fac_estado)==0) ? true : false) ;
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtcli_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtcli_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "CLI_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtcli_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A1cli_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A1cli_codigo", GXutil.ltrim( GXutil.str( A1cli_codigo, 4, 0)));
            }
            else
            {
               A1cli_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtcli_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A1cli_codigo", GXutil.ltrim( GXutil.str( A1cli_codigo, 4, 0)));
            }
            A2cli_nombres = httpContext.cgiGet( edtcli_nombres_Internalname) ;
            n2cli_nombres = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A2cli_nombres", A2cli_nombres);
            A3cli_apellidos = httpContext.cgiGet( edtcli_apellidos_Internalname) ;
            n3cli_apellidos = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A3cli_apellidos", A3cli_apellidos);
            /* Read saved values. */
            Z14fac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z14fac_codigo"), ",", ".")) ;
            Z15fac_numero = (short)(localUtil.ctol( httpContext.cgiGet( "Z15fac_numero"), ",", ".")) ;
            n15fac_numero = ((0==A15fac_numero) ? true : false) ;
            Z16fac_fecha = localUtil.ctod( httpContext.cgiGet( "Z16fac_fecha"), 0) ;
            n16fac_fecha = (GXutil.nullDate().equals(A16fac_fecha) ? true : false) ;
            Z20fac_estado = httpContext.cgiGet( "Z20fac_estado") ;
            n20fac_estado = ((GXutil.strcmp("", A20fac_estado)==0) ? true : false) ;
            Z1cli_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z1cli_codigo"), ",", ".")) ;
            O17fac_subtotal = localUtil.ctond( httpContext.cgiGet( "O17fac_subtotal")) ;
            IsConfirmed = (short)(localUtil.ctol( httpContext.cgiGet( "IsConfirmed"), ",", ".")) ;
            IsModified = (short)(localUtil.ctol( httpContext.cgiGet( "IsModified"), ",", ".")) ;
            Gx_mode = httpContext.cgiGet( "Mode") ;
            nRC_Gridfactura_detalle = (short)(localUtil.ctol( httpContext.cgiGet( "nRC_Gridfactura_detalle"), ",", ".")) ;
            A14fac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "FAC_CODIGO"), ",", ".")) ;
            Gx_mode = httpContext.cgiGet( "vMODE") ;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( GXutil.strcmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A14fac_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
               httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons( ) ;
               standaloneModal( ) ;
            }
            else
            {
               standaloneModal( ) ;
            }
         }
      }
   }

   public void process( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read Transaction buttons. */
         sEvt = httpContext.cgiGet( "_EventName") ;
         EvtGridId = httpContext.cgiGet( "_EventGridId") ;
         EvtRowId = httpContext.cgiGet( "_EventRowId") ;
         if ( GXutil.len( sEvt) > 0 )
         {
            sEvtType = GXutil.left( sEvt, 1) ;
            sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
            /* Check if conditions changed and reset current page numbers */
            if ( GXutil.strcmp(sEvtType, "M") != 0 )
            {
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( GXutil.strcmp(Gx_mode, "DSP") != 0 )
                        {
                           btn_enter( ) ;
                        }
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                     else if ( GXutil.strcmp(sEvt, "FIRST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_first( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "PREVIOUS") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_previous( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "NEXT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_next( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "LAST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_last( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "SELECT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_select( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "DELETE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( GXutil.strcmp(Gx_mode, "DSP") != 0 )
                        {
                           btn_delete( ) ;
                        }
                     }
                  }
                  else
                  {
                     sEvtType = GXutil.right( sEvt, 4) ;
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-4) ;
                  }
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void afterTrn( )
   {
      if ( trnEnded == 1 )
      {
         trnEnded = 0 ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            /* Clear variables for new insertion. */
            initAll044( ) ;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
         }
      }
   }

   public String toString( )
   {
      return "" ;
   }

   public GXContentInfo getContentInfo( )
   {
      return (GXContentInfo)(null) ;
   }

   public void disable_std_buttons( )
   {
      imgBtn_delete2_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
      imgBtn_delete2_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
      bttBtn_delete_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
      if ( ( GXutil.strcmp(sMode4, "DSP") == 0 ) || ( GXutil.strcmp(sMode4, "DLT") == 0 ) )
      {
         imgBtn_delete2_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
         imgBtn_delete2_separator_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
         bttBtn_delete_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
         if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_Visible, 5, 0)));
            imgBtn_enter2_separator_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_separator_Visible, 5, 0)));
            bttBtn_enter_Visible = 0 ;
            httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_enter_Visible, 5, 0)));
         }
         disableAttributes044( ) ;
      }
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_confdelete"), 0, "");
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_mustconfirm"), 0, "");
         }
      }
   }

   public void confirm_040( )
   {
      beforeValidate044( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            onDeleteControls044( ) ;
         }
         else
         {
            checkExtendedTable044( ) ;
            closeExtendedTableCursors044( ) ;
         }
      }
      if ( AnyError == 0 )
      {
         /* Save parent mode. */
         sMode4 = Gx_mode ;
         confirm_0412( ) ;
         if ( AnyError == 0 )
         {
            /* Restore parent mode. */
            Gx_mode = sMode4 ;
            httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            IsConfirmed = (short)(1) ;
         }
         /* Restore parent mode. */
         Gx_mode = sMode4 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
   }

   public void confirm_0412( )
   {
      s17fac_subtotal = O17fac_subtotal ;
      n17fac_subtotal = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
      s18fac_impuesto = O18fac_impuesto ;
      httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
      s19fac_total = O19fac_total ;
      httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
      nGXsfl_77_idx = (short)(0) ;
      while ( nGXsfl_77_idx < nRC_Gridfactura_detalle )
      {
         readRow0412( ) ;
         if ( ( nRcdExists_12 != 0 ) || ( nIsMod_12 != 0 ) )
         {
            getKey0412( ) ;
            if ( ( nRcdExists_12 == 0 ) && ( nRcdDeleted_12 == 0 ) )
            {
               if ( RcdFound12 == 0 )
               {
                  Gx_mode = "INS" ;
                  httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  beforeValidate0412( ) ;
                  if ( AnyError == 0 )
                  {
                     checkExtendedTable0412( ) ;
                     closeExtendedTableCursors0412( ) ;
                     if ( AnyError == 0 )
                     {
                        IsConfirmed = (short)(1) ;
                     }
                     O17fac_subtotal = A17fac_subtotal ;
                     n17fac_subtotal = false ;
                     httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
                     O18fac_impuesto = A18fac_impuesto ;
                     httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
                     O19fac_total = A19fac_total ;
                     httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( RcdFound12 != 0 )
               {
                  if ( nRcdDeleted_12 != 0 )
                  {
                     Gx_mode = "DLT" ;
                     httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     getByPrimaryKey0412( ) ;
                     load0412( ) ;
                     beforeValidate0412( ) ;
                     if ( AnyError == 0 )
                     {
                        onDeleteControls0412( ) ;
                        O17fac_subtotal = A17fac_subtotal ;
                        n17fac_subtotal = false ;
                        httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
                        O18fac_impuesto = A18fac_impuesto ;
                        httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
                        O19fac_total = A19fac_total ;
                        httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
                     }
                  }
                  else
                  {
                     if ( nIsMod_12 != 0 )
                     {
                        Gx_mode = "UPD" ;
                        httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        beforeValidate0412( ) ;
                        if ( AnyError == 0 )
                        {
                           checkExtendedTable0412( ) ;
                           closeExtendedTableCursors0412( ) ;
                           if ( AnyError == 0 )
                           {
                              IsConfirmed = (short)(1) ;
                           }
                           O17fac_subtotal = A17fac_subtotal ;
                           n17fac_subtotal = false ;
                           httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
                           O18fac_impuesto = A18fac_impuesto ;
                           httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
                           O19fac_total = A19fac_total ;
                           httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
                        }
                     }
                  }
               }
               else
               {
                  if ( nRcdDeleted_12 == 0 )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         httpContext.changePostValue( edtdetfac_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( edtpro_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( edtpro_nombre_Internalname, GXutil.rtrim( A11pro_nombre)) ;
         httpContext.changePostValue( edtpro_descripcion_Internalname, GXutil.rtrim( A12pro_descripcion)) ;
         httpContext.changePostValue( edtdetfac_cantidad_Internalname, GXutil.ltrim( localUtil.ntoc( A22detfac_cantidad, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( edtpro_precio_Internalname, GXutil.ltrim( localUtil.ntoc( A13pro_precio, (byte)(10), (byte)(2), ",", ""))) ;
         httpContext.changePostValue( edtdetfac_total_Internalname, GXutil.ltrim( localUtil.ntoc( A24detfac_total, (byte)(10), (byte)(2), ",", ""))) ;
         httpContext.changePostValue( "ZT_"+"Z21detfac_codigo_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( Z21detfac_codigo, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "ZT_"+"Z22detfac_cantidad_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( Z22detfac_cantidad, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "ZT_"+"Z10pro_codigo_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( Z10pro_codigo, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "T24detfac_total_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( O24detfac_total, (byte)(10), (byte)(2), ",", ""))) ;
         httpContext.changePostValue( "nRcdDeleted_12_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( nRcdDeleted_12, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "nRcdExists_12_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( nRcdExists_12, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "nIsMod_12_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( nIsMod_12, (byte)(4), (byte)(0), ",", ""))) ;
         if ( nIsMod_12 != 0 )
         {
            httpContext.changePostValue( "DETFAC_CODIGO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_codigo_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "PRO_CODIGO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_codigo_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "PRO_NOMBRE_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_nombre_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "PRO_DESCRIPCION_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_descripcion_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "DETFAC_CANTIDAD_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_cantidad_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "PRO_PRECIO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_precio_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "DETFAC_TOTAL_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_total_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
         }
      }
      O17fac_subtotal = s17fac_subtotal ;
      n17fac_subtotal = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
      O18fac_impuesto = s18fac_impuesto ;
      httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
      O19fac_total = s19fac_total ;
      httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
      /* Start of After( level) rules */
      /* End of After( level) rules */
   }

   public void resetCaption040( )
   {
   }

   public void zm044( int GX_JID )
   {
      if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
         {
            Z15fac_numero = T00046_A15fac_numero[0] ;
            Z16fac_fecha = T00046_A16fac_fecha[0] ;
            Z20fac_estado = T00046_A20fac_estado[0] ;
            Z1cli_codigo = T00046_A1cli_codigo[0] ;
         }
         else
         {
            Z15fac_numero = A15fac_numero ;
            Z16fac_fecha = A16fac_fecha ;
            Z20fac_estado = A20fac_estado ;
            Z1cli_codigo = A1cli_codigo ;
         }
      }
      if ( GX_JID == -9 )
      {
         Z14fac_codigo = A14fac_codigo ;
         Z15fac_numero = A15fac_numero ;
         Z16fac_fecha = A16fac_fecha ;
         Z20fac_estado = A20fac_estado ;
         Z1cli_codigo = A1cli_codigo ;
         Z17fac_subtotal = A17fac_subtotal ;
         Z2cli_nombres = A2cli_nombres ;
         Z3cli_apellidos = A3cli_apellidos ;
      }
   }

   public void standaloneNotModal( )
   {
      imgprompt_1_Link = ((GXutil.strcmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010"+"',["+"{Ctrl:gx.dom.el('"+"CLI_CODIGO"+"'), id:'"+"CLI_CODIGO"+"'"+",isOut: true}"+"],"+"null"+","+"'', false"+","+"false"+");") ;
      imgBtn_delete2_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
   }

   public void standaloneModal( )
   {
      if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
      {
         imgBtn_enter2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
      else
      {
         imgBtn_enter2_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
   }

   public void load044( )
   {
      /* Using cursor T000411 */
      pr_default.execute(7, new Object[] {new Short(A14fac_codigo)});
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound4 = (short)(1) ;
         A15fac_numero = T000411_A15fac_numero[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A15fac_numero", GXutil.ltrim( GXutil.str( A15fac_numero, 4, 0)));
         n15fac_numero = T000411_n15fac_numero[0] ;
         A16fac_fecha = T000411_A16fac_fecha[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A16fac_fecha", localUtil.format(A16fac_fecha, "99/99/99"));
         n16fac_fecha = T000411_n16fac_fecha[0] ;
         A20fac_estado = T000411_A20fac_estado[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A20fac_estado", A20fac_estado);
         n20fac_estado = T000411_n20fac_estado[0] ;
         A2cli_nombres = T000411_A2cli_nombres[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A2cli_nombres", A2cli_nombres);
         n2cli_nombres = T000411_n2cli_nombres[0] ;
         A3cli_apellidos = T000411_A3cli_apellidos[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A3cli_apellidos", A3cli_apellidos);
         n3cli_apellidos = T000411_n3cli_apellidos[0] ;
         A1cli_codigo = T000411_A1cli_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A1cli_codigo", GXutil.ltrim( GXutil.str( A1cli_codigo, 4, 0)));
         A17fac_subtotal = T000411_A17fac_subtotal[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         n17fac_subtotal = T000411_n17fac_subtotal[0] ;
         zm044( -9) ;
      }
      pr_default.close(7);
      onLoadActions044( ) ;
   }

   public void onLoadActions044( )
   {
      O17fac_subtotal = A17fac_subtotal ;
      n17fac_subtotal = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
   }

   public void checkExtendedTable044( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      if ( ! ( GXutil.nullDate().equals(A16fac_fecha) || (( A16fac_fecha.after( localUtil.ymdtod( 1753, 1, 1) ) ) || ( A16fac_fecha.equals( localUtil.ymdtod( 1753, 1, 1) ) )) ) )
      {
         httpContext.GX_msglist.addItem("Campo Fecha de la factura fuera de rango", "OutOfRange", 1, "");
         AnyError = (short)(1) ;
      }
      /* Using cursor T00047 */
      pr_default.execute(5, new Object[] {new Short(A1cli_codigo)});
      if ( (pr_default.getStatus(5) == 101) )
      {
         AnyError1 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'cliente'.", "ForeignKeyNotFound", 1, "CLI_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtcli_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError1 == 0 )
      {
         A2cli_nombres = T00047_A2cli_nombres[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A2cli_nombres", A2cli_nombres);
         n2cli_nombres = T00047_n2cli_nombres[0] ;
         A3cli_apellidos = T00047_A3cli_apellidos[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A3cli_apellidos", A3cli_apellidos);
         n3cli_apellidos = T00047_n3cli_apellidos[0] ;
      }
      pr_default.close(5);
   }

   public void closeExtendedTableCursors044( )
   {
      pr_default.close(5);
   }

   public void enableDisable( )
   {
   }

   public void gxload_10( short A1cli_codigo )
   {
      /* Using cursor T000412 */
      pr_default.execute(8, new Object[] {new Short(A1cli_codigo)});
      if ( (pr_default.getStatus(8) == 101) )
      {
         AnyError1 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'cliente'.", "ForeignKeyNotFound", 1, "CLI_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtcli_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError1 == 0 )
      {
         A2cli_nombres = T000412_A2cli_nombres[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A2cli_nombres", A2cli_nombres);
         n2cli_nombres = T000412_n2cli_nombres[0] ;
         A3cli_apellidos = T000412_A3cli_apellidos[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A3cli_apellidos", A3cli_apellidos);
         n3cli_apellidos = T000412_n3cli_apellidos[0] ;
      }
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.GX_webresponse.addString("new Array( new Array(");
      httpContext.GX_webresponse.addString("\""+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A2cli_nombres))+"\""+","+"\""+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A3cli_apellidos))+"\"");
      httpContext.GX_webresponse.addString(")");
      if ( (pr_default.getStatus(8) == 101) )
      {
         httpContext.GX_webresponse.addString(",");
         httpContext.GX_webresponse.addString("101");
      }
      httpContext.GX_webresponse.addString(")");
      pr_default.close(8);
   }

   public void getKey044( )
   {
      /* Using cursor T000413 */
      pr_default.execute(9, new Object[] {new Short(A14fac_codigo)});
      if ( (pr_default.getStatus(9) != 101) )
      {
         RcdFound4 = (short)(1) ;
      }
      else
      {
         RcdFound4 = (short)(0) ;
      }
      pr_default.close(9);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00046 */
      pr_default.execute(4, new Object[] {new Short(A14fac_codigo)});
      if ( (pr_default.getStatus(4) != 101) )
      {
         zm044( 9) ;
         RcdFound4 = (short)(1) ;
         A14fac_codigo = T00046_A14fac_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
         A15fac_numero = T00046_A15fac_numero[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A15fac_numero", GXutil.ltrim( GXutil.str( A15fac_numero, 4, 0)));
         n15fac_numero = T00046_n15fac_numero[0] ;
         A16fac_fecha = T00046_A16fac_fecha[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A16fac_fecha", localUtil.format(A16fac_fecha, "99/99/99"));
         n16fac_fecha = T00046_n16fac_fecha[0] ;
         A20fac_estado = T00046_A20fac_estado[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A20fac_estado", A20fac_estado);
         n20fac_estado = T00046_n20fac_estado[0] ;
         A1cli_codigo = T00046_A1cli_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A1cli_codigo", GXutil.ltrim( GXutil.str( A1cli_codigo, 4, 0)));
         Z14fac_codigo = A14fac_codigo ;
         sMode4 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         load044( ) ;
         Gx_mode = sMode4 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         RcdFound4 = (short)(0) ;
         initializeNonKey044( ) ;
         sMode4 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal( ) ;
         Gx_mode = sMode4 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      pr_default.close(4);
   }

   public void getEqualNoModal( )
   {
      getKey044( ) ;
      if ( RcdFound4 == 0 )
      {
      }
      else
      {
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound4 = (short)(0) ;
      /* Using cursor T000414 */
      pr_default.execute(10, new Object[] {new Short(A14fac_codigo)});
      if ( (pr_default.getStatus(10) != 101) )
      {
         while ( (pr_default.getStatus(10) != 101) && ( ( T000414_A14fac_codigo[0] < A14fac_codigo ) ) )
         {
            pr_default.readNext(10);
         }
         if ( (pr_default.getStatus(10) != 101) && ( ( T000414_A14fac_codigo[0] > A14fac_codigo ) ) )
         {
            A14fac_codigo = T000414_A14fac_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
            RcdFound4 = (short)(1) ;
         }
      }
      pr_default.close(10);
   }

   public void move_previous( )
   {
      RcdFound4 = (short)(0) ;
      /* Using cursor T000415 */
      pr_default.execute(11, new Object[] {new Short(A14fac_codigo)});
      if ( (pr_default.getStatus(11) != 101) )
      {
         while ( (pr_default.getStatus(11) != 101) && ( ( T000415_A14fac_codigo[0] > A14fac_codigo ) ) )
         {
            pr_default.readNext(11);
         }
         if ( (pr_default.getStatus(11) != 101) && ( ( T000415_A14fac_codigo[0] < A14fac_codigo ) ) )
         {
            A14fac_codigo = T000415_A14fac_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
            RcdFound4 = (short)(1) ;
         }
      }
      pr_default.close(11);
   }

   public void btn_enter( )
   {
      nKeyPressed = (byte)(1) ;
      getKey044( ) ;
      if ( RcdFound4 == 1 )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
            AnyError = (short)(1) ;
         }
         else if ( A14fac_codigo != Z14fac_codigo )
         {
            A14fac_codigo = Z14fac_codigo ;
            httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "");
            AnyError = (short)(1) ;
         }
         else if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            A17fac_subtotal = O17fac_subtotal ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            A18fac_impuesto = O18fac_impuesto ;
            httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
            A19fac_total = O19fac_total ;
            httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
            delete( ) ;
            afterTrn( ) ;
            GX_FocusControl = edtfac_numero_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            /* Update record */
            A17fac_subtotal = O17fac_subtotal ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            A18fac_impuesto = O18fac_impuesto ;
            httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
            A19fac_total = O19fac_total ;
            httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
            update044( ) ;
            GX_FocusControl = edtfac_numero_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }
      else
      {
         if ( A14fac_codigo != Z14fac_codigo )
         {
            /* Insert record */
            A17fac_subtotal = O17fac_subtotal ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            A18fac_impuesto = O18fac_impuesto ;
            httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
            A19fac_total = O19fac_total ;
            httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
            GX_FocusControl = edtfac_numero_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            insert044( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "" ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( GXutil.strcmp(Gx_mode, "UPD") == 0 )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "");
               AnyError = (short)(1) ;
            }
            else
            {
               /* Insert record */
               A17fac_subtotal = O17fac_subtotal ;
               n17fac_subtotal = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
               A18fac_impuesto = O18fac_impuesto ;
               httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
               A19fac_total = O19fac_total ;
               httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
               GX_FocusControl = edtfac_numero_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               insert044( ) ;
               if ( AnyError == 1 )
               {
                  GX_FocusControl = "" ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
         }
      }
      afterTrn( ) ;
   }

   public void btn_delete( )
   {
      if ( A14fac_codigo != Z14fac_codigo )
      {
         A14fac_codigo = Z14fac_codigo ;
         httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforedlt"), 1, "");
         AnyError = (short)(1) ;
      }
      else
      {
         A17fac_subtotal = O17fac_subtotal ;
         n17fac_subtotal = false ;
         httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         A18fac_impuesto = O18fac_impuesto ;
         httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
         A19fac_total = O19fac_total ;
         httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
         delete( ) ;
         afterTrn( ) ;
         GX_FocusControl = edtfac_numero_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError != 0 )
      {
      }
      getByPrimaryKey( ) ;
      CloseOpenCursors();
   }

   public void btn_get( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      if ( RcdFound4 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "");
         AnyError = (short)(1) ;
      }
      GX_FocusControl = edtfac_numero_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_first( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart044( ) ;
      if ( RcdFound4 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edtfac_numero_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd044( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_previous( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_previous( ) ;
      if ( RcdFound4 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edtfac_numero_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_next( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_next( ) ;
      if ( RcdFound4 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
      }
      GX_FocusControl = edtfac_numero_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_last( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart044( ) ;
      if ( RcdFound4 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         while ( RcdFound4 != 0 )
         {
            scanNext044( ) ;
         }
      }
      GX_FocusControl = edtfac_numero_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd044( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_select( )
   {
      getEqualNoModal( ) ;
   }

   public void checkOptimisticConcurrency044( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         /* Using cursor T00045 */
         pr_default.execute(3, new Object[] {new Short(A14fac_codigo)});
         if ( (pr_default.getStatus(3) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"factura"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(3) == 101) || ( Z15fac_numero != T00045_A15fac_numero[0] ) || !( Z16fac_fecha.equals( T00045_A16fac_fecha[0] ) ) || ( GXutil.strcmp(Z20fac_estado, T00045_A20fac_estado[0]) != 0 ) || ( Z1cli_codigo != T00045_A1cli_codigo[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"factura"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert044( )
   {
      beforeValidate044( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable044( ) ;
      }
      if ( AnyError == 0 )
      {
         zm044( 0) ;
         checkOptimisticConcurrency044( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm044( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert044( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000416 */
                  pr_default.execute(12, new Object[] {new Boolean(n15fac_numero), new Short(A15fac_numero), new Boolean(n16fac_fecha), A16fac_fecha, new Boolean(n20fac_estado), A20fac_estado, new Short(A1cli_codigo)});
                  /* Retrieving last key number assigned */
                  /* Using cursor T000417 */
                  pr_default.execute(13);
                  A14fac_codigo = T000417_A14fac_codigo[0] ;
                  httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
                  pr_default.close(13);
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        processLevel044( ) ;
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucadded"), 0, "");
                           resetCaption040( ) ;
                        }
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load044( ) ;
         }
         endLevel044( ) ;
      }
      closeExtendedTableCursors044( ) ;
   }

   public void update044( )
   {
      beforeValidate044( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable044( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency044( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm044( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate044( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000418 */
                  pr_default.execute(14, new Object[] {new Boolean(n15fac_numero), new Short(A15fac_numero), new Boolean(n16fac_fecha), A16fac_fecha, new Boolean(n20fac_estado), A20fac_estado, new Short(A1cli_codigo), new Short(A14fac_codigo)});
                  if ( (pr_default.getStatus(14) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"factura"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate044( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        processLevel044( ) ;
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucupdated"), 0, "");
                           resetCaption040( ) ;
                        }
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel044( ) ;
      }
      closeExtendedTableCursors044( ) ;
   }

   public void deferredUpdate044( )
   {
   }

   public void delete( )
   {
      beforeValidate044( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency044( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls044( ) ;
         afterConfirm044( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete044( ) ;
            if ( AnyError == 0 )
            {
               A17fac_subtotal = O17fac_subtotal ;
               n17fac_subtotal = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
               A18fac_impuesto = O18fac_impuesto ;
               httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
               A19fac_total = O19fac_total ;
               httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
               scanStart0412( ) ;
               while ( RcdFound12 != 0 )
               {
                  getByPrimaryKey0412( ) ;
                  delete0412( ) ;
                  scanNext0412( ) ;
                  O17fac_subtotal = A17fac_subtotal ;
                  n17fac_subtotal = false ;
                  httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
                  O18fac_impuesto = A18fac_impuesto ;
                  httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
                  O19fac_total = A19fac_total ;
                  httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
               }
               scanEnd0412( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000419 */
                  pr_default.execute(15, new Object[] {new Short(A14fac_codigo)});
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound4 == 0 )
                        {
                           initAll044( ) ;
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                        }
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucdeleted"), 0, "");
                        resetCaption040( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
      }
      sMode4 = Gx_mode ;
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      endLevel044( ) ;
      Gx_mode = sMode4 ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
   }

   public void onDeleteControls044( )
   {
      standaloneModal( ) ;
      if ( AnyError == 0 )
      {
         /* Delete mode formulas */
         /* Using cursor T000420 */
         pr_default.execute(16, new Object[] {new Short(A1cli_codigo)});
         A2cli_nombres = T000420_A2cli_nombres[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A2cli_nombres", A2cli_nombres);
         n2cli_nombres = T000420_n2cli_nombres[0] ;
         A3cli_apellidos = T000420_A3cli_apellidos[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A3cli_apellidos", A3cli_apellidos);
         n3cli_apellidos = T000420_n3cli_apellidos[0] ;
         pr_default.close(16);
      }
   }

   public void processNestedLevel0412( )
   {
      s17fac_subtotal = O17fac_subtotal ;
      n17fac_subtotal = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
      s18fac_impuesto = O18fac_impuesto ;
      httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
      s19fac_total = O19fac_total ;
      httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
      nGXsfl_77_idx = (short)(0) ;
      while ( nGXsfl_77_idx < nRC_Gridfactura_detalle )
      {
         readRow0412( ) ;
         if ( ( nRcdExists_12 != 0 ) || ( nIsMod_12 != 0 ) )
         {
            standaloneNotModal0412( ) ;
            getKey0412( ) ;
            if ( ( nRcdExists_12 == 0 ) && ( nRcdDeleted_12 == 0 ) )
            {
               if ( RcdFound12 == 0 )
               {
                  Gx_mode = "INS" ;
                  httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  insert0412( ) ;
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                  AnyError = (short)(1) ;
               }
            }
            else
            {
               if ( RcdFound12 != 0 )
               {
                  if ( ( nRcdDeleted_12 != 0 ) && ( nRcdExists_12 != 0 ) )
                  {
                     Gx_mode = "DLT" ;
                     httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     delete0412( ) ;
                  }
                  else
                  {
                     if ( ( nIsMod_12 != 0 ) && ( nRcdExists_12 != 0 ) )
                     {
                        Gx_mode = "UPD" ;
                        httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        update0412( ) ;
                     }
                  }
               }
               else
               {
                  if ( nRcdDeleted_12 == 0 )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
            O17fac_subtotal = A17fac_subtotal ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            O18fac_impuesto = A18fac_impuesto ;
            httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
            O19fac_total = A19fac_total ;
            httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
         }
         httpContext.changePostValue( edtdetfac_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( edtpro_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( edtpro_nombre_Internalname, GXutil.rtrim( A11pro_nombre)) ;
         httpContext.changePostValue( edtpro_descripcion_Internalname, GXutil.rtrim( A12pro_descripcion)) ;
         httpContext.changePostValue( edtdetfac_cantidad_Internalname, GXutil.ltrim( localUtil.ntoc( A22detfac_cantidad, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( edtpro_precio_Internalname, GXutil.ltrim( localUtil.ntoc( A13pro_precio, (byte)(10), (byte)(2), ",", ""))) ;
         httpContext.changePostValue( edtdetfac_total_Internalname, GXutil.ltrim( localUtil.ntoc( A24detfac_total, (byte)(10), (byte)(2), ",", ""))) ;
         httpContext.changePostValue( "ZT_"+"Z21detfac_codigo_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( Z21detfac_codigo, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "ZT_"+"Z22detfac_cantidad_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( Z22detfac_cantidad, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "ZT_"+"Z10pro_codigo_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( Z10pro_codigo, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "T24detfac_total_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( O24detfac_total, (byte)(10), (byte)(2), ",", ""))) ;
         httpContext.changePostValue( "nRcdDeleted_12_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( nRcdDeleted_12, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "nRcdExists_12_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( nRcdExists_12, (byte)(4), (byte)(0), ",", ""))) ;
         httpContext.changePostValue( "nIsMod_12_"+sGXsfl_77_idx, GXutil.ltrim( localUtil.ntoc( nIsMod_12, (byte)(4), (byte)(0), ",", ""))) ;
         if ( nIsMod_12 != 0 )
         {
            httpContext.changePostValue( "DETFAC_CODIGO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_codigo_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "PRO_CODIGO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_codigo_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "PRO_NOMBRE_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_nombre_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "PRO_DESCRIPCION_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_descripcion_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "DETFAC_CANTIDAD_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_cantidad_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "PRO_PRECIO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_precio_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
            httpContext.changePostValue( "DETFAC_TOTAL_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_total_Enabled, (byte)(5), (byte)(0), ".", ""))) ;
         }
      }
      /* Start of After( level) rules */
      /* End of After( level) rules */
      initAll0412( ) ;
      if ( AnyError != 0 )
      {
         O17fac_subtotal = s17fac_subtotal ;
         n17fac_subtotal = false ;
         httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         O18fac_impuesto = s18fac_impuesto ;
         httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
         O19fac_total = s19fac_total ;
         httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
      }
      nRcdExists_12 = (short)(0) ;
      nIsMod_12 = (short)(0) ;
      nRcdDeleted_12 = (short)(0) ;
   }

   public void processLevel044( )
   {
      /* Save parent mode. */
      sMode4 = Gx_mode ;
      processNestedLevel0412( ) ;
      if ( AnyError != 0 )
      {
         O17fac_subtotal = s17fac_subtotal ;
         n17fac_subtotal = false ;
         httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         O18fac_impuesto = s18fac_impuesto ;
         httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
         O19fac_total = s19fac_total ;
         httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
      }
      /* Restore parent mode. */
      Gx_mode = sMode4 ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      /* ' Update level parameters */
   }

   public void endLevel044( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         pr_default.close(3);
      }
      if ( AnyError == 0 )
      {
         beforeComplete044( ) ;
      }
      if ( AnyError == 0 )
      {
         pr_default.close(1);
         pr_default.close(0);
         pr_default.close(16);
         pr_default.close(6);
         pr_default.close(2);
         Application.commit(context, remoteHandle, "DEFAULT", "factura");
         if ( AnyError == 0 )
         {
            confirmValues040( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         pr_default.close(1);
         pr_default.close(0);
         pr_default.close(16);
         pr_default.close(6);
         pr_default.close(2);
         Application.rollback(context, remoteHandle, "DEFAULT", "factura");
      }
      IsModified = (short)(0) ;
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanStart044( )
   {
      /* Using cursor T000421 */
      pr_default.execute(17);
      RcdFound4 = (short)(0) ;
      if ( (pr_default.getStatus(17) != 101) )
      {
         RcdFound4 = (short)(1) ;
         A14fac_codigo = T000421_A14fac_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
      }
      /* Load Subordinate Levels */
   }

   public void scanNext044( )
   {
      pr_default.readNext(17);
      RcdFound4 = (short)(0) ;
      if ( (pr_default.getStatus(17) != 101) )
      {
         RcdFound4 = (short)(1) ;
         A14fac_codigo = T000421_A14fac_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
      }
   }

   public void scanEnd044( )
   {
      pr_default.close(17);
   }

   public void afterConfirm044( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert044( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate044( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete044( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete044( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate044( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes044( )
   {
      edtfac_numero_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtfac_numero_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtfac_numero_Enabled, 5, 0)));
      edtfac_fecha_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtfac_fecha_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtfac_fecha_Enabled, 5, 0)));
      edtfac_subtotal_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtfac_subtotal_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtfac_subtotal_Enabled, 5, 0)));
      edtfac_impuesto_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtfac_impuesto_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtfac_impuesto_Enabled, 5, 0)));
      edtfac_total_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtfac_total_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtfac_total_Enabled, 5, 0)));
      edtfac_estado_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtfac_estado_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtfac_estado_Enabled, 5, 0)));
      edtcli_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtcli_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtcli_codigo_Enabled, 5, 0)));
      edtcli_nombres_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtcli_nombres_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtcli_nombres_Enabled, 5, 0)));
      edtcli_apellidos_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtcli_apellidos_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtcli_apellidos_Enabled, 5, 0)));
   }

   public void zm0412( int GX_JID )
   {
      if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
         {
            Z22detfac_cantidad = T00043_A22detfac_cantidad[0] ;
            Z10pro_codigo = T00043_A10pro_codigo[0] ;
         }
         else
         {
            Z22detfac_cantidad = A22detfac_cantidad ;
            Z10pro_codigo = A10pro_codigo ;
         }
      }
      if ( GX_JID == -12 )
      {
         Z14fac_codigo = A14fac_codigo ;
         Z21detfac_codigo = A21detfac_codigo ;
         Z22detfac_cantidad = A22detfac_cantidad ;
         Z10pro_codigo = A10pro_codigo ;
         Z13pro_precio = A13pro_precio ;
         Z11pro_nombre = A11pro_nombre ;
         Z12pro_descripcion = A12pro_descripcion ;
      }
   }

   public void standaloneNotModal0412( )
   {
      edtdetfac_codigo_Enabled = 0 ;
   }

   public void standaloneModal0412( )
   {
      if ( ( GXutil.strcmp(sMode12, "DSP") == 0 ) || ( GXutil.strcmp(sMode12, "DLT") == 0 ) )
      {
         disableAttributes0412( ) ;
      }
   }

   public void load0412( )
   {
      /* Using cursor T000422 */
      pr_default.execute(18, new Object[] {new Short(A14fac_codigo), new Short(A21detfac_codigo)});
      if ( (pr_default.getStatus(18) != 101) )
      {
         RcdFound12 = (short)(1) ;
         A22detfac_cantidad = T000422_A22detfac_cantidad[0] ;
         A13pro_precio = T000422_A13pro_precio[0] ;
         n13pro_precio = T000422_n13pro_precio[0] ;
         A11pro_nombre = T000422_A11pro_nombre[0] ;
         n11pro_nombre = T000422_n11pro_nombre[0] ;
         A12pro_descripcion = T000422_A12pro_descripcion[0] ;
         n12pro_descripcion = T000422_n12pro_descripcion[0] ;
         A10pro_codigo = T000422_A10pro_codigo[0] ;
         zm0412( -12) ;
      }
      pr_default.close(18);
      onLoadActions0412( ) ;
   }

   public void onLoadActions0412( )
   {
      A24detfac_total = DecimalUtil.doubleToDec(A22detfac_cantidad).multiply(A13pro_precio) ;
      O24detfac_total = A24detfac_total ;
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
      {
         A17fac_subtotal = O17fac_subtotal.add(A24detfac_total) ;
         n17fac_subtotal = false ;
         httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  )
         {
            A17fac_subtotal = O17fac_subtotal.add(A24detfac_total).subtract(O24detfac_total) ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
            {
               A17fac_subtotal = O17fac_subtotal.subtract(O24detfac_total) ;
               n17fac_subtotal = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            }
         }
      }
      A18fac_impuesto = A17fac_subtotal.multiply(DecimalUtil.doubleToDec(0.19,4,2)) ;
      httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
      A19fac_total = A17fac_subtotal.add(A18fac_impuesto) ;
      httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
   }

   public void checkExtendedTable0412( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal0412( ) ;
      /* Using cursor T00044 */
      pr_default.execute(2, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(2) == 101) )
      {
         AnyError10 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'producto'.", "ForeignKeyNotFound", 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError10 == 0 )
      {
         A13pro_precio = T00044_A13pro_precio[0] ;
         n13pro_precio = T00044_n13pro_precio[0] ;
         A11pro_nombre = T00044_A11pro_nombre[0] ;
         n11pro_nombre = T00044_n11pro_nombre[0] ;
         A12pro_descripcion = T00044_A12pro_descripcion[0] ;
         n12pro_descripcion = T00044_n12pro_descripcion[0] ;
      }
      pr_default.close(2);
      A24detfac_total = DecimalUtil.doubleToDec(A22detfac_cantidad).multiply(A13pro_precio) ;
      if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
      {
         A17fac_subtotal = O17fac_subtotal.add(A24detfac_total) ;
         n17fac_subtotal = false ;
         httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
      }
      else
      {
         if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  )
         {
            A17fac_subtotal = O17fac_subtotal.add(A24detfac_total).subtract(O24detfac_total) ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
            {
               A17fac_subtotal = O17fac_subtotal.subtract(O24detfac_total) ;
               n17fac_subtotal = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            }
         }
      }
      A18fac_impuesto = A17fac_subtotal.multiply(DecimalUtil.doubleToDec(0.19,4,2)) ;
      httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
      A19fac_total = A17fac_subtotal.add(A18fac_impuesto) ;
      httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
   }

   public void closeExtendedTableCursors0412( )
   {
      pr_default.close(2);
   }

   public void enableDisable0412( )
   {
   }

   public void gxload_13( short A10pro_codigo )
   {
      /* Using cursor T000423 */
      pr_default.execute(19, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(19) == 101) )
      {
         AnyError10 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'producto'.", "ForeignKeyNotFound", 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError10 == 0 )
      {
         A13pro_precio = T000423_A13pro_precio[0] ;
         n13pro_precio = T000423_n13pro_precio[0] ;
         A11pro_nombre = T000423_A11pro_nombre[0] ;
         n11pro_nombre = T000423_n11pro_nombre[0] ;
         A12pro_descripcion = T000423_A12pro_descripcion[0] ;
         n12pro_descripcion = T000423_n12pro_descripcion[0] ;
      }
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.GX_webresponse.addString("new Array( new Array(");
      httpContext.GX_webresponse.addString("\""+PrivateUtilities.encodeJSConstant( GXutil.ltrim( localUtil.ntoc( A13pro_precio, (byte)(10), (byte)(2), ".", "")))+"\""+","+"\""+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A11pro_nombre))+"\""+","+"\""+PrivateUtilities.encodeJSConstant( GXutil.rtrim( A12pro_descripcion))+"\"");
      httpContext.GX_webresponse.addString(")");
      if ( (pr_default.getStatus(19) == 101) )
      {
         httpContext.GX_webresponse.addString(",");
         httpContext.GX_webresponse.addString("101");
      }
      httpContext.GX_webresponse.addString(")");
      pr_default.close(19);
   }

   public void getKey0412( )
   {
      /* Using cursor T000424 */
      pr_default.execute(20, new Object[] {new Short(A14fac_codigo), new Short(A21detfac_codigo)});
      if ( (pr_default.getStatus(20) != 101) )
      {
         RcdFound12 = (short)(1) ;
      }
      else
      {
         RcdFound12 = (short)(0) ;
      }
      pr_default.close(20);
   }

   public void getByPrimaryKey0412( )
   {
      /* Using cursor T00043 */
      pr_default.execute(1, new Object[] {new Short(A14fac_codigo), new Short(A21detfac_codigo)});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm0412( 12) ;
         RcdFound12 = (short)(1) ;
         initializeNonKey0412( ) ;
         A21detfac_codigo = T00043_A21detfac_codigo[0] ;
         A22detfac_cantidad = T00043_A22detfac_cantidad[0] ;
         A10pro_codigo = T00043_A10pro_codigo[0] ;
         Z14fac_codigo = A14fac_codigo ;
         Z21detfac_codigo = A21detfac_codigo ;
         sMode12 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         load0412( ) ;
         Gx_mode = sMode12 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         RcdFound12 = (short)(0) ;
         initializeNonKey0412( ) ;
         sMode12 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal0412( ) ;
         Gx_mode = sMode12 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      pr_default.close(1);
   }

   public void checkOptimisticConcurrency0412( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         /* Using cursor T00042 */
         pr_default.execute(0, new Object[] {new Short(A14fac_codigo), new Short(A21detfac_codigo)});
         if ( (pr_default.getStatus(0) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"facturaLevel1"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( Z22detfac_cantidad != T00042_A22detfac_cantidad[0] ) || ( Z10pro_codigo != T00042_A10pro_codigo[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"facturaLevel1"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert0412( )
   {
      beforeValidate0412( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable0412( ) ;
      }
      if ( AnyError == 0 )
      {
         zm0412( 0) ;
         checkOptimisticConcurrency0412( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm0412( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert0412( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000425 */
                  pr_default.execute(21, new Object[] {new Short(A14fac_codigo), new Short(A21detfac_codigo), new Short(A22detfac_cantidad), new Short(A10pro_codigo)});
                  if ( (pr_default.getStatus(21) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        /* Save values for previous() function. */
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load0412( ) ;
         }
         endLevel0412( ) ;
      }
      closeExtendedTableCursors0412( ) ;
   }

   public void update0412( )
   {
      beforeValidate0412( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable0412( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency0412( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm0412( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate0412( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000426 */
                  pr_default.execute(22, new Object[] {new Short(A22detfac_cantidad), new Short(A10pro_codigo), new Short(A14fac_codigo), new Short(A21detfac_codigo)});
                  if ( (pr_default.getStatus(22) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"facturaLevel1"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate0412( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        getByPrimaryKey0412( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel0412( ) ;
      }
      closeExtendedTableCursors0412( ) ;
   }

   public void deferredUpdate0412( )
   {
   }

   public void delete0412( )
   {
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      beforeValidate0412( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency0412( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls0412( ) ;
         afterConfirm0412( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete0412( ) ;
            if ( AnyError == 0 )
            {
               /* No cascading delete specified. */
               /* Using cursor T000427 */
               pr_default.execute(23, new Object[] {new Short(A14fac_codigo), new Short(A21detfac_codigo)});
               if ( AnyError == 0 )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode12 = Gx_mode ;
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      endLevel0412( ) ;
      Gx_mode = sMode12 ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
   }

   public void onDeleteControls0412( )
   {
      standaloneModal0412( ) ;
      if ( AnyError == 0 )
      {
         /* Delete mode formulas */
         /* Using cursor T000428 */
         pr_default.execute(24, new Object[] {new Short(A10pro_codigo)});
         A13pro_precio = T000428_A13pro_precio[0] ;
         n13pro_precio = T000428_n13pro_precio[0] ;
         A11pro_nombre = T000428_A11pro_nombre[0] ;
         n11pro_nombre = T000428_n11pro_nombre[0] ;
         A12pro_descripcion = T000428_A12pro_descripcion[0] ;
         n12pro_descripcion = T000428_n12pro_descripcion[0] ;
         pr_default.close(24);
         A24detfac_total = DecimalUtil.doubleToDec(A22detfac_cantidad).multiply(A13pro_precio) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            A17fac_subtotal = O17fac_subtotal.add(A24detfac_total) ;
            n17fac_subtotal = false ;
            httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
         }
         else
         {
            if ( ( GXutil.strcmp(Gx_mode, "UPD") == 0 )  )
            {
               A17fac_subtotal = O17fac_subtotal.add(A24detfac_total).subtract(O24detfac_total) ;
               n17fac_subtotal = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
            }
            else
            {
               if ( ( GXutil.strcmp(Gx_mode, "DLT") == 0 )  )
               {
                  A17fac_subtotal = O17fac_subtotal.subtract(O24detfac_total) ;
                  n17fac_subtotal = false ;
                  httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
               }
            }
         }
         A18fac_impuesto = A17fac_subtotal.multiply(DecimalUtil.doubleToDec(0.19,4,2)) ;
         httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
         A19fac_total = A17fac_subtotal.add(A18fac_impuesto) ;
         httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
      }
   }

   public void endLevel0412( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         pr_default.close(0);
      }
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanStart0412( )
   {
      /* Using cursor T000429 */
      pr_default.execute(25, new Object[] {new Short(A14fac_codigo)});
      RcdFound12 = (short)(0) ;
      if ( (pr_default.getStatus(25) != 101) )
      {
         RcdFound12 = (short)(1) ;
         A21detfac_codigo = T000429_A21detfac_codigo[0] ;
      }
      /* Load Subordinate Levels */
   }

   public void scanNext0412( )
   {
      pr_default.readNext(25);
      RcdFound12 = (short)(0) ;
      if ( (pr_default.getStatus(25) != 101) )
      {
         RcdFound12 = (short)(1) ;
         A21detfac_codigo = T000429_A21detfac_codigo[0] ;
      }
   }

   public void scanEnd0412( )
   {
      pr_default.close(25);
   }

   public void afterConfirm0412( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert0412( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate0412( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete0412( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete0412( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate0412( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes0412( )
   {
      edtdetfac_codigo_Enabled = 0 ;
      edtpro_codigo_Enabled = 0 ;
      edtpro_nombre_Enabled = 0 ;
      edtpro_descripcion_Enabled = 0 ;
      edtdetfac_cantidad_Enabled = 0 ;
      edtpro_precio_Enabled = 0 ;
      edtdetfac_total_Enabled = 0 ;
   }

   public void addRow0412( )
   {
      nGXsfl_77_idx = (short)(nGXsfl_77_idx+1) ;
      sGXsfl_77_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_77_idx, 4, 0)), (short)(4), "0") ;
      edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_77_idx ;
      edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_77_idx ;
      imgprompt_10_Internalname = "PROMPT_10_"+sGXsfl_77_idx ;
      edtpro_nombre_Internalname = "PRO_NOMBRE_"+sGXsfl_77_idx ;
      edtpro_descripcion_Internalname = "PRO_DESCRIPCION_"+sGXsfl_77_idx ;
      edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_77_idx ;
      edtpro_precio_Internalname = "PRO_PRECIO_"+sGXsfl_77_idx ;
      edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_77_idx ;
      sendRow0412( ) ;
   }

   public void sendRow0412( )
   {
      Gridfactura_detalleRow = GXWebRow.GetNew(context) ;
      if ( subGridfactura_detalle_Backcolorstyle == 0 )
      {
         /* None style subfile background logic. */
         subGridfactura_detalle_Backstyle = (byte)(0) ;
         if ( GXutil.strcmp(subGridfactura_detalle_Class, "") != 0 )
         {
            subGridfactura_detalle_Linesclass = subGridfactura_detalle_Class+"Odd" ;
         }
      }
      else if ( subGridfactura_detalle_Backcolorstyle == 1 )
      {
         /* Uniform style subfile background logic. */
         subGridfactura_detalle_Backstyle = (byte)(0) ;
         subGridfactura_detalle_Backcolor = subGridfactura_detalle_Allbackcolor ;
         httpContext.ajax_rsp_assign_prop("", false, "Gridfactura_detalleContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGridfactura_detalle_Backcolor, 9, 0)));
         if ( GXutil.strcmp(subGridfactura_detalle_Class, "") != 0 )
         {
            subGridfactura_detalle_Linesclass = subGridfactura_detalle_Class+"Uniform" ;
         }
      }
      else if ( subGridfactura_detalle_Backcolorstyle == 2 )
      {
         /* Header style subfile background logic. */
         subGridfactura_detalle_Backstyle = (byte)(1) ;
         if ( GXutil.strcmp(subGridfactura_detalle_Class, "") != 0 )
         {
            subGridfactura_detalle_Linesclass = subGridfactura_detalle_Class+"Odd" ;
         }
         subGridfactura_detalle_Backcolor = (int)(0xF0F0F0) ;
         httpContext.ajax_rsp_assign_prop("", false, "Gridfactura_detalleContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGridfactura_detalle_Backcolor, 9, 0)));
      }
      else if ( subGridfactura_detalle_Backcolorstyle == 3 )
      {
         /* Report style subfile background logic. */
         subGridfactura_detalle_Backstyle = (byte)(1) ;
         if ( ((int)(nGXsfl_77_idx) % (2)) == 0 )
         {
            subGridfactura_detalle_Backcolor = (int)(0x0) ;
            httpContext.ajax_rsp_assign_prop("", false, "Gridfactura_detalleContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGridfactura_detalle_Backcolor, 9, 0)));
            if ( GXutil.strcmp(subGridfactura_detalle_Class, "") != 0 )
            {
               subGridfactura_detalle_Linesclass = subGridfactura_detalle_Class+"Even" ;
            }
         }
         else
         {
            subGridfactura_detalle_Backcolor = (int)(0xF0F0F0) ;
            httpContext.ajax_rsp_assign_prop("", false, "Gridfactura_detalleContainerDiv", "Backcolor", GXutil.ltrim( GXutil.str( subGridfactura_detalle_Backcolor, 9, 0)));
            if ( GXutil.strcmp(subGridfactura_detalle_Class, "") != 0 )
            {
               subGridfactura_detalle_Linesclass = subGridfactura_detalle_Class+"Odd" ;
            }
         }
      }
      imgprompt_10_Link = ((GXutil.strcmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0030"+"',["+"{Ctrl:gx.dom.el('"+"PRO_CODIGO_"+sGXsfl_77_idx+"'), id:'"+"PRO_CODIGO_"+sGXsfl_77_idx+"'"+",isOut: true}"+"],"+"gx.dom.form()."+"nIsMod_12_"+sGXsfl_77_idx+","+"'', false"+","+"false"+");") ;
      /* * Property Link not supported in */
      /* * Property Link not supported in */
      /* * Property Link not supported in */
      /*
         Assignment error:
         ================
         Expression: [ t('iif(',1),t('Gx_mode',23),t(=,10),t('''DSP''',3),t('OR',9),t('Gx_mode',23),t(=,10),t('''UPD''',3),t(',',7),t('""',3),t(',',7),t('"javascript:"',3),t(+,5),t('"gx.popup.openPrompt(''"',3),t(+,5),t('"gx00c1"',3),t(+,5),t('"'',["',3),t(+,5),t('"''"',3),t(+,5),t('ltrim(',1),t('ntoc(',1),t(14,2),t(',',7),t(4,3),t(',',7),t(0,3),t(',',7),t('"."',3),t(',',7),t('""',3),t(')',4),t(')',4),t(+,5),t('"''"',3),t(+,5),t('","',3),t(+,5),t('"{Ctrl:gx.dom.el(''"',3),t(+,5),t('"DETFAC_CODIGO_"',3),t(+,5),t(sGXsfl_77_idx,23),t(+,5),t('"''), id:''"',3),t(+,5),t('"DETFAC_CODIGO_"',3),t(+,5),t(sGXsfl_77_idx,23),t(+,5),t('"''"',3),t(+,5),t('",isOut: true}"',3),t(+,5),t('"],"',3),t(+,5),t('"gx.dom.form()."',3),t(+,5),t('"nIsMod_12_"',3),t(+,5),t(sGXsfl_77_idx,23),t(+,5),t('","',3),t(+,5),t('"'''', false"',3),t(+,5),t('","',3),t(+,5),t('"false"',3),t(+,5),t('");"',3),t(')',4) ]
         Target    : [ t(prompt_14,3),t('Link',3) ]
         ForType   : 29
         Type      : []
      */
      /* Subfile cell */
      /* Single line edit */
      ClassString = "Attribute" ;
      StyleString = "" ;
      ROClassString = ClassString ;
      Gridfactura_detalleRow.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtdetfac_codigo_Internalname,GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ",", "")),((edtdetfac_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A21detfac_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A21detfac_codigo), "ZZZ9")),"","","","","",edtdetfac_codigo_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(0),new Integer(edtdetfac_codigo_Enabled),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(77),new Integer(1),new Integer(1),new Boolean(true),"right"});
      /* Subfile cell */
      /* Single line edit */
      TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_12_" + sGXsfl_77_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_77_idx + "',77)\"" ;
      ClassString = "Attribute" ;
      StyleString = "" ;
      ROClassString = ClassString ;
      Gridfactura_detalleRow.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtpro_codigo_Internalname,GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ",", "")),((edtpro_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A10pro_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A10pro_codigo), "ZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(79);\"","","","","",edtpro_codigo_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(edtpro_codigo_Enabled),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(77),new Integer(1),new Integer(-1),new Boolean(true),"right"});
      /* Subfile cell */
      /* Static images/pictures */
      ClassString = "Image" ;
      StyleString = "" ;
      Gridfactura_detalleRow.AddColumnProperties("bitmap", 1, httpContext.isAjaxCallMode( ), new Object[] {imgprompt_10_Internalname,"prompt.gif",imgprompt_10_Link,"","","GeneXusX",new Integer(imgprompt_10_Visible),new Integer(1),"","",new Integer(0),new Integer(0),new Integer(0),"",new Integer(0),"",new Integer(0),new Integer(0),new Integer(0),"","",StyleString,ClassString,"","","''",""});
      /* Subfile cell */
      /* Single line edit */
      ClassString = "Attribute" ;
      StyleString = "" ;
      ROClassString = ClassString ;
      Gridfactura_detalleRow.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtpro_nombre_Internalname,GXutil.rtrim( A11pro_nombre),"","","","","","",edtpro_nombre_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(edtpro_nombre_Enabled),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(45),new Integer(0),new Integer(0),new Integer(77),new Integer(1),new Integer(-1),new Boolean(true),"left"});
      /* Subfile cell */
      /* Single line edit */
      ClassString = "Attribute" ;
      StyleString = "" ;
      ROClassString = ClassString ;
      Gridfactura_detalleRow.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtpro_descripcion_Internalname,GXutil.rtrim( A12pro_descripcion),"","","","","","",edtpro_descripcion_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(edtpro_descripcion_Enabled),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(200),new Integer(0),new Integer(0),new Integer(77),new Integer(1),new Integer(-1),new Boolean(true),"left"});
      /* Subfile cell */
      /* Single line edit */
      TempTags = " gxoch1=\"gx.fn.setControlValue('nIsMod_12_" + sGXsfl_77_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_77_idx + "',77)\"" ;
      ClassString = "Attribute" ;
      StyleString = "" ;
      ROClassString = ClassString ;
      Gridfactura_detalleRow.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtdetfac_cantidad_Internalname,GXutil.ltrim( localUtil.ntoc( A22detfac_cantidad, (byte)(4), (byte)(0), ",", "")),((edtdetfac_cantidad_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A22detfac_cantidad), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A22detfac_cantidad), "ZZZ9")),TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(82);\"","","","","",edtdetfac_cantidad_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(edtdetfac_cantidad_Enabled),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(4),new Integer(0),new Integer(0),new Integer(77),new Integer(1),new Integer(-1),new Boolean(true),"right"});
      /* Subfile cell */
      /* Single line edit */
      ClassString = "Attribute" ;
      StyleString = "" ;
      ROClassString = ClassString ;
      Gridfactura_detalleRow.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtpro_precio_Internalname,GXutil.ltrim( localUtil.ntoc( A13pro_precio, (byte)(10), (byte)(2), ",", "")),((edtpro_precio_Enabled!=0) ? GXutil.ltrim( localUtil.format( A13pro_precio, "ZZZZZZ9.99")) : localUtil.format( A13pro_precio, "ZZZZZZ9.99")),"","","","","",edtpro_precio_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(edtpro_precio_Enabled),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(10),new Integer(0),new Integer(0),new Integer(77),new Integer(1),new Integer(-1),new Boolean(true),"right"});
      /* Subfile cell */
      /* Single line edit */
      ClassString = "Attribute" ;
      StyleString = "" ;
      ROClassString = ClassString ;
      Gridfactura_detalleRow.AddColumnProperties("edit", 1, httpContext.isAjaxCallMode( ), new Object[] {edtdetfac_total_Internalname,GXutil.ltrim( localUtil.ntoc( A24detfac_total, (byte)(10), (byte)(2), ",", "")),((edtdetfac_total_Enabled!=0) ? GXutil.ltrim( localUtil.format( A24detfac_total, "ZZZZZZ9.99")) : localUtil.format( A24detfac_total, "ZZZZZZ9.99")),"","","","","",edtdetfac_total_Jsonclick,new Integer(0),ClassString,StyleString,ROClassString,new Integer(-1),new Integer(edtdetfac_total_Enabled),new Integer(0),new Integer(0),"px",new Integer(17),"px",new Integer(10),new Integer(0),new Integer(0),new Integer(77),new Integer(1),new Integer(-1),new Boolean(true),"right"});
      httpContext.ajax_sending_grid_row(Gridfactura_detalleRow);
      GXCCtl = "Z21detfac_codigo_" + sGXsfl_77_idx ;
      GxWebStd.gx_hidden_field( httpContext, GXCCtl, GXutil.ltrim( localUtil.ntoc( Z21detfac_codigo, (byte)(4), (byte)(0), ",", "")));
      GXCCtl = "Z22detfac_cantidad_" + sGXsfl_77_idx ;
      GxWebStd.gx_hidden_field( httpContext, GXCCtl, GXutil.ltrim( localUtil.ntoc( Z22detfac_cantidad, (byte)(4), (byte)(0), ",", "")));
      GXCCtl = "Z10pro_codigo_" + sGXsfl_77_idx ;
      GxWebStd.gx_hidden_field( httpContext, GXCCtl, GXutil.ltrim( localUtil.ntoc( Z10pro_codigo, (byte)(4), (byte)(0), ",", "")));
      GXCCtl = "O24detfac_total_" + sGXsfl_77_idx ;
      GxWebStd.gx_hidden_field( httpContext, GXCCtl, GXutil.ltrim( localUtil.ntoc( O24detfac_total, (byte)(10), (byte)(2), ",", "")));
      GXCCtl = "nRcdDeleted_12_" + sGXsfl_77_idx ;
      GxWebStd.gx_hidden_field( httpContext, GXCCtl, GXutil.ltrim( localUtil.ntoc( nRcdDeleted_12, (byte)(4), (byte)(0), ",", "")));
      GXCCtl = "nRcdExists_12_" + sGXsfl_77_idx ;
      GxWebStd.gx_hidden_field( httpContext, GXCCtl, GXutil.ltrim( localUtil.ntoc( nRcdExists_12, (byte)(4), (byte)(0), ",", "")));
      GXCCtl = "nIsMod_12_" + sGXsfl_77_idx ;
      GxWebStd.gx_hidden_field( httpContext, GXCCtl, GXutil.ltrim( localUtil.ntoc( nIsMod_12, (byte)(4), (byte)(0), ",", "")));
      GXCCtl = "vMODE_" + sGXsfl_77_idx ;
      GxWebStd.gx_hidden_field( httpContext, GXCCtl, GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "DETFAC_CODIGO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_codigo_Enabled, (byte)(5), (byte)(0), ".", "")));
      GxWebStd.gx_hidden_field( httpContext, "PRO_CODIGO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_codigo_Enabled, (byte)(5), (byte)(0), ".", "")));
      GxWebStd.gx_hidden_field( httpContext, "PRO_NOMBRE_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_nombre_Enabled, (byte)(5), (byte)(0), ".", "")));
      GxWebStd.gx_hidden_field( httpContext, "PRO_DESCRIPCION_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_descripcion_Enabled, (byte)(5), (byte)(0), ".", "")));
      GxWebStd.gx_hidden_field( httpContext, "DETFAC_CANTIDAD_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_cantidad_Enabled, (byte)(5), (byte)(0), ".", "")));
      GxWebStd.gx_hidden_field( httpContext, "PRO_PRECIO_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtpro_precio_Enabled, (byte)(5), (byte)(0), ".", "")));
      GxWebStd.gx_hidden_field( httpContext, "DETFAC_TOTAL_"+sGXsfl_77_idx+"Enabled", GXutil.ltrim( localUtil.ntoc( edtdetfac_total_Enabled, (byte)(5), (byte)(0), ".", "")));
      GxWebStd.gx_hidden_field( httpContext, "PROMPT_10_"+sGXsfl_77_idx+"Link", GXutil.rtrim( imgprompt_10_Link));
      httpContext.ajax_sending_grid_row(null);
      Gridfactura_detalleContainer.AddRow(Gridfactura_detalleRow);
   }

   public void readRow0412( )
   {
      nGXsfl_77_idx = (short)(nGXsfl_77_idx+1) ;
      sGXsfl_77_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_77_idx, 4, 0)), (short)(4), "0") ;
      edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_77_idx ;
      edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_77_idx ;
      imgprompt_10_Internalname = "PROMPT_10_"+sGXsfl_77_idx ;
      edtpro_nombre_Internalname = "PRO_NOMBRE_"+sGXsfl_77_idx ;
      edtpro_descripcion_Internalname = "PRO_DESCRIPCION_"+sGXsfl_77_idx ;
      edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_77_idx ;
      edtpro_precio_Internalname = "PRO_PRECIO_"+sGXsfl_77_idx ;
      edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_77_idx ;
      edtdetfac_codigo_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "DETFAC_CODIGO_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
      edtpro_codigo_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "PRO_CODIGO_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
      edtpro_nombre_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "PRO_NOMBRE_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
      edtpro_descripcion_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "PRO_DESCRIPCION_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
      edtdetfac_cantidad_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "DETFAC_CANTIDAD_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
      edtpro_precio_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "PRO_PRECIO_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
      edtdetfac_total_Enabled = (int)(localUtil.ctol( httpContext.cgiGet( "DETFAC_TOTAL_"+sGXsfl_77_idx+"Enabled"), ",", ".")) ;
      imgprompt_1_Link = httpContext.cgiGet( "PROMPT_10_"+sGXsfl_77_idx+"Link") ;
      A21detfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_codigo_Internalname), ",", ".")) ;
      if ( ( ( localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".") > 9999 ) ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = true ;
         A10pro_codigo = (short)(0) ;
      }
      else
      {
         A10pro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".")) ;
      }
      A11pro_nombre = httpContext.cgiGet( edtpro_nombre_Internalname) ;
      n11pro_nombre = false ;
      A12pro_descripcion = httpContext.cgiGet( edtpro_descripcion_Internalname) ;
      n12pro_descripcion = false ;
      if ( ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_cantidad_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_cantidad_Internalname), ",", ".") > 9999 ) ) )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "DETFAC_CANTIDAD");
         AnyError = (short)(1) ;
         GX_FocusControl = edtdetfac_cantidad_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = true ;
         A22detfac_cantidad = (short)(0) ;
      }
      else
      {
         A22detfac_cantidad = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_cantidad_Internalname), ",", ".")) ;
      }
      A13pro_precio = localUtil.ctond( httpContext.cgiGet( edtpro_precio_Internalname)) ;
      n13pro_precio = false ;
      A24detfac_total = localUtil.ctond( httpContext.cgiGet( edtdetfac_total_Internalname)) ;
      GXCCtl = "Z21detfac_codigo_" + sGXsfl_77_idx ;
      Z21detfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( GXCCtl), ",", ".")) ;
      GXCCtl = "Z22detfac_cantidad_" + sGXsfl_77_idx ;
      Z22detfac_cantidad = (short)(localUtil.ctol( httpContext.cgiGet( GXCCtl), ",", ".")) ;
      GXCCtl = "Z10pro_codigo_" + sGXsfl_77_idx ;
      Z10pro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( GXCCtl), ",", ".")) ;
      GXCCtl = "O24detfac_total_" + sGXsfl_77_idx ;
      O24detfac_total = localUtil.ctond( httpContext.cgiGet( GXCCtl)) ;
      GXCCtl = "nRcdDeleted_12_" + sGXsfl_77_idx ;
      nRcdDeleted_12 = (short)(localUtil.ctol( httpContext.cgiGet( GXCCtl), ",", ".")) ;
      GXCCtl = "nRcdExists_12_" + sGXsfl_77_idx ;
      nRcdExists_12 = (short)(localUtil.ctol( httpContext.cgiGet( GXCCtl), ",", ".")) ;
      GXCCtl = "nIsMod_12_" + sGXsfl_77_idx ;
      nIsMod_12 = (short)(localUtil.ctol( httpContext.cgiGet( GXCCtl), ",", ".")) ;
   }

   public void assign_properties_default( )
   {
      defedtdetfac_codigo_Enabled = edtdetfac_codigo_Enabled ;
   }

   public void confirmValues040( )
   {
      nGXsfl_77_idx = (short)(0) ;
      sGXsfl_77_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_77_idx, 4, 0)), (short)(4), "0") ;
      edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_77_idx ;
      edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_77_idx ;
      imgprompt_10_Internalname = "PROMPT_10_"+sGXsfl_77_idx ;
      edtpro_nombre_Internalname = "PRO_NOMBRE_"+sGXsfl_77_idx ;
      edtpro_descripcion_Internalname = "PRO_DESCRIPCION_"+sGXsfl_77_idx ;
      edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_77_idx ;
      edtpro_precio_Internalname = "PRO_PRECIO_"+sGXsfl_77_idx ;
      edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_77_idx ;
      while ( nGXsfl_77_idx < nRC_Gridfactura_detalle )
      {
         nGXsfl_77_idx = (short)(nGXsfl_77_idx+1) ;
         sGXsfl_77_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_77_idx, 4, 0)), (short)(4), "0") ;
         edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_77_idx ;
         edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_77_idx ;
         imgprompt_10_Internalname = "PROMPT_10_"+sGXsfl_77_idx ;
         edtpro_nombre_Internalname = "PRO_NOMBRE_"+sGXsfl_77_idx ;
         edtpro_descripcion_Internalname = "PRO_DESCRIPCION_"+sGXsfl_77_idx ;
         edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_77_idx ;
         edtpro_precio_Internalname = "PRO_PRECIO_"+sGXsfl_77_idx ;
         edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_77_idx ;
         httpContext.changePostValue( "Z21detfac_codigo_"+sGXsfl_77_idx, httpContext.cgiGet( "ZT_"+"Z21detfac_codigo_"+sGXsfl_77_idx)) ;
         httpContext.deletePostValue( "ZT_"+"Z21detfac_codigo_"+sGXsfl_77_idx) ;
         httpContext.changePostValue( "Z22detfac_cantidad_"+sGXsfl_77_idx, httpContext.cgiGet( "ZT_"+"Z22detfac_cantidad_"+sGXsfl_77_idx)) ;
         httpContext.deletePostValue( "ZT_"+"Z22detfac_cantidad_"+sGXsfl_77_idx) ;
         httpContext.changePostValue( "Z10pro_codigo_"+sGXsfl_77_idx, httpContext.cgiGet( "ZT_"+"Z10pro_codigo_"+sGXsfl_77_idx)) ;
         httpContext.deletePostValue( "ZT_"+"Z10pro_codigo_"+sGXsfl_77_idx) ;
      }
      httpContext.changePostValue( "O24detfac_total", httpContext.cgiGet( "T24detfac_total")) ;
      httpContext.deletePostValue( "T24detfac_total") ;
   }

   public void renderHtmlHeaders( )
   {
      GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), "IE=EmulateIE7");
   }

   public void renderHtmlOpenForm( )
   {
      httpContext.writeText( "<title>") ;
      httpContext.writeText( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      MasterPageObj.master_styles();
      if ( ! httpContext.isSmartDevice( ) )
      {
         httpContext.AddJavascriptSource("gxgral.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      else
      {
         httpContext.AddJavascriptSource("gxapiSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfxSD.js", "?58720");
         httpContext.AddJavascriptSource("gxtypesSD.js", "?58720");
         httpContext.AddJavascriptSource("gxpopupSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfrmutlSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgridSD.js", "?58720");
         httpContext.AddJavascriptSource("JavaScripTableSD.js", "?58720");
         httpContext.AddJavascriptSource("rijndaelSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgralSD.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      httpContext.AddJavascriptSource("calendar.js", "?58720");
      httpContext.AddJavascriptSource("calendar-setup.js", "?58720");
      httpContext.AddJavascriptSource("calendar-es.js", "?58720");
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      FormProcess = " onkeyup=\"gx.evt.onkeyup(event)\" onkeypress=\"gx.evt.onkeypress(event,true,false)\" onkeydown=\"gx.evt.onkeypress(null,true,false)\"" ;
      httpContext.writeText( "<body") ;
      httpContext.writeText( " "+"class=\"Form\""+" "+" style=\"-moz-opacity:0;opacity:0;"+"background-color:"+WebUtils.getHTMLColor( Form.getIBackground())+";") ;
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         httpContext.writeText( " background-image:url("+httpContext.convertURL( Form.getBackground())+")") ;
      }
      httpContext.writeText( "\""+FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" onsubmit=\"try{return gx.csv.validForm()}catch(e){return true;}\" name=\"MAINFORM\" method=\"post\" action=\""+formatLink("factura") + "?" + GXutil.URLEncode(GXutil.rtrim(Gx_mode))+"\" class=\""+"Form"+"\">") ;
      GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
   }

   public void renderHtmlCloseForm( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      GxWebStd.gx_hidden_field( httpContext, "Z14fac_codigo", GXutil.ltrim( localUtil.ntoc( Z14fac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z15fac_numero", GXutil.ltrim( localUtil.ntoc( Z15fac_numero, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z16fac_fecha", localUtil.dtoc( Z16fac_fecha, 0, "/"));
      GxWebStd.gx_hidden_field( httpContext, "Z20fac_estado", GXutil.rtrim( Z20fac_estado));
      GxWebStd.gx_hidden_field( httpContext, "Z1cli_codigo", GXutil.ltrim( localUtil.ntoc( Z1cli_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "O17fac_subtotal", GXutil.ltrim( localUtil.ntoc( O17fac_subtotal, (byte)(10), (byte)(2), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsConfirmed", GXutil.ltrim( localUtil.ntoc( IsConfirmed, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsModified", GXutil.ltrim( localUtil.ntoc( IsModified, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Mode", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "nRC_Gridfactura_detalle", GXutil.ltrim( localUtil.ntoc( nGXsfl_77_idx, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "FAC_CODIGO", GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "vMODE", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      httpContext.writeTextNL( "</form>") ;
      include_jscripts( ) ;
   }

   public byte executeStartEvent( )
   {
      standaloneStartup( ) ;
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      return gxajaxcallmode ;
   }

   public void renderHtmlContent( )
   {
      draw( ) ;
   }

   public void dispatchEvents( )
   {
      process( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public String getPgmname( )
   {
      return "factura" ;
   }

   public String getPgmdesc( )
   {
      return "factura" ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("factura") + "?" + GXutil.URLEncode(GXutil.rtrim(Gx_mode)) ;
   }

   public void initializeNonKey044( )
   {
      A18fac_impuesto = DecimalUtil.ZERO ;
      httpContext.ajax_rsp_assign_attri("", false, "A18fac_impuesto", GXutil.ltrim( GXutil.str( A18fac_impuesto, 10, 2)));
      A19fac_total = DecimalUtil.ZERO ;
      httpContext.ajax_rsp_assign_attri("", false, "A19fac_total", GXutil.ltrim( GXutil.str( A19fac_total, 10, 2)));
      A15fac_numero = (short)(0) ;
      n15fac_numero = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A15fac_numero", GXutil.ltrim( GXutil.str( A15fac_numero, 4, 0)));
      n15fac_numero = ((0==A15fac_numero) ? true : false) ;
      A16fac_fecha = GXutil.nullDate() ;
      n16fac_fecha = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A16fac_fecha", localUtil.format(A16fac_fecha, "99/99/99"));
      n16fac_fecha = (GXutil.nullDate().equals(A16fac_fecha) ? true : false) ;
      A17fac_subtotal = DecimalUtil.ZERO ;
      n17fac_subtotal = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
      A20fac_estado = "" ;
      n20fac_estado = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A20fac_estado", A20fac_estado);
      n20fac_estado = ((GXutil.strcmp("", A20fac_estado)==0) ? true : false) ;
      A1cli_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A1cli_codigo", GXutil.ltrim( GXutil.str( A1cli_codigo, 4, 0)));
      A2cli_nombres = "" ;
      n2cli_nombres = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A2cli_nombres", A2cli_nombres);
      A3cli_apellidos = "" ;
      n3cli_apellidos = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A3cli_apellidos", A3cli_apellidos);
      O17fac_subtotal = A17fac_subtotal ;
      n17fac_subtotal = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A17fac_subtotal", GXutil.ltrim( GXutil.str( A17fac_subtotal, 10, 2)));
   }

   public void initAll044( )
   {
      A14fac_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
      initializeNonKey044( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void initializeNonKey0412( )
   {
      A24detfac_total = DecimalUtil.ZERO ;
      A10pro_codigo = (short)(0) ;
      A22detfac_cantidad = (short)(0) ;
      A13pro_precio = DecimalUtil.ZERO ;
      n13pro_precio = false ;
      A11pro_nombre = "" ;
      n11pro_nombre = false ;
      A12pro_descripcion = "" ;
      n12pro_descripcion = false ;
      O24detfac_total = A24detfac_total ;
   }

   public void initAll0412( )
   {
      A21detfac_codigo = (short)(0) ;
      initializeNonKey0412( ) ;
   }

   public void standaloneModalInsert0412( )
   {
   }

   public void define_styles( )
   {
      httpContext.AddStyleSheetFile("calendar-system.css", "?95080");
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?94235");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?58720");
      httpContext.AddJavascriptSource("factura.js", "?94235");
      /* End function include_jscripts */
   }

   public void init_level_properties12( )
   {
      edtdetfac_codigo_Enabled = defedtdetfac_codigo_Enabled ;
   }

   public void init_default_properties( )
   {
      imgBtn_first_Internalname = "BTN_FIRST" ;
      imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR" ;
      imgBtn_previous_Internalname = "BTN_PREVIOUS" ;
      imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR" ;
      imgBtn_next_Internalname = "BTN_NEXT" ;
      imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR" ;
      imgBtn_last_Internalname = "BTN_LAST" ;
      imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR" ;
      imgBtn_select_Internalname = "BTN_SELECT" ;
      imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR" ;
      imgBtn_enter2_Internalname = "BTN_ENTER2" ;
      imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR" ;
      imgBtn_cancel2_Internalname = "BTN_CANCEL2" ;
      imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR" ;
      imgBtn_delete2_Internalname = "BTN_DELETE2" ;
      imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR" ;
      tblTabletoolbar_Internalname = "TABLETOOLBAR" ;
      lblTextblockfac_numero_Internalname = "TEXTBLOCKFAC_NUMERO" ;
      edtfac_numero_Internalname = "FAC_NUMERO" ;
      lblTextblockfac_fecha_Internalname = "TEXTBLOCKFAC_FECHA" ;
      edtfac_fecha_Internalname = "FAC_FECHA" ;
      lblTextblockfac_subtotal_Internalname = "TEXTBLOCKFAC_SUBTOTAL" ;
      edtfac_subtotal_Internalname = "FAC_SUBTOTAL" ;
      lblTextblockfac_impuesto_Internalname = "TEXTBLOCKFAC_IMPUESTO" ;
      edtfac_impuesto_Internalname = "FAC_IMPUESTO" ;
      lblTextblockfac_total_Internalname = "TEXTBLOCKFAC_TOTAL" ;
      edtfac_total_Internalname = "FAC_TOTAL" ;
      lblTextblockfac_estado_Internalname = "TEXTBLOCKFAC_ESTADO" ;
      edtfac_estado_Internalname = "FAC_ESTADO" ;
      lblTextblockcli_nombres_Internalname = "TEXTBLOCKCLI_NOMBRES" ;
      edtcli_codigo_Internalname = "CLI_CODIGO" ;
      edtcli_nombres_Internalname = "CLI_NOMBRES" ;
      edtcli_apellidos_Internalname = "CLI_APELLIDOS" ;
      lblTitledetalle_Internalname = "TITLEDETALLE" ;
      tblTable3_Internalname = "TABLE3" ;
      tblTable2_Internalname = "TABLE2" ;
      bttBtn_enter_Internalname = "BTN_ENTER" ;
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      bttBtn_delete_Internalname = "BTN_DELETE" ;
      tblTable1_Internalname = "TABLE1" ;
      grpGroupdata_Internalname = "GROUPDATA" ;
      tblTablemain_Internalname = "TABLEMAIN" ;
      Form.setInternalname( "FORM" );
      imgprompt_1_Internalname = "PROMPT_1" ;
      subGridfactura_detalle_Internalname = "GRIDFACTURA_DETALLE" ;
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "factura" );
      edtdetfac_total_Jsonclick = "" ;
      edtpro_precio_Jsonclick = "" ;
      edtdetfac_cantidad_Jsonclick = "" ;
      edtpro_descripcion_Jsonclick = "" ;
      edtpro_nombre_Jsonclick = "" ;
      imgprompt_10_Visible = 1 ;
      imgprompt_10_Link = "" ;
      imgprompt_1_Visible = 1 ;
      edtpro_codigo_Jsonclick = "" ;
      edtdetfac_codigo_Jsonclick = "" ;
      subGridfactura_detalle_Class = "Grid" ;
      imgBtn_delete2_separator_Visible = 1 ;
      imgBtn_delete2_Enabled = 1 ;
      imgBtn_delete2_Visible = 1 ;
      imgBtn_cancel2_separator_Visible = 1 ;
      imgBtn_cancel2_Visible = 1 ;
      imgBtn_enter2_separator_Visible = 1 ;
      imgBtn_enter2_Enabled = 1 ;
      imgBtn_enter2_Visible = 1 ;
      imgBtn_select_separator_Visible = 1 ;
      imgBtn_select_Visible = 1 ;
      imgBtn_last_separator_Visible = 1 ;
      imgBtn_last_Visible = 1 ;
      imgBtn_next_separator_Visible = 1 ;
      imgBtn_next_Visible = 1 ;
      imgBtn_previous_separator_Visible = 1 ;
      imgBtn_previous_Visible = 1 ;
      imgBtn_first_separator_Visible = 1 ;
      imgBtn_first_Visible = 1 ;
      subGridfactura_detalle_Allowcollapsing = (byte)(0) ;
      edtdetfac_total_Enabled = 0 ;
      edtpro_precio_Enabled = 0 ;
      edtdetfac_cantidad_Enabled = 1 ;
      edtpro_descripcion_Enabled = 0 ;
      edtpro_nombre_Enabled = 0 ;
      edtpro_codigo_Enabled = 1 ;
      edtdetfac_codigo_Enabled = 0 ;
      subGridfactura_detalle_Backcolorstyle = (byte)(2) ;
      edtcli_apellidos_Jsonclick = "" ;
      edtcli_apellidos_Enabled = 0 ;
      edtcli_nombres_Jsonclick = "" ;
      edtcli_nombres_Enabled = 0 ;
      imgprompt_1_Visible = 1 ;
      imgprompt_1_Link = "" ;
      edtcli_codigo_Jsonclick = "" ;
      edtcli_codigo_Enabled = 1 ;
      edtfac_estado_Jsonclick = "" ;
      edtfac_estado_Enabled = 1 ;
      edtfac_total_Jsonclick = "" ;
      edtfac_total_Enabled = 0 ;
      edtfac_impuesto_Jsonclick = "" ;
      edtfac_impuesto_Enabled = 0 ;
      edtfac_subtotal_Jsonclick = "" ;
      edtfac_subtotal_Enabled = 0 ;
      edtfac_fecha_Jsonclick = "" ;
      edtfac_fecha_Enabled = 1 ;
      edtfac_numero_Jsonclick = "" ;
      edtfac_numero_Enabled = 1 ;
      bttBtn_delete_Visible = 1 ;
      bttBtn_cancel_Visible = 1 ;
      bttBtn_enter_Visible = 1 ;
      httpContext.GX_msglist.setDisplaymode( (short)(1) );
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void gxnrgridfactura_detalle_newrow( short nRC_Gridfactura_detalle ,
                                               short nGXsfl_77_idx ,
                                               String sGXsfl_77_idx )
   {
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      Gx_mode = "INS" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_77_idx ;
      edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_77_idx ;
      imgprompt_10_Internalname = "PROMPT_10_"+sGXsfl_77_idx ;
      edtpro_nombre_Internalname = "PRO_NOMBRE_"+sGXsfl_77_idx ;
      edtpro_descripcion_Internalname = "PRO_DESCRIPCION_"+sGXsfl_77_idx ;
      edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_77_idx ;
      edtpro_precio_Internalname = "PRO_PRECIO_"+sGXsfl_77_idx ;
      edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_77_idx ;
      while ( nGXsfl_77_idx <= nRC_Gridfactura_detalle )
      {
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         standaloneNotModal0412( ) ;
         standaloneModal0412( ) ;
         dynload_actions( ) ;
         sendRow0412( ) ;
         nGXsfl_77_idx = (short)(nGXsfl_77_idx+1) ;
         sGXsfl_77_idx = GXutil.padl( GXutil.ltrim( GXutil.str( nGXsfl_77_idx, 4, 0)), (short)(4), "0") ;
         edtdetfac_codigo_Internalname = "DETFAC_CODIGO_"+sGXsfl_77_idx ;
         edtpro_codigo_Internalname = "PRO_CODIGO_"+sGXsfl_77_idx ;
         imgprompt_10_Internalname = "PROMPT_10_"+sGXsfl_77_idx ;
         edtpro_nombre_Internalname = "PRO_NOMBRE_"+sGXsfl_77_idx ;
         edtpro_descripcion_Internalname = "PRO_DESCRIPCION_"+sGXsfl_77_idx ;
         edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD_"+sGXsfl_77_idx ;
         edtpro_precio_Internalname = "PRO_PRECIO_"+sGXsfl_77_idx ;
         edtdetfac_total_Internalname = "DETFAC_TOTAL_"+sGXsfl_77_idx ;
      }
      httpContext.GX_webresponse.addString(Gridfactura_detalleContainer.ToJavascriptSource());
      /* End function gxnrGridfactura_detalle_newrow */
   }

   public void valid_Cli_codigo( short GX_Parm1 ,
                                 String GX_Parm2 ,
                                 String GX_Parm3 )
   {
      A1cli_codigo = GX_Parm1 ;
      A2cli_nombres = GX_Parm2 ;
      n2cli_nombres = false ;
      A3cli_apellidos = GX_Parm3 ;
      n3cli_apellidos = false ;
      /* Using cursor T000420 */
      pr_default.execute(16, new Object[] {new Short(A1cli_codigo)});
      if ( (pr_default.getStatus(16) == 101) )
      {
         AnyError1 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'cliente'.", "ForeignKeyNotFound", 1, "CLI_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtcli_codigo_Internalname ;
      }
      if ( AnyError1 == 0 )
      {
         A2cli_nombres = T000420_A2cli_nombres[0] ;
         n2cli_nombres = T000420_n2cli_nombres[0] ;
         A3cli_apellidos = T000420_A3cli_apellidos[0] ;
         n3cli_apellidos = T000420_n3cli_apellidos[0] ;
      }
      pr_default.close(16);
      dynload_actions( ) ;
      if ( AnyError == 1 )
      {
         A2cli_nombres = "" ;
         n2cli_nombres = false ;
         A3cli_apellidos = "" ;
         n3cli_apellidos = false ;
      }
      isValidOutput.add(GXutil.rtrim( A2cli_nombres));
      isValidOutput.add(GXutil.rtrim( A3cli_apellidos));
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   public void valid_Pro_codigo( short GX_Parm1 ,
                                 java.math.BigDecimal GX_Parm2 ,
                                 String GX_Parm3 ,
                                 String GX_Parm4 )
   {
      A10pro_codigo = GX_Parm1 ;
      A13pro_precio = GX_Parm2 ;
      n13pro_precio = false ;
      A11pro_nombre = GX_Parm3 ;
      n11pro_nombre = false ;
      A12pro_descripcion = GX_Parm4 ;
      n12pro_descripcion = false ;
      /* Using cursor T000428 */
      pr_default.execute(24, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(24) == 101) )
      {
         AnyError10 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'producto'.", "ForeignKeyNotFound", 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
      }
      if ( AnyError10 == 0 )
      {
         A13pro_precio = T000428_A13pro_precio[0] ;
         n13pro_precio = T000428_n13pro_precio[0] ;
         A11pro_nombre = T000428_A11pro_nombre[0] ;
         n11pro_nombre = T000428_n11pro_nombre[0] ;
         A12pro_descripcion = T000428_A12pro_descripcion[0] ;
         n12pro_descripcion = T000428_n12pro_descripcion[0] ;
      }
      pr_default.close(24);
      dynload_actions( ) ;
      if ( AnyError == 1 )
      {
         A13pro_precio = DecimalUtil.ZERO ;
         n13pro_precio = false ;
         A11pro_nombre = "" ;
         n11pro_nombre = false ;
         A12pro_descripcion = "" ;
         n12pro_descripcion = false ;
      }
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A13pro_precio, (byte)(10), (byte)(2), ".", "")));
      isValidOutput.add(GXutil.rtrim( A11pro_nombre));
      isValidOutput.add(GXutil.rtrim( A12pro_descripcion));
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(24);
      pr_default.close(16);
      pr_default.close(6);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      sPrefix = "" ;
      wcpOGx_mode = "" ;
      scmdbuf = "" ;
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      Gx_mode = "" ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      GX_FocusControl = "" ;
      sStyleString = "" ;
      ClassString = "" ;
      StyleString = "" ;
      TempTags = "" ;
      bttBtn_enter_Jsonclick = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      bttBtn_delete_Jsonclick = "" ;
      lblTextblockfac_numero_Jsonclick = "" ;
      lblTextblockfac_fecha_Jsonclick = "" ;
      A16fac_fecha = GXutil.nullDate() ;
      lblTextblockfac_subtotal_Jsonclick = "" ;
      A17fac_subtotal = DecimalUtil.ZERO ;
      lblTextblockfac_impuesto_Jsonclick = "" ;
      A18fac_impuesto = DecimalUtil.ZERO ;
      lblTextblockfac_total_Jsonclick = "" ;
      A19fac_total = DecimalUtil.ZERO ;
      lblTextblockfac_estado_Jsonclick = "" ;
      A20fac_estado = "" ;
      lblTextblockcli_nombres_Jsonclick = "" ;
      A2cli_nombres = "" ;
      A3cli_apellidos = "" ;
      Gridfactura_detalleContainer = new com.genexus.webpanels.GXWebGrid(context);
      Gridfactura_detalleColumn = new com.genexus.webpanels.GXWebColumn();
      A11pro_nombre = "" ;
      A12pro_descripcion = "" ;
      A13pro_precio = DecimalUtil.ZERO ;
      A24detfac_total = DecimalUtil.ZERO ;
      B17fac_subtotal = DecimalUtil.ZERO ;
      sMode12 = "" ;
      edtpro_codigo_Internalname = "" ;
      lblTitledetalle_Jsonclick = "" ;
      imgBtn_first_Jsonclick = "" ;
      imgBtn_first_separator_Jsonclick = "" ;
      imgBtn_previous_Jsonclick = "" ;
      imgBtn_previous_separator_Jsonclick = "" ;
      imgBtn_next_Jsonclick = "" ;
      imgBtn_next_separator_Jsonclick = "" ;
      imgBtn_last_Jsonclick = "" ;
      imgBtn_last_separator_Jsonclick = "" ;
      imgBtn_select_Jsonclick = "" ;
      imgBtn_select_separator_Jsonclick = "" ;
      imgBtn_enter2_Jsonclick = "" ;
      imgBtn_enter2_separator_Jsonclick = "" ;
      imgBtn_cancel2_Jsonclick = "" ;
      imgBtn_cancel2_separator_Jsonclick = "" ;
      imgBtn_delete2_Jsonclick = "" ;
      imgBtn_delete2_separator_Jsonclick = "" ;
      Z16fac_fecha = GXutil.nullDate() ;
      Z20fac_estado = "" ;
      O17fac_subtotal = DecimalUtil.ZERO ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      sMode4 = "" ;
      s17fac_subtotal = DecimalUtil.ZERO ;
      s18fac_impuesto = DecimalUtil.ZERO ;
      O18fac_impuesto = DecimalUtil.ZERO ;
      s19fac_total = DecimalUtil.ZERO ;
      O19fac_total = DecimalUtil.ZERO ;
      edtdetfac_codigo_Internalname = "" ;
      edtpro_nombre_Internalname = "" ;
      edtpro_descripcion_Internalname = "" ;
      edtdetfac_cantidad_Internalname = "" ;
      edtpro_precio_Internalname = "" ;
      edtdetfac_total_Internalname = "" ;
      T24detfac_total = DecimalUtil.ZERO ;
      O24detfac_total = DecimalUtil.ZERO ;
      Z17fac_subtotal = DecimalUtil.ZERO ;
      Z2cli_nombres = "" ;
      Z3cli_apellidos = "" ;
      T000411_A14fac_codigo = new short[1] ;
      T000411_A15fac_numero = new short[1] ;
      T000411_n15fac_numero = new boolean[] {false} ;
      T000411_A16fac_fecha = new java.util.Date[] {GXutil.nullDate()} ;
      T000411_n16fac_fecha = new boolean[] {false} ;
      T000411_A20fac_estado = new String[] {""} ;
      T000411_n20fac_estado = new boolean[] {false} ;
      T000411_A2cli_nombres = new String[] {""} ;
      T000411_n2cli_nombres = new boolean[] {false} ;
      T000411_A3cli_apellidos = new String[] {""} ;
      T000411_n3cli_apellidos = new boolean[] {false} ;
      T000411_A1cli_codigo = new short[1] ;
      T000411_A17fac_subtotal = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      T000411_n17fac_subtotal = new boolean[] {false} ;
      T00047_A2cli_nombres = new String[] {""} ;
      T00047_n2cli_nombres = new boolean[] {false} ;
      T00047_A3cli_apellidos = new String[] {""} ;
      T00047_n3cli_apellidos = new boolean[] {false} ;
      T000412_A2cli_nombres = new String[] {""} ;
      T000412_n2cli_nombres = new boolean[] {false} ;
      T000412_A3cli_apellidos = new String[] {""} ;
      T000412_n3cli_apellidos = new boolean[] {false} ;
      T000413_A14fac_codigo = new short[1] ;
      T00046_A14fac_codigo = new short[1] ;
      T00046_A15fac_numero = new short[1] ;
      T00046_n15fac_numero = new boolean[] {false} ;
      T00046_A16fac_fecha = new java.util.Date[] {GXutil.nullDate()} ;
      T00046_n16fac_fecha = new boolean[] {false} ;
      T00046_A20fac_estado = new String[] {""} ;
      T00046_n20fac_estado = new boolean[] {false} ;
      T00046_A1cli_codigo = new short[1] ;
      T000414_A14fac_codigo = new short[1] ;
      T000415_A14fac_codigo = new short[1] ;
      T00045_A14fac_codigo = new short[1] ;
      T00045_A15fac_numero = new short[1] ;
      T00045_n15fac_numero = new boolean[] {false} ;
      T00045_A16fac_fecha = new java.util.Date[] {GXutil.nullDate()} ;
      T00045_n16fac_fecha = new boolean[] {false} ;
      T00045_A20fac_estado = new String[] {""} ;
      T00045_n20fac_estado = new boolean[] {false} ;
      T00045_A1cli_codigo = new short[1] ;
      T000417_A14fac_codigo = new short[1] ;
      T000420_A2cli_nombres = new String[] {""} ;
      T000420_n2cli_nombres = new boolean[] {false} ;
      T000420_A3cli_apellidos = new String[] {""} ;
      T000420_n3cli_apellidos = new boolean[] {false} ;
      T000421_A14fac_codigo = new short[1] ;
      Z13pro_precio = DecimalUtil.ZERO ;
      Z11pro_nombre = "" ;
      Z12pro_descripcion = "" ;
      T000422_A14fac_codigo = new short[1] ;
      T000422_A21detfac_codigo = new short[1] ;
      T000422_A22detfac_cantidad = new short[1] ;
      T000422_A13pro_precio = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      T000422_n13pro_precio = new boolean[] {false} ;
      T000422_A11pro_nombre = new String[] {""} ;
      T000422_n11pro_nombre = new boolean[] {false} ;
      T000422_A12pro_descripcion = new String[] {""} ;
      T000422_n12pro_descripcion = new boolean[] {false} ;
      T000422_A10pro_codigo = new short[1] ;
      T00044_A13pro_precio = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      T00044_n13pro_precio = new boolean[] {false} ;
      T00044_A11pro_nombre = new String[] {""} ;
      T00044_n11pro_nombre = new boolean[] {false} ;
      T00044_A12pro_descripcion = new String[] {""} ;
      T00044_n12pro_descripcion = new boolean[] {false} ;
      T000423_A13pro_precio = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      T000423_n13pro_precio = new boolean[] {false} ;
      T000423_A11pro_nombre = new String[] {""} ;
      T000423_n11pro_nombre = new boolean[] {false} ;
      T000423_A12pro_descripcion = new String[] {""} ;
      T000423_n12pro_descripcion = new boolean[] {false} ;
      T000424_A14fac_codigo = new short[1] ;
      T000424_A21detfac_codigo = new short[1] ;
      T00043_A14fac_codigo = new short[1] ;
      T00043_A21detfac_codigo = new short[1] ;
      T00043_A22detfac_cantidad = new short[1] ;
      T00043_A10pro_codigo = new short[1] ;
      T00042_A14fac_codigo = new short[1] ;
      T00042_A21detfac_codigo = new short[1] ;
      T00042_A22detfac_cantidad = new short[1] ;
      T00042_A10pro_codigo = new short[1] ;
      T000428_A13pro_precio = new java.math.BigDecimal[] {DecimalUtil.ZERO} ;
      T000428_n13pro_precio = new boolean[] {false} ;
      T000428_A11pro_nombre = new String[] {""} ;
      T000428_n11pro_nombre = new boolean[] {false} ;
      T000428_A12pro_descripcion = new String[] {""} ;
      T000428_n12pro_descripcion = new boolean[] {false} ;
      T000429_A14fac_codigo = new short[1] ;
      T000429_A21detfac_codigo = new short[1] ;
      imgprompt_10_Internalname = "" ;
      Gridfactura_detalleRow = new com.genexus.webpanels.GXWebRow();
      subGridfactura_detalle_Linesclass = "" ;
      ROClassString = "" ;
      GXCCtl = "" ;
      GXt_char7 = "" ;
      GXt_char6 = "" ;
      GXt_char5 = "" ;
      GXt_char4 = "" ;
      GXt_char3 = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      sDynURL = "" ;
      FormProcess = "" ;
      GXt_char9 = "" ;
      GXt_char8 = "" ;
      GXt_char10 = "" ;
      isValidOutput = new com.genexus.GxUnknownObjectCollection();
      pr_default = new DataStoreProvider(context, remoteHandle, new factura__default(),
         new Object[] {
             new Object[] {
            T00042_A14fac_codigo, T00042_A21detfac_codigo, T00042_A22detfac_cantidad, T00042_A10pro_codigo
            }
            , new Object[] {
            T00043_A14fac_codigo, T00043_A21detfac_codigo, T00043_A22detfac_cantidad, T00043_A10pro_codigo
            }
            , new Object[] {
            T00044_A13pro_precio, T00044_n13pro_precio, T00044_A11pro_nombre, T00044_n11pro_nombre, T00044_A12pro_descripcion, T00044_n12pro_descripcion
            }
            , new Object[] {
            T00045_A14fac_codigo, T00045_A15fac_numero, T00045_n15fac_numero, T00045_A16fac_fecha, T00045_n16fac_fecha, T00045_A20fac_estado, T00045_n20fac_estado, T00045_A1cli_codigo
            }
            , new Object[] {
            T00046_A14fac_codigo, T00046_A15fac_numero, T00046_n15fac_numero, T00046_A16fac_fecha, T00046_n16fac_fecha, T00046_A20fac_estado, T00046_n20fac_estado, T00046_A1cli_codigo
            }
            , new Object[] {
            T00047_A2cli_nombres, T00047_n2cli_nombres, T00047_A3cli_apellidos, T00047_n3cli_apellidos
            }
            , new Object[] {
            T00049_A17fac_subtotal, T00049_n17fac_subtotal
            }
            , new Object[] {
            T000411_A14fac_codigo, T000411_A15fac_numero, T000411_n15fac_numero, T000411_A16fac_fecha, T000411_n16fac_fecha, T000411_A20fac_estado, T000411_n20fac_estado, T000411_A2cli_nombres, T000411_n2cli_nombres, T000411_A3cli_apellidos,
            T000411_n3cli_apellidos, T000411_A1cli_codigo, T000411_A17fac_subtotal, T000411_n17fac_subtotal
            }
            , new Object[] {
            T000412_A2cli_nombres, T000412_n2cli_nombres, T000412_A3cli_apellidos, T000412_n3cli_apellidos
            }
            , new Object[] {
            T000413_A14fac_codigo
            }
            , new Object[] {
            T000414_A14fac_codigo
            }
            , new Object[] {
            T000415_A14fac_codigo
            }
            , new Object[] {
            }
            , new Object[] {
            T000417_A14fac_codigo
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000420_A2cli_nombres, T000420_n2cli_nombres, T000420_A3cli_apellidos, T000420_n3cli_apellidos
            }
            , new Object[] {
            T000421_A14fac_codigo
            }
            , new Object[] {
            T000422_A14fac_codigo, T000422_A21detfac_codigo, T000422_A22detfac_cantidad, T000422_A13pro_precio, T000422_n13pro_precio, T000422_A11pro_nombre, T000422_n11pro_nombre, T000422_A12pro_descripcion, T000422_n12pro_descripcion, T000422_A10pro_codigo
            }
            , new Object[] {
            T000423_A13pro_precio, T000423_n13pro_precio, T000423_A11pro_nombre, T000423_n11pro_nombre, T000423_A12pro_descripcion, T000423_n12pro_descripcion
            }
            , new Object[] {
            T000424_A14fac_codigo, T000424_A21detfac_codigo
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000428_A13pro_precio, T000428_n13pro_precio, T000428_A11pro_nombre, T000428_n11pro_nombre, T000428_A12pro_descripcion, T000428_n12pro_descripcion
            }
            , new Object[] {
            T000429_A14fac_codigo, T000429_A21detfac_codigo
            }
         }
      );
   }

   private byte GxWebError ;
   private byte nKeyPressed ;
   private byte subGridfactura_detalle_Backcolorstyle ;
   private byte subGridfactura_detalle_Allowcollapsing ;
   private byte subGridfactura_detalle_Collapsed ;
   private byte Gx_BScreen ;
   private byte subGridfactura_detalle_Backstyle ;
   private byte gxajaxcallmode ;
   private byte wbTemp ;
   private short nIsMod_12 ;
   private short A1cli_codigo ;
   private short A10pro_codigo ;
   private short nRC_Gridfactura_detalle ;
   private short nGXsfl_77_idx=1 ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short A15fac_numero ;
   private short A21detfac_codigo ;
   private short A22detfac_cantidad ;
   private short nBlankRcdCount12 ;
   private short nRcdExists_12 ;
   private short RcdFound12 ;
   private short nBlankRcdUsr12 ;
   private short Z14fac_codigo ;
   private short Z15fac_numero ;
   private short Z1cli_codigo ;
   private short A14fac_codigo ;
   private short nRcdDeleted_12 ;
   private short Z21detfac_codigo ;
   private short Z22detfac_cantidad ;
   private short Z10pro_codigo ;
   private short RcdFound4 ;
   private int trnEnded ;
   private int bttBtn_enter_Visible ;
   private int bttBtn_cancel_Visible ;
   private int bttBtn_delete_Visible ;
   private int edtfac_numero_Enabled ;
   private int edtfac_fecha_Enabled ;
   private int edtfac_subtotal_Enabled ;
   private int edtfac_impuesto_Enabled ;
   private int edtfac_total_Enabled ;
   private int edtfac_estado_Enabled ;
   private int edtcli_codigo_Enabled ;
   private int imgprompt_1_Visible ;
   private int edtcli_nombres_Enabled ;
   private int edtcli_apellidos_Enabled ;
   private int edtdetfac_codigo_Enabled ;
   private int edtpro_codigo_Enabled ;
   private int edtpro_nombre_Enabled ;
   private int edtpro_descripcion_Enabled ;
   private int edtdetfac_cantidad_Enabled ;
   private int edtpro_precio_Enabled ;
   private int edtdetfac_total_Enabled ;
   private int fRowAdded ;
   private int imgBtn_first_Visible ;
   private int imgBtn_first_separator_Visible ;
   private int imgBtn_previous_Visible ;
   private int imgBtn_previous_separator_Visible ;
   private int imgBtn_next_Visible ;
   private int imgBtn_next_separator_Visible ;
   private int imgBtn_last_Visible ;
   private int imgBtn_last_separator_Visible ;
   private int imgBtn_select_Visible ;
   private int imgBtn_select_separator_Visible ;
   private int imgBtn_enter2_Visible ;
   private int imgBtn_enter2_Enabled ;
   private int imgBtn_enter2_separator_Visible ;
   private int imgBtn_cancel2_Visible ;
   private int imgBtn_cancel2_separator_Visible ;
   private int imgBtn_delete2_Visible ;
   private int imgBtn_delete2_Enabled ;
   private int imgBtn_delete2_separator_Visible ;
   private int GRIDFACTURA_DETALLE_nFirstRecordOnPage ;
   private int GX_JID ;
   private int AnyError1 ;
   private int AnyError10 ;
   private int subGridfactura_detalle_Backcolor ;
   private int subGridfactura_detalle_Allbackcolor ;
   private int imgprompt_10_Visible ;
   private int defedtdetfac_codigo_Enabled ;
   private int idxLst ;
   private java.math.BigDecimal A17fac_subtotal ;
   private java.math.BigDecimal A18fac_impuesto ;
   private java.math.BigDecimal A19fac_total ;
   private java.math.BigDecimal A13pro_precio ;
   private java.math.BigDecimal A24detfac_total ;
   private java.math.BigDecimal B17fac_subtotal ;
   private java.math.BigDecimal O17fac_subtotal ;
   private java.math.BigDecimal s17fac_subtotal ;
   private java.math.BigDecimal s18fac_impuesto ;
   private java.math.BigDecimal O18fac_impuesto ;
   private java.math.BigDecimal s19fac_total ;
   private java.math.BigDecimal O19fac_total ;
   private java.math.BigDecimal T24detfac_total ;
   private java.math.BigDecimal O24detfac_total ;
   private java.math.BigDecimal Z17fac_subtotal ;
   private java.math.BigDecimal Z13pro_precio ;
   private String sPrefix ;
   private String sGXsfl_77_idx="0001" ;
   private String wcpOGx_mode ;
   private String scmdbuf ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String Gx_mode ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String GX_FocusControl ;
   private String edtfac_numero_Internalname ;
   private String sStyleString ;
   private String tblTablemain_Internalname ;
   private String ClassString ;
   private String StyleString ;
   private String grpGroupdata_Internalname ;
   private String tblTable1_Internalname ;
   private String TempTags ;
   private String bttBtn_enter_Internalname ;
   private String bttBtn_enter_Jsonclick ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String bttBtn_delete_Internalname ;
   private String bttBtn_delete_Jsonclick ;
   private String tblTable2_Internalname ;
   private String lblTextblockfac_numero_Internalname ;
   private String lblTextblockfac_numero_Jsonclick ;
   private String edtfac_numero_Jsonclick ;
   private String lblTextblockfac_fecha_Internalname ;
   private String lblTextblockfac_fecha_Jsonclick ;
   private String edtfac_fecha_Internalname ;
   private String edtfac_fecha_Jsonclick ;
   private String lblTextblockfac_subtotal_Internalname ;
   private String lblTextblockfac_subtotal_Jsonclick ;
   private String edtfac_subtotal_Internalname ;
   private String edtfac_subtotal_Jsonclick ;
   private String lblTextblockfac_impuesto_Internalname ;
   private String lblTextblockfac_impuesto_Jsonclick ;
   private String edtfac_impuesto_Internalname ;
   private String edtfac_impuesto_Jsonclick ;
   private String lblTextblockfac_total_Internalname ;
   private String lblTextblockfac_total_Jsonclick ;
   private String edtfac_total_Internalname ;
   private String edtfac_total_Jsonclick ;
   private String lblTextblockfac_estado_Internalname ;
   private String lblTextblockfac_estado_Jsonclick ;
   private String edtfac_estado_Internalname ;
   private String edtfac_estado_Jsonclick ;
   private String lblTextblockcli_nombres_Internalname ;
   private String lblTextblockcli_nombres_Jsonclick ;
   private String edtcli_codigo_Internalname ;
   private String edtcli_codigo_Jsonclick ;
   private String imgprompt_1_Internalname ;
   private String imgprompt_1_Link ;
   private String edtcli_nombres_Internalname ;
   private String edtcli_nombres_Jsonclick ;
   private String edtcli_apellidos_Internalname ;
   private String edtcli_apellidos_Jsonclick ;
   private String sMode12 ;
   private String edtpro_codigo_Internalname ;
   private String tblTable3_Internalname ;
   private String lblTitledetalle_Internalname ;
   private String lblTitledetalle_Jsonclick ;
   private String tblTabletoolbar_Internalname ;
   private String imgBtn_first_Internalname ;
   private String imgBtn_first_Jsonclick ;
   private String imgBtn_first_separator_Internalname ;
   private String imgBtn_first_separator_Jsonclick ;
   private String imgBtn_previous_Internalname ;
   private String imgBtn_previous_Jsonclick ;
   private String imgBtn_previous_separator_Internalname ;
   private String imgBtn_previous_separator_Jsonclick ;
   private String imgBtn_next_Internalname ;
   private String imgBtn_next_Jsonclick ;
   private String imgBtn_next_separator_Internalname ;
   private String imgBtn_next_separator_Jsonclick ;
   private String imgBtn_last_Internalname ;
   private String imgBtn_last_Jsonclick ;
   private String imgBtn_last_separator_Internalname ;
   private String imgBtn_last_separator_Jsonclick ;
   private String imgBtn_select_Internalname ;
   private String imgBtn_select_Jsonclick ;
   private String imgBtn_select_separator_Internalname ;
   private String imgBtn_select_separator_Jsonclick ;
   private String imgBtn_enter2_Internalname ;
   private String imgBtn_enter2_Jsonclick ;
   private String imgBtn_enter2_separator_Internalname ;
   private String imgBtn_enter2_separator_Jsonclick ;
   private String imgBtn_cancel2_Internalname ;
   private String imgBtn_cancel2_Jsonclick ;
   private String imgBtn_cancel2_separator_Internalname ;
   private String imgBtn_cancel2_separator_Jsonclick ;
   private String imgBtn_delete2_Internalname ;
   private String imgBtn_delete2_Jsonclick ;
   private String imgBtn_delete2_separator_Internalname ;
   private String imgBtn_delete2_separator_Jsonclick ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String sMode4 ;
   private String edtdetfac_codigo_Internalname ;
   private String edtpro_nombre_Internalname ;
   private String edtpro_descripcion_Internalname ;
   private String edtdetfac_cantidad_Internalname ;
   private String edtpro_precio_Internalname ;
   private String edtdetfac_total_Internalname ;
   private String imgprompt_10_Internalname ;
   private String subGridfactura_detalle_Class ;
   private String subGridfactura_detalle_Linesclass ;
   private String imgprompt_10_Link ;
   private String ROClassString ;
   private String edtdetfac_codigo_Jsonclick ;
   private String edtpro_codigo_Jsonclick ;
   private String edtpro_nombre_Jsonclick ;
   private String edtpro_descripcion_Jsonclick ;
   private String edtdetfac_cantidad_Jsonclick ;
   private String edtpro_precio_Jsonclick ;
   private String edtdetfac_total_Jsonclick ;
   private String GXCCtl ;
   private String GXt_char7 ;
   private String GXt_char6 ;
   private String GXt_char5 ;
   private String GXt_char4 ;
   private String GXt_char3 ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private String sDynURL ;
   private String FormProcess ;
   private String GXt_char9 ;
   private String GXt_char8 ;
   private String subGridfactura_detalle_Internalname ;
   private String GXt_char10 ;
   private java.util.Date A16fac_fecha ;
   private java.util.Date Z16fac_fecha ;
   private boolean entryPointCalled ;
   private boolean wbErr ;
   private boolean n17fac_subtotal ;
   private boolean n15fac_numero ;
   private boolean n16fac_fecha ;
   private boolean n20fac_estado ;
   private boolean n2cli_nombres ;
   private boolean n3cli_apellidos ;
   private boolean n13pro_precio ;
   private boolean n11pro_nombre ;
   private boolean n12pro_descripcion ;
   private String A20fac_estado ;
   private String A2cli_nombres ;
   private String A3cli_apellidos ;
   private String A11pro_nombre ;
   private String A12pro_descripcion ;
   private String Z20fac_estado ;
   private String Z2cli_nombres ;
   private String Z3cli_apellidos ;
   private String Z11pro_nombre ;
   private String Z12pro_descripcion ;
   private com.genexus.webpanels.GXWebGrid Gridfactura_detalleContainer ;
   private com.genexus.webpanels.GXWebRow Gridfactura_detalleRow ;
   private com.genexus.webpanels.GXWebColumn Gridfactura_detalleColumn ;
   private com.genexus.webpanels.GXMasterPage MasterPageObj ;
   private com.genexus.GxUnknownObjectCollection isValidOutput ;
   private IDataStoreProvider pr_default ;
   private short[] T000411_A14fac_codigo ;
   private short[] T000411_A15fac_numero ;
   private boolean[] T000411_n15fac_numero ;
   private java.util.Date[] T000411_A16fac_fecha ;
   private boolean[] T000411_n16fac_fecha ;
   private String[] T000411_A20fac_estado ;
   private boolean[] T000411_n20fac_estado ;
   private String[] T000411_A2cli_nombres ;
   private boolean[] T000411_n2cli_nombres ;
   private String[] T000411_A3cli_apellidos ;
   private boolean[] T000411_n3cli_apellidos ;
   private short[] T000411_A1cli_codigo ;
   private java.math.BigDecimal[] T000411_A17fac_subtotal ;
   private boolean[] T000411_n17fac_subtotal ;
   private String[] T00047_A2cli_nombres ;
   private boolean[] T00047_n2cli_nombres ;
   private String[] T00047_A3cli_apellidos ;
   private boolean[] T00047_n3cli_apellidos ;
   private String[] T000412_A2cli_nombres ;
   private boolean[] T000412_n2cli_nombres ;
   private String[] T000412_A3cli_apellidos ;
   private boolean[] T000412_n3cli_apellidos ;
   private short[] T000413_A14fac_codigo ;
   private short[] T00046_A14fac_codigo ;
   private short[] T00046_A15fac_numero ;
   private boolean[] T00046_n15fac_numero ;
   private java.util.Date[] T00046_A16fac_fecha ;
   private boolean[] T00046_n16fac_fecha ;
   private String[] T00046_A20fac_estado ;
   private boolean[] T00046_n20fac_estado ;
   private short[] T00046_A1cli_codigo ;
   private short[] T000414_A14fac_codigo ;
   private short[] T000415_A14fac_codigo ;
   private short[] T00045_A14fac_codigo ;
   private short[] T00045_A15fac_numero ;
   private boolean[] T00045_n15fac_numero ;
   private java.util.Date[] T00045_A16fac_fecha ;
   private boolean[] T00045_n16fac_fecha ;
   private String[] T00045_A20fac_estado ;
   private boolean[] T00045_n20fac_estado ;
   private short[] T00045_A1cli_codigo ;
   private short[] T000417_A14fac_codigo ;
   private String[] T000420_A2cli_nombres ;
   private boolean[] T000420_n2cli_nombres ;
   private String[] T000420_A3cli_apellidos ;
   private boolean[] T000420_n3cli_apellidos ;
   private short[] T000421_A14fac_codigo ;
   private short[] T000422_A14fac_codigo ;
   private short[] T000422_A21detfac_codigo ;
   private short[] T000422_A22detfac_cantidad ;
   private java.math.BigDecimal[] T000422_A13pro_precio ;
   private boolean[] T000422_n13pro_precio ;
   private String[] T000422_A11pro_nombre ;
   private boolean[] T000422_n11pro_nombre ;
   private String[] T000422_A12pro_descripcion ;
   private boolean[] T000422_n12pro_descripcion ;
   private short[] T000422_A10pro_codigo ;
   private java.math.BigDecimal[] T00044_A13pro_precio ;
   private boolean[] T00044_n13pro_precio ;
   private String[] T00044_A11pro_nombre ;
   private boolean[] T00044_n11pro_nombre ;
   private String[] T00044_A12pro_descripcion ;
   private boolean[] T00044_n12pro_descripcion ;
   private java.math.BigDecimal[] T000423_A13pro_precio ;
   private boolean[] T000423_n13pro_precio ;
   private String[] T000423_A11pro_nombre ;
   private boolean[] T000423_n11pro_nombre ;
   private String[] T000423_A12pro_descripcion ;
   private boolean[] T000423_n12pro_descripcion ;
   private short[] T000424_A14fac_codigo ;
   private short[] T000424_A21detfac_codigo ;
   private short[] T00043_A14fac_codigo ;
   private short[] T00043_A21detfac_codigo ;
   private short[] T00043_A22detfac_cantidad ;
   private short[] T00043_A10pro_codigo ;
   private short[] T00042_A14fac_codigo ;
   private short[] T00042_A21detfac_codigo ;
   private short[] T00042_A22detfac_cantidad ;
   private short[] T00042_A10pro_codigo ;
   private java.math.BigDecimal[] T000428_A13pro_precio ;
   private boolean[] T000428_n13pro_precio ;
   private String[] T000428_A11pro_nombre ;
   private boolean[] T000428_n11pro_nombre ;
   private String[] T000428_A12pro_descripcion ;
   private boolean[] T000428_n12pro_descripcion ;
   private short[] T000429_A14fac_codigo ;
   private short[] T000429_A21detfac_codigo ;
   private java.math.BigDecimal[] T00049_A17fac_subtotal ;
   private boolean[] T00049_n17fac_subtotal ;
   private com.genexus.webpanels.GXWebForm Form ;
}

final  class factura__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00042", "SELECT [fac_codigo], [detfac_codigo], [detfac_cantidad], [pro_codigo] FROM [facturaLevel1] WITH (UPDLOCK) WHERE [fac_codigo] = ? AND [detfac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00043", "SELECT [fac_codigo], [detfac_codigo], [detfac_cantidad], [pro_codigo] FROM [facturaLevel1] WITH (NOLOCK) WHERE [fac_codigo] = ? AND [detfac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00044", "SELECT [pro_precio], [pro_nombre], [pro_descripcion] FROM [producto] WITH (NOLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00045", "SELECT [fac_codigo], [fac_numero], [fac_fecha], [fac_estado], [cli_codigo] FROM [factura] WITH (UPDLOCK) WHERE [fac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00046", "SELECT [fac_codigo], [fac_numero], [fac_fecha], [fac_estado], [cli_codigo] FROM [factura] WITH (NOLOCK) WHERE [fac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00047", "SELECT [cli_nombres], [cli_apellidos] FROM [cliente] WITH (NOLOCK) WHERE [cli_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00049", "SELECT COALESCE( T1.[fac_subtotal], 0) AS fac_subtotal FROM (SELECT SUM(T2.[detfac_cantidad] * CAST(COALESCE( T3.[pro_precio], 0) AS decimal( 15, 5))) AS fac_subtotal, T2.[fac_codigo] FROM ([facturaLevel1] T2 WITH (UPDLOCK) INNER JOIN [producto] T3 WITH (NOLOCK) ON T3.[pro_codigo] = T2.[pro_codigo]) GROUP BY T2.[fac_codigo] ) T1 WHERE T1.[fac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000411", "SELECT TM1.[fac_codigo], TM1.[fac_numero], TM1.[fac_fecha], TM1.[fac_estado], T3.[cli_nombres], T3.[cli_apellidos], TM1.[cli_codigo], COALESCE( T2.[fac_subtotal], 0) AS fac_subtotal FROM (([factura] TM1 WITH (NOLOCK) LEFT JOIN (SELECT SUM(T4.[detfac_cantidad] * CAST(COALESCE( T5.[pro_precio], 0) AS decimal( 15, 5))) AS fac_subtotal, T4.[fac_codigo] FROM ([facturaLevel1] T4 WITH (NOLOCK) INNER JOIN [producto] T5 WITH (NOLOCK) ON T5.[pro_codigo] = T4.[pro_codigo]) GROUP BY T4.[fac_codigo] ) T2 ON T2.[fac_codigo] = TM1.[fac_codigo]) INNER JOIN [cliente] T3 WITH (NOLOCK) ON T3.[cli_codigo] = TM1.[cli_codigo]) WHERE TM1.[fac_codigo] = ? ORDER BY TM1.[fac_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
         ,new ForEachCursor("T000412", "SELECT [cli_nombres], [cli_apellidos] FROM [cliente] WITH (NOLOCK) WHERE [cli_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000413", "SELECT [fac_codigo] FROM [factura] WITH (NOLOCK) WHERE [fac_codigo] = ?  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000414", "SELECT TOP 1 [fac_codigo] FROM [factura] WITH (NOLOCK) WHERE ( [fac_codigo] > ?) ORDER BY [fac_codigo]  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new ForEachCursor("T000415", "SELECT TOP 1 [fac_codigo] FROM [factura] WITH (NOLOCK) WHERE ( [fac_codigo] < ?) ORDER BY [fac_codigo] DESC  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new UpdateCursor("T000416", "INSERT INTO [factura] ([fac_numero], [fac_fecha], [fac_estado], [cli_codigo]) VALUES (?, ?, ?, ?)", GX_NOMASK)
         ,new ForEachCursor("T000417", "SELECT Ident_Current('[factura]') ",true, GX_NOMASK, false, this,1,0,false )
         ,new UpdateCursor("T000418", "UPDATE [factura] SET [fac_numero]=?, [fac_fecha]=?, [fac_estado]=?, [cli_codigo]=?  WHERE [fac_codigo] = ?", GX_NOMASK)
         ,new UpdateCursor("T000419", "DELETE FROM [factura]  WHERE [fac_codigo] = ?", GX_NOMASK)
         ,new ForEachCursor("T000420", "SELECT [cli_nombres], [cli_apellidos] FROM [cliente] WITH (NOLOCK) WHERE [cli_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000421", "SELECT [fac_codigo] FROM [factura] WITH (NOLOCK) ORDER BY [fac_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
         ,new ForEachCursor("T000422", "SELECT T1.[fac_codigo], T1.[detfac_codigo], T1.[detfac_cantidad], T2.[pro_precio], T2.[pro_nombre], T2.[pro_descripcion], T1.[pro_codigo] FROM ([facturaLevel1] T1 WITH (NOLOCK) INNER JOIN [producto] T2 WITH (NOLOCK) ON T2.[pro_codigo] = T1.[pro_codigo]) WHERE T1.[fac_codigo] = ? and T1.[detfac_codigo] = ? ORDER BY T1.[fac_codigo], T1.[detfac_codigo] ",true, GX_NOMASK, false, this,11,0,false )
         ,new ForEachCursor("T000423", "SELECT [pro_precio], [pro_nombre], [pro_descripcion] FROM [producto] WITH (NOLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000424", "SELECT [fac_codigo], [detfac_codigo] FROM [facturaLevel1] WITH (NOLOCK) WHERE [fac_codigo] = ? AND [detfac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new UpdateCursor("T000425", "INSERT INTO [facturaLevel1] ([fac_codigo], [detfac_codigo], [detfac_cantidad], [pro_codigo]) VALUES (?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T000426", "UPDATE [facturaLevel1] SET [detfac_cantidad]=?, [pro_codigo]=?  WHERE [fac_codigo] = ? AND [detfac_codigo] = ?", GX_NOMASK)
         ,new UpdateCursor("T000427", "DELETE FROM [facturaLevel1]  WHERE [fac_codigo] = ? AND [detfac_codigo] = ?", GX_NOMASK)
         ,new ForEachCursor("T000428", "SELECT [pro_precio], [pro_nombre], [pro_descripcion] FROM [producto] WITH (NOLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000429", "SELECT [fac_codigo], [detfac_codigo] FROM [facturaLevel1] WITH (NOLOCK) WHERE [fac_codigo] = ? ORDER BY [fac_codigo], [detfac_codigo] ",true, GX_NOMASK, false, this,11,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
            case 1 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((short[]) buf[3])[0] = rslt.getShort(4) ;
               break;
            case 2 :
               ((java.math.BigDecimal[]) buf[0])[0] = rslt.getBigDecimal(1,2) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 3 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(5) ;
               break;
            case 4 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(5) ;
               break;
            case 5 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 6 :
               ((java.math.BigDecimal[]) buf[0])[0] = rslt.getBigDecimal(1,2) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               break;
            case 7 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((java.util.Date[]) buf[3])[0] = rslt.getGXDate(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[10])[0] = rslt.wasNull();
               ((short[]) buf[11])[0] = rslt.getShort(7) ;
               ((java.math.BigDecimal[]) buf[12])[0] = rslt.getBigDecimal(8,2) ;
               ((boolean[]) buf[13])[0] = rslt.wasNull();
               break;
            case 8 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 9 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 10 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 11 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 13 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 16 :
               ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               break;
            case 17 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 18 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((short[]) buf[2])[0] = rslt.getShort(3) ;
               ((java.math.BigDecimal[]) buf[3])[0] = rslt.getBigDecimal(4,2) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
               ((boolean[]) buf[8])[0] = rslt.wasNull();
               ((short[]) buf[9])[0] = rslt.getShort(7) ;
               break;
            case 19 :
               ((java.math.BigDecimal[]) buf[0])[0] = rslt.getBigDecimal(1,2) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 20 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               break;
            case 24 :
               ((java.math.BigDecimal[]) buf[0])[0] = rslt.getBigDecimal(1,2) ;
               ((boolean[]) buf[1])[0] = rslt.wasNull();
               ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
               ((boolean[]) buf[3])[0] = rslt.wasNull();
               ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
               ((boolean[]) buf[5])[0] = rslt.wasNull();
               break;
            case 25 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 1 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 2 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 3 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 4 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 5 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 6 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 7 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 8 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 9 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 10 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 11 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 12 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(1, ((Number) parms[1]).shortValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(2, (java.util.Date)parms[3]);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 10);
               }
               stmt.setShort(4, ((Number) parms[6]).shortValue());
               break;
            case 14 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(1, ((Number) parms[1]).shortValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.TIMESTAMP );
               }
               else
               {
                  stmt.setDate(2, (java.util.Date)parms[3]);
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.VARCHAR );
               }
               else
               {
                  stmt.setVarchar(3, (String)parms[5], 10);
               }
               stmt.setShort(4, ((Number) parms[6]).shortValue());
               stmt.setShort(5, ((Number) parms[7]).shortValue());
               break;
            case 15 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 16 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 18 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 19 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 20 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 21 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 22 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               stmt.setShort(3, ((Number) parms[2]).shortValue());
               stmt.setShort(4, ((Number) parms[3]).shortValue());
               break;
            case 23 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               stmt.setShort(2, ((Number) parms[1]).shortValue());
               break;
            case 24 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 25 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
      }
   }

}

