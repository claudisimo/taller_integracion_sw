/*
               File: detalle_factura_impl
        Description: detalle_factura
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 22, 2022 12:30:36.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class detalle_factura_impl extends GXDataArea
{
   public void initenv( )
   {
      if ( GxWebError != 0 )
      {
         return  ;
      }
   }

   public void inittrn( )
   {
      initialize_properties( ) ;
      entryPointCalled = false ;
      gxfirstwebparm = httpContext.GetNextPar( ) ;
      gxfirstwebparm_bkp = gxfirstwebparm ;
      gxfirstwebparm = httpContext.DecryptAjaxCall( gxfirstwebparm, "High") ;
      if ( GXutil.strcmp(gxfirstwebparm, "dyncall") == 0 )
      {
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         dyncall( httpContext.GetNextPar( )) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_2") == 0 )
      {
         A14fac_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_2( A14fac_codigo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
      {
         A10pro_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
         httpContext.setAjaxCallMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxload_3( A10pro_codigo) ;
         return  ;
      }
      else if ( GXutil.strcmp(gxfirstwebparm, "gxajaxEvt") == 0 )
      {
         httpContext.setAjaxEventMode();
         if ( ! httpContext.IsValidAjaxCall( true) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = httpContext.GetNextPar( ) ;
      }
      else
      {
         if ( ! httpContext.IsValidAjaxCall( false) )
         {
            GxWebError = (byte)(1) ;
            return  ;
         }
         gxfirstwebparm = gxfirstwebparm_bkp ;
      }
      Form.getMeta().addItem("Generator", "GeneXus Java", (short)(0)) ;
      Form.getMeta().addItem("Version", "10_1_8-58720", (short)(0)) ;
      Form.getMeta().addItem("Description", "detalle_factura", (short)(0)) ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      GX_FocusControl = edtdetfac_codigo_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      wbErr = false ;
      httpContext.setTheme("GeneXusX");
   }

   public detalle_factura_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public detalle_factura_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( detalle_factura_impl.class ));
   }

   public detalle_factura_impl( int remoteHandle ,
                                ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void webExecute( )
   {
      initenv( ) ;
      inittrn( ) ;
      if ( ( GxWebError == 0 ) && ! httpContext.isAjaxCallMode( ) )
      {
         MasterPageObj = new menu_administrador_impl (remoteHandle, context.copy());
         MasterPageObj.setDataArea(this,false);
         MasterPageObj.webExecute();
         if ( httpContext.isAjaxRequest( ) )
         {
            httpContext.enableOutput();
            if ( ! httpContext.isAjaxRequest( ) )
            {
               httpContext.GX_webresponse.addHeader("Cache-Control", "max-age=0");
            }
            if ( (GXutil.strcmp("", httpContext.wjLoc)==0) )
            {
               httpContext.GX_webresponse.addString(httpContext.getJSONResponse( ));
            }
            else
            {
               if ( httpContext.isAjaxRequest( ) )
               {
                  httpContext.disableOutput();
               }
               renderHtmlHeaders( ) ;
               httpContext.redirect( httpContext.wjLoc );
               httpContext.dispatchAjaxCommands();
            }
         }
      }
      if ( httpContext.isAjaxCallMode( ) )
      {
         cleanup();
      }
   }

   public void draw( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         disable_std_buttons( ) ;
         enableDisable( ) ;
         set_caption( ) ;
         /* Form start */
         wb_table1_2_055( true) ;
      }
      return  ;
   }

   public void wb_table1_2_055e( boolean wbgen )
   {
      if ( wbgen )
      {
      }
      /* Execute Exit event if defined. */
   }

   public void wb_table1_2_055( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table2_5_055( true) ;
      }
      return  ;
   }

   public void wb_table2_5_055e( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Control Group */
         ClassString = "Group" ;
         StyleString = "" ;
         httpContext.writeText( "<fieldset id=\""+grpGroupdata_Internalname+"\""+" style=\"-moz-border-radius:3pt;\""+" class=\""+ClassString+"\">") ;
         httpContext.writeText( "<legend class=\""+ClassString+"Title"+"\">"+"detalle_factura"+"</legend>") ;
         wb_table3_27_055( true) ;
      }
      return  ;
   }

   public void wb_table3_27_055e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</fieldset>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_055e( true) ;
      }
      else
      {
         wb_table1_2_055e( false) ;
      }
   }

   public void wb_table3_27_055( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         ClassString = "ErrorViewer" ;
         StyleString = "" ;
         GxWebStd.gx_msg_list( httpContext, "", httpContext.GX_msglist.getDisplaymode(), StyleString, ClassString, "", "false");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         wb_table4_33_055( true) ;
      }
      return  ;
   }

   public void wb_table4_33_055e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"" ;
         ClassString = "BtnEnter" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "rounded", "EENTER.", TempTags, "", httpContext.getButtonType( ), "HLP_detalle_factura.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"" ;
         ClassString = "BtnCancel" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "rounded", "ECANCEL.", TempTags, "", httpContext.getButtonType( ), "HLP_detalle_factura.htm");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"" ;
         ClassString = "BtnDelete" ;
         StyleString = "" ;
         GxWebStd.gx_button_ctrl( httpContext, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "rounded", "EDELETE.", TempTags, "", httpContext.getButtonType( ), "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table3_27_055e( true) ;
      }
      else
      {
         wb_table3_27_055e( false) ;
      }
   }

   public void wb_table4_33_055( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockdetfac_codigo_Internalname, "detfac_codigo", "", "", lblTextblockdetfac_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtdetfac_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A21detfac_codigo, (byte)(4), (byte)(0), ",", "")), ((edtdetfac_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A21detfac_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A21detfac_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(38);\"", "", "", "", "", edtdetfac_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtdetfac_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockdetfac_cantidad_Internalname, "detfac_cantidad", "", "", lblTextblockdetfac_cantidad_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A22detfac_cantidad", GXutil.ltrim( GXutil.str( A22detfac_cantidad, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtdetfac_cantidad_Internalname, GXutil.ltrim( localUtil.ntoc( A22detfac_cantidad, (byte)(4), (byte)(0), ",", "")), ((edtdetfac_cantidad_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A22detfac_cantidad), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A22detfac_cantidad), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(43);\"", "", "", "", "", edtdetfac_cantidad_Jsonclick, 0, ClassString, StyleString, "", 1, edtdetfac_cantidad_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockdetfac_precio_Internalname, "detfac_precio", "", "", lblTextblockdetfac_precio_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A23detfac_precio", GXutil.ltrim( GXutil.str( A23detfac_precio, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtdetfac_precio_Internalname, GXutil.ltrim( localUtil.ntoc( A23detfac_precio, (byte)(4), (byte)(0), ",", "")), ((edtdetfac_precio_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A23detfac_precio), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A23detfac_precio), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(48);\"", "", "", "", "", edtdetfac_precio_Jsonclick, 0, ClassString, StyleString, "", 1, edtdetfac_precio_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockdetfac_total_Internalname, "detfac_total", "", "", lblTextblockdetfac_total_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A24detfac_total", GXutil.ltrim( GXutil.str( A24detfac_total, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtdetfac_total_Internalname, GXutil.ltrim( localUtil.ntoc( A24detfac_total, (byte)(4), (byte)(0), ",", "")), ((edtdetfac_total_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A24detfac_total), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A24detfac_total), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(53);\"", "", "", "", "", edtdetfac_total_Jsonclick, 0, ClassString, StyleString, "", 1, edtdetfac_total_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockfac_codigo_Internalname, "C�digo de la factura", "", "", lblTextblockfac_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtfac_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ",", "")), ((edtfac_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A14fac_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A14fac_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(58);\"", "", "", "", "", edtfac_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtfac_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_detalle_factura.htm");
         /* Static images/pictures */
         ClassString = "Image" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgprompt_14_Internalname, "prompt.gif", imgprompt_14_Link, "", "", "GeneXusX", imgprompt_14_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "''", "", "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td valign=\"top\" class=\"td5\" >") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblTextblockpro_codigo_Internalname, "C�digo del producto", "", "", lblTextblockpro_codigo_Jsonclick, "", StyleString, ClassString, 0, "", 1, 1, (short)(0), "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td>") ;
         /* Single line edit */
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"" ;
         ClassString = "Attribute" ;
         StyleString = "" ;
         GxWebStd.gx_single_line_edit( httpContext, edtpro_codigo_Internalname, GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ",", "")), ((edtpro_codigo_Enabled!=0) ? GXutil.ltrim( localUtil.format( DecimalUtil.doubleToDec(A10pro_codigo), "ZZZ9")) : localUtil.format( DecimalUtil.doubleToDec(A10pro_codigo), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(63);\"", "", "", "", "", edtpro_codigo_Jsonclick, 0, ClassString, StyleString, "", 1, edtpro_codigo_Enabled, 0, 4, "chr", 1, "row", 4, (byte)(0), (short)(0), 0, (byte)(1), (byte)(-1), true, "right", "HLP_detalle_factura.htm");
         /* Static images/pictures */
         ClassString = "Image" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgprompt_10_Internalname, "prompt.gif", imgprompt_10_Link, "", "", "GeneXusX", imgprompt_10_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "''", "", "HLP_detalle_factura.htm");
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table4_33_055e( true) ;
      }
      else
      {
         wb_table4_33_055e( false) ;
      }
   }

   public void wb_table2_5_055( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "<div style=\"WHITE-SPACE: nowrap\" class=\"ToolbarMain\">") ;
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_Internalname, context.getHttpContext().getImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_Visible, 1, "", "Primero", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_first_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_first_separator_Jsonclick, "EFIRST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_Internalname, context.getHttpContext().getImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_previous_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "EPREVIOUS.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_Internalname, context.getHttpContext().getImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_Visible, 1, "", "Siguiente", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_next_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_next_separator_Jsonclick, "ENEXT.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_Internalname, context.getHttpContext().getImagePath( "29211874-e613-48e5-9011-8017d984217e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_Visible, 1, "", "Ultimo", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_last_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_last_separator_Jsonclick, "ELAST.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_Internalname, context.getHttpContext().getImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_Visible, 1, "", "Seleccionar", 0, 0, 0, "", 0, "", 0, 0, 4, imgBtn_select_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "gx.popup.openPrompt('"+"gx0050"+"',["+"{Ctrl:gx.dom.el('"+"DETFAC_CODIGO"+"'), id:'"+"DETFAC_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_select_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 4, imgBtn_select_separator_Jsonclick, "ESELECT.", StyleString, ClassString, "", ""+TempTags, "", "gx.popup.openPrompt('"+"gx0050"+"',["+"{Ctrl:gx.dom.el('"+"DETFAC_CODIGO"+"'), id:'"+"DETFAC_CODIGO"+"'"+",isOut:true,isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_Internalname, context.getHttpContext().getImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_enter2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "EENTER.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_Internalname, context.getHttpContext().getImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_Visible, 1, "", "Cancelar", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_cancel2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "ECANCEL.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"" ;
         ClassString = "ImageHandCenter" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_Internalname, context.getHttpContext().getImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         /* Active images/pictures */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"" ;
         ClassString = "ImageTop" ;
         StyleString = "" ;
         GxWebStd.gx_bitmap( httpContext, imgBtn_delete2_separator_Internalname, context.getHttpContext().getImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", "GeneXusX"), "", "", "", "GeneXusX", imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "EDELETE.", StyleString, ClassString, "", ""+TempTags, "", "", "HLP_detalle_factura.htm");
         httpContext.writeText( "</div>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "<tr>") ;
         httpContext.writeText( "<td>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table2_5_055e( true) ;
      }
      else
      {
         wb_table2_5_055e( false) ;
      }
   }

   public void userMain( )
   {
      standaloneStartup( ) ;
   }

   public void standaloneStartup( )
   {
      standaloneStartupServer( ) ;
      disable_std_buttons( ) ;
      enableDisable( ) ;
      process( ) ;
   }

   public void standaloneStartupServer( )
   {
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      httpContext.wbGlbDoneStart = (byte)(1) ;
      assign_properties_default( ) ;
      if ( AnyError == 0 )
      {
         if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "DETFAC_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtdetfac_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A21detfac_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
            }
            else
            {
               A21detfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
            }
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_cantidad_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_cantidad_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "DETFAC_CANTIDAD");
               AnyError = (short)(1) ;
               GX_FocusControl = edtdetfac_cantidad_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A22detfac_cantidad = (short)(0) ;
               n22detfac_cantidad = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A22detfac_cantidad", GXutil.ltrim( GXutil.str( A22detfac_cantidad, 4, 0)));
            }
            else
            {
               A22detfac_cantidad = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_cantidad_Internalname), ",", ".")) ;
               n22detfac_cantidad = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A22detfac_cantidad", GXutil.ltrim( GXutil.str( A22detfac_cantidad, 4, 0)));
            }
            n22detfac_cantidad = ((0==A22detfac_cantidad) ? true : false) ;
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_precio_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_precio_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "DETFAC_PRECIO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtdetfac_precio_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A23detfac_precio = (short)(0) ;
               n23detfac_precio = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A23detfac_precio", GXutil.ltrim( GXutil.str( A23detfac_precio, 4, 0)));
            }
            else
            {
               A23detfac_precio = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_precio_Internalname), ",", ".")) ;
               n23detfac_precio = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A23detfac_precio", GXutil.ltrim( GXutil.str( A23detfac_precio, 4, 0)));
            }
            n23detfac_precio = ((0==A23detfac_precio) ? true : false) ;
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_total_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtdetfac_total_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "DETFAC_TOTAL");
               AnyError = (short)(1) ;
               GX_FocusControl = edtdetfac_total_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A24detfac_total = (short)(0) ;
               n24detfac_total = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A24detfac_total", GXutil.ltrim( GXutil.str( A24detfac_total, 4, 0)));
            }
            else
            {
               A24detfac_total = (short)(localUtil.ctol( httpContext.cgiGet( edtdetfac_total_Internalname), ",", ".")) ;
               n24detfac_total = false ;
               httpContext.ajax_rsp_assign_attri("", false, "A24detfac_total", GXutil.ltrim( GXutil.str( A24detfac_total, 4, 0)));
            }
            n24detfac_total = ((0==A24detfac_total) ? true : false) ;
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtfac_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtfac_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "FAC_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtfac_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A14fac_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
            }
            else
            {
               A14fac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtfac_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
            }
            if ( ( ( localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".") < 0 ) ) || ( ( localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".") > 9999 ) ) )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_badnum"), 1, "PRO_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtpro_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true ;
               A10pro_codigo = (short)(0) ;
               httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
            }
            else
            {
               A10pro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( edtpro_codigo_Internalname), ",", ".")) ;
               httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
            }
            /* Read saved values. */
            Z21detfac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z21detfac_codigo"), ",", ".")) ;
            Z22detfac_cantidad = (short)(localUtil.ctol( httpContext.cgiGet( "Z22detfac_cantidad"), ",", ".")) ;
            n22detfac_cantidad = ((0==A22detfac_cantidad) ? true : false) ;
            Z23detfac_precio = (short)(localUtil.ctol( httpContext.cgiGet( "Z23detfac_precio"), ",", ".")) ;
            n23detfac_precio = ((0==A23detfac_precio) ? true : false) ;
            Z24detfac_total = (short)(localUtil.ctol( httpContext.cgiGet( "Z24detfac_total"), ",", ".")) ;
            n24detfac_total = ((0==A24detfac_total) ? true : false) ;
            Z14fac_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z14fac_codigo"), ",", ".")) ;
            Z10pro_codigo = (short)(localUtil.ctol( httpContext.cgiGet( "Z10pro_codigo"), ",", ".")) ;
            IsConfirmed = (short)(localUtil.ctol( httpContext.cgiGet( "IsConfirmed"), ",", ".")) ;
            IsModified = (short)(localUtil.ctol( httpContext.cgiGet( "IsModified"), ",", ".")) ;
            Gx_mode = httpContext.cgiGet( "Mode") ;
            Gx_mode = httpContext.cgiGet( "vMODE") ;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( GXutil.strcmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A21detfac_codigo = (short)(GXutil.lval( httpContext.GetNextPar( ))) ;
               httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }
   }

   public void process( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read Transaction buttons. */
         sEvt = httpContext.cgiGet( "_EventName") ;
         EvtGridId = httpContext.cgiGet( "_EventGridId") ;
         EvtRowId = httpContext.cgiGet( "_EventRowId") ;
         if ( GXutil.len( sEvt) > 0 )
         {
            sEvtType = GXutil.left( sEvt, 1) ;
            sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
            if ( GXutil.strcmp(sEvtType, "M") != 0 )
            {
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "ENTER") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_enter( ) ;
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                     else if ( GXutil.strcmp(sEvt, "FIRST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_first( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "PREVIOUS") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_previous( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "NEXT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_next( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "LAST") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_last( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "SELECT") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_select( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "DELETE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        btn_delete( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        afterkeyloadscreen( ) ;
                     }
                  }
                  else
                  {
                  }
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void afterTrn( )
   {
      if ( trnEnded == 1 )
      {
         trnEnded = 0 ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         if ( ( GXutil.strcmp(Gx_mode, "INS") == 0 )  )
         {
            /* Clear variables for new insertion. */
            initAll055( ) ;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
         }
      }
   }

   public String toString( )
   {
      return "" ;
   }

   public GXContentInfo getContentInfo( )
   {
      return (GXContentInfo)(null) ;
   }

   public void disable_std_buttons( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
      {
         imgBtn_delete2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
      }
   }

   public void disable_std_buttons_dsp( )
   {
      imgBtn_delete2_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
      imgBtn_delete2_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
      bttBtn_delete_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
      imgBtn_first_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_first_Visible, 5, 0)));
      imgBtn_first_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_first_separator_Visible, 5, 0)));
      imgBtn_previous_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_previous_Visible, 5, 0)));
      imgBtn_previous_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_previous_separator_Visible, 5, 0)));
      imgBtn_next_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_next_Visible, 5, 0)));
      imgBtn_next_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_next_separator_Visible, 5, 0)));
      imgBtn_last_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_last_Visible, 5, 0)));
      imgBtn_last_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_last_separator_Visible, 5, 0)));
      imgBtn_select_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_select_Visible, 5, 0)));
      imgBtn_select_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_select_separator_Visible, 5, 0)));
      imgBtn_delete2_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_Visible, 5, 0)));
      imgBtn_delete2_separator_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_delete2_separator_Visible, 5, 0)));
      bttBtn_delete_Visible = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_delete_Visible, 5, 0)));
      if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
      {
         imgBtn_enter2_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_Visible, 5, 0)));
         imgBtn_enter2_separator_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", GXutil.ltrim( GXutil.str( imgBtn_enter2_separator_Visible, 5, 0)));
         bttBtn_enter_Visible = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", GXutil.ltrim( GXutil.str( bttBtn_enter_Visible, 5, 0)));
      }
      disableAttributes055( ) ;
   }

   public void set_caption( )
   {
      if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_confdelete"), 0, "");
         }
         else
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_mustconfirm"), 0, "");
         }
      }
   }

   public void resetCaption050( )
   {
   }

   public void zm055( int GX_JID )
   {
      if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
         {
            Z22detfac_cantidad = T00053_A22detfac_cantidad[0] ;
            Z23detfac_precio = T00053_A23detfac_precio[0] ;
            Z24detfac_total = T00053_A24detfac_total[0] ;
            Z14fac_codigo = T00053_A14fac_codigo[0] ;
            Z10pro_codigo = T00053_A10pro_codigo[0] ;
         }
         else
         {
            Z22detfac_cantidad = A22detfac_cantidad ;
            Z23detfac_precio = A23detfac_precio ;
            Z24detfac_total = A24detfac_total ;
            Z14fac_codigo = A14fac_codigo ;
            Z10pro_codigo = A10pro_codigo ;
         }
      }
      if ( GX_JID == -1 )
      {
         Z21detfac_codigo = A21detfac_codigo ;
         Z22detfac_cantidad = A22detfac_cantidad ;
         Z23detfac_precio = A23detfac_precio ;
         Z24detfac_total = A24detfac_total ;
         Z14fac_codigo = A14fac_codigo ;
         Z10pro_codigo = A10pro_codigo ;
      }
   }

   public void standaloneNotModal( )
   {
      imgprompt_14_Link = ((GXutil.strcmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0040"+"',["+"{Ctrl:gx.dom.el('"+"FAC_CODIGO"+"'), id:'"+"FAC_CODIGO"+"'"+",isOut: true}"+"],"+"null"+","+"'', false"+","+"false"+");") ;
      imgprompt_10_Link = ((GXutil.strcmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0030"+"',["+"{Ctrl:gx.dom.el('"+"PRO_CODIGO"+"'), id:'"+"PRO_CODIGO"+"'"+",isOut: true}"+"],"+"null"+","+"'', false"+","+"false"+");") ;
   }

   public void standaloneModal( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
      {
         imgBtn_delete2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
      }
      else
      {
         imgBtn_delete2_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_delete2_Enabled, 5, 0)));
      }
      if ( GXutil.strcmp(Gx_mode, "DSP") == 0 )
      {
         imgBtn_enter2_Enabled = 0 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
      else
      {
         imgBtn_enter2_Enabled = 1 ;
         httpContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", GXutil.ltrim( GXutil.str( imgBtn_enter2_Enabled, 5, 0)));
      }
   }

   public void load055( )
   {
      /* Using cursor T00056 */
      pr_default.execute(4, new Object[] {new Short(A21detfac_codigo)});
      if ( (pr_default.getStatus(4) != 101) )
      {
         RcdFound5 = (short)(1) ;
         A22detfac_cantidad = T00056_A22detfac_cantidad[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A22detfac_cantidad", GXutil.ltrim( GXutil.str( A22detfac_cantidad, 4, 0)));
         n22detfac_cantidad = T00056_n22detfac_cantidad[0] ;
         A23detfac_precio = T00056_A23detfac_precio[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A23detfac_precio", GXutil.ltrim( GXutil.str( A23detfac_precio, 4, 0)));
         n23detfac_precio = T00056_n23detfac_precio[0] ;
         A24detfac_total = T00056_A24detfac_total[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A24detfac_total", GXutil.ltrim( GXutil.str( A24detfac_total, 4, 0)));
         n24detfac_total = T00056_n24detfac_total[0] ;
         A14fac_codigo = T00056_A14fac_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
         A10pro_codigo = T00056_A10pro_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
         zm055( -1) ;
      }
      pr_default.close(4);
      onLoadActions055( ) ;
   }

   public void onLoadActions055( )
   {
   }

   public void checkExtendedTable055( )
   {
      Gx_BScreen = (byte)(1) ;
      standaloneModal( ) ;
      /* Using cursor T00054 */
      pr_default.execute(2, new Object[] {new Short(A14fac_codigo)});
      if ( (pr_default.getStatus(2) == 101) )
      {
         AnyError14 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'factura'.", "ForeignKeyNotFound", 1, "FAC_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtfac_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError14 == 0 )
      {
      }
      pr_default.close(2);
      /* Using cursor T00055 */
      pr_default.execute(3, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(3) == 101) )
      {
         AnyError10 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'producto'.", "ForeignKeyNotFound", 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError10 == 0 )
      {
      }
      pr_default.close(3);
   }

   public void closeExtendedTableCursors055( )
   {
      pr_default.close(2);
      pr_default.close(3);
   }

   public void enableDisable( )
   {
   }

   public void gxload_2( short A14fac_codigo )
   {
      /* Using cursor T00057 */
      pr_default.execute(5, new Object[] {new Short(A14fac_codigo)});
      if ( (pr_default.getStatus(5) == 101) )
      {
         AnyError14 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'factura'.", "ForeignKeyNotFound", 1, "FAC_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtfac_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError14 == 0 )
      {
      }
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.GX_webresponse.addString("new Array( new Array(");
      httpContext.GX_webresponse.addString("");
      httpContext.GX_webresponse.addString(")");
      if ( (pr_default.getStatus(5) == 101) )
      {
         httpContext.GX_webresponse.addString(",");
         httpContext.GX_webresponse.addString("101");
      }
      httpContext.GX_webresponse.addString(")");
      pr_default.close(5);
   }

   public void gxload_3( short A10pro_codigo )
   {
      /* Using cursor T00058 */
      pr_default.execute(6, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(6) == 101) )
      {
         AnyError10 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'producto'.", "ForeignKeyNotFound", 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError10 == 0 )
      {
      }
      GxWebStd.set_html_headers( httpContext, 0, "", "");
      httpContext.GX_webresponse.addString("new Array( new Array(");
      httpContext.GX_webresponse.addString("");
      httpContext.GX_webresponse.addString(")");
      if ( (pr_default.getStatus(6) == 101) )
      {
         httpContext.GX_webresponse.addString(",");
         httpContext.GX_webresponse.addString("101");
      }
      httpContext.GX_webresponse.addString(")");
      pr_default.close(6);
   }

   public void getKey055( )
   {
      /* Using cursor T00059 */
      pr_default.execute(7, new Object[] {new Short(A21detfac_codigo)});
      if ( (pr_default.getStatus(7) != 101) )
      {
         RcdFound5 = (short)(1) ;
      }
      else
      {
         RcdFound5 = (short)(0) ;
      }
      pr_default.close(7);
   }

   public void getByPrimaryKey( )
   {
      /* Using cursor T00053 */
      pr_default.execute(1, new Object[] {new Short(A21detfac_codigo)});
      if ( (pr_default.getStatus(1) != 101) )
      {
         zm055( 1) ;
         RcdFound5 = (short)(1) ;
         A21detfac_codigo = T00053_A21detfac_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
         A22detfac_cantidad = T00053_A22detfac_cantidad[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A22detfac_cantidad", GXutil.ltrim( GXutil.str( A22detfac_cantidad, 4, 0)));
         n22detfac_cantidad = T00053_n22detfac_cantidad[0] ;
         A23detfac_precio = T00053_A23detfac_precio[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A23detfac_precio", GXutil.ltrim( GXutil.str( A23detfac_precio, 4, 0)));
         n23detfac_precio = T00053_n23detfac_precio[0] ;
         A24detfac_total = T00053_A24detfac_total[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A24detfac_total", GXutil.ltrim( GXutil.str( A24detfac_total, 4, 0)));
         n24detfac_total = T00053_n24detfac_total[0] ;
         A14fac_codigo = T00053_A14fac_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
         A10pro_codigo = T00053_A10pro_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
         Z21detfac_codigo = A21detfac_codigo ;
         sMode5 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal( ) ;
         load055( ) ;
         Gx_mode = sMode5 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         RcdFound5 = (short)(0) ;
         initializeNonKey055( ) ;
         sMode5 = Gx_mode ;
         Gx_mode = "DSP" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         standaloneModal( ) ;
         Gx_mode = sMode5 ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      pr_default.close(1);
   }

   public void getEqualNoModal( )
   {
      getKey055( ) ;
      if ( RcdFound5 == 0 )
      {
         Gx_mode = "INS" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      else
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      getByPrimaryKey( ) ;
   }

   public void move_next( )
   {
      RcdFound5 = (short)(0) ;
      /* Using cursor T000510 */
      pr_default.execute(8, new Object[] {new Short(A21detfac_codigo)});
      if ( (pr_default.getStatus(8) != 101) )
      {
         while ( (pr_default.getStatus(8) != 101) && ( ( T000510_A21detfac_codigo[0] < A21detfac_codigo ) ) )
         {
            pr_default.readNext(8);
         }
         if ( (pr_default.getStatus(8) != 101) && ( ( T000510_A21detfac_codigo[0] > A21detfac_codigo ) ) )
         {
            A21detfac_codigo = T000510_A21detfac_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
            RcdFound5 = (short)(1) ;
         }
      }
      pr_default.close(8);
   }

   public void move_previous( )
   {
      RcdFound5 = (short)(0) ;
      /* Using cursor T000511 */
      pr_default.execute(9, new Object[] {new Short(A21detfac_codigo)});
      if ( (pr_default.getStatus(9) != 101) )
      {
         while ( (pr_default.getStatus(9) != 101) && ( ( T000511_A21detfac_codigo[0] > A21detfac_codigo ) ) )
         {
            pr_default.readNext(9);
         }
         if ( (pr_default.getStatus(9) != 101) && ( ( T000511_A21detfac_codigo[0] < A21detfac_codigo ) ) )
         {
            A21detfac_codigo = T000511_A21detfac_codigo[0] ;
            httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
            RcdFound5 = (short)(1) ;
         }
      }
      pr_default.close(9);
   }

   public void btn_enter( )
   {
      nKeyPressed = (byte)(1) ;
      getKey055( ) ;
      if ( RcdFound5 == 1 )
      {
         if ( GXutil.strcmp(Gx_mode, "INS") == 0 )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "DETFAC_CODIGO");
            AnyError = (short)(1) ;
            GX_FocusControl = edtdetfac_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else if ( A21detfac_codigo != Z21detfac_codigo )
         {
            A21detfac_codigo = Z21detfac_codigo ;
            httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforeupd"), "CandidateKeyNotFound", 1, "DETFAC_CODIGO");
            AnyError = (short)(1) ;
            GX_FocusControl = edtdetfac_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else if ( GXutil.strcmp(Gx_mode, "DLT") == 0 )
         {
            delete( ) ;
            afterTrn( ) ;
            GX_FocusControl = edtdetfac_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            Gx_mode = "UPD" ;
            httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            /* Update record */
            update055( ) ;
            GX_FocusControl = edtdetfac_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }
      else
      {
         if ( A21detfac_codigo != Z21detfac_codigo )
         {
            Gx_mode = "INS" ;
            httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            /* Insert record */
            GX_FocusControl = edtdetfac_codigo_Internalname ;
            httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            insert055( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "" ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( GXutil.strcmp(Gx_mode, "UPD") == 0 )
            {
               httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_recdeleted"), 1, "DETFAC_CODIGO");
               AnyError = (short)(1) ;
               GX_FocusControl = edtdetfac_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            else
            {
               Gx_mode = "INS" ;
               httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               /* Insert record */
               GX_FocusControl = edtdetfac_codigo_Internalname ;
               httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               insert055( ) ;
               if ( AnyError == 1 )
               {
                  GX_FocusControl = "" ;
                  httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
         }
      }
      afterTrn( ) ;
   }

   public void btn_delete( )
   {
      if ( A21detfac_codigo != Z21detfac_codigo )
      {
         A21detfac_codigo = Z21detfac_codigo ;
         httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_getbeforedlt"), 1, "DETFAC_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtdetfac_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      else
      {
         delete( ) ;
         afterTrn( ) ;
         GX_FocusControl = edtdetfac_codigo_Internalname ;
         httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      }
      if ( AnyError != 0 )
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      getByPrimaryKey( ) ;
      CloseOpenCursors();
   }

   public void btn_get( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      if ( RcdFound5 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_keynfound"), "PrimaryKeyNotFound", 1, "DETFAC_CODIGO");
         AnyError = (short)(1) ;
      }
      GX_FocusControl = edtdetfac_cantidad_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_first( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart055( ) ;
      if ( RcdFound5 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      GX_FocusControl = edtdetfac_cantidad_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd055( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_previous( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_previous( ) ;
      if ( RcdFound5 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      GX_FocusControl = edtdetfac_cantidad_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_next( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      move_next( ) ;
      if ( RcdFound5 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      GX_FocusControl = edtdetfac_cantidad_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_last( )
   {
      nKeyPressed = (byte)(2) ;
      IsConfirmed = (short)(0) ;
      scanStart055( ) ;
      if ( RcdFound5 == 0 )
      {
         httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_norectobrow"), 0, "");
      }
      else
      {
         while ( RcdFound5 != 0 )
         {
            scanNext055( ) ;
         }
         Gx_mode = "UPD" ;
         httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }
      GX_FocusControl = edtdetfac_cantidad_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      scanEnd055( ) ;
      getByPrimaryKey( ) ;
      standaloneNotModal( ) ;
      standaloneModal( ) ;
   }

   public void btn_select( )
   {
      getEqualNoModal( ) ;
   }

   public void checkOptimisticConcurrency055( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         /* Using cursor T00052 */
         pr_default.execute(0, new Object[] {new Short(A21detfac_codigo)});
         if ( (pr_default.getStatus(0) == 103) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"detalle_factura"}), "RecordIsLocked", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
         if ( (pr_default.getStatus(0) == 101) || ( Z22detfac_cantidad != T00052_A22detfac_cantidad[0] ) || ( Z23detfac_precio != T00052_A23detfac_precio[0] ) || ( Z24detfac_total != T00052_A24detfac_total[0] ) || ( Z14fac_codigo != T00052_A14fac_codigo[0] ) || ( Z10pro_codigo != T00052_A10pro_codigo[0] ) )
         {
            httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_waschg", new Object[] {"detalle_factura"}), "RecordWasChanged", 1, "");
            AnyError = (short)(1) ;
            return  ;
         }
      }
   }

   public void insert055( )
   {
      beforeValidate055( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable055( ) ;
      }
      if ( AnyError == 0 )
      {
         zm055( 0) ;
         checkOptimisticConcurrency055( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm055( ) ;
            if ( AnyError == 0 )
            {
               beforeInsert055( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000512 */
                  pr_default.execute(10, new Object[] {new Short(A21detfac_codigo), new Boolean(n22detfac_cantidad), new Short(A22detfac_cantidad), new Boolean(n23detfac_precio), new Short(A23detfac_precio), new Boolean(n24detfac_total), new Short(A24detfac_total), new Short(A14fac_codigo), new Short(A10pro_codigo)});
                  if ( (pr_default.getStatus(10) == 1) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_noupdate"), "DuplicatePrimaryKey", 1, "");
                     AnyError = (short)(1) ;
                  }
                  if ( AnyError == 0 )
                  {
                     /* Start of After( Insert) rules */
                     /* End of After( Insert) rules */
                     if ( AnyError == 0 )
                     {
                        /* Save values for previous() function. */
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucadded"), 0, "");
                        resetCaption050( ) ;
                     }
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
         else
         {
            load055( ) ;
         }
         endLevel055( ) ;
      }
      closeExtendedTableCursors055( ) ;
   }

   public void update055( )
   {
      beforeValidate055( ) ;
      if ( AnyError == 0 )
      {
         checkExtendedTable055( ) ;
      }
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency055( ) ;
         if ( AnyError == 0 )
         {
            afterConfirm055( ) ;
            if ( AnyError == 0 )
            {
               beforeUpdate055( ) ;
               if ( AnyError == 0 )
               {
                  /* Using cursor T000513 */
                  pr_default.execute(11, new Object[] {new Boolean(n22detfac_cantidad), new Short(A22detfac_cantidad), new Boolean(n23detfac_precio), new Short(A23detfac_precio), new Boolean(n24detfac_total), new Short(A24detfac_total), new Short(A14fac_codigo), new Short(A10pro_codigo), new Short(A21detfac_codigo)});
                  if ( (pr_default.getStatus(11) == 103) )
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_lock", new Object[] {"detalle_factura"}), "RecordIsLocked", 1, "");
                     AnyError = (short)(1) ;
                  }
                  deferredUpdate055( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( update) rules */
                     /* End of After( update) rules */
                     if ( AnyError == 0 )
                     {
                        getByPrimaryKey( ) ;
                        httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucupdated"), 0, "");
                        resetCaption050( ) ;
                     }
                  }
                  else
                  {
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                     AnyError = (short)(1) ;
                  }
               }
            }
         }
         endLevel055( ) ;
      }
      closeExtendedTableCursors055( ) ;
   }

   public void deferredUpdate055( )
   {
   }

   public void delete( )
   {
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      beforeValidate055( ) ;
      if ( AnyError == 0 )
      {
         checkOptimisticConcurrency055( ) ;
      }
      if ( AnyError == 0 )
      {
         onDeleteControls055( ) ;
         afterConfirm055( ) ;
         if ( AnyError == 0 )
         {
            beforeDelete055( ) ;
            if ( AnyError == 0 )
            {
               /* No cascading delete specified. */
               /* Using cursor T000514 */
               pr_default.execute(12, new Object[] {new Short(A21detfac_codigo)});
               if ( AnyError == 0 )
               {
                  /* Start of After( delete) rules */
                  /* End of After( delete) rules */
                  if ( AnyError == 0 )
                  {
                     move_next( ) ;
                     if ( RcdFound5 == 0 )
                     {
                        initAll055( ) ;
                        Gx_mode = "INS" ;
                        httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     }
                     else
                     {
                        getByPrimaryKey( ) ;
                        Gx_mode = "UPD" ;
                        httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     }
                     httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_sucdeleted"), 0, "");
                     resetCaption050( ) ;
                  }
               }
               else
               {
                  httpContext.GX_msglist.addItem(localUtil.getMessages().getMessage("GXM_unexp"), 1, "");
                  AnyError = (short)(1) ;
               }
            }
         }
      }
      sMode5 = Gx_mode ;
      Gx_mode = "DLT" ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      endLevel055( ) ;
      Gx_mode = sMode5 ;
      httpContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
   }

   public void onDeleteControls055( )
   {
      standaloneModal( ) ;
      /* No delete mode formulas found. */
   }

   public void endLevel055( )
   {
      if ( GXutil.strcmp(Gx_mode, "INS") != 0 )
      {
         pr_default.close(0);
      }
      if ( AnyError == 0 )
      {
         beforeComplete055( ) ;
      }
      if ( AnyError == 0 )
      {
         Application.commit(context, remoteHandle, "DEFAULT", "detalle_factura");
         if ( AnyError == 0 )
         {
            confirmValues050( ) ;
         }
         /* After transaction rules */
         /* Execute 'After Trn' event if defined. */
         trnEnded = 1 ;
      }
      else
      {
         Application.rollback(context, remoteHandle, "DEFAULT", "detalle_factura");
      }
      IsModified = (short)(0) ;
      if ( AnyError != 0 )
      {
         httpContext.wjLoc = "" ;
         httpContext.nUserReturn = (byte)(0) ;
      }
   }

   public void scanStart055( )
   {
      /* Using cursor T000515 */
      pr_default.execute(13);
      RcdFound5 = (short)(0) ;
      if ( (pr_default.getStatus(13) != 101) )
      {
         RcdFound5 = (short)(1) ;
         A21detfac_codigo = T000515_A21detfac_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
      }
      /* Load Subordinate Levels */
   }

   public void scanNext055( )
   {
      pr_default.readNext(13);
      RcdFound5 = (short)(0) ;
      if ( (pr_default.getStatus(13) != 101) )
      {
         RcdFound5 = (short)(1) ;
         A21detfac_codigo = T000515_A21detfac_codigo[0] ;
         httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
      }
   }

   public void scanEnd055( )
   {
      pr_default.close(13);
   }

   public void afterConfirm055( )
   {
      /* After Confirm Rules */
   }

   public void beforeInsert055( )
   {
      /* Before Insert Rules */
   }

   public void beforeUpdate055( )
   {
      /* Before Update Rules */
   }

   public void beforeDelete055( )
   {
      /* Before Delete Rules */
   }

   public void beforeComplete055( )
   {
      /* Before Complete Rules */
   }

   public void beforeValidate055( )
   {
      /* Before Validate Rules */
   }

   public void disableAttributes055( )
   {
      edtdetfac_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtdetfac_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtdetfac_codigo_Enabled, 5, 0)));
      edtdetfac_cantidad_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtdetfac_cantidad_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtdetfac_cantidad_Enabled, 5, 0)));
      edtdetfac_precio_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtdetfac_precio_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtdetfac_precio_Enabled, 5, 0)));
      edtdetfac_total_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtdetfac_total_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtdetfac_total_Enabled, 5, 0)));
      edtfac_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtfac_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtfac_codigo_Enabled, 5, 0)));
      edtpro_codigo_Enabled = 0 ;
      httpContext.ajax_rsp_assign_prop("", false, edtpro_codigo_Internalname, "Enabled", GXutil.ltrim( GXutil.str( edtpro_codigo_Enabled, 5, 0)));
   }

   public void assign_properties_default( )
   {
   }

   public void confirmValues050( )
   {
   }

   public void renderHtmlHeaders( )
   {
      GxWebStd.gx_html_headers( httpContext, 0, "", "", Form.getMeta(), Form.getMetaequiv(), "IE=EmulateIE7");
   }

   public void renderHtmlOpenForm( )
   {
      httpContext.writeText( "<title>") ;
      httpContext.writeText( Form.getCaption()) ;
      httpContext.writeTextNL( "</title>") ;
      if ( GXutil.len( sDynURL) > 0 )
      {
         httpContext.writeText( "<BASE href=\""+sDynURL+"\" />") ;
      }
      define_styles( ) ;
      MasterPageObj.master_styles();
      if ( ! httpContext.isSmartDevice( ) )
      {
         httpContext.AddJavascriptSource("gxgral.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      else
      {
         httpContext.AddJavascriptSource("gxapiSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfxSD.js", "?58720");
         httpContext.AddJavascriptSource("gxtypesSD.js", "?58720");
         httpContext.AddJavascriptSource("gxpopupSD.js", "?58720");
         httpContext.AddJavascriptSource("gxfrmutlSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgridSD.js", "?58720");
         httpContext.AddJavascriptSource("JavaScripTableSD.js", "?58720");
         httpContext.AddJavascriptSource("rijndaelSD.js", "?58720");
         httpContext.AddJavascriptSource("gxgralSD.js", "?58720");
         httpContext.AddJavascriptSource("gxcfg.js", "?58720");
      }
      httpContext.writeText( Form.getHeaderrawhtml()) ;
      httpContext.closeHtmlHeader();
      FormProcess = " onkeyup=\"gx.evt.onkeyup(event)\" onkeypress=\"gx.evt.onkeypress(event,true,false)\" onkeydown=\"gx.evt.onkeypress(null,true,false)\"" ;
      httpContext.writeText( "<body") ;
      httpContext.writeText( " "+"class=\"Form\""+" "+" style=\"-moz-opacity:0;opacity:0;"+"background-color:"+WebUtils.getHTMLColor( Form.getIBackground())+";") ;
      if ( ! ( (GXutil.strcmp("", Form.getBackground())==0) ) )
      {
         httpContext.writeText( " background-image:url("+httpContext.convertURL( Form.getBackground())+")") ;
      }
      httpContext.writeText( "\""+FormProcess+">") ;
      httpContext.skipLines( 1 );
      httpContext.writeTextNL( "<form id=\"MAINFORM\" onsubmit=\"try{return gx.csv.validForm()}catch(e){return true;}\" name=\"MAINFORM\" method=\"post\" action=\""+formatLink("detalle_factura") +"\" class=\""+"Form"+"\">") ;
      GxWebStd.gx_hidden_field( httpContext, "_EventName", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventGridId", "");
      GxWebStd.gx_hidden_field( httpContext, "_EventRowId", "");
   }

   public void renderHtmlCloseForm( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      GxWebStd.gx_hidden_field( httpContext, "Z21detfac_codigo", GXutil.ltrim( localUtil.ntoc( Z21detfac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z22detfac_cantidad", GXutil.ltrim( localUtil.ntoc( Z22detfac_cantidad, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z23detfac_precio", GXutil.ltrim( localUtil.ntoc( Z23detfac_precio, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z24detfac_total", GXutil.ltrim( localUtil.ntoc( Z24detfac_total, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z14fac_codigo", GXutil.ltrim( localUtil.ntoc( Z14fac_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Z10pro_codigo", GXutil.ltrim( localUtil.ntoc( Z10pro_codigo, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsConfirmed", GXutil.ltrim( localUtil.ntoc( IsConfirmed, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "IsModified", GXutil.ltrim( localUtil.ntoc( IsModified, (byte)(4), (byte)(0), ",", "")));
      GxWebStd.gx_hidden_field( httpContext, "Mode", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "vMODE", GXutil.rtrim( Gx_mode));
      GxWebStd.gx_hidden_field( httpContext, "GX_FocusControl", GX_FocusControl);
      httpContext.SendAjaxEncryptionKey();
      httpContext.SendComponentObjects();
      httpContext.SendServerCommands();
      httpContext.SendState();
      httpContext.writeTextNL( "</form>") ;
      include_jscripts( ) ;
   }

   public byte executeStartEvent( )
   {
      standaloneStartup( ) ;
      gxajaxcallmode = (byte)((httpContext.isAjaxCallMode( ) ? 1 : 0)) ;
      return gxajaxcallmode ;
   }

   public void renderHtmlContent( )
   {
      draw( ) ;
   }

   public void dispatchEvents( )
   {
      process( ) ;
   }

   public boolean hasEnterEvent( )
   {
      return true ;
   }

   public String getPgmname( )
   {
      return "detalle_factura" ;
   }

   public String getPgmdesc( )
   {
      return "detalle_factura" ;
   }

   public com.genexus.webpanels.GXWebForm getForm( )
   {
      return Form ;
   }

   public String getSelfLink( )
   {
      return formatLink("detalle_factura")  ;
   }

   public void initializeNonKey055( )
   {
      A22detfac_cantidad = (short)(0) ;
      n22detfac_cantidad = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A22detfac_cantidad", GXutil.ltrim( GXutil.str( A22detfac_cantidad, 4, 0)));
      n22detfac_cantidad = ((0==A22detfac_cantidad) ? true : false) ;
      A23detfac_precio = (short)(0) ;
      n23detfac_precio = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A23detfac_precio", GXutil.ltrim( GXutil.str( A23detfac_precio, 4, 0)));
      n23detfac_precio = ((0==A23detfac_precio) ? true : false) ;
      A24detfac_total = (short)(0) ;
      n24detfac_total = false ;
      httpContext.ajax_rsp_assign_attri("", false, "A24detfac_total", GXutil.ltrim( GXutil.str( A24detfac_total, 4, 0)));
      n24detfac_total = ((0==A24detfac_total) ? true : false) ;
      A14fac_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A14fac_codigo", GXutil.ltrim( GXutil.str( A14fac_codigo, 4, 0)));
      A10pro_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A10pro_codigo", GXutil.ltrim( GXutil.str( A10pro_codigo, 4, 0)));
   }

   public void initAll055( )
   {
      A21detfac_codigo = (short)(0) ;
      httpContext.ajax_rsp_assign_attri("", false, "A21detfac_codigo", GXutil.ltrim( GXutil.str( A21detfac_codigo, 4, 0)));
      initializeNonKey055( ) ;
   }

   public void standaloneModalInsert( )
   {
   }

   public void define_styles( )
   {
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= Form.getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( Form.getJscriptsrc().item(idxLst)), "?12303695");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("messages.spa.js", "?58720");
      httpContext.AddJavascriptSource("detalle_factura.js", "?12303697");
      /* End function include_jscripts */
   }

   public void init_default_properties( )
   {
      imgBtn_first_Internalname = "BTN_FIRST" ;
      imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR" ;
      imgBtn_previous_Internalname = "BTN_PREVIOUS" ;
      imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR" ;
      imgBtn_next_Internalname = "BTN_NEXT" ;
      imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR" ;
      imgBtn_last_Internalname = "BTN_LAST" ;
      imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR" ;
      imgBtn_select_Internalname = "BTN_SELECT" ;
      imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR" ;
      imgBtn_enter2_Internalname = "BTN_ENTER2" ;
      imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR" ;
      imgBtn_cancel2_Internalname = "BTN_CANCEL2" ;
      imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR" ;
      imgBtn_delete2_Internalname = "BTN_DELETE2" ;
      imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR" ;
      tblTabletoolbar_Internalname = "TABLETOOLBAR" ;
      lblTextblockdetfac_codigo_Internalname = "TEXTBLOCKDETFAC_CODIGO" ;
      edtdetfac_codigo_Internalname = "DETFAC_CODIGO" ;
      lblTextblockdetfac_cantidad_Internalname = "TEXTBLOCKDETFAC_CANTIDAD" ;
      edtdetfac_cantidad_Internalname = "DETFAC_CANTIDAD" ;
      lblTextblockdetfac_precio_Internalname = "TEXTBLOCKDETFAC_PRECIO" ;
      edtdetfac_precio_Internalname = "DETFAC_PRECIO" ;
      lblTextblockdetfac_total_Internalname = "TEXTBLOCKDETFAC_TOTAL" ;
      edtdetfac_total_Internalname = "DETFAC_TOTAL" ;
      lblTextblockfac_codigo_Internalname = "TEXTBLOCKFAC_CODIGO" ;
      edtfac_codigo_Internalname = "FAC_CODIGO" ;
      lblTextblockpro_codigo_Internalname = "TEXTBLOCKPRO_CODIGO" ;
      edtpro_codigo_Internalname = "PRO_CODIGO" ;
      tblTable2_Internalname = "TABLE2" ;
      bttBtn_enter_Internalname = "BTN_ENTER" ;
      bttBtn_cancel_Internalname = "BTN_CANCEL" ;
      bttBtn_delete_Internalname = "BTN_DELETE" ;
      tblTable1_Internalname = "TABLE1" ;
      grpGroupdata_Internalname = "GROUPDATA" ;
      tblTablemain_Internalname = "TABLEMAIN" ;
      Form.setInternalname( "FORM" );
      imgprompt_14_Internalname = "PROMPT_14" ;
      imgprompt_10_Internalname = "PROMPT_10" ;
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      Form.setHeaderrawhtml( "" );
      Form.setBackground( "" );
      Form.setIBackground( (int)(0xFFFFFF) );
      Form.setCaption( "detalle_factura" );
      imgBtn_delete2_separator_Visible = 1 ;
      imgBtn_delete2_Enabled = 1 ;
      imgBtn_delete2_Visible = 1 ;
      imgBtn_cancel2_separator_Visible = 1 ;
      imgBtn_cancel2_Visible = 1 ;
      imgBtn_enter2_separator_Visible = 1 ;
      imgBtn_enter2_Enabled = 1 ;
      imgBtn_enter2_Visible = 1 ;
      imgBtn_select_separator_Visible = 1 ;
      imgBtn_select_Visible = 1 ;
      imgBtn_last_separator_Visible = 1 ;
      imgBtn_last_Visible = 1 ;
      imgBtn_next_separator_Visible = 1 ;
      imgBtn_next_Visible = 1 ;
      imgBtn_previous_separator_Visible = 1 ;
      imgBtn_previous_Visible = 1 ;
      imgBtn_first_separator_Visible = 1 ;
      imgBtn_first_Visible = 1 ;
      imgprompt_10_Visible = 1 ;
      imgprompt_10_Link = "" ;
      edtpro_codigo_Jsonclick = "" ;
      edtpro_codigo_Enabled = 1 ;
      imgprompt_14_Visible = 1 ;
      imgprompt_14_Link = "" ;
      edtfac_codigo_Jsonclick = "" ;
      edtfac_codigo_Enabled = 1 ;
      edtdetfac_total_Jsonclick = "" ;
      edtdetfac_total_Enabled = 1 ;
      edtdetfac_precio_Jsonclick = "" ;
      edtdetfac_precio_Enabled = 1 ;
      edtdetfac_cantidad_Jsonclick = "" ;
      edtdetfac_cantidad_Enabled = 1 ;
      edtdetfac_codigo_Jsonclick = "" ;
      edtdetfac_codigo_Enabled = 1 ;
      bttBtn_delete_Visible = 1 ;
      bttBtn_cancel_Visible = 1 ;
      bttBtn_enter_Visible = 1 ;
      httpContext.GX_msglist.setDisplaymode( (short)(1) );
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void afterkeyloadscreen( )
   {
      IsConfirmed = (short)(0) ;
      getEqualNoModal( ) ;
      GX_FocusControl = edtdetfac_cantidad_Internalname ;
      httpContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
      standaloneNotModal( ) ;
      standaloneModal( ) ;
      /* End function AfterKeyLoadScreen */
   }

   public void valid_Detfac_codigo( short GX_Parm1 ,
                                    short GX_Parm2 ,
                                    short GX_Parm3 ,
                                    short GX_Parm4 ,
                                    short GX_Parm5 ,
                                    short GX_Parm6 )
   {
      A21detfac_codigo = GX_Parm1 ;
      A22detfac_cantidad = GX_Parm2 ;
      n22detfac_cantidad = false ;
      A23detfac_precio = GX_Parm3 ;
      n23detfac_precio = false ;
      A24detfac_total = GX_Parm4 ;
      n24detfac_total = false ;
      A14fac_codigo = GX_Parm5 ;
      A10pro_codigo = GX_Parm6 ;
      httpContext.wbHandled = (byte)(1) ;
      afterkeyloadscreen( ) ;
      draw( ) ;
      dynload_actions( ) ;
      if ( AnyError == 1 )
      {
      }
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A22detfac_cantidad, (byte)(4), (byte)(0), ".", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A23detfac_precio, (byte)(4), (byte)(0), ".", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A24detfac_total, (byte)(4), (byte)(0), ".", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A14fac_codigo, (byte)(4), (byte)(0), ".", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( A10pro_codigo, (byte)(4), (byte)(0), ".", "")));
      isValidOutput.add(GXutil.rtrim( Gx_mode));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z21detfac_codigo, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z22detfac_cantidad, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z23detfac_precio, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z24detfac_total, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z14fac_codigo, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(GXutil.ltrim( localUtil.ntoc( Z10pro_codigo, (byte)(4), (byte)(0), ",", "")));
      isValidOutput.add(imgBtn_delete2_Enabled);
      isValidOutput.add(imgBtn_enter2_Enabled);
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   public void valid_Fac_codigo( short GX_Parm1 )
   {
      A14fac_codigo = GX_Parm1 ;
      /* Using cursor T000516 */
      pr_default.execute(14, new Object[] {new Short(A14fac_codigo)});
      if ( (pr_default.getStatus(14) == 101) )
      {
         AnyError14 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'factura'.", "ForeignKeyNotFound", 1, "FAC_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtfac_codigo_Internalname ;
      }
      if ( AnyError14 == 0 )
      {
      }
      pr_default.close(14);
      dynload_actions( ) ;
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   public void valid_Pro_codigo( short GX_Parm1 )
   {
      A10pro_codigo = GX_Parm1 ;
      /* Using cursor T000517 */
      pr_default.execute(15, new Object[] {new Short(A10pro_codigo)});
      if ( (pr_default.getStatus(15) == 101) )
      {
         AnyError10 = 1 ;
         httpContext.GX_msglist.addItem("No existe 'producto'.", "ForeignKeyNotFound", 1, "PRO_CODIGO");
         AnyError = (short)(1) ;
         GX_FocusControl = edtpro_codigo_Internalname ;
      }
      if ( AnyError10 == 0 )
      {
      }
      pr_default.close(15);
      dynload_actions( ) ;
      isValidOutput.add(httpContext.GX_msglist.ToJavascriptSource());
      httpContext.GX_webresponse.addString(isValidOutput.toJSonString());
      wbTemp = httpContext.setContentType( "application/json") ;
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
      pr_default.close(14);
      pr_default.close(15);
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      sPrefix = "" ;
      scmdbuf = "" ;
      gxfirstwebparm = "" ;
      gxfirstwebparm_bkp = "" ;
      PreviousTooltip = "" ;
      PreviousCaption = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      GX_FocusControl = "" ;
      sStyleString = "" ;
      ClassString = "" ;
      StyleString = "" ;
      TempTags = "" ;
      bttBtn_enter_Jsonclick = "" ;
      bttBtn_cancel_Jsonclick = "" ;
      bttBtn_delete_Jsonclick = "" ;
      lblTextblockdetfac_codigo_Jsonclick = "" ;
      lblTextblockdetfac_cantidad_Jsonclick = "" ;
      lblTextblockdetfac_precio_Jsonclick = "" ;
      lblTextblockdetfac_total_Jsonclick = "" ;
      lblTextblockfac_codigo_Jsonclick = "" ;
      lblTextblockpro_codigo_Jsonclick = "" ;
      imgBtn_first_Jsonclick = "" ;
      imgBtn_first_separator_Jsonclick = "" ;
      imgBtn_previous_Jsonclick = "" ;
      imgBtn_previous_separator_Jsonclick = "" ;
      imgBtn_next_Jsonclick = "" ;
      imgBtn_next_separator_Jsonclick = "" ;
      imgBtn_last_Jsonclick = "" ;
      imgBtn_last_separator_Jsonclick = "" ;
      imgBtn_select_Jsonclick = "" ;
      imgBtn_select_separator_Jsonclick = "" ;
      imgBtn_enter2_Jsonclick = "" ;
      imgBtn_enter2_separator_Jsonclick = "" ;
      imgBtn_cancel2_Jsonclick = "" ;
      imgBtn_cancel2_separator_Jsonclick = "" ;
      imgBtn_delete2_Jsonclick = "" ;
      imgBtn_delete2_separator_Jsonclick = "" ;
      Gx_mode = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      T00056_A21detfac_codigo = new short[1] ;
      T00056_A22detfac_cantidad = new short[1] ;
      T00056_n22detfac_cantidad = new boolean[] {false} ;
      T00056_A23detfac_precio = new short[1] ;
      T00056_n23detfac_precio = new boolean[] {false} ;
      T00056_A24detfac_total = new short[1] ;
      T00056_n24detfac_total = new boolean[] {false} ;
      T00056_A14fac_codigo = new short[1] ;
      T00056_A10pro_codigo = new short[1] ;
      T00054_A14fac_codigo = new short[1] ;
      T00055_A10pro_codigo = new short[1] ;
      T00057_A14fac_codigo = new short[1] ;
      T00058_A10pro_codigo = new short[1] ;
      T00059_A21detfac_codigo = new short[1] ;
      T00053_A21detfac_codigo = new short[1] ;
      T00053_A22detfac_cantidad = new short[1] ;
      T00053_n22detfac_cantidad = new boolean[] {false} ;
      T00053_A23detfac_precio = new short[1] ;
      T00053_n23detfac_precio = new boolean[] {false} ;
      T00053_A24detfac_total = new short[1] ;
      T00053_n24detfac_total = new boolean[] {false} ;
      T00053_A14fac_codigo = new short[1] ;
      T00053_A10pro_codigo = new short[1] ;
      sMode5 = "" ;
      T000510_A21detfac_codigo = new short[1] ;
      T000511_A21detfac_codigo = new short[1] ;
      T00052_A21detfac_codigo = new short[1] ;
      T00052_A22detfac_cantidad = new short[1] ;
      T00052_n22detfac_cantidad = new boolean[] {false} ;
      T00052_A23detfac_precio = new short[1] ;
      T00052_n23detfac_precio = new boolean[] {false} ;
      T00052_A24detfac_total = new short[1] ;
      T00052_n24detfac_total = new boolean[] {false} ;
      T00052_A14fac_codigo = new short[1] ;
      T00052_A10pro_codigo = new short[1] ;
      T000515_A21detfac_codigo = new short[1] ;
      sDynURL = "" ;
      FormProcess = "" ;
      GXt_char2 = "" ;
      GXt_char1 = "" ;
      GXt_char3 = "" ;
      isValidOutput = new com.genexus.GxUnknownObjectCollection();
      T000516_A14fac_codigo = new short[1] ;
      T000517_A10pro_codigo = new short[1] ;
      pr_default = new DataStoreProvider(context, remoteHandle, new detalle_factura__default(),
         new Object[] {
             new Object[] {
            T00052_A21detfac_codigo, T00052_A22detfac_cantidad, T00052_n22detfac_cantidad, T00052_A23detfac_precio, T00052_n23detfac_precio, T00052_A24detfac_total, T00052_n24detfac_total, T00052_A14fac_codigo, T00052_A10pro_codigo
            }
            , new Object[] {
            T00053_A21detfac_codigo, T00053_A22detfac_cantidad, T00053_n22detfac_cantidad, T00053_A23detfac_precio, T00053_n23detfac_precio, T00053_A24detfac_total, T00053_n24detfac_total, T00053_A14fac_codigo, T00053_A10pro_codigo
            }
            , new Object[] {
            T00054_A14fac_codigo
            }
            , new Object[] {
            T00055_A10pro_codigo
            }
            , new Object[] {
            T00056_A21detfac_codigo, T00056_A22detfac_cantidad, T00056_n22detfac_cantidad, T00056_A23detfac_precio, T00056_n23detfac_precio, T00056_A24detfac_total, T00056_n24detfac_total, T00056_A14fac_codigo, T00056_A10pro_codigo
            }
            , new Object[] {
            T00057_A14fac_codigo
            }
            , new Object[] {
            T00058_A10pro_codigo
            }
            , new Object[] {
            T00059_A21detfac_codigo
            }
            , new Object[] {
            T000510_A21detfac_codigo
            }
            , new Object[] {
            T000511_A21detfac_codigo
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            }
            , new Object[] {
            T000515_A21detfac_codigo
            }
            , new Object[] {
            T000516_A14fac_codigo
            }
            , new Object[] {
            T000517_A10pro_codigo
            }
         }
      );
   }

   private byte GxWebError ;
   private byte nKeyPressed ;
   private byte Gx_BScreen ;
   private byte gxajaxcallmode ;
   private byte wbTemp ;
   private short A14fac_codigo ;
   private short A10pro_codigo ;
   private short IsConfirmed ;
   private short IsModified ;
   private short AnyError ;
   private short A21detfac_codigo ;
   private short A22detfac_cantidad ;
   private short A23detfac_precio ;
   private short A24detfac_total ;
   private short Z21detfac_codigo ;
   private short Z22detfac_cantidad ;
   private short Z23detfac_precio ;
   private short Z24detfac_total ;
   private short Z14fac_codigo ;
   private short Z10pro_codigo ;
   private short RcdFound5 ;
   private int trnEnded ;
   private int bttBtn_enter_Visible ;
   private int bttBtn_cancel_Visible ;
   private int bttBtn_delete_Visible ;
   private int edtdetfac_codigo_Enabled ;
   private int edtdetfac_cantidad_Enabled ;
   private int edtdetfac_precio_Enabled ;
   private int edtdetfac_total_Enabled ;
   private int edtfac_codigo_Enabled ;
   private int imgprompt_14_Visible ;
   private int edtpro_codigo_Enabled ;
   private int imgprompt_10_Visible ;
   private int imgBtn_first_Visible ;
   private int imgBtn_first_separator_Visible ;
   private int imgBtn_previous_Visible ;
   private int imgBtn_previous_separator_Visible ;
   private int imgBtn_next_Visible ;
   private int imgBtn_next_separator_Visible ;
   private int imgBtn_last_Visible ;
   private int imgBtn_last_separator_Visible ;
   private int imgBtn_select_Visible ;
   private int imgBtn_select_separator_Visible ;
   private int imgBtn_enter2_Visible ;
   private int imgBtn_enter2_Enabled ;
   private int imgBtn_enter2_separator_Visible ;
   private int imgBtn_cancel2_Visible ;
   private int imgBtn_cancel2_separator_Visible ;
   private int imgBtn_delete2_Visible ;
   private int imgBtn_delete2_Enabled ;
   private int imgBtn_delete2_separator_Visible ;
   private int GX_JID ;
   private int AnyError14 ;
   private int AnyError10 ;
   private int idxLst ;
   private String sPrefix ;
   private String scmdbuf ;
   private String gxfirstwebparm ;
   private String gxfirstwebparm_bkp ;
   private String PreviousTooltip ;
   private String PreviousCaption ;
   private String GX_FocusControl ;
   private String edtdetfac_codigo_Internalname ;
   private String sStyleString ;
   private String tblTablemain_Internalname ;
   private String ClassString ;
   private String StyleString ;
   private String grpGroupdata_Internalname ;
   private String tblTable1_Internalname ;
   private String TempTags ;
   private String bttBtn_enter_Internalname ;
   private String bttBtn_enter_Jsonclick ;
   private String bttBtn_cancel_Internalname ;
   private String bttBtn_cancel_Jsonclick ;
   private String bttBtn_delete_Internalname ;
   private String bttBtn_delete_Jsonclick ;
   private String tblTable2_Internalname ;
   private String lblTextblockdetfac_codigo_Internalname ;
   private String lblTextblockdetfac_codigo_Jsonclick ;
   private String edtdetfac_codigo_Jsonclick ;
   private String lblTextblockdetfac_cantidad_Internalname ;
   private String lblTextblockdetfac_cantidad_Jsonclick ;
   private String edtdetfac_cantidad_Internalname ;
   private String edtdetfac_cantidad_Jsonclick ;
   private String lblTextblockdetfac_precio_Internalname ;
   private String lblTextblockdetfac_precio_Jsonclick ;
   private String edtdetfac_precio_Internalname ;
   private String edtdetfac_precio_Jsonclick ;
   private String lblTextblockdetfac_total_Internalname ;
   private String lblTextblockdetfac_total_Jsonclick ;
   private String edtdetfac_total_Internalname ;
   private String edtdetfac_total_Jsonclick ;
   private String lblTextblockfac_codigo_Internalname ;
   private String lblTextblockfac_codigo_Jsonclick ;
   private String edtfac_codigo_Internalname ;
   private String edtfac_codigo_Jsonclick ;
   private String imgprompt_14_Internalname ;
   private String imgprompt_14_Link ;
   private String lblTextblockpro_codigo_Internalname ;
   private String lblTextblockpro_codigo_Jsonclick ;
   private String edtpro_codigo_Internalname ;
   private String edtpro_codigo_Jsonclick ;
   private String imgprompt_10_Internalname ;
   private String imgprompt_10_Link ;
   private String tblTabletoolbar_Internalname ;
   private String imgBtn_first_Internalname ;
   private String imgBtn_first_Jsonclick ;
   private String imgBtn_first_separator_Internalname ;
   private String imgBtn_first_separator_Jsonclick ;
   private String imgBtn_previous_Internalname ;
   private String imgBtn_previous_Jsonclick ;
   private String imgBtn_previous_separator_Internalname ;
   private String imgBtn_previous_separator_Jsonclick ;
   private String imgBtn_next_Internalname ;
   private String imgBtn_next_Jsonclick ;
   private String imgBtn_next_separator_Internalname ;
   private String imgBtn_next_separator_Jsonclick ;
   private String imgBtn_last_Internalname ;
   private String imgBtn_last_Jsonclick ;
   private String imgBtn_last_separator_Internalname ;
   private String imgBtn_last_separator_Jsonclick ;
   private String imgBtn_select_Internalname ;
   private String imgBtn_select_Jsonclick ;
   private String imgBtn_select_separator_Internalname ;
   private String imgBtn_select_separator_Jsonclick ;
   private String imgBtn_enter2_Internalname ;
   private String imgBtn_enter2_Jsonclick ;
   private String imgBtn_enter2_separator_Internalname ;
   private String imgBtn_enter2_separator_Jsonclick ;
   private String imgBtn_cancel2_Internalname ;
   private String imgBtn_cancel2_Jsonclick ;
   private String imgBtn_cancel2_separator_Internalname ;
   private String imgBtn_cancel2_separator_Jsonclick ;
   private String imgBtn_delete2_Internalname ;
   private String imgBtn_delete2_Jsonclick ;
   private String imgBtn_delete2_separator_Internalname ;
   private String imgBtn_delete2_separator_Jsonclick ;
   private String Gx_mode ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String sMode5 ;
   private String sDynURL ;
   private String FormProcess ;
   private String GXt_char2 ;
   private String GXt_char1 ;
   private String GXt_char3 ;
   private boolean entryPointCalled ;
   private boolean wbErr ;
   private boolean n22detfac_cantidad ;
   private boolean n23detfac_precio ;
   private boolean n24detfac_total ;
   private com.genexus.webpanels.GXMasterPage MasterPageObj ;
   private com.genexus.GxUnknownObjectCollection isValidOutput ;
   private IDataStoreProvider pr_default ;
   private short[] T00056_A21detfac_codigo ;
   private short[] T00056_A22detfac_cantidad ;
   private boolean[] T00056_n22detfac_cantidad ;
   private short[] T00056_A23detfac_precio ;
   private boolean[] T00056_n23detfac_precio ;
   private short[] T00056_A24detfac_total ;
   private boolean[] T00056_n24detfac_total ;
   private short[] T00056_A14fac_codigo ;
   private short[] T00056_A10pro_codigo ;
   private short[] T00054_A14fac_codigo ;
   private short[] T00055_A10pro_codigo ;
   private short[] T00057_A14fac_codigo ;
   private short[] T00058_A10pro_codigo ;
   private short[] T00059_A21detfac_codigo ;
   private short[] T00053_A21detfac_codigo ;
   private short[] T00053_A22detfac_cantidad ;
   private boolean[] T00053_n22detfac_cantidad ;
   private short[] T00053_A23detfac_precio ;
   private boolean[] T00053_n23detfac_precio ;
   private short[] T00053_A24detfac_total ;
   private boolean[] T00053_n24detfac_total ;
   private short[] T00053_A14fac_codigo ;
   private short[] T00053_A10pro_codigo ;
   private short[] T000510_A21detfac_codigo ;
   private short[] T000511_A21detfac_codigo ;
   private short[] T00052_A21detfac_codigo ;
   private short[] T00052_A22detfac_cantidad ;
   private boolean[] T00052_n22detfac_cantidad ;
   private short[] T00052_A23detfac_precio ;
   private boolean[] T00052_n23detfac_precio ;
   private short[] T00052_A24detfac_total ;
   private boolean[] T00052_n24detfac_total ;
   private short[] T00052_A14fac_codigo ;
   private short[] T00052_A10pro_codigo ;
   private short[] T000515_A21detfac_codigo ;
   private short[] T000516_A14fac_codigo ;
   private short[] T000517_A10pro_codigo ;
   private com.genexus.webpanels.GXWebForm Form ;
}

final  class detalle_factura__default extends DataStoreHelperBase implements ILocalDataStoreHelper
{
   public Cursor[] getCursors( )
   {
      return new Cursor[] {
          new ForEachCursor("T00052", "SELECT [detfac_codigo], [detfac_cantidad], [detfac_precio], [detfac_total], [fac_codigo], [pro_codigo] FROM [detalle_factura] WITH (UPDLOCK) WHERE [detfac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00053", "SELECT [detfac_codigo], [detfac_cantidad], [detfac_precio], [detfac_total], [fac_codigo], [pro_codigo] FROM [detalle_factura] WITH (NOLOCK) WHERE [detfac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00054", "SELECT [fac_codigo] FROM [factura] WITH (NOLOCK) WHERE [fac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00055", "SELECT [pro_codigo] FROM [producto] WITH (NOLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00056", "SELECT TM1.[detfac_codigo], TM1.[detfac_cantidad], TM1.[detfac_precio], TM1.[detfac_total], TM1.[fac_codigo], TM1.[pro_codigo] FROM [detalle_factura] TM1 WITH (NOLOCK) WHERE TM1.[detfac_codigo] = ? ORDER BY TM1.[detfac_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
         ,new ForEachCursor("T00057", "SELECT [fac_codigo] FROM [factura] WITH (NOLOCK) WHERE [fac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00058", "SELECT [pro_codigo] FROM [producto] WITH (NOLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T00059", "SELECT [detfac_codigo] FROM [detalle_factura] WITH (NOLOCK) WHERE [detfac_codigo] = ?  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000510", "SELECT TOP 1 [detfac_codigo] FROM [detalle_factura] WITH (NOLOCK) WHERE ( [detfac_codigo] > ?) ORDER BY [detfac_codigo]  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new ForEachCursor("T000511", "SELECT TOP 1 [detfac_codigo] FROM [detalle_factura] WITH (NOLOCK) WHERE ( [detfac_codigo] < ?) ORDER BY [detfac_codigo] DESC  OPTION (FAST 1)",true, GX_NOMASK, false, this,1,0,true )
         ,new UpdateCursor("T000512", "INSERT INTO [detalle_factura] ([detfac_codigo], [detfac_cantidad], [detfac_precio], [detfac_total], [fac_codigo], [pro_codigo]) VALUES (?, ?, ?, ?, ?, ?)", GX_NOMASK)
         ,new UpdateCursor("T000513", "UPDATE [detalle_factura] SET [detfac_cantidad]=?, [detfac_precio]=?, [detfac_total]=?, [fac_codigo]=?, [pro_codigo]=?  WHERE [detfac_codigo] = ?", GX_NOMASK)
         ,new UpdateCursor("T000514", "DELETE FROM [detalle_factura]  WHERE [detfac_codigo] = ?", GX_NOMASK)
         ,new ForEachCursor("T000515", "SELECT [detfac_codigo] FROM [detalle_factura] WITH (NOLOCK) ORDER BY [detfac_codigo]  OPTION (FAST 100)",true, GX_NOMASK, false, this,100,0,false )
         ,new ForEachCursor("T000516", "SELECT [fac_codigo] FROM [factura] WITH (NOLOCK) WHERE [fac_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
         ,new ForEachCursor("T000517", "SELECT [pro_codigo] FROM [producto] WITH (NOLOCK) WHERE [pro_codigo] = ? ",true, GX_NOMASK, false, this,1,0,false )
      };
   }

   public void getResults( int cursor ,
                           IFieldGetter rslt ,
                           Object[] buf ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((short[]) buf[3])[0] = rslt.getShort(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((short[]) buf[5])[0] = rslt.getShort(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(5) ;
               ((short[]) buf[8])[0] = rslt.getShort(6) ;
               break;
            case 1 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((short[]) buf[3])[0] = rslt.getShort(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((short[]) buf[5])[0] = rslt.getShort(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(5) ;
               ((short[]) buf[8])[0] = rslt.getShort(6) ;
               break;
            case 2 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 3 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 4 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               ((short[]) buf[1])[0] = rslt.getShort(2) ;
               ((boolean[]) buf[2])[0] = rslt.wasNull();
               ((short[]) buf[3])[0] = rslt.getShort(3) ;
               ((boolean[]) buf[4])[0] = rslt.wasNull();
               ((short[]) buf[5])[0] = rslt.getShort(4) ;
               ((boolean[]) buf[6])[0] = rslt.wasNull();
               ((short[]) buf[7])[0] = rslt.getShort(5) ;
               ((short[]) buf[8])[0] = rslt.getShort(6) ;
               break;
            case 5 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 6 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 7 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 8 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 9 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 13 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 14 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
            case 15 :
               ((short[]) buf[0])[0] = rslt.getShort(1) ;
               break;
      }
   }

   public void setParameters( int cursor ,
                              IFieldSetter stmt ,
                              Object[] parms ) throws SQLException
   {
      switch ( cursor )
      {
            case 0 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 1 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 2 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 3 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 4 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 5 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 6 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 7 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 8 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 9 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 10 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               if ( ((Boolean) parms[1]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(2, ((Number) parms[2]).shortValue());
               }
               if ( ((Boolean) parms[3]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(3, ((Number) parms[4]).shortValue());
               }
               if ( ((Boolean) parms[5]).booleanValue() )
               {
                  stmt.setNull( 4 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(4, ((Number) parms[6]).shortValue());
               }
               stmt.setShort(5, ((Number) parms[7]).shortValue());
               stmt.setShort(6, ((Number) parms[8]).shortValue());
               break;
            case 11 :
               if ( ((Boolean) parms[0]).booleanValue() )
               {
                  stmt.setNull( 1 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(1, ((Number) parms[1]).shortValue());
               }
               if ( ((Boolean) parms[2]).booleanValue() )
               {
                  stmt.setNull( 2 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(2, ((Number) parms[3]).shortValue());
               }
               if ( ((Boolean) parms[4]).booleanValue() )
               {
                  stmt.setNull( 3 , Types.NUMERIC );
               }
               else
               {
                  stmt.setShort(3, ((Number) parms[5]).shortValue());
               }
               stmt.setShort(4, ((Number) parms[6]).shortValue());
               stmt.setShort(5, ((Number) parms[7]).shortValue());
               stmt.setShort(6, ((Number) parms[8]).shortValue());
               break;
            case 12 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 14 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
            case 15 :
               stmt.setShort(1, ((Number) parms[0]).shortValue());
               break;
      }
   }

}

