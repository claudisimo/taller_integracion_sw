/*
               File: menu_administrador_impl
        Description: Menu_ Administrador
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:8.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

public final  class menu_administrador_impl extends GXMasterPage
{
   public menu_administrador_impl( com.genexus.internet.HttpContext context )
   {
      super(context);
   }

   public menu_administrador_impl( int remoteHandle )
   {
      super( remoteHandle , new ModelContext( menu_administrador_impl.class ));
   }

   public menu_administrador_impl( int remoteHandle ,
                                   ModelContext context )
   {
      super( remoteHandle , context);
   }

   protected void createObjects( )
   {
   }

   public void initweb( )
   {
      initialize_properties( ) ;
      httpContext.setTheme("GeneXusX");
   }

   public void webExecute( )
   {
      initweb( ) ;
      if ( ! httpContext.isAjaxCallMode( ) )
      {
         pa0D2( ) ;
         if ( ! httpContext.isAjaxCallMode( ) )
         {
         }
         if ( ( GxWebError == 0 ) && ! httpContext.isAjaxCallMode( ) )
         {
            ws0D2( ) ;
            if ( ! httpContext.isAjaxCallMode( ) )
            {
               we0D2( ) ;
            }
         }
      }
      cleanup();
   }

   public void renderHtmlHeaders( )
   {
      getDataAreaObject().renderHtmlHeaders();
   }

   public void renderHtmlOpenForm( )
   {
      getDataAreaObject().renderHtmlOpenForm();
   }

   public void renderHtmlCloseForm0D2( )
   {
      /* Send hidden variables. */
      /* Send saved values. */
      getDataAreaObject().renderHtmlCloseForm();
      httpContext.AddJavascriptSource("menu_administrador.js", "?942859");
      httpContext.writeTextNL( "</body>") ;
      httpContext.writeTextNL( "</html>") ;
   }

   public void wb0D0( )
   {
      if ( httpContext.isAjaxRequest( ) )
      {
         httpContext.disableOutput();
      }
      if ( ! wbLoad )
      {
         renderHtmlHeaders( ) ;
         renderHtmlOpenForm( ) ;
         if ( ! ShowMPWhenPopUp( ) && httpContext.isPopUpObject( ) )
         {
            getDataAreaObject().renderHtmlContent();
            return  ;
         }
         wb_table1_2_0D2( true) ;
      }
      else
      {
         wb_table1_2_0D2( false) ;
      }
      return  ;
   }

   public void wb_table1_2_0D2e( boolean wbgen )
   {
      if ( wbgen )
      {
         httpContext.writeText( "<p></p>") ;
      }
      wbLoad = true ;
   }

   public void start0D2( )
   {
      wbLoad = false ;
      wbEnd = 0 ;
      wbStart = 0 ;
      httpContext.wjLoc = "" ;
      httpContext.nUserReturn = (byte)(0) ;
      httpContext.wbHandled = (byte)(0) ;
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
      }
      wbErr = false ;
      strup0D0( ) ;
      if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) )
      {
         if ( getDataAreaObject().executeStartEvent() != 0 )
         {
            httpContext.setAjaxCallMode();
         }
      }
   }

   public void ws0D2( )
   {
      start0D2( ) ;
      evt0D2( ) ;
   }

   public void evt0D2( )
   {
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) && ! wbErr )
         {
            /* Read Web Panel buttons. */
            sEvt = httpContext.cgiGet( "_EventName") ;
            EvtGridId = httpContext.cgiGet( "_EventGridId") ;
            EvtRowId = httpContext.cgiGet( "_EventRowId") ;
            if ( GXutil.len( sEvt) > 0 )
            {
               sEvtType = GXutil.left( sEvt, 1) ;
               sEvt = GXutil.right( sEvt, GXutil.len( sEvt)-1) ;
               if ( GXutil.strcmp(sEvtType, "E") == 0 )
               {
                  sEvtType = GXutil.right( sEvt, 1) ;
                  if ( GXutil.strcmp(sEvtType, ".") == 0 )
                  {
                     sEvt = GXutil.left( sEvt, GXutil.len( sEvt)-1) ;
                     if ( GXutil.strcmp(sEvt, "RFR_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                     }
                     else if ( GXutil.strcmp(sEvt, "USU_MPAGE.CLICK_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e110D2 */
                        e110D2 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "ROL_MPAGE.CLICK_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e120D2 */
                        e120D2 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "BIT_MPAGE.CLICK_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e130D2 */
                        e130D2 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "MANUALSIS_MPAGE.CLICK_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e140D2 */
                        e140D2 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "LOGOUT_MPAGE.CLICK_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e150D2 */
                        e150D2 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "START_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e160D2 */
                        e160D2 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "LOAD_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                        /* Execute user event: e170D2 */
                        e170D2 ();
                     }
                     else if ( GXutil.strcmp(sEvt, "ENTER_MPAGE") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        if ( ! wbErr )
                        {
                           Rfr0gs = false ;
                           if ( ! Rfr0gs )
                           {
                           }
                           dynload_actions( ) ;
                        }
                        /* No code required for Cancel button. It is implemented as the Reset button. */
                     }
                     else if ( GXutil.strcmp(sEvt, "LSCR") == 0 )
                     {
                        httpContext.wbHandled = (byte)(1) ;
                        dynload_actions( ) ;
                     }
                  }
                  else
                  {
                  }
               }
               if ( httpContext.wbHandled == 0 )
               {
                  getDataAreaObject().dispatchEvents();
               }
               httpContext.wbHandled = (byte)(1) ;
            }
         }
      }
   }

   public void we0D2( )
   {
      if ( ! GxWebStd.gx_redirect( httpContext) )
      {
         Rfr0gs = true ;
         refresh( ) ;
         if ( ! GxWebStd.gx_redirect( httpContext) )
         {
            renderHtmlCloseForm0D2( ) ;
         }
      }
   }

   public void pa0D2( )
   {
      if ( nDonePA == 0 )
      {
         nDonePA = (byte)(1) ;
      }
   }

   public void dynload_actions( )
   {
      /* End function dynload_actions */
   }

   public void refresh( )
   {
      rf0D2( ) ;
      /* End function Refresh */
   }

   public void rf0D2( )
   {
      if ( (GXutil.strcmp("", httpContext.wjLoc)==0) && ( httpContext.nUserReturn != 1 ) )
      {
         /* Execute user event: e170D2 */
         e170D2 ();
         wb0D0( ) ;
      }
   }

   public void strup0D0( )
   {
      /* Before Start, stand alone formulas. */
      Gx_err = (short)(0) ;
      /* Execute Start event if defined. */
      httpContext.wbGlbDoneStart = (byte)(0) ;
      /* Execute user event: e160D2 */
      e160D2 ();
      httpContext.wbGlbDoneStart = (byte)(1) ;
      /* After Start, stand alone formulas. */
      if ( GXutil.strcmp(httpContext.getRequestMethod( ), "POST") == 0 )
      {
         /* Read saved SDTs. */
         /* Read variables values. */
         /* Read saved values. */
         /* Read subfile selected row values. */
         /* Read hidden variables. */
      }
      else
      {
         dynload_actions( ) ;
      }
   }

   public void e110D2( )
   {
      /* Usu_Click Routine */
      httpContext.wjLoc = formatLink("wp_usuarios")  ;
   }

   public void e120D2( )
   {
      /* Rol_Click Routine */
      httpContext.wjLoc = formatLink("wp_rol")  ;
   }

   public void e130D2( )
   {
      /* Bit_Click Routine */
      httpContext.wjLoc = formatLink("wp_bitacora")  ;
   }

   public void e140D2( )
   {
      /* Manualsis_Click Routine */
      httpContext.wjLoc = formatLink("aprocdownload")  ;
      GXv_char1[0] = AV5usuario ;
      GXv_dtime2[0] = GXutil.now( ) ;
      GXv_char3[0] = "Descarga manual de sistema" ;
      new guarda_bitacora(remoteHandle, context).execute( GXv_char1, GXv_dtime2, GXv_char3) ;
      menu_administrador_impl.this.AV5usuario = GXv_char1[0] ;
   }

   public void e150D2( )
   {
      /* Logout_Click Routine */
      AV9session.destroy();
      httpContext.wjLoc = formatLink("login")  ;
   }

   protected void GXStart( )
   {
      /* Execute user event: e160D2 */
      e160D2 ();
      if (returnInSub) return;
   }

   public void e160D2( )
   {
      /* Start Routine */
      AV5usuario = AV9session.getValue("usuario") ;
      if ( (GXutil.strcmp("", AV5usuario)==0) )
      {
         httpContext.wjLoc = formatLink("login")  ;
      }
      /* User Code */
       AV10realpath = getHttpContext().getDefaultPath();
      AV10realpath = GXutil.strReplace( AV10realpath, "\\", "\\\\") ;
      AV10realpath = GXutil.trim( AV10realpath) + "\\\\PublicTempStorage\\\\" ;
      if ( GXutil.strSearch( AV10realpath, ":", 1) == 0 )
      {
         AV10realpath = GXutil.strReplace( AV10realpath, "\\\\", "/") ;
      }
      AV9session.setValue("ruta", AV10realpath);
      AV9session.setValue("archivo", "manual_sistema.pdf");
   }

   protected void nextLoad( )
   {
   }

   protected void e170D2( )
   {
      /* Load Routine */
   }

   public void wb_table1_2_0D2( boolean wbgen )
   {
      if ( wbgen )
      {
         /* Table start */
         sStyleString = "" ;
         GxWebStd.gx_table_start( httpContext, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
         httpContext.writeText( "<tbody>") ;
         httpContext.writeText( "<tr align=\"center\" >") ;
         httpContext.writeText( "<td valign=\"top\"  style=\"text-align:"+httpContext.getCssProperty( "Align", "center")+";width:56px\">") ;
         httpContext.writeText( "<p>") ;
         httpContext.writeText( "Men� Administrador&nbsp;") ;
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "<br>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "font-family:'Arial'; font-size:8.0pt; font-weight:bold; font-style:normal;" ;
         GxWebStd.gx_label_ctrl( httpContext, lblLogout_Internalname, "(LOGOUT)", "", "", lblLogout_Jsonclick, "ELOGOUT_MPAGE.CLICK_MPAGE.", StyleString, ClassString, 5, "", 1, 1, (short)(0), "HLP_Menu_Administrador.htm");
         httpContext.writeText( "</p>") ;
         httpContext.writeText( "<hr/>") ;
         httpContext.writeText( "<p>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblUsu_Internalname, "Usuario", "", "", lblUsu_Jsonclick, "EUSU_MPAGE.CLICK_MPAGE.", StyleString, ClassString, 5, "", 1, 1, (short)(0), "HLP_Menu_Administrador.htm");
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "<br>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblRol_Internalname, "Rol", "", "", lblRol_Jsonclick, "EROL_MPAGE.CLICK_MPAGE.", StyleString, ClassString, 5, "", 1, 1, (short)(0), "HLP_Menu_Administrador.htm");
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "<br>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblBit_Internalname, "Bit�cora", "", "", lblBit_Jsonclick, "EBIT_MPAGE.CLICK_MPAGE.", StyleString, ClassString, 5, "", 1, 1, (short)(0), "HLP_Menu_Administrador.htm");
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "</p>") ;
         httpContext.writeText( "<hr/>") ;
         httpContext.writeText( "<p></p>") ;
         httpContext.writeText( "<p>") ;
         /* Text block */
         ClassString = "TextBlock" ;
         StyleString = "" ;
         GxWebStd.gx_label_ctrl( httpContext, lblManualsis_Internalname, "Manual de Sistema", "", "", lblManualsis_Jsonclick, "EMANUALSIS_MPAGE.CLICK_MPAGE.", StyleString, ClassString, 5, "", 1, 1, (short)(0), "HLP_Menu_Administrador.htm");
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "<br>") ;
         httpContext.writeText( "</p>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "<td style=\"width:100%\">") ;
         httpContext.writeText( "<p>") ;
         getDataAreaObject().renderHtmlContent();
         httpContext.writeText( "</p>") ;
         httpContext.writeText( "<p></p>") ;
         httpContext.writeText( "</td>") ;
         httpContext.writeText( "</tr>") ;
         httpContext.writeText( "</tbody>") ;
         /* End of table */
         httpContext.writeText( "</table>") ;
         wb_table1_2_0D2e( true) ;
      }
      else
      {
         wb_table1_2_0D2e( false) ;
      }
   }

   public void setparameters( Object[] obj )
   {
   }

   public String getresponse( String sGXDynURL )
   {
      initialize_properties( ) ;
      BackMsgLst = httpContext.GX_msglist ;
      httpContext.GX_msglist = LclMsgLst ;
      sDynURL = sGXDynURL ;
      nGotPars = 1 ;
      nGXWrapped = 1 ;
      httpContext.setWrapped(true);
      pa0D2( ) ;
      ws0D2( ) ;
      we0D2( ) ;
      httpContext.setWrapped(false);
      httpContext.GX_msglist = BackMsgLst ;
      return ((java.io.ByteArrayOutputStream) httpContext.getOutputStream()).toString();
   }

   public void responsestatic( String sGXDynURL )
   {
   }

   public void master_styles( )
   {
      define_styles( ) ;
   }

   public void define_styles( )
   {
      httpContext.AddThemeStyleSheetFile("", "GeneXusX.css", "?2054686");
      idxLst = 1 ;
      while ( idxLst <= getDataAreaObject().getForm().getJscriptsrc().getCount() )
      {
         httpContext.AddJavascriptSource(GXutil.rtrim( getDataAreaObject().getForm().getJscriptsrc().item(idxLst)), "?942867");
         idxLst = (int)(idxLst+1) ;
      }
      /* End function define_styles */
   }

   public void include_jscripts( )
   {
      httpContext.AddJavascriptSource("menu_administrador.js", "?942867");
      /* End function include_jscripts */
   }

   public void init_default_properties( )
   {
      lblLogout_Internalname = "LOGOUT_MPAGE" ;
      lblUsu_Internalname = "USU_MPAGE" ;
      lblRol_Internalname = "ROL_MPAGE" ;
      lblBit_Internalname = "BIT_MPAGE" ;
      lblManualsis_Internalname = "MANUALSIS_MPAGE" ;
      tblTable1_Internalname = "TABLE1_MPAGE" ;
      getDataAreaObject().getForm().setInternalname( "FORM_MPAGE" );
   }

   public void initialize_properties( )
   {
      init_default_properties( ) ;
      lblRol_Jsonclick = "" ;
      Contholder1.setDataArea(getDataAreaObject());
   }

   protected void cleanup( )
   {
      super.cleanup();
      CloseOpenCursors();
   }

   protected void CloseOpenCursors( )
   {
   }

   /* Aggregate/select formulas */
   public void initialize( )
   {
      Contholder1 = new com.genexus.webpanels.GXDataAreaControl();
      sPrefix = "" ;
      sEvt = "" ;
      EvtGridId = "" ;
      EvtRowId = "" ;
      sEvtType = "" ;
      AV5usuario = "" ;
      GXv_char1 = new String [1] ;
      GXv_dtime2 = new java.util.Date [1] ;
      GXv_char3 = new String [1] ;
      AV9session = httpContext.getWebSession();
      AV10realpath = "" ;
      sStyleString = "" ;
      GXt_char4 = "" ;
      ClassString = "" ;
      StyleString = "" ;
      lblLogout_Jsonclick = "" ;
      lblUsu_Jsonclick = "" ;
      lblBit_Jsonclick = "" ;
      lblManualsis_Jsonclick = "" ;
      BackMsgLst = new com.genexus.internet.MsgList();
      LclMsgLst = new com.genexus.internet.MsgList();
      sDynURL = "" ;
      Form = new com.genexus.webpanels.GXWebForm();
      /* GeneXus formulas. */
      Gx_err = (short)(0) ;
   }

   private byte GxWebError ;
   private byte nDonePA ;
   private byte nGotPars ;
   private byte nGXWrapped ;
   private short wbEnd ;
   private short wbStart ;
   private short Gx_err ;
   private int idxLst ;
   private String sPrefix ;
   private String sEvt ;
   private String EvtGridId ;
   private String EvtRowId ;
   private String sEvtType ;
   private String AV5usuario ;
   private String GXv_char1[] ;
   private String GXv_char3[] ;
   private String AV10realpath ;
   private String sStyleString ;
   private String tblTable1_Internalname ;
   private String GXt_char4 ;
   private String ClassString ;
   private String StyleString ;
   private String lblLogout_Internalname ;
   private String lblLogout_Jsonclick ;
   private String lblUsu_Internalname ;
   private String lblUsu_Jsonclick ;
   private String lblRol_Internalname ;
   private String lblRol_Jsonclick ;
   private String lblBit_Internalname ;
   private String lblBit_Jsonclick ;
   private String lblManualsis_Internalname ;
   private String lblManualsis_Jsonclick ;
   private String sDynURL ;
   private java.util.Date GXv_dtime2[] ;
   private boolean wbLoad ;
   private boolean Rfr0gs ;
   private boolean wbErr ;
   private boolean returnInSub ;
   private com.genexus.internet.MsgList BackMsgLst ;
   private com.genexus.internet.MsgList LclMsgLst ;
   private com.genexus.webpanels.GXDataAreaControl Contholder1 ;
   private com.genexus.webpanels.GXWebForm Form ;
   private com.genexus.webpanels.WebSession AV9session ;
}

