/*
               File: Gx0050
        Description: Selection List detalle_factura
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 22, 2022 12:30:43.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/gx0050")
public final  class gx0050 extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new gx0050_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Selection List detalle_factura";
   }

}

