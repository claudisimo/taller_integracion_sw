/*
               File: Gx00A0
        Description: Selection List Bitacora
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 22, 2022 9:25:40.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/gx00a0")
public final  class gx00a0 extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new gx00a0_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "Selection List Bitacora";
   }

}

