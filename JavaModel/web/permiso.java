/*
               File: permiso
        Description: permiso
             Author: GeneXus Java Generator version 10_1_8-58720
       Generated on: May 23, 2022 9:42:5.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
import com.genexus.*;
import com.genexus.db.*;
import com.genexus.distributed.*;
import com.genexus.webpanels.*;
import java.sql.*;
import com.genexus.search.*;

@javax.servlet.annotation.WebServlet(value ="/servlet/permiso")
public final  class permiso extends GXWebObjectStub
{
   protected void doExecute( com.genexus.internet.HttpContext context ) throws Exception
   {
      new permiso_impl(context).doExecute();
   }

   public String getServletInfo( )
   {
      return "permiso";
   }

}

